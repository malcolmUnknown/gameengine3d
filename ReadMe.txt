To get this project running do the following:
<pure execution purpose:>
o) Open command window and navigate to the project folder direction
o) Execute the command: java -Djava.library.path="<Path_to_LWJGL-2.9.3>\\native\\windows\\" -jar dist\\GameEngine3D.jar
Example: Execute from directory D:\Frank\GameEngine3D_Java\GameEngine3D the following command:
java -Djava.library.path="D:\\Frank\\GameEngine3D_Java\\GameEngine3D\\LWJGL-2.9.3\\native\\windows\\" -jar dist\\GameEngine3D.jar

<netbeans-project:>
o) Open the project with netbeans and ignore errors due to missing library
o) Add LWJGL (Version 2.9.x) library to netbeans (via Tools->Library) [a library version is included in the project folder]
o) Edit Project-properties: 
	-> Ensure the lwjgl-library is correctly linked under the item "Libraries"
	-> Adjust the path of the library according to your location of the library under the topic item "Run"-> "VM-Option". 
	It should look like:
	-Djava.library.path="D:\Frank\GameEngine3D_Java\GameEngine3D\LWJGL-2.9.3\native\windows\"


>> For navigation in 3D use
Mouse for Rotation (right button to unlock mouse, left to lock mouse)
Keys for Motion (direction forward (q,e), sidewards (a,d), upwards (y,c))