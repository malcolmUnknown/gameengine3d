#version 120

attribute vec3 aPosition;
attribute vec2 aTextureCoord;
attribute vec3 aNormal;

varying vec2 coordinateTexture;
varying vec3 surfaceNormal;
varying vec3 worldPosition;

uniform mat4 T_aModelTransformation;
uniform mat4 T_aModelViewPerspectiveTransformation;

void main()
{    
        gl_Position = T_aModelViewPerspectiveTransformation * vec4( aPosition, 1.0);
        coordinateTexture = aTextureCoord;
        surfaceNormal = (T_aModelTransformation * vec4(aNormal,0.0f)).xyz;
        worldPosition = (T_aModelTransformation * vec4(aPosition,1.0f)).xyz;
}