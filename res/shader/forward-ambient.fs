#version 120 

varying vec2 coordinateTexture;

uniform vec3 R_aAmbientIntensity;
uniform sampler2D aDiffuseSampler;

void main()
{
    gl_FragColor = texture2D(aDiffuseSampler, coordinateTexture.xy)*vec4(R_aAmbientIntensity, 1);
}