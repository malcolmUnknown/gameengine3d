#version 120

attribute vec3 aPosition;
attribute vec2 aTextureCoord;

varying vec2 coordinateTexture;

uniform mat4 T_aModelViewPerspectiveTransformation;

void main()
{    
    gl_Position = T_aModelViewPerspectiveTransformation * vec4( aPosition, 1.0);
    coordinateTexture = aTextureCoord;
}