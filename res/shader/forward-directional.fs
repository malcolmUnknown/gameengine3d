#version 120
#include "lighting.glh"

varying vec2 coordinateTexture;
varying vec3 surfaceNormal;
varying vec3 worldPosition;

uniform sampler2D aDiffuseSampler;
uniform DirectionalLight R_aDirectionalLight;

void main()
{
    gl_FragColor = texture2D(aDiffuseSampler, coordinateTexture.xy) * 
        calculateDirectionalLight(R_aDirectionalLight, normalize(surfaceNormal), worldPosition);
}