#version 330

layout (location = 0) in vec3 position;
layout (location = 1) in vec2 textureCoord;
layout (location = 2) in vec3 normal;

out vec2 CoordinateTexture;
out vec3 SurfaceNormal;
out vec3 WorldPosition;

uniform mat4 transform;
uniform mat4 transformProjected;

void main()
{    
	gl_Position = transformProjected * vec4( position, 1.0);
        CoordinateTexture = textureCoord;
        SurfaceNormal = (transform * vec4(normal,0.0f)).xyz;
        WorldPosition = (transform * vec4(position,1.0f)).xyz;
}