 fragColor = vec4(1.0, 1.0, 1.0, 1.0);; 
 //color = vec4(clamp(position, 0.0, uniformFloat), 1.0);


out vec4 color;
color = vec4(clamp(position, 0.0, 1.0), 1.0);


out vec4 fragColor;
in vec4 color;
gl_FragColor = color;


#phongFragments -> look if Epsilon is possible for comparision