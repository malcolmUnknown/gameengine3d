#version 120
#include "lighting.glh"

varying vec2 coordinateTexture;
varying vec3 surfaceNormal;
varying vec3 worldPosition;

uniform sampler2D aDiffuseSampler;
uniform PointLight R_aPointLight;

void main()
{
    gl_FragColor = texture2D(aDiffuseSampler, coordinateTexture.xy) * calculatePointLight(R_aPointLight, normalize(surfaceNormal), worldPosition);
}