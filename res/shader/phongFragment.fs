#version 330

const int MAX_POINT_LIGHTS = 4;
const int MAX_SPOT_LIGHTS = 4;

in vec2 CoordinateTexture;
in vec3 SurfaceNormal;
in vec3 WorldPosition;

out vec4 fragColor;

struct BaseLight
{
    vec3 color;
    float intensity;
};

struct DirectionalLight
{
    BaseLight base;
    vec3 direction;
};

struct Attenuation
{
    float constant;
    float linear;
    float exponent;
};

struct PointLight
{
    BaseLight base;
    Attenuation attenuation;
    vec3 position;
    float range;
};

struct SpotLight
{
	PointLight pointLight;
	vec3 direction;
	float cutoff;
};

uniform vec3 baseColor;
uniform vec3 CameraPosition;
uniform vec3 ambientLight;
uniform sampler2D sampler;

uniform float specularIntensity;
uniform float specularPower;

uniform DirectionalLight directionalLight;
uniform PointLight pointLights[MAX_POINT_LIGHTS];
uniform SpotLight spotLights[MAX_SPOT_LIGHTS];

vec4 calculateLight(BaseLight base, vec3 direction, vec3 normal)
{
    float diffuseFactor = dot(-direction, normal);
    vec4 diffuseColor = vec4(0,0,0,0);
    vec4 specularColor = vec4(0,0,0,0);

    if (diffuseFactor >0)
    {
        diffuseColor = vec4(base.color, 1.0) * base.intensity * diffuseFactor;
        
        
        vec3 DirectionToEye = normalize( CameraPosition - WorldPosition);
        vec3 RefectionDirection = normalize( reflect( direction, normal));
        
        float specularFactor = dot(DirectionToEye,RefectionDirection);
        specularFactor = pow(specularFactor, specularPower);

        if (specularFactor >0)
        {
            specularColor = vec4(base.color, 1.0) * specularIntensity *specularFactor;
        }
    }
    return diffuseColor + specularColor;
};

vec4 calculateDirectionalLight(DirectionalLight directLight, vec3 normal)
{
    return calculateLight(directLight.base, -directLight.direction, normal);
};

vec4 calculatePointLight(PointLight pointLight, vec3 normal)
{
    vec3 lightDirection = WorldPosition - pointLight.position;
    float distanceToPoint = length(lightDirection);
    if( distanceToPoint > pointLight.range)
        return vec4(0,0,0,0);
    lightDirection = normalize(lightDirection);

    vec4 Color = calculateLight(pointLight.base, lightDirection, normal);

    float LightAttenutation = 0.0001 +
            pointLight.attenuation.constant +
            pointLight.attenuation.linear * distanceToPoint +
            pointLight.attenuation.exponent * distanceToPoint * distanceToPoint;

    return Color / LightAttenutation;
};
vec4 calculateSpotLight(SpotLight spotLight, vec3 normal)
{
	vec3 lightDirection = normalize(WorldPosition - spotLight.pointLight.position);
	float spotFactor = dot(lightDirection, spotLight.direction);
	vec4 Color = vec4(0,0,0,0);
	
	if (spotFactor > spotLight.cutoff)
	{
		Color = calculatePointLight(spotLight.pointLight, normal) 
			*(1.0 -(1.0 - spotFactor)/(1.0 - spotLight.cutoff));
	}
	return Color;
};
void main()
{
    vec4 totalLight = vec4(ambientLight,1);
    vec4 color = vec4(baseColor,1);
    vec4 textureColor = texture(sampler, CoordinateTexture.xy);

    if (textureColor != vec4(0,0,0,0))
      color *= textureColor;

    vec3 TriangleNormal= normalize(SurfaceNormal);

    totalLight += calculateDirectionalLight(directionalLight, TriangleNormal);

    for (int i=0; i< MAX_POINT_LIGHTS; i++)
        if( pointLights[i].base.intensity > 0)
            totalLight += calculatePointLight(pointLights[i], TriangleNormal);
			
    for (int i=0; i< MAX_SPOT_LIGHTS; i++)
        if( spotLights[i].pointLight.base.intensity > 0)
            totalLight += calculateSpotLight(spotLights[i], TriangleNormal);

    fragColor = color * totalLight;
}