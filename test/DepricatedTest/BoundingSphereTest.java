/* GameEngine 3D - A programm to design games with 3D content 
 * Copyright (C) 2016-2056 Frank Küwen
 *  
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package DepricatedTest;

import Depricated.BoundingSphere;
import Depricated.BoundingSphere;
import ProjectCenter.CoreEngine.Precision;
import ProjectCenter.CoreEngine.Vector3f;
import ProjectCenter.PhysicsEngine.IntersectData;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.BeforeClass;

public class BoundingSphereTest {
    
    public BoundingSphereTest() {
    }
    
    @BeforeClass
    public  static void setupClass(){
    System.out.println("--- START TEST: BoundingSphereTest ---");
    }
    
    @AfterClass
    public static void tearDownClass(){
        System.out.println("--- END TEST: BoundingSphereTest ---\n");
    }

    @Test
    public void testGetPoint() {
        System.out.println("getPoint");
        
        BoundingSphere instance = new BoundingSphere(Vector3f.UNIT_X, 2.f);
        Vector3f expResult = Vector3f.UNIT_X;
        Vector3f result = instance.getPosition();
        assertEquals(true, result.equals(expResult));
    }

    @Test
    public void testGetRadius() {
        System.out.println("getRadius");
        
        BoundingSphere instance = new BoundingSphere(Vector3f.UNIT_X, 2.f);;
        float expResult = 2.0F;
        float result = instance.getRadius();
        assertEquals(true, Precision.equals(result, expResult) );
    }

    @Test
    public void testIntersectBoundingSphere() {
        System.out.println("IntersectBoundingSphere");
        
        BoundingSphere instance = new BoundingSphere(new Vector3f(0,0,0), 1.f);
        BoundingSphere aOther = new BoundingSphere(new Vector3f(0,3,0), 1.f);
        IntersectData expResult = new IntersectData(false, new Vector3f(0,1,0));
        IntersectData result = instance.IntersectBoundingSphere(aOther);
        assertEquals(expResult.isIntersecting(), result.isIntersecting());
        assertEquals(true, Precision.equals(expResult.getDistance(), result.getDistance()));
        
        aOther = new BoundingSphere(new Vector3f(0,0,2), 1.f);
        expResult = new IntersectData(false, new Vector3f(0,0,0));
        result = instance.IntersectBoundingSphere(aOther);
        assertEquals(expResult.isIntersecting(), result.isIntersecting());
        assertEquals(true, Precision.equals(expResult.getDistance(), result.getDistance()));
        
        aOther = new BoundingSphere(new Vector3f(1,0,0), 1.f);
        expResult = new IntersectData(true, new Vector3f(-1f,0,0));
        result = instance.IntersectBoundingSphere(aOther);
        assertEquals(expResult.isIntersecting(), result.isIntersecting());
        assertEquals(true, Precision.equals(expResult.getDistance(), result.getDistance()));
    }
    
}
