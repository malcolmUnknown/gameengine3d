/* GameEngine 3D - A programm to design games with 3D content 
 * Copyright (C) 2016-2056 Frank Küwen
 *  
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package DepricatedTest;

import Depricated.SectionPlane;
import Depricated.BoundingSphere;
import Depricated.BoundingSphere;
import Depricated.SectionPlane;
import ProjectCenter.CoreEngine.Precision;
import ProjectCenter.CoreEngine.Vector3f;
import ProjectCenter.PhysicsEngine.IntersectData;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.BeforeClass;

public class SectionPlaneTest {
    
    public SectionPlaneTest() {
    }
    
    @BeforeClass
    public  static void setupClass(){
    System.out.println("--- START TEST: SectionPlaneTest ---");
    }
    
    @AfterClass
    public static void tearDownClass(){
        System.out.println("--- END TEST: SectionPlaneTest ---\n");
    }

    @Test
    public void testNormalized() {
        System.out.println("Normalized");
        SectionPlane instance = new SectionPlane(new Vector3f(0,2,0), new Vector3f(0,3,0));
        SectionPlane expResult = new SectionPlane(new Vector3f(0,1,0), new Vector3f(0,3,0));
        SectionPlane result = instance.Normalized();
        assertEquals(true, result.getNormal().equals(expResult.getNormal()));
        assertEquals(true, Precision.equals(result.getDistance(), expResult.getDistance()));
    }

    @Test
    public void testIntersectSphere() {
        System.out.println("IntersectSphere");
        SectionPlane instance = new SectionPlane(new Vector3f(0,1,0), new Vector3f(0,0,0));
        BoundingSphere aSphere = new BoundingSphere(new Vector3f(0,0,0), 1);
        IntersectData expResult = new IntersectData(true, new Vector3f(0,-1f,0));
        IntersectData result = instance.IntersectSphere(aSphere);
        assertEquals(expResult.isIntersecting(), result.isIntersecting());
        assertEquals(true, Precision.equals(expResult.getDistance(), result.getDistance()));
           
        aSphere = new BoundingSphere(new Vector3f(0,3,0), 1);
        expResult = new IntersectData(false, new Vector3f(0,+2f,0));
        result = instance.IntersectSphere(aSphere);
        assertEquals(expResult.isIntersecting(), result.isIntersecting());
        assertEquals(true, Precision.equals(expResult.getDistance(), result.getDistance()));
        
        aSphere = new BoundingSphere(new Vector3f(0,-3,0), 1);
        expResult = new IntersectData(false, new Vector3f(0,2f,0));
        result = instance.IntersectSphere(aSphere);
        assertEquals(expResult.isIntersecting(), result.isIntersecting());
        assertEquals(true, Precision.equals(expResult.getDistance(), result.getDistance()));
     
        aSphere = new BoundingSphere(new Vector3f(0,0,2), 1);
        expResult = new IntersectData(true, new Vector3f(0,-1f,0));
        result = instance.IntersectSphere(aSphere);
        assertEquals(expResult.isIntersecting(), result.isIntersecting());
        assertEquals(true, Precision.equals(expResult.getDistance(), result.getDistance()));
    
        aSphere = new BoundingSphere(new Vector3f(1,0,0), 1);
        expResult = new IntersectData(true, new Vector3f(0,-1f,0));
        result = instance.IntersectSphere(aSphere);
        assertEquals(expResult.isIntersecting(), result.isIntersecting());
        assertEquals(true, Precision.equals(expResult.getDistance(), result.getDistance()));
    }

    @Test
    public void testGetNormal() {
        System.out.println("getNormal");
        SectionPlane instance = new SectionPlane(new Vector3f(0,2,0), new Vector3f(0,3,0));
        Vector3f expResult = new Vector3f(0,2,0);
        Vector3f result = instance.getNormal();
        assertEquals(true, result.equals(expResult));
    }

    @Test
    public void testGetDistance() {
        System.out.println("getDistance");
        SectionPlane instance = new SectionPlane(new Vector3f(0,2,0), new Vector3f(0,3,0));
        float expResult = 6.0F;
        float result = instance.getDistance();
        assertEquals(true, Precision.equals(result, expResult));
    }
    
}
