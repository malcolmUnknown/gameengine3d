/* GameEngine 3D - A programm to design games with 3D content 
 * Copyright (C) 2016-2056 Frank Küwen
 *  
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package DepricatedTest;

import Depricated.AxisAllignedBoundingBox;
import Depricated.AxisAllignedBoundingBox;
import ProjectCenter.CoreEngine.Precision;
import ProjectCenter.CoreEngine.Vector3f;
import ProjectCenter.PhysicsEngine.IntersectData;
import org.junit.AfterClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.BeforeClass;

public class AxisAllignedBoundingBoxTest {
    
    public AxisAllignedBoundingBoxTest() {
    }
    
    @BeforeClass
    public  static void setupClass(){
    System.out.println("--- START TEST: AxisAllignedBoundingBoxTest ---");
    }
    
    @AfterClass
    public static void tearDownClass(){
        System.out.println("--- END TEST: AxisAllignedBoundingBoxTest ---\n");
    }

    @Test
    public void testIntersectAxisAlligendBoundingBox() {
        System.out.println("IntersectAxisAlligendBoundingBox");
        
        AxisAllignedBoundingBox aOther = new AxisAllignedBoundingBox( new Vector3f(1,1,1),  new Vector3f(2,2,2));
        AxisAllignedBoundingBox instance = new AxisAllignedBoundingBox( new Vector3f(0,0,0),  new Vector3f(1,1,1));
        IntersectData expResult = new IntersectData(false, new Vector3f());
        IntersectData result = instance.IntersectAxisAlligendBoundingBox(aOther);
        assertEquals(expResult.isIntersecting(), result.isIntersecting());
        assertEquals(true, Precision.equals(expResult.getDistance(), result.getDistance()));
        
        aOther = new AxisAllignedBoundingBox( new Vector3f(1,0,0),  new Vector3f(2,1,1));
        expResult = new IntersectData(false,  new Vector3f(0,-1,-1));
        result = instance.IntersectAxisAlligendBoundingBox(aOther);
        assertEquals(expResult.isIntersecting(), result.isIntersecting());
        assertEquals(true, Precision.equals(expResult.getDistance(), result.getDistance()));
    
        aOther = new AxisAllignedBoundingBox( new Vector3f(0,0,-2),  new Vector3f(1,1,-1));
        expResult = new IntersectData(false,  new Vector3f(-1,-1,1));
        result = instance.IntersectAxisAlligendBoundingBox(aOther);
        assertEquals(expResult.isIntersecting(), result.isIntersecting());
        assertEquals(true, Precision.equals(expResult.getDistance(), result.getDistance()));
        
        aOther = new AxisAllignedBoundingBox( new Vector3f(0,0.5f,0),  new Vector3f(1,1.5f,1));
        expResult = new IntersectData(true,  new Vector3f(-1,-0.5f,-1));
        result = instance.IntersectAxisAlligendBoundingBox(aOther);
        assertEquals(expResult.isIntersecting(), result.isIntersecting());
        assertEquals(true, Precision.equals(expResult.getDistance(), result.getDistance()));        
    }

    @Test
    public void testGetMinExtends() {
        System.out.println("getMinExtends");
        
        AxisAllignedBoundingBox instance = new AxisAllignedBoundingBox( new Vector3f(1,0,0),  new Vector3f(3,3,3));
        Vector3f expResult = new Vector3f(1,0,0);
        Vector3f result = instance.getMinExtends();
        assertEquals(true, result.equals(expResult));
    }

    @Test
    public void testGetMaxExtends() {
        System.out.println("getMaxExtends");
        
        AxisAllignedBoundingBox instance = new AxisAllignedBoundingBox( new Vector3f(1,0,0),  new Vector3f(3,3,3));
        Vector3f expResult = new Vector3f(3,3,3);
        Vector3f result = instance.getMaxExtends();
        assertEquals(true, result.equals(expResult));
    }
}
