/* GameEngine 3D - A programm to design games with 3D content 
 * Copyright (C) 2016-2056 Frank Küwen
 *  
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package DepricatedTest;

import Depricated.PhysicsObject;
import Depricated.BoundingSphere;
import ProjectCenter.CoreEngine.Vector3f;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.BeforeClass;

public class PhysicsObjectTest {
    
    public PhysicsObjectTest() {
    }
    
    @BeforeClass
    public  static void setupClass(){
    System.out.println("--- START TEST: PhysicsObjectTest ---");
    }
    
    @AfterClass
    public static void tearDownClass(){
        System.out.println("--- END TEST: PhysicsObjectTest ---\n");
    }

    @Test
    public void testIntegrate() {
        System.out.println("Integrate");
        float aTimeDelta = 1.0F;
        PhysicsObject instance = new PhysicsObject(new BoundingSphere( new Vector3f(0,0,0), 1.f), new Vector3f(5,0,0));
        instance.integrate(aTimeDelta);
        Vector3f expResult = new Vector3f(5,0,0);
        assertEquals(true, instance.getPosition().equals(expResult));
        
        instance = new PhysicsObject(new BoundingSphere( new Vector3f(0,0,0), 1.f), new Vector3f(5,0,0));
        aTimeDelta = 4.0F;
        expResult = new Vector3f(20,0,0);
        instance.integrate(aTimeDelta);
        assertEquals(true, instance.getPosition().equals(expResult));
        
        instance = new PhysicsObject(new BoundingSphere( new Vector3f(0,0,0), 1.f), new Vector3f(5,0,0));
        aTimeDelta = 4.0F;
        instance.integrate(aTimeDelta);
        instance.integrate(aTimeDelta);
        expResult = new Vector3f(40,0,0);
        assertEquals(true, instance.getPosition().equals(expResult));
    
    }

    @Test
    public void testGetPosition() {
        System.out.println("getPosition");
        PhysicsObject instance = new PhysicsObject(new BoundingSphere( new Vector3f(0,0,0), 1.f), new Vector3f(5,0,0));
        Vector3f expResult = new Vector3f(0,0,0);
        Vector3f result = instance.getPosition();
        assertEquals(true, result.equals(expResult));
    }

    @Test
    public void testGetVelocity() {
        System.out.println("getVelocity");
        PhysicsObject instance = new PhysicsObject(new BoundingSphere( new Vector3f(0,0,0), 1.f), new Vector3f(5,0,0));
        Vector3f expResult = new Vector3f(5,0,0);
        Vector3f result = instance.getVelocity();
        assertEquals(true, result.equals(expResult));
    }
    
}
