/* GameEngine 3D - A programm to design games with 3D content 
 * Copyright (C) 2016-2056 Frank Küwen
 *  
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package ProjectCenter.CoreEngine;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

public class Matrix4fTest {

    public Matrix4fTest() {
    }

    @BeforeClass
    public static void setUpClass() {
        System.out.println("--- START TEST: Matrix4fTest ---");
    }

    @AfterClass
    public static void tearDownClass() {
        System.out.println("--- END TEST: Matrix4fTest ---\n");
    }

    /**
     * Test of clone method, of class Matrix4f.
     */
    @Test
    public void testClone() {
        System.out.println("clone");
        Matrix4f aOriginal = new Matrix4f();
        aOriginal.initZeroMatrix();
        Matrix4f instance = new Matrix4f().Identity();
        Matrix4f expResult = new Matrix4f().Identity();
        assertEquals(true, instance.equals(expResult));

        expResult.initZeroMatrix();
        Matrix4f result = instance.clone(aOriginal);
        assertEquals(true, result.equals(expResult));
    }

    /**
     * Test of toString method, of class Matrix4f.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        Matrix4f instance = new Matrix4f().Identity();
        String expResult
                = "| +1.00 | +0.00 | +0.00 | +0.00 |\n"
                + "| +0.00 | +1.00 | +0.00 | +0.00 |\n"
                + "| +0.00 | +0.00 | +1.00 | +0.00 |\n"
                + "| +0.00 | +0.00 | +0.00 | +1.00 |\n";
        String result = instance.toString();
        assertEquals(expResult, result);
    }

    /**
     * Test of initTranslation method, of class Matrix4f.
     */
    @Test
    public void testInitTranslation() {
        System.out.println("initTranslation");
        Vector3f aVector = new Vector3f(4, 1, 9);
        Matrix4f instance = new Matrix4f();
        Matrix4f expResult = new Matrix4f().Identity();
        expResult.set(0, 3, 4);
        expResult.set(1, 3, 1);
        expResult.set(2, 3, 9);
        Matrix4f result = instance.initTranslation(aVector);
        assertEquals(true, result.equals(expResult));
    }

    /**
     * Test of initRotation method, of class Matrix4f.
     */
    @Test
    public void testInitRotation_Vector3f_float() {
        System.out.println("initRotation");
        Vector3f aNormedAxis = new Vector3f(0, 0, 1);
        float aAngle = (float) Math.PI;
        Matrix4f instance = new Matrix4f().Identity();
        Matrix4f expResult = new Matrix4f().Identity();
        expResult.set(0, 1, 0);
        expResult.set(1, 0, 0);
        expResult.set(1, 1, -1);
        expResult.set(0, 0, -1);
        Matrix4f result = instance.initRotation(aNormedAxis, aAngle);
        assertEquals(true, result.equals(expResult));

        expResult.set(0, 1, -1);
        expResult.set(1, 0, 1);
        expResult.set(1, 1, 0);
        expResult.set(0, 0, 0);
        aAngle = (float) Math.PI / 2.f;
        result = instance.initRotation(aNormedAxis, aAngle);
        assertEquals(true, result.equals(expResult));
    }

    /**
     * Test of initScaling method, of class Matrix4f.
     */
    @Test
    public void testInitScaling() {
        System.out.println("initScaling");
        Vector3f aVector = new Vector3f(1, 2, 3);
        Matrix4f instance = new Matrix4f();
        Matrix4f expResult = new Matrix4f().Identity();
        expResult.set(0, 0, 1);
        expResult.set(1, 1, 2);
        expResult.set(2, 2, 3);
        Matrix4f result = instance.initScaling(aVector);
        assertEquals(true, result.equals(expResult));
    }

    /**
     * Test of initMapping method, of class Matrix4f.
     */
    @Test
    public void testInitMapping() {
        System.out.println("initMapping");
        Vector3f aMapX = new Vector3f(0, 2, 0);
        Vector3f aMapY = new Vector3f(1, 0, 0);
        Vector3f aMapZ = new Vector3f(0, 0, 3);
        Vector3f aLocation = new Vector3f(5, 4, 3);
        Matrix4f instance = new Matrix4f();
        Matrix4f expResult = new Matrix4f();
        expResult.initZeroMatrix();
        expResult.set(0, 1, 1);
        expResult.set(1, 0, 2);
        expResult.set(2, 2, 3);
        expResult.set(0, 3, 5);
        expResult.set(1, 3, 4);
        expResult.set(2, 3, 3);
        expResult.set(3, 3, 1);

        Matrix4f result = instance.initMapping(aMapX, aMapY, aMapZ, aLocation);
        assertEquals(true, result.equals(expResult));
    }

    /**
     * Test of initRotation method, of class Matrix4f.
     */
    @Test
    public void testInitRotation_Vector3f_Vector3f() {
        System.out.println("initRotation");
        Vector3f aForwardView = new Vector3f(2, 0, 0);
        Vector3f aUpView = new Vector3f(0, 2, 0);
        Matrix4f instance = new Matrix4f();
        Matrix4f expResult = new Matrix4f().Identity();
        expResult.set(0, 2, -1);
        expResult.set(2, 0, 1);
        expResult.set(2, 2, 0);
        expResult.set(0, 0, 0);

        Matrix4f result = instance.initRotation(aForwardView, aUpView);
        assertEquals(true, result.equals(expResult));
    }

    /**
     * Test of initZeroMatrix method, of class Matrix4f.
     */
    @Test
    public void testInitZeroMatrix() {
        System.out.println("initZeroMatrix");
        Matrix4f instance = new Matrix4f();
        instance.initZeroMatrix();
        Matrix4f expResult = new Matrix4f();
        for (int I = 0; I < Matrix4f.ROWS; I++) {
            for (int J = 0; J < Matrix4f.COLUMNS; J++) {
                expResult.set(I, J, 0);
            }
        }
        assertEquals(true, instance.equals(expResult));
    }

    /**
     * Test of Identity method, of class Matrix4f.
     */
    @Test
    public void testIdentity() {
        System.out.println("Identity");
        Matrix4f instance = new Matrix4f();
        Matrix4f result = instance.Identity();
        Matrix4f expResult = new Matrix4f();
        for (int I = 0; I < Matrix4f.ROWS; I++) {
            for (int J = 0; J < Matrix4f.COLUMNS; J++) {
                expResult.set(I, J, 0);
            }
        }
        expResult.set(0, 0, 1);
        expResult.set(1, 1, 1);
        expResult.set(2, 2, 1);
        expResult.set(3, 3, 1);
        assertEquals(true, result.equals(expResult));
    }

    /**
     * Test of mul method, of class Matrix4f.
     */
    @Test
    public void testMul_Vector3f() {
        System.out.println("mul");
        Vector3f aVector = new Vector3f(1, 2, 3);
        Matrix4f instance = new Matrix4f();
        Vector3f expResult = new Vector3f(-2, 1, 3);
        instance = instance.initRotation(new Vector3f(0, 0, 1), (float) Math.PI / 2.f);
        Vector3f result = instance.mul(aVector);
        assertEquals(true, result.equals(expResult));
    }

    /**
     * Test of mul method, of class Matrix4f.
     */
    @Test
    public void testMul_Matrix4f() {
        System.out.println("mul");
        Matrix4f aMatrix = new Matrix4f().initTranslation(new Vector3f(1, 2, 3));
        Matrix4f instance = new Matrix4f().initScaling(new Vector3f(6, 5, 4));
        Matrix4f expResult = new Matrix4f().initTranslation(new Vector3f(6, 10, 12));
        expResult.set(0, 0, 6);
        expResult.set(1, 1, 5);
        expResult.set(2, 2, 4);
        Matrix4f result = instance.mul(aMatrix);
        assertEquals(true, result.equals(expResult));
    }

    /**
     * Test of transposed method, of class Matrix4f.
     */
    @Test
    public void testTransposed() {
        System.out.println("transposed");
        Matrix4f instance = new Matrix4f().initTranslation(new Vector3f(1, 2, 3));
        Matrix4f expResult = new Matrix4f().Identity();
        expResult.set(3, 0, 1);
        expResult.set(3, 1, 2);
        expResult.set(3, 2, 3);
        Matrix4f result = instance.transposed();
        assertEquals(true, result.equals(expResult));
    }

    /**
     * Test of getM method, of class Matrix4f.
     */
    @Test
    public void testGetM() {
        System.out.println("getM");
        Matrix4f instance = new Matrix4f().initTranslation(new Vector3f(6, 10, 12));
        instance.set(0, 0, 6);
        instance.set(1, 1, 5);
        instance.set(2, 2, 4);
        float[][] expResult = new float[Matrix4f.ROWS][Matrix4f.COLUMNS];
        for (int I = 0; I < Matrix4f.ROWS; I++) {
            for (int J = 0; J < Matrix4f.COLUMNS; J++) {
                if (I == J) {
                    if (I == 0) {
                        expResult[I][J] = 6;
                    } else if (I == 1) {
                        expResult[I][J] = 5;
                    } else if (I == 2) {
                        expResult[I][J] = 4;
                    } else {
                        expResult[I][J] = 1;
                    }
                } else if (J == 3) {
                    if (I == 0) {
                        expResult[I][J] = 6;
                    } else if (I == 1) {
                        expResult[I][J] = 10;
                    } else if (I == 2) {
                        expResult[I][J] = 12;
                    } else {
                        expResult[I][J] = 1;
                    }
                } else {
                    expResult[I][J] = 0;
                }
            }
        }
        float[][] result = instance.getM();
        assertArrayEquals(expResult, result);
    }

    /**
     * Test of setM method, of class Matrix4f.
     */
    @Test
    public void testSetM() {
        System.out.println("setM");
        float[][] m = new float[Matrix4f.ROWS][Matrix4f.COLUMNS];
        Matrix4f instance = new Matrix4f();

        for (int I = 0; I < Matrix4f.ROWS; I++) {
            for (int J = 0; J < Matrix4f.COLUMNS; J++) {
                m[I][J] = 0;
            }
        }
        m[2][3] = 10;
        instance.setM(m);

        assertArrayEquals(m, instance.getM());
    }

    /**
     * Test of get method, of class Matrix4f.
     */
    @Test
    public void testGet() {
        System.out.println("get");
        Matrix4f instance = new Matrix4f().initScaling(new Vector3f(3,4,5));
        float expResult = 3.0F;
        float result = instance.get(0, 0);
        assertEquals(true, Precision.equals(expResult, result));
        result = instance.get(1, 1);
        expResult = 4.0F;
        assertEquals(true, Precision.equals(expResult, result));
        result = instance.get(2, 2);
        expResult = 5.0F;
        assertEquals(true, Precision.equals(expResult, result));
    }

    /**
     * Test of set method, of class Matrix4f.
     */
    @Test
    public void testSet() {
        System.out.println("set");
        int x = 0;
        int y = 0;
        float aValue = 3.0F;
        Matrix4f instance = new Matrix4f();
        assertEquals(true, Precision.equals(0, instance.get(0, 0)));
        instance.set(0, 0, aValue);
        assertEquals(true, Precision.equals(aValue, instance.get(0, 0)));
        
         aValue = 23.0F;
        assertEquals(true, Precision.equals(0, instance.get(3, 1)));
        instance.set(3, 1, aValue);
        assertEquals(true, Precision.equals(aValue, instance.get(3, 1)));
    }

    /**
     * Test of equals method, of class Matrix4f.
     */
    @Test
    public void testEquals() {
        System.out.println("equals");
        Matrix4f aMatrix = new Matrix4f().Identity();
        Matrix4f instance = new Matrix4f();
        boolean expResult = false;
        boolean result = instance.equals(aMatrix);
        assertEquals(expResult, result);
        
        aMatrix = new Matrix4f();
        expResult = true;
        result = instance.equals(aMatrix);
        assertEquals(expResult, result);
        
        
        aMatrix.set(1, 2, 0.5f);
        expResult = false;
        result = instance.equals(aMatrix);
        assertEquals(expResult, result);
        
        
        aMatrix.set(1, 2, 0.0005f);
        expResult = false;
        result = instance.equals(aMatrix);
        assertEquals(expResult, result);
        
        aMatrix.set(1, 2, 0.0000005f);
        expResult = true;
        result = instance.equals(aMatrix);
        assertEquals(expResult, result);
    }
//
//    /**
//     * Test of initPerspective method, of class Matrix4f.
//     */
//    @Test
//    public void testInitPerspective() {
//        System.out.println("initPerspective");
//        float aFieldOfView = 0.0F;
//        float aAspectRatio = 0.0F;
//        float aZNear = 0.0F;
//        float aZFar = 0.0F;
//        Matrix4f instance = new Matrix4f();
//        Matrix4f expResult = null;
//        Matrix4f result = instance.initPerspective(aFieldOfView, aAspectRatio, aZNear, aZFar);
//        assertEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
//
//    /**
//     * Test of initOrthographic method, of class Matrix4f.
//     */
//    @Test
//    public void testInitOrthographic() {
//        System.out.println("initOrthographic");
//        float aLeft = 0.0F;
//        float aRight = 0.0F;
//        float aTop = 0.0F;
//        float aBottom = 0.0F;
//        float aNear = 0.0F;
//        float aFar = 0.0F;
//        Matrix4f instance = new Matrix4f();
//        Matrix4f expResult = null;
//        Matrix4f result = instance.initOrthographic(aLeft, aRight, aTop, aBottom, aNear, aFar);
//        assertEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
//
//    /**
//     * Test of initProjectionOrthographic method, of class Matrix4f.
//     */
//    @Test
//    public void testInitProjectionOrthographic() {
//        System.out.println("initProjectionOrthographic");
//        float aOrthoScale = 0.0F;
//        float aWidth = 0.0F;
//        float aHeight = 0.0F;
//        float aZNear = 0.0F;
//        float aZFar = 0.0F;
//        Matrix4f instance = new Matrix4f();
//        Matrix4f expResult = null;
//        Matrix4f result = instance.initProjectionOrthographic(aOrthoScale, aWidth, aHeight, aZNear, aZFar);
//        assertEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }

}
