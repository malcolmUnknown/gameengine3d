/* GameEngine 3D - A programm to design games with 3D content 
 * Copyright (C) 2016-2056 Frank Küwen
 *  
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package ProjectCenter.CoreEngine;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

public class QuaternionTest {

    public QuaternionTest() {
    }

    @BeforeClass
    public static void setUpClass() {
        System.out.println("--- START TEST: QuaternionTest ---");
    }

    @AfterClass
    public static void tearDownClass() {
        System.out.println("--- END TEST: QuaternionTest ---");
    }

    /**
     * Test of toString method, of class Quaternion.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        Quaternion instance = new Quaternion(2.3f, 4.523f, 1.39f, 3.335f);
        String expResult = "[+2.30|+4.52|+1.39|+3.34]";
        String result = instance.toString();
        assertEquals(expResult, result);
    }

    /**
     * Test of set method, of class Quaternion.
     */
    @Test
    public void testSet_4args() {
        System.out.println("set");
        float aX = 2.3F;
        float aY = 4.523F;
        float aZ = 1.39F;
        float aW = 3.335F;
        Quaternion instance = new Quaternion();
        instance.set(aX, aY, aZ, aW);
        assertEquals(true, instance.equals(new Quaternion(2.3f, 4.523f, 1.39f, 3.335f)));
    }

    /**
     * Test of set method, of class Quaternion.
     */
    @Test
    public void testSet_Quaternion() {
        System.out.println("set");
        Quaternion aOriginal = new Quaternion(2.3f, 4.523f, 1.39f, 3.335f);
        Quaternion instance = new Quaternion();
        instance.set(aOriginal);
        assertEquals(true, instance.equals(aOriginal));
    }

    /**
     * Test of clone method, of class Quaternion.
     */
    @Test
    public void testClone() {
        System.out.println("clone");
        Quaternion aOriginal = new Quaternion(2.3f, 4.523f, 1.39f, 3.335f);
        Quaternion instance = new Quaternion();
        Quaternion expResult = new Quaternion(2.3f, 4.523f, 1.39f, 3.335f);
        Quaternion result = instance.clone(aOriginal);
        assertEquals(true, result.equals(expResult));
    }

    /**
     * Test of length method, of class Quaternion.
     */
    @Test
    public void testLength() {
        System.out.println("length");
        Quaternion instance = new Quaternion(2.3f, 4.523f, 1.39f, 3.335f);
        float expResult = (float) Math.sqrt(2.3 * 2.3 + 4.523 * 4.523 + 1.39 * 1.39 + 3.335 * 3.335);
        float result = instance.length();
        assertEquals(true, Precision.equals(result, expResult));
    }

    /**
     * Test of normalized method, of class Quaternion.
     */
    @Test
    public void testNormalized() {
        System.out.println("normalized");
        Quaternion instance = new Quaternion(0, 0, 0, 6);
        Quaternion expResult = new Quaternion(0, 0, 0, 1);
        Quaternion result = instance.normalized();
        assertEquals(true, result.equals(expResult));

        instance = new Quaternion(1, 1, 1, 1);
        expResult = new Quaternion(1, 1, 1, 1).mul((float) (1.f / (Math.sqrt(4))));
        result = instance.normalized();
        assertEquals(true, result.equals(expResult));
    }

    /**
     * Test of conjugate method, of class Quaternion.
     */
    @Test
    public void testConjugate() {
        System.out.println("conjugate");
        Quaternion instance = new Quaternion(1, 1, 1, 1);
        Quaternion expResult = new Quaternion(-1, -1, -1, 1);
        Quaternion result = instance.conjugate();
        assertEquals(true, result.equals(expResult));
    }

    /**
     * Test of mul method, of class Quaternion.
     */
    @Test
    public void testMul_Quaternion() {
        System.out.println("mul");
        Quaternion aQuarterion = new Quaternion(1, 1, 1, 1);
        Quaternion instance = new Quaternion(-1, -1, -1, 1);
        Quaternion expResult = new Quaternion(0, 0, 0, 4);
        Quaternion result = instance.mul(aQuarterion);
        assertEquals(true, result.equals(expResult));

        aQuarterion = new Quaternion(1, 0, 0, 1);
        instance = new Quaternion(0, 0, -1, 1);
        expResult = new Quaternion(1, -1, -1, 1);
        result = instance.mul(aQuarterion);
        assertEquals(true, result.equals(expResult));
    }

    /**
     * Test of mul method, of class Quaternion.
     */
    @Test
    public void testMul_Vector3f() {
        System.out.println("mul");
        Vector3f aVector = new Vector3f(1, 2, 3);
        Quaternion instance = new Quaternion(6, 7, 4, 3);
        Quaternion expResult = instance.mul(new Quaternion(1, 2, 3, 0));
        Quaternion result = instance.mul(aVector);
        assertEquals(true, result.equals(expResult));
    }

    /**
     * Test of mul method, of class Quaternion.
     */
    @Test
    public void testMul_float() {
        System.out.println("mul");
        float aScalar = 3.0F;
        Quaternion instance = new Quaternion(6, 7, 4, 3);
        Quaternion expResult = new Quaternion(18, 21, 12, 9);
        Quaternion result = instance.mul(aScalar);
        assertEquals(true, result.equals(expResult));
    }

    /**
     * Test of dot method, of class Quaternion.
     */
    @Test
    public void testDot() {
        System.out.println("dot");
        Quaternion aQuaternion = new Quaternion(1, 2, 4, 3);
        Quaternion instance = new Quaternion(6, 7, 4, 3);
        float expResult = 45.0F;
        float result = instance.dot(aQuaternion);
        assertEquals(true, Precision.equals(result, expResult));
    }

    /**
     * Test of add method, of class Quaternion.
     */
    @Test
    public void testAdd() {
        System.out.println("add");
        Quaternion aQuaternion = new Quaternion(2, 5, 4, 3);
        Quaternion instance = new Quaternion(6, 7, 4, 3);
        Quaternion expResult = new Quaternion(8, 12, 8, 6);
        Quaternion result = instance.add(aQuaternion);
        assertEquals(true, result.equals(expResult));
    }

    /**
     * Test of sub method, of class Quaternion.
     */
    @Test
    public void testSub() {
        System.out.println("sub");
        Quaternion aQuaternion = new Quaternion(3, 12, 4, 7);
        Quaternion instance = new Quaternion(8, 4, 5, 16);
        Quaternion expResult = new Quaternion(5, -8, 1, 9);
        Quaternion result = instance.sub(aQuaternion);
        assertEquals(true, result.equals(expResult));
    }

    /**
     * Test of linearExtrapolationNormalized method, of class Quaternion.
     */
    @Test
    public void testLinearExtrapolationNormalized() {
        System.out.println("linearExtrapolationNormalized");
        Quaternion aQuaternion = new Quaternion(-12, -6, -7, 4);
        float aScalar = 1.0F;
        boolean aShortest = false;
        Quaternion instance = new Quaternion(1, 2, 3, 4);
        Quaternion expResult = new Quaternion(-12, -6, -7, 4).normalized();
        Quaternion result = instance.linearExtrapolationNormalized(aQuaternion, aScalar, aShortest);
        assertEquals(true, result.equals(expResult));

        aScalar = 0.0F;
        aShortest = false;
        expResult = new Quaternion(1, 2, 3, 4).normalized();
        result = instance.linearExtrapolationNormalized(aQuaternion, aScalar, aShortest);
        assertEquals(true, result.equals(expResult));

        aScalar = 3.0F;
        aShortest = false;
        expResult = new Quaternion(-38, -22, -27, 4).normalized();
        result = instance.linearExtrapolationNormalized(aQuaternion, aScalar, aShortest);
        assertEquals(true, result.equals(expResult));

        aScalar = 3.0F;
        aShortest = true;
        expResult = new Quaternion(34, 14, 15, -20).normalized();
        result = instance.linearExtrapolationNormalized(aQuaternion, aScalar, aShortest);
        assertEquals(true, result.equals(expResult));
    }

    /**
     * Test of linearExtrapolationSpherical method, of class Quaternion.
     */
    @Test
    public void testLinearExtrapolationSpherical() {
        System.out.println("linearExtrapolationSpherical");

        Quaternion aQuaternion = new Quaternion(-12, -6, -7, 4);
        float aScalar = 1.0F;
        boolean aShortest = false;
        Quaternion instance = new Quaternion(1, 2, 3, 4);
        Quaternion expResult = new Quaternion(-12, -6, -7, 4).normalized();
        Quaternion result = instance.linearExtrapolationSpherical(aQuaternion, aScalar, aShortest);
        assertEquals(true, result.equals(expResult));

        aScalar = 0.0F;
        aShortest = false;
        expResult = new Quaternion(1, 2, 3, 4).normalized();
        result = instance.linearExtrapolationSpherical(aQuaternion, aScalar, aShortest);
        assertEquals(true, result.equals(expResult));

        aScalar = 3.0F;
        aShortest = false;
        expResult = new Quaternion(-38, -22, -27, 4).normalized();
        result = instance.linearExtrapolationSpherical(aQuaternion, aScalar, aShortest);
        assertEquals(true, result.equals(expResult));

        aScalar = 3.0F;
        aShortest = true;
        expResult = new Quaternion(34, 14, 15, -20).normalized();
        result = instance.linearExtrapolationSpherical(aQuaternion, aScalar, aShortest);
        assertEquals(true, result.equals(expResult));

        float angle = (float) Math.PI / 4.f;
        aQuaternion = new Quaternion(0, 0, (float) Math.sin(angle), (float) Math.cos(angle));
        instance = new Quaternion(0, 0, (float) Math.sin(-angle), (float) Math.cos(-angle));
        aScalar = 1.0F;
        aShortest = false;
        expResult = aQuaternion;
        result = instance.linearExtrapolationSpherical(aQuaternion, aScalar, aShortest);
        assertEquals(true, result.equals(expResult));

        aScalar = 0.0F;
        aShortest = false;
        expResult = instance;
        result = instance.linearExtrapolationSpherical(aQuaternion, aScalar, aShortest);
        assertEquals(true, result.equals(expResult));

        aScalar = 2.0F;
        aShortest = false;
        expResult = instance.mul(-1);
        result = instance.linearExtrapolationSpherical(aQuaternion, aScalar, aShortest);
        assertEquals(true, result.equals(expResult));

        aScalar = 1.5F;
        aShortest = false;
        expResult = instance.mul(-1).add(aQuaternion).normalized();
        result = instance.linearExtrapolationSpherical(aQuaternion, aScalar, aShortest);
        assertEquals(true, result.equals(expResult));
    }

    /**
     * Test of toRotationMatrix method, of class Quaternion.
     */
    @Test
    public void testToRotationMatrix() {
        System.out.println("toRotationMatrix");
        float angle = (float) Math.PI / 4.f;
        Quaternion instance = new Quaternion(0, 0, (float) Math.sin(angle), (float) Math.cos(angle));
        Matrix4f expResult = new Matrix4f().initRotation(new Vector3f(0, 0, 1), -angle * 2);
        Matrix4f result = instance.toRotationMatrix();
        assertEquals(true, result.equals(expResult));
    }

    /**
     * Test of getRight method, of class Quaternion.
     */
    @Test
    public void testGetRight() {
        System.out.println("getRight");
        Quaternion instance = new Quaternion(0, 0, 0, 1);
        Vector3f expResult = new Vector3f(1, 0, 0);
        Vector3f result = instance.getRight();
        assertEquals(true, result.equals(expResult));
        
        instance = new Quaternion(new Vector3f(0,0,1), (float) Math.PI/2.f);
        expResult = new Vector3f(0,1,  0);
        result = instance.getRight();
        assertEquals(true, result.equals(expResult));
    }

    /**
     * Test of getUp method, of class Quaternion.
     */
    @Test
    public void testGetUp() {
        System.out.println("getUp");
        Quaternion instance = new Quaternion(0, 0, 0, 1);
        Vector3f expResult = new Vector3f(0, 1, 0);
        Vector3f result = instance.getUp();
        assertEquals(true, result.equals(expResult));
        
        instance = new Quaternion(new Vector3f(0,0,1), (float) Math.PI/2.f);
        expResult = new Vector3f(-1,0,  0);
        result = instance.getUp();
        assertEquals(true, result.equals(expResult));
    }

    /**
     * Test of getForward method, of class Quaternion.
     */
    @Test
    public void testGetForward() {
        System.out.println("getForward");
        Quaternion instance = new Quaternion(0, 0, 0, 1);
        Vector3f expResult = new Vector3f(0, 0, 1);
        Vector3f result = instance.getForward();
        assertEquals(true, result.equals(expResult));
        
        instance = new Quaternion(new Vector3f(1,0,0), (float) Math.PI/2.f);
        expResult = new Vector3f(0,-1,  0);
        result = instance.getForward();
        assertEquals(true, result.equals(expResult));
    }

    /**
     * Test of getLeft method, of class Quaternion.
     */
    @Test
    public void testGetLeft() {
        System.out.println("getLeft");
        Quaternion instance = new Quaternion(0, 0, 0, 1);
        Vector3f expResult = new Vector3f(-1, 0, 0);
        Vector3f result = instance.getLeft();
        assertEquals(true, result.equals(expResult));
        
        instance = new Quaternion(new Vector3f(0,0,1), (float) Math.PI/2.f);
        expResult = new Vector3f(0,-1,  0);
        result = instance.getLeft();
        assertEquals(true, result.equals(expResult));
    }

    /**
     * Test of getDown method, of class Quaternion.
     */
    @Test
    public void testGetDown() {
        System.out.println("getDown");
        Quaternion instance = new Quaternion(0, 0, 0, 1);
        Vector3f expResult = new Vector3f(0, -1, 0);
        Vector3f result = instance.getDown();
        assertEquals(true, result.equals(expResult));
        
        instance = new Quaternion(new Vector3f(0,0,1), (float) Math.PI/2.f);
        expResult = new Vector3f(1,0,  0);
        result = instance.getDown();
        assertEquals(true, result.equals(expResult));
    }

    /**
     * Test of getBackward method, of class Quaternion.
     */
    @Test
    public void testGetBackward() {
        System.out.println("getBackward");
        Quaternion instance = new Quaternion(0, 0, 0, 1);
        Vector3f expResult = new Vector3f(0, 0, -1);
        Vector3f result = instance.getBackward();
        assertEquals(true, result.equals(expResult));
        
        instance = new Quaternion(new Vector3f(1,0,0), (float) Math.PI/2.f);
        expResult = new Vector3f(0,1,  0);
        result = instance.getBackward();
        assertEquals(true, result.equals(expResult));
    }

    /**
     * Test of getX method, of class Quaternion.
     */
    @Test
    public void testGetX() {
        System.out.println("getX");
        Quaternion instance = new Quaternion(120, 0, 0, 0);
        float expResult = 120.0F;
        float result = instance.getX();
        assertEquals(true, Precision.equals(expResult, result));
    }

    /**
     * Test of setX method, of class Quaternion.
     */
    @Test
    public void testSetX() {
        System.out.println("setX");
        float x = 123.9f;
        Quaternion instance = new Quaternion();
        instance.setX(x);
        assertEquals(true, instance.equals(new Quaternion(123.9f, 0, 0, 0)));
    }

    /**
     * Test of getY method, of class Quaternion.
     */
    @Test
    public void testGetY() {
        System.out.println("getY");
        Quaternion instance = new Quaternion(0, 23, 0, 0);
        float expResult = 23.0F;
        float result = instance.getY();
        assertEquals(true, Precision.equals(expResult, result));
    }

    /**
     * Test of setY method, of class Quaternion.
     */
    @Test
    public void testSetY() {
        System.out.println("setY");
        float y = 20.0F;
        Quaternion instance = new Quaternion();
        instance.setY(y);
        assertEquals(true, instance.equals(new Quaternion(0, 20, 0, 0)));
    }

    /**
     * Test of getZ method, of class Quaternion.
     */
    @Test
    public void testGetZ() {
        System.out.println("getZ");
        Quaternion instance = new Quaternion(0, 0, 220, 0);
        float expResult = 220.0F;
        float result = instance.getZ();
        assertEquals(true, Precision.equals(expResult, result));
    }

    /**
     * Test of setZ method, of class Quaternion.
     */
    @Test
    public void testSetZ() {
        System.out.println("setZ");
        float z = 334.0F;
        Quaternion instance = new Quaternion();
        instance.setZ(z);
        assertEquals(true, instance.equals(new Quaternion(0, 0, 334, 0)));
    }

    /**
     * Test of getW method, of class Quaternion.
     */
    @Test
    public void testGetW() {
        System.out.println("getW");
        Quaternion instance = new Quaternion(0, 0, 0, 34);
        float expResult = 34.0F;
        float result = instance.getW();
        assertEquals(true, Precision.equals(expResult, result));
    }

    /**
     * Test of setW method, of class Quaternion.
     */
    @Test
    public void testSetW() {
        System.out.println("setW");
        float w = 90.0F;
        Quaternion instance = new Quaternion();
        instance.setW(w);
        assertEquals(true, instance.equals(new Quaternion(0, 0, 0, 90)));
    }

    /**
     * Test of equals method, of class Quaternion.
     */
    @Test
    public void testEquals() {
        System.out.println("equals");
        Quaternion aQuaternion = new Quaternion(1, 0, 0, 0);
        Quaternion instance = new Quaternion();
        boolean expResult = false;
        boolean result = instance.equals(aQuaternion);
        assertEquals(expResult, result);

        aQuaternion = new Quaternion(1, 0, 0, 0);
        expResult = false;
        result = instance.equals(aQuaternion);
        assertEquals(expResult, result);

        aQuaternion = new Quaternion(0.002f, 0, 0, 0);
        expResult = false;
        result = instance.equals(aQuaternion);
        assertEquals(expResult, result);

        aQuaternion = new Quaternion(0, 0.00002f, 0, 0);
        expResult = false;
        result = instance.equals(aQuaternion);
        assertEquals(expResult, result);

        aQuaternion = new Quaternion(0, 0, -0.0000002f, 0);
        expResult = true;
        result = instance.equals(aQuaternion);
        assertEquals(expResult, result);
    }

}
