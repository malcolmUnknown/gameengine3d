/* GameEngine 3D - A programm to design games with 3D content 
 * Copyright (C) 2016-2056 Frank Küwen
 *  
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package ProjectCenter.CoreEngine;

import Components.BaseLight;
import java.util.ArrayList;
import java.util.UUID;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

public class GameObjectTest {
    
    public GameObjectTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
        System.out.println("--- START TEST: GameObjectTest ---");
    }
    
    @AfterClass
    public static void tearDownClass() {
        System.out.println("--- END TEST: GameObjectTest ---\n");
    }

    /**
     * Test of addChild method, of class GameObject.
     */
    @Test
    public void testAddChild() {
        System.out.println("addChild");
        GameObject aNewChild = new GameObject();
        GameObject instance = new GameObject();
        assertEquals(1, instance.getAllAttached().size());
        instance.addChild(aNewChild);
        assertEquals(2, instance.getAllAttached().size());
        assertEquals(aNewChild, instance.getAllAttached().get(0));
    }

    /**
     * Test of addComponent method, of class GameObject.
     */
    @Test
    public void testAddComponent() {
        System.out.println("addComponent");
        GameObject instance = new GameObject();        
        BaseLight aNewComponent = new BaseLight(new Vector3f(),2);
        assertEquals(0, instance.getAllAttachedComponents().size());
        instance.addComponent(aNewComponent);
        assertEquals(1, instance.getAllAttachedComponents().size());
        assertEquals(aNewComponent, instance.getAllAttachedComponents().get(0));
    }

    /**
     * Test of getAllAttached method, of class GameObject.
     */
    @Test
    public void testGetAllAttached() {
        System.out.println("getAllAttached");
        GameObject instance = new GameObject();
        ArrayList<GameObject> result = instance.getAllAttached();
        ArrayList<GameObject> expResult = new ArrayList<>();
        expResult.add(instance);
        assertEquals(expResult, result);
        
        GameObject lChild = new GameObject();
        instance.addChild(lChild);
        result = instance.getAllAttached();
        expResult =  new ArrayList<>();
        expResult.add(lChild);
        expResult.add(instance);
        assertEquals(expResult, result);
    }

    /**
     * Test of getTransform method, of class GameObject.
     */
    @Test
    public void testGetTransform() {
        System.out.println("getTransform");
        GameObject instance = new GameObject();
        Vector3f expPosition = new Vector3f();
        Vector3f expScale = new Vector3f(1,1,1);
        Quaternion expRotation = new Quaternion(0, 0, 0, 1);
        Transform result = instance.getTransform();
        assertEquals(true, result.getPosition().equals(expPosition));
        assertEquals(true, result.getRotation().equals(expRotation));
        assertEquals(true, result.getScale().equals(expScale));
        
        result.setPosition(new Vector3f(2,0,0));
        result.setScale(new Vector3f(4,1,3));
        result.setRotation(new Quaternion(new Vector3f(0,0,1), (float) Math.PI/2));
        expPosition = new Vector3f(2,0,0);
        expScale = new Vector3f(4,1,3);
        expRotation = new Quaternion(new Vector3f(0,0,1), (float) Math.PI/2);
        assertEquals(true, instance.getTransform().getPosition().equals(expPosition));
        assertEquals(true, instance.getTransform().getRotation().equals(expRotation));
        assertEquals(true, instance.getTransform().getScale().equals(expScale));
        
        
        GameObject lChild =new GameObject();
        lChild.getTransform().setPosition(new Vector3f(0,0,-2));
        lChild.getTransform().setRotation(new Quaternion(new Vector3f(0,0,-1), (float) Math.PI/2));
        lChild.getTransform().setScale(new Vector3f(1,2,3));
        instance.addChild(lChild);
        expPosition = new Vector3f(0,0,-2);
        expScale = new Vector3f(1,2,3);
        expRotation = new Quaternion(new Vector3f(0,0,-1), (float) Math.PI/2);
        assertEquals(true, lChild.getTransform().getPosition().equals(expPosition));
        assertEquals(true, lChild.getTransform().getRotation().equals(expRotation));
        assertEquals(true, lChild.getTransform().getScale().equals(expScale));
        
        expPosition = new Vector3f(2,0,-6);
        expScale = new Vector3f(8,-1,9);
        expRotation = new Quaternion(0,0,0,1);
        result.setPosition(lChild.getTransform().getTransformedPosition());
        result.setRotation(lChild.getTransform().getTransformedRotation());
        result.setScale(lChild.getTransform().getTransformedScale());
        assertEquals(true, result.getPosition().equals(expPosition));
        assertEquals(true, result.getRotation().equals(expRotation));
        assertEquals(true, result.getScale().equals(expScale));
    }

    /**
     * Test of getIdentity method, of class GameObject.
     */
    @Test
    public void testGetIdentity() {
        System.out.println("getIdentity");
        GameObject instance = new GameObject();
        UUID result = instance.getIdentity();
        
        assertEquals(4, result.version());
    }

//    /**
//     * Test of inputAll method, of class GameObject.
//     */
//    @Test
//    public void testInputAll() {
//        System.out.println("inputAll");
//        float aDelta = 3.0F;
//        GameObject instance = new GameObject();
//        instance.inputAll(aDelta);
//    }

//    /**
//     * Test of updateAll method, of class GameObject.
//     */
//    @Test
//    public void testUpdateAll() {
//        System.out.println("updateAll");
//        float aDelta = 1.0F;
//        GameObject instance = new GameObject();
//        instance.updateAll(aDelta);
//    }

//    /**
//     * Test of refreshStateAll method, of class GameObject.
//     */
//    @Test
//    public void testUpdateAllStates() {
//        System.out.println("refreshStateAll");
//        GameObject instance = new GameObject();
//        instance.refreshStateAll();
//    }

//    /**
//     * Test of renderAll method, of class GameObject.
//     */
//    @Test
//    public void testRenderAll() {
//        System.out.println("renderAll");
//        Shader aShader = null;
//        RenderingEngine aRenderingEngine = null;
//        GameObject instance = new GameObject();
//        instance.renderAll(aShader, aRenderingEngine);
//    }

//    /**
//     * Test of input method, of class GameObject.
//     */
//    @Test
//    public void testInput() {
//        System.out.println("input");
//        float aDelta = 0.0F;
//        GameObject instance = new GameObject();
//        instance.input(aDelta);
//    }

//    /**
//     * Test of refreshState method, of class GameObject.
//     */
//    @Test
//    public void testUpdateState() {
//        System.out.println("refreshState");
//        GameObject instance = new GameObject();
//        instance.refreshState();
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
//
//    /**
//     * Test of update method, of class GameObject.
//     */
//    @Test
//    public void testUpdate() {
//        System.out.println("update");
//        float aDelta = 0.0F;
//        GameObject instance = new GameObject();
//        instance.update(aDelta);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
//
//    /**
//     * Test of render method, of class GameObject.
//     */
//    @Test
//    public void testRender() {
//        System.out.println("render");
//        Shader aShader = null;
//        RenderingEngine aRenderingEngine = null;
//        GameObject instance = new GameObject();
//        instance.render(aShader, aRenderingEngine);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }

//    /**
//     * Test of setEngine method, of class GameObject.
//     */
//    @Test
//    public void testSetEngine() {
//        System.out.println("setEngine");
//        CoreEngine aEngine = null;
//        GameObject instance = new GameObject();
//        instance.setEngine(aEngine);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
    
}
