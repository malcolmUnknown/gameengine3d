/* GameEngine 3D - A programm to design games with 3D content 
 * Copyright (C) 2016-2056 Frank Küwen
 *  
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package ProjectCenter.CoreEngine;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

public class Vector3fTest {


    public Vector3fTest() {
    }
    @BeforeClass
    public static void setUpClass() {
        System.out.println("--- START TEST: Vector3fTest ---");
    }

    @AfterClass
    public static void tearDownClass() {
        System.out.println("--- END TEST: Vector3fTest ---\n");
    }

    /**
     * Test of set method, of class Vector3f.
     */
    @Test
    public void testSet_3args() {
        System.out.println("set");
        float aX = 0.0F;
        float aY = 0.0F;
        float aZ = 0.0F;
        Vector3f instance = new Vector3f();
        instance.set(aX, aY, aZ);
        assertEquals(true, instance.equals(new Vector3f()));

        aX = 3.0F;
        aY = 1.0F;
        aZ = 4.0F;
        instance.set(aX, aY, aZ);
        assertEquals(true, instance.equals(new Vector3f(3, 1, 4)));
    }

    /**
     * Test of set method, of class Vector3f.
     */
    @Test
    public void testSet_Vector3f() {
        System.out.println("set");
        Vector3f aOriginal = new Vector3f(3, 1, 4);
        Vector3f instance = new Vector3f();
        instance.set(aOriginal);
        assertEquals(true, instance.equals(new Vector3f(3, 1, 4)));
    }

    /**
     * Test of clone method, of class Vector3f.
     */
    @Test
    public void testClone() {
        System.out.println("clone");
        Vector3f aOriginal = new Vector3f(3, 1, 4);
        Vector3f instance = new Vector3f();
        Vector3f expResult = new Vector3f(3, 1, 4);
        Vector3f result = instance.clone(aOriginal);
        assertEquals(true, instance.equals(expResult));
    }

    /**
     * Test of toString method, of class Vector3f.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        Vector3f instance = new Vector3f(2.14f, 9, 4.024587f);
        String expResult = "(+2.14|+9.00|+4.02)";
        String result = instance.toString();
        assertEquals(expResult, result);
    }

    /**
     * Test of length method, of class Vector3f.
     */
    @Test
    public void testLength() {
        System.out.println("length");
        Vector3f instance = new Vector3f(1, 0, 0);
        float expResult = 1.0F;
        float result = instance.length();
        assertEquals(true, Precision.equals(expResult, result));

        expResult = (float) Math.sqrt(2);
        instance = new Vector3f(0, 1, 1);
        result = instance.length();
        assertEquals(true, Precision.equals(expResult, result));
    }

    /**
     * Test of abs method, of class Vector3f.
     */
    @Test
    public void testAbs() {
        System.out.println("abs");
        Vector3f instance = new Vector3f(-2.14f, -9, 4.024587f);
        Vector3f expResult = new Vector3f(2.14f, 9, 4.024587f);
        Vector3f result = instance.abs();
        assertEquals(true, result.equals(expResult));
    }

    /**
     * Test of max method, of class Vector3f.
     */
    @Test
    public void testMax() {
        System.out.println("max");
        Vector3f instance = new Vector3f(-2.14f, -9, 4.024587f);
        float expResult = 4.024587f;
        float result = instance.max();
        assertEquals(true, Precision.equals(expResult, result));
    }

    /**
     * Test of min method, of class Vector3f.
     */
    @Test
    public void testMin() {
        System.out.println("min");
        Vector3f instance = new Vector3f(-2.14f, -9, 4.024587f);
        float expResult = -9.0F;
        float result = instance.min();
        assertEquals(true, Precision.equals(expResult, result));
    }

    /**
     * Test of dot method, of class Vector3f.
     */
    @Test
    public void testDot() {
        System.out.println("dot");
        Vector3f aVector = new Vector3f(0, 2, 0);
        Vector3f instance = new Vector3f(-2.14f, -9, 4.024587f);
        float expResult = -18.0F;
        float result = instance.dot(aVector);
        assertEquals(true, Precision.equals(expResult, result));

        aVector = new Vector3f(-9, 2.14f, 0);
        instance = new Vector3f(-2.14f, -9, 4.024587f);
        expResult = 0.0F;
        result = instance.dot(aVector);
        assertEquals(true, Precision.equals(expResult, result));
    }

    /**
     * Test of add method, of class Vector3f.
     */
    @Test
    public void testAdd_Vector3f() {
        System.out.println("add");
        Vector3f aVector = new Vector3f(1, 2, 0);
        Vector3f instance = new Vector3f(0, 1, 3);
        Vector3f expResult = new Vector3f(1, 3, 3);
        Vector3f result = instance.add(aVector);
        assertEquals(true, result.equals(expResult));
    }

    /**
     * Test of add method, of class Vector3f.
     */
    @Test
    public void testAdd_float() {
        System.out.println("add");
        float aScalar = 3.0F;
        Vector3f instance = new Vector3f(1, 2, 0);
        Vector3f expResult = new Vector3f(4, 5, 3);
        Vector3f result = instance.add(aScalar);
        assertEquals(true, result.equals(expResult));
    }

    /**
     * Test of sub method, of class Vector3f.
     */
    @Test
    public void testSub_Vector3f() {
        System.out.println("sub");
        Vector3f aVector = new Vector3f(3, 3, 3);
        Vector3f instance = new Vector3f(4, 5, 3);
        Vector3f expResult = new Vector3f(1, 2, 0);
        Vector3f result = instance.sub(aVector);
        assertEquals(true, result.equals(expResult));
    }

    /**
     * Test of sub method, of class Vector3f.
     */
    @Test
    public void testSub_float() {
        System.out.println("sub");
        float aScalar = 3.0F;
        Vector3f instance = new Vector3f(4, 5, 3);
        Vector3f expResult = new Vector3f(1, 2, 0);
        Vector3f result = instance.sub(aScalar);
        assertEquals(true, result.equals(expResult));
    }

    /**
     * Test of mul method, of class Vector3f.
     */
    @Test
    public void testMul_Vector3f() {
        System.out.println("mul");
        Vector3f aVector = new Vector3f(1, 2, 3);
        Vector3f instance = new Vector3f(3, 8, 4);
        Vector3f expResult = new Vector3f(3, 16, 12);
        Vector3f result = instance.mul(aVector);
        assertEquals(true, result.equals(expResult));
    }

    /**
     * Test of mul method, of class Vector3f.
     */
    @Test
    public void testMul_float() {
        System.out.println("mul");
        float aScalar = 2.0F;
        Vector3f instance = new Vector3f(3, 8, 4);
        Vector3f expResult = new Vector3f(6, 16, 8);
        Vector3f result = instance.mul(aScalar);
        assertEquals(true, result.equals(expResult));
    }

    /**
     * Test of div method, of class Vector3f.
     */
    @Test
    public void testDiv_Vector3f() {
        System.out.println("div");
        Vector3f aVector = new Vector3f(2, 4, 8);
        Vector3f instance = new Vector3f(3, 8, 4);
        Vector3f expResult = new Vector3f((float) 3 / 2.f, 2, 0.5f);
        Vector3f result = instance.div(aVector);
        assertEquals(true, result.equals(expResult));
    }

    /**
     * Test of div method, of class Vector3f.
     */
    @Test
    public void testDiv_float() {
        System.out.println("div");
        float aScalar = 2.0F;
        Vector3f instance = new Vector3f(3, 8, 4);
        Vector3f expResult = new Vector3f(1.5f, 4, 2);
        Vector3f result = instance.div(aScalar);
        assertEquals(true, result.equals(expResult));
    }

    /**
     * Test of linearExtrapolation method, of class Vector3f.
     */
    @Test
    public void testLinearExtrapolation() {
        System.out.println("linearExtrapolation");
        Vector3f aVector = new Vector3f();
        float aScalar = 1.5F;
        Vector3f instance = new Vector3f(2, 6, 9);
        Vector3f expResult = new Vector3f(-3, -9, -13.5f).add(instance);
        Vector3f result = instance.linearExtrapolation(aVector, aScalar);
        assertEquals(true, result.equals(expResult));

        aVector = new Vector3f(9, 9, 9);
        aScalar = 1.5F;
        expResult = new Vector3f(10.5f + 2, 4.5f + 6, 9);
        result = instance.linearExtrapolation(aVector, aScalar);
        assertEquals(true, result.equals(expResult));
    }

    /**
     * Test of rotateWithDegreeAroundAxis method, of class Vector3f.
     */
    @Test
    public void testRotateWithDegreeAroundAxis() {
        System.out.println("rotateWithDegreeAroundAxis");
        Vector3f aNormedAxis = new Vector3f(0, 0, 1);
        float aAngle = (float) Math.PI / 2.0F;
        Vector3f instance = new Vector3f(1, 1, 1);
        Vector3f expResult = new Vector3f(-1, 1, 1);
        Vector3f result = instance.rotateWithDegreeAroundAxis(aNormedAxis, aAngle);
        assertEquals(true, result.equals(expResult));

        aNormedAxis = new Vector3f(0, 0, 1);
        aAngle = (float) Math.PI;
        expResult = new Vector3f(-1, -1, 1);
        result = instance.rotateWithDegreeAroundAxis(aNormedAxis, aAngle);
        assertEquals(true, result.equals(expResult));

        aNormedAxis = new Vector3f(1, 1, 0).normalized();
        aAngle = (float) Math.PI;
        expResult = new Vector3f(1, 1, -1);
        result = instance.rotateWithDegreeAroundAxis(aNormedAxis, aAngle);
        assertEquals(true, result.equals(expResult));
    }
//

    /**
     * Test of rotate method, of class Vector3f.
     */
    @Test
    public void testRotate_Vector3f_float() {
        System.out.println("rotate");
        Vector3f aNormedAxis = new Vector3f(0, 0, 1);
        float aAngle = (float) Math.PI / 2.0F;
        Vector3f instance = new Vector3f(1, 1, 1);
        Vector3f expResult = new Vector3f(-1, 1, 1);
        Vector3f result = instance.rotate(aNormedAxis, aAngle);
        assertEquals(true, result.equals(expResult));

        aNormedAxis = new Vector3f(0, 0, 1);
        aAngle = (float) Math.PI;
        expResult = new Vector3f(-1, -1, 1);
        result = instance.rotate(aNormedAxis, aAngle);
        assertEquals(true, result.equals(expResult));

        aNormedAxis = new Vector3f(1, 1, 0).normalized();
        aAngle = (float) Math.PI;
        expResult = new Vector3f(1, 1, -1);
        result = instance.rotate(aNormedAxis, aAngle);
        assertEquals(true, result.equals(expResult));
    }

    /**
     * Test of rotate method, of class Vector3f.
     */
    @Test
    public void testRotate_Quaternion() {
        System.out.println("rotate");
        Vector3f aNormedAxis = new Vector3f(0, 0, 1);
        float aAngle = (float) Math.PI / 2.0F;
        Quaternion aRotation = new Quaternion(aNormedAxis, aAngle);
        Vector3f instance = new Vector3f(1, 1, 1);
        Vector3f expResult = new Vector3f(-1, 1, 1);
        Vector3f result = instance.rotate(aRotation);
        assertEquals(true, result.equals(expResult));

        aNormedAxis = new Vector3f(0, 0, 1);
        aAngle = (float) Math.PI;
        aRotation = new Quaternion(aNormedAxis, aAngle);
        expResult = new Vector3f(-1, -1, 1);
        result = instance.rotate(aRotation);
        assertEquals(true, result.equals(expResult));

        aNormedAxis = new Vector3f(1, 1, 0).normalized();
        aAngle = (float) Math.PI/ 2.0F;
        aRotation = new Quaternion(aNormedAxis, aAngle);
        expResult = new Vector3f(1, 1, 0).add(new Vector3f((float)Math.sqrt(0.5),-(float)Math.sqrt(0.5),0));
        result = instance.rotate(aRotation);
        assertEquals(true, result.equals(expResult));

        aNormedAxis = new Vector3f(1, 1, 0).normalized();
        aAngle = (float) Math.PI;
        aRotation = new Quaternion(aNormedAxis, aAngle);
        expResult = new Vector3f(1, 1, -1);
        result = instance.rotate(aRotation);
        assertEquals(true, result.equals(expResult));
    }

    /**
     * Test of normalized method, of class Vector3f.
     */
    @Test
    public void testNormalized() {
        System.out.println("normalized");
        Vector3f instance = new Vector3f(1, 1, 0);
        Vector3f expResult = new Vector3f(1, 1, 0).div((float) Math.sqrt(2));
        Vector3f result = instance.normalized();
        assertEquals(true, result.equals(expResult));
    }

    /**
     * Test of cross method, of class Vector3f.
     */
    @Test
    public void testCross() {
        System.out.println("cross");
        Vector3f aVector = new Vector3f(1, 1, 0);
        Vector3f instance = new Vector3f();
        Vector3f expResult = new Vector3f();
        Vector3f result = instance.cross(aVector);
        assertEquals(true, result.equals(expResult));

        aVector = new Vector3f(1, 1, 0);
        instance = new Vector3f(0, 0, 1);
        expResult = new Vector3f(-1, 1, 0);
        result = instance.cross(aVector);
        assertEquals(true, result.equals(expResult));

        aVector = new Vector3f(0, 4, 0);
        instance = new Vector3f(6, 0, 0);
        expResult = new Vector3f(0, 0, 24);
        result = instance.cross(aVector);
        assertEquals(true, result.equals(expResult));
    }

    /**
     * Test of getParameter method, of class Vector3f.
     */
    @Test
    public void testGetParameter() {
        System.out.println("getParameter");
        Vector3f aVector = new Vector3f();
        Vector3f instance = new Vector3f();
        float expResult = 0.0F;
        float result = instance.getParameter(aVector);
        assertEquals(true, Precision.equals(expResult, result));

        aVector = new Vector3f(2, 2, 7);
        instance = new Vector3f(1, 1, 0);
        expResult = 2.0F;
        result = instance.getParameter(aVector);
        assertEquals(true, Precision.equals(expResult, result));

        aVector = new Vector3f(2, -5, 7);
        instance = new Vector3f(1, 1, 0);
        expResult = -1.5F;
        result = instance.getParameter(aVector);
        assertEquals(true, Precision.equals(expResult, result));

        aVector = new Vector3f(2, -2, 7);
        instance = new Vector3f(1, 1, 0);
        expResult = 0.0F;
        result = instance.getParameter(aVector);
        assertEquals(true, Precision.equals(expResult, result));
    }

    /**
     * Test of getX method, of class Vector3f.
     */
    @Test
    public void testGetX() {
        System.out.println("getX");
        Vector3f instance = new Vector3f(5, 8, -4);
        float expResult = 5.0F;
        float result = instance.getX();
        assertEquals(true, Precision.equals(expResult, result));
    }

    /**
     * Test of getY method, of class Vector3f.
     */
    @Test
    public void testGetY() {
        System.out.println("getY");
        Vector3f instance = new Vector3f(5, 8, -4);
        float expResult = 8.0F;
        float result = instance.getY();
        assertEquals(true, Precision.equals(expResult, result));
    }

    /**
     * Test of getZ method, of class Vector3f.
     */
    @Test
    public void testGetZ() {
        System.out.println("getZ");
        Vector3f instance = new Vector3f(5, 8, -4);
        float expResult = -4.0F;
        float result = instance.getZ();
        assertEquals(true, Precision.equals(expResult, result));
    }

    /**
     * Test of setX method, of class Vector3f.
     */
    @Test
    public void testSetX() {
        System.out.println("setX");
        float aY = 5.0F;
        Vector3f instance = new Vector3f();
        instance.setX(aY);
        assertEquals(true, instance.equals(new Vector3f(5, 0, 0)));
    }

    /**
     * Test of setY method, of class Vector3f.
     */
    @Test
    public void testSetY() {
        System.out.println("setY");
        float aY = 78.0F;
        Vector3f instance = new Vector3f();
        instance.setY(aY);
        assertEquals(true, instance.equals(new Vector3f(0, 78, 0)));
    }

    /**
     * Test of setZ method, of class Vector3f.
     */
    @Test
    public void testSetZ() {
        System.out.println("setZ");
        float aZ = 334.0F;
        Vector3f instance = new Vector3f();
        instance.setZ(aZ);
        assertEquals(true, instance.equals(new Vector3f(0, 0, 334)));
    }

    /**
     * Test of getYZ method, of class Vector3f.
     */
    @Test
    public void testGetYZ() {
        System.out.println("getYZ");
        Vector3f instance = new Vector3f(5, 8, -4);
        Vector2f expResult = new Vector2f(8, -4);
        Vector2f result = instance.getYZ();
        assertEquals(true, result.equals(expResult));
    }

    /**
     * Test of getXY method, of class Vector3f.
     */
    @Test
    public void testGetXY() {
        System.out.println("getXY");
        Vector3f instance = new Vector3f(5, 8, -4);
        Vector2f expResult = new Vector2f(5, 8);
        Vector2f result = instance.getXY();
        assertEquals(true, result.equals(expResult));
    }

    /**
     * Test of getZX method, of class Vector3f.
     */
    @Test
    public void testGetZX() {
        System.out.println("getZX");
        Vector3f instance = new Vector3f(5, 8, -4);
        Vector2f expResult = new Vector2f(-4, 5);
        Vector2f result = instance.getZX();
        assertEquals(true, result.equals(expResult));
    }

    /**
     * Test of getZY method, of class Vector3f.
     */
    @Test
    public void testGetZY() {
        System.out.println("getZY");
        Vector3f instance = new Vector3f(5, 8, -4);
        Vector2f expResult = new Vector2f(-4, 8);
        Vector2f result = instance.getZY();
        assertEquals(true, result.equals(expResult));
    }

    /**
     * Test of getYX method, of class Vector3f.
     */
    @Test
    public void testGetYX() {
        System.out.println("getYX");
        Vector3f instance = new Vector3f(5, 8, -4);
        Vector2f expResult = new Vector2f(8, 5);
        Vector2f result = instance.getYX();
        assertEquals(true, result.equals(expResult));
    }

    /**
     * Test of getXZ method, of class Vector3f.
     */
    @Test
    public void testGetXZ() {
        System.out.println("getXZ");
        Vector3f instance = new Vector3f(5, 8, -4);
        Vector2f expResult = new Vector2f(5, -4);
        Vector2f result = instance.getXZ();
        assertEquals(true, result.equals(expResult));
    }

    /**
     * Test of equals method, of class Vector3f.
     */
    @Test
    public void testEquals() {
        System.out.println("equals");
        Vector3f aVector = new Vector3f(0, 9, 0);
        Vector3f instance = new Vector3f();
        boolean expResult = false;
        boolean result = instance.equals(aVector);
        assertEquals(expResult, result);
        
        aVector = new Vector3f(0, 0, 0);
        expResult = true;
        result = instance.equals(aVector);
        assertEquals(expResult, result);
        
        
        aVector = new Vector3f(0.002f, 0, 0);
        expResult = false;
        result = instance.equals(aVector);
        assertEquals(expResult, result);
        
        
        aVector = new Vector3f(0, 0,-0.00002f);
        expResult = false;
        result = instance.equals(aVector);
        assertEquals(expResult, result);
        
        aVector = new Vector3f(0,-0.0000002f, 0);
        expResult = true;
        result = instance.equals(aVector);
        assertEquals(expResult, result);
    }
}
