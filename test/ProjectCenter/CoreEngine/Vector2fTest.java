/* GameEngine 3D - A programm to design games with 3D content 
 * Copyright (C) 2016-2056 Frank Küwen
 *  
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package ProjectCenter.CoreEngine;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

public class Vector2fTest {

    public Vector2fTest() {
    }

    @BeforeClass
    public static void setUpClass() {
        System.out.println("--- START TEST: Vector2fTest ---");
    }

    @AfterClass
    public static void tearDownClass() {
        System.out.println("--- END TEST: Vector2fTest ---\n");
    }

    /**
     * Test of set method, of class Vector2f.
     */
    @Test
    public void testSet_float_float() {
        System.out.println("set");
        float aX = 7.0F;
        float aY = 3.0F;
        Vector2f instance = new Vector2f();
        instance.set(aX, aY);
        assertEquals(true, instance.equals(new Vector2f(7, 3)));

        aX = 0.0F;
        aY = 0.0F;;
        instance.set(aX, aY);
        assertEquals(true, instance.equals(new Vector2f()));
    }

    /**
     * Test of set method, of class Vector2f.
     */
    @Test
    public void testSet_Vector2f() {
        System.out.println("set");
        Vector2f aOriginal = new Vector2f(3, 2);
        Vector2f instance = new Vector2f();
        instance.set(aOriginal);
        assertEquals(true, instance.equals(new Vector2f(3, 2)));
    }

    /**
     * Test of clone method, of class Vector2f.
     */
    @Test
    public void testClone() {
        System.out.println("clone");
        Vector2f aOriginal = new Vector2f(1, 3);
        Vector2f instance = new Vector2f();
        Vector2f expResult = new Vector2f();
        assertEquals(true, instance.equals(expResult));
        Vector2f result = instance.clone(aOriginal);

        expResult = new Vector2f(1, 3);
        assertEquals(true, result.equals(expResult));
    }

    /**
     * Test of toString method, of class Vector2f.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        Vector2f instance = new Vector2f(2.14f, 4.024587f);
        String expResult = "(+2.14|+4.02)";
        String result = instance.toString();
        assertEquals(expResult, result);
    }

    /**
     * Test of length method, of class Vector2f.
     */
    @Test
    public void testLength() {
        System.out.println("length");
        Vector2f instance = new Vector2f(1, 0);
        float expResult = 1.0F;
        float result = instance.length();
        assertEquals(true, Precision.equals(expResult, result));

        expResult = (float) Math.sqrt(2);
        instance = new Vector2f(1, 1);
        result = instance.length();
        assertEquals(true, Precision.equals(expResult, result));
    }

    /**
     * Test of abs method, of class Vector2f.
     */
    @Test
    public void testAbs() {
        System.out.println("abs");
        Vector2f instance = new Vector2f(-2.14f, 4.024587f);
        Vector2f expResult = new Vector2f(2.14f, 4.024587f);
        Vector2f result = instance.abs();
        assertEquals(true, result.equals(expResult));
    }

    /**
     * Test of max method, of class Vector2f.
     */
    @Test
    public void testMax() {
        System.out.println("max");
        Vector2f instance = new Vector2f(-2.14f, 4.024587f);
        float expResult = 4.024587f;
        float result = instance.max();
        assertEquals(true, Precision.equals(expResult, result));
    }

    /**
     * Test of min method, of class Vector2f.
     */
    @Test
    public void testMin() {
        System.out.println("max");
        Vector2f instance = new Vector2f(-2.14f, 4.024587f);
        float expResult = -2.14f;
        float result = instance.min();
        assertEquals(true, Precision.equals(expResult, result));
    }

    /**
     * Test of dot method, of class Vector2f.
     */
    @Test
    public void testDot() {
        System.out.println("dot");
        Vector2f aVector = new Vector2f(2, 0);
        Vector2f instance = new Vector2f(-2.14f, -9);
        float expResult = -4.28F;
        float result = instance.dot(aVector);
        assertEquals(true, Precision.equals(expResult, result));

        aVector = new Vector2f(-9, 2.14f);
        instance = new Vector2f(-2.14f, -9);
        expResult = 0.0F;
        result = instance.dot(aVector);
        assertEquals(true, Precision.equals(expResult, result));
    }

    /**
     * Test of add method, of class Vector2f.
     */
    @Test
    public void testAdd_Vector2f() {
        System.out.println("add");
        Vector2f aVector = new Vector2f(1, 2);
        Vector2f instance = new Vector2f(0, 1);
        Vector2f expResult = new Vector2f(1, 3);
        Vector2f result = instance.add(aVector);
        assertEquals(true, result.equals(expResult));
    }

    /**
     * Test of add method, of class Vector2f.
     */
    @Test
    public void testAdd_float() {
        System.out.println("add");
        float aScalar = 3.0F;
        Vector2f instance = new Vector2f(1, 2);
        Vector2f expResult = new Vector2f(4, 5);
        Vector2f result = instance.add(aScalar);
        assertEquals(true, result.equals(expResult));
    }

    /**
     * Test of sub method, of class Vector2f.
     */
    @Test
    public void testSub_Vector2f() {
        System.out.println("sub");
        Vector2f aVector = new Vector2f(3, 3);
        Vector2f instance = new Vector2f(4, 5);
        Vector2f expResult = new Vector2f(1, 2);
        Vector2f result = instance.sub(aVector);
        assertEquals(true, result.equals(expResult));
    }

    /**
     * Test of sub method, of class Vector2f.
     */
    @Test
    public void testSub_float() {
        System.out.println("sub");
        float aScalar = 3.0F;
        Vector2f instance = new Vector2f(4, 5);
        Vector2f expResult = new Vector2f(1, 2);
        Vector2f result = instance.sub(aScalar);
        assertEquals(true, result.equals(expResult));
    }

    /**
     * Test of mul method, of class Vector2f.
     */
    @Test
    public void testMul_Vector2f() {
        System.out.println("mul");
        Vector2f aVector = new Vector2f(2, 3);;
        Vector2f instance = new Vector2f(1, 3);
        Vector2f expResult = new Vector2f(2, 9);
        Vector2f result = instance.mul(aVector);
        assertEquals(true, result.equals(expResult));
    }

    /**
     * Test of mul method, of class Vector2f.
     */
    @Test
    public void testMul_float() {
        System.out.println("mul");
        float aScalar = 2.0F;
        Vector2f instance = new Vector2f(3, 8);
        Vector2f expResult = new Vector2f(6, 16);
        Vector2f result = instance.mul(aScalar);
        assertEquals(true, result.equals(expResult));
    }

    /**
     * Test of div method, of class Vector2f.
     */
    @Test
    public void testDiv_Vector2f() {
        System.out.println("div");
        Vector2f aVector = new Vector2f(2, 8);
        Vector2f instance = new Vector2f(3, 4);
        Vector2f expResult = new Vector2f((float) 1.5f, 0.5f);
        Vector2f result = instance.div(aVector);
        assertEquals(true, result.equals(expResult));
    }

    /**
     * Test of div method, of class Vector2f.
     */
    @Test
    public void testDiv_float() {
        System.out.println("div");
        float aScalar = 2.0F;
        Vector2f instance = new Vector2f( 8, 4);
        Vector2f expResult = new Vector2f( 4, 2);
        Vector2f result = instance.div(aScalar);
        assertEquals(true, result.equals(expResult));
    }

    /**
     * Test of linearExtrapolation method, of class Vector2f.
     */
    @Test
    public void testLinearExtrapolation() {
        System.out.println("linearExtrapolation");
        Vector2f aVector = new Vector2f();
        float aScalar = 1.5F;
        Vector2f instance = new Vector2f(2, 6);
        Vector2f expResult = new Vector2f(-3, -9).add(instance);
        Vector2f result = instance.linearExtrapolation(aVector, aScalar);
        assertEquals(true, result.equals(expResult));

        aVector = new Vector2f(9, 9);
        aScalar = 1.5F;
        expResult = new Vector2f(10.5f + 2, 4.5f + 6);
        result = instance.linearExtrapolation(aVector, aScalar);
        assertEquals(true, result.equals(expResult));
    }

    /**
     * Test of rotateWithDegree method, of class Vector2f.
     */
    @Test
    public void testRotateWithDegree() {
        System.out.println("rotateWithDegree");
        float aAngle = (float) Math.PI / 2.0F;
        Vector2f instance = new Vector2f(1, 1);
        Vector2f expResult = new Vector2f(-1, 1);
        Vector2f result = instance.rotateWithDegree((float) (aAngle*180.f/Math.PI));
        assertEquals(true, result.equals(expResult));
        
        aAngle = (float) Math.PI;
        instance = new Vector2f(1, 1);
        expResult = new Vector2f(-1, -1);
        result = instance.rotateWithDegree( (float) (aAngle*180.f/Math.PI));
        assertEquals(true, result.equals(expResult));
    }

    /**
     * Test of normalized method, of class Vector2f.
     */
    @Test
    public void testNormalized() {
        System.out.println("normalized");
        Vector2f instance = new Vector2f(1, 1);
        Vector2f expResult = new Vector2f(1, 1).div((float) Math.sqrt(2));
        Vector2f result = instance.normalized();
        assertEquals(true, result.equals(expResult));
    }

    /**
     * Test of determinate method, of class Vector2f.
     */
    @Test
    public void testDeterminate() {
        System.out.println("determinate");
        Vector2f aVector = new Vector2f(4, 3);
        Vector2f instance = new Vector2f(2,0);
        float expResult = 6.0F;
        float result = instance.determinate(aVector);
        assertEquals(true, Precision.equals(expResult, result));
    }

    /**
     * Test of getX method, of class Vector2f.
     */
    @Test
    public void testGetX() {
        System.out.println("getX");
        Vector2f instance = new Vector2f(5, 8);
        float expResult = 5.0F;
        float result = instance.getX();
        assertEquals(true, Precision.equals(expResult, result));
    }

    /**
     * Test of getY method, of class Vector2f.
     */
    @Test
    public void testGetY() {
        System.out.println("getY");
        Vector2f instance = new Vector2f(5, 8);
        float expResult = 8.0F;
        float result = instance.getY();
        assertEquals(true, Precision.equals(expResult, result));
    }

    /**
     * Test of setX method, of class Vector2f.
     */
    @Test
    public void testSetX() {
        System.out.println("setX");
        float aY = 5.0F;
        Vector2f instance = new Vector2f();
        instance.setX(aY);
        assertEquals(true, instance.equals(new Vector2f(5,  0)));
    }

    /**
     * Test of setY method, of class Vector2f.
     */
    @Test
    public void testSetY() {
        System.out.println("setY");
        float aY = 78.0F;
        Vector2f instance = new Vector2f();
        instance.setY(aY);
        assertEquals(true, instance.equals(new Vector2f(0, 78)));
    }

    /**
     * Test of equals method, of class Vector2f.
     */
    @Test
    public void testEquals() {
        System.out.println("equals");
        Vector2f aVector = new Vector2f(0, 9);;
        Vector2f instance = new Vector2f();
        boolean expResult = false;
        boolean result = instance.equals(aVector);
        assertEquals(expResult, result);
        
        aVector = new Vector2f(0, 0);
        expResult = true;
        result = instance.equals(aVector);
        assertEquals(expResult, result);
        
        
        aVector = new Vector2f(0.002f, 0);
        expResult = false;
        result = instance.equals(aVector);
        assertEquals(expResult, result);
        
        
        aVector = new Vector2f(0, -0.00002f);
        expResult = false;
        result = instance.equals(aVector);
        assertEquals(expResult, result);
        
        aVector = new Vector2f(0,-0.0000002f);
        expResult = true;
        result = instance.equals(aVector);
        assertEquals(expResult, result);
    }

}
