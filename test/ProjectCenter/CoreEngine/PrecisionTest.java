/* GameEngine 3D - A programm to design games with 3D content 
 * Copyright (C) 2016-2056 Frank Küwen
 *  
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package ProjectCenter.CoreEngine;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

public class PrecisionTest {

    public PrecisionTest() {
    }

    @BeforeClass
    public static void setUpClass() {
        System.out.println("--- START TEST: PrecisionTest ---");
    }

    @AfterClass
    public static void tearDownClass() {
        System.out.println("--- END TEST: PrecisionTest ---");
    }

    /**
     * Test of getEpsilon method, of class Precision.
     */
    @Test
    public void testGetEpsilon() {
        System.out.println("getEpsilon");
        float expResult = 1e-6f;
        float result = Precision.getEpsilon();
        assertEquals(expResult, result, 1e-6);
    }

    /**
     * Test of setEpsilon method, of class Precision.
     */
    @Test
    public void testSetEpsilon() {
        System.out.println("setEpsilon");
        float aEpsilon = 1e-4F;
        Precision.setEpsilon(aEpsilon);
        assertEquals(aEpsilon, Precision.getEpsilon(), 1e-7);
    }

    /**
     * Test of setEpsilon method, of class Precision.
     */
    @Test
    public void testResetEpsilon() {
        System.out.println("resetEpsilon");
        float aEpsilon = 1e-4F;
        Precision.setEpsilon(aEpsilon);
        assertEquals(aEpsilon, Precision.getEpsilon(), 1e-7);
        Precision.resetEpsilon();
        assertEquals(1e-6F, Precision.getEpsilon(), 1e-7);
    }

    /**
     * Test of equals method, of class Precision.
     */
    @Test
    public void testEquals_3args() {
        System.out.println("equals");
        float aFirst = 2.0F + 1e-8f;
        float aSecond = 2.0F;
        float aEpsilon = 1e-7F;
        boolean expResult = true;
        boolean result = Precision.equals(aFirst, aSecond, aEpsilon);
        assertEquals(expResult, result);

        aSecond = 2.0F + 1e-6f;
        expResult = false;
        result = Precision.equals(aFirst, aSecond, aEpsilon);
        assertEquals(expResult, result);

        aSecond = 2.0F - 1e-6f;
        expResult = false;
        result = Precision.equals(aFirst, aSecond, aEpsilon);
        assertEquals(expResult, result);
    }

    /**
     * Test of greater method, of class Precision.
     */
    @Test
    public void testGreater_3args() {
        System.out.println("greater");
        float aFirst = 2.0F;
        float aSecond = 2.0F;
        float aEpsilon = 1e-7F;
        boolean expResult = false;
        boolean result = Precision.greater(aFirst, aSecond, aEpsilon);
        assertEquals(expResult, result);

        aSecond = 2.0F + 1e-6f;
        expResult = false;
        result = Precision.greater(aFirst, aSecond, aEpsilon);
        assertEquals(expResult, result);

        aSecond = 2.0F - 1e-6f;
        expResult = true;
        result = Precision.greater(aFirst, aSecond, aEpsilon);
        assertEquals(expResult, result);
    }

    /**
     * Test of less method, of class Precision.
     */
    @Test
    public void testLess_3args() {
        System.out.println("less");
        float aFirst = 2.0F;
        float aSecond = 2.0F;
        float aEpsilon = 1e-7F;
        boolean expResult = false;
        boolean result = Precision.less(aFirst, aSecond, aEpsilon);
        assertEquals(expResult, result);

        aSecond = 2.0F - 1e-6f;
        expResult = false;
        result = Precision.less(aFirst, aSecond, aEpsilon);
        assertEquals(expResult, result);

        aSecond = 2.0F + 1e-6f;
        expResult = true;
        result = Precision.less(aFirst, aSecond, aEpsilon);
        assertEquals(expResult, result);
    }

    /**
     * Test of greaterEqual method, of class Precision.
     */
    @Test
    public void testGreaterEqual_3args() {
        System.out.println("greaterEqual");
        float aFirst = 2.0F;
        float aSecond = 2.0F;
        float aEpsilon = 1e-7F;
        boolean expResult = true;
        boolean result = Precision.greaterEqual(aFirst, aSecond, aEpsilon);
        assertEquals(expResult, result);

        aSecond = 2.0F + 1e-6f;
        expResult = false;
        result = Precision.greaterEqual(aFirst, aSecond, aEpsilon);
        assertEquals(expResult, result);

        aSecond = 2.0F - 1e-6f;
        expResult = true;
        result = Precision.greater(aFirst, aSecond, aEpsilon);
        assertEquals(expResult, result);
    }

    /**
     * Test of lessEqual method, of class Precision.
     */
    @Test
    public void testLessEqual_3args() {
        System.out.println("lessEqual");        
        float aFirst = 2.0F;
        float aSecond = 2.0F;
        float aEpsilon = 1e-7F;
        boolean expResult = true;
        boolean result = Precision.lessEqual(aFirst, aSecond, aEpsilon);
        assertEquals(expResult, result);

        aSecond = 2.0F - 1e-6f;
        expResult = false;
        result = Precision.lessEqual(aFirst, aSecond, aEpsilon);
        assertEquals(expResult, result);

        aSecond = 2.0F + 1e-6f;
        expResult = true;
        result = Precision.lessEqual(aFirst, aSecond, aEpsilon);
        assertEquals(expResult, result);
    }

    /**
     * Test of equals method, of class Precision.
     */
    @Test
    public void testEquals_float_float() {
        System.out.println("equals");
        float aFirst = 2.0F + 1e-8f;
        float aSecond = 2.0F;
        boolean expResult = true;
        Precision.resetEpsilon();
        boolean result = Precision.equals(aFirst, aSecond);
        assertEquals(expResult, result);

        aSecond = 2.0F + 2e-6f;
        expResult = false;
        result = Precision.equals(aFirst, aSecond);
        assertEquals(expResult, result);

        aSecond = 2.0F - 2e-6f;
        expResult = false;
        result = Precision.equals(aFirst, aSecond);
        assertEquals(expResult, result);
    }

    /**
     * Test of less method, of class Precision.
     */
    @Test
    public void testLess_float_float() {
        System.out.println("less");
        float aFirst = 2.0F + 1e-8f;
        float aSecond = 2.0F;
        boolean expResult = false;
        Precision.resetEpsilon();
        boolean result = Precision.less(aFirst, aSecond);
        assertEquals(expResult, result);

        aSecond = 2.0F + 2e-6f;
        expResult = true;
        result = Precision.less(aFirst, aSecond);
        assertEquals(expResult, result);

        aSecond = 2.0F - 2e-6f;
        expResult = false;
        result = Precision.less(aFirst, aSecond);
        assertEquals(expResult, result);
    }

    /**
     * Test of greater method, of class Precision.
     */
    @Test
    public void testGreater_float_float() {
        System.out.println("greater");
        float aFirst = 2.0F + 1e-8f;
        float aSecond = 2.0F;
        boolean expResult = false;
        Precision.resetEpsilon();
        boolean result = Precision.greater(aFirst, aSecond);
        assertEquals(expResult, result);

        aSecond = 2.0F + 2e-6f;
        expResult = false;
        result = Precision.greater(aFirst, aSecond);
        assertEquals(expResult, result);

        aSecond = 2.0F - 2e-6f;
        expResult = true;
        result = Precision.greater(aFirst, aSecond);
        assertEquals(expResult, result);
    }

    /**
     * Test of greaterEqual method, of class Precision.
     */
    @Test
    public void testGreaterEqual_float_float() {
        System.out.println("greaterEqual");
        float aFirst = 2.0F + 1e-8f;
        float aSecond = 2.0F;
        boolean expResult = true;
        Precision.resetEpsilon();
        boolean result = Precision.greaterEqual(aFirst, aSecond);
        assertEquals(expResult, result);

        aSecond = 2.0F + 2e-6f;
        expResult = false;
        result = Precision.greaterEqual(aFirst, aSecond);
        assertEquals(expResult, result);

        aSecond = 2.0F - 2e-6f;
        expResult = true;
        result = Precision.greaterEqual(aFirst, aSecond);
        assertEquals(expResult, result);
    }

    /**
     * Test of lessEqual method, of class Precision.
     */
    @Test
    public void testLessEqual_float_float() {
        System.out.println("lessEqual");
        float aFirst = 2.0F + 1e-8f;
        float aSecond = 2.0F;
        boolean expResult = true;
        Precision.resetEpsilon();
        boolean result = Precision.lessEqual(aFirst, aSecond);
        assertEquals(expResult, result);

        aSecond = 2.0F + 2e-6f;
        expResult = true;
        result = Precision.lessEqual(aFirst, aSecond);
        assertEquals(expResult, result);

        aSecond = 2.0F - 2e-6f;
        expResult = false;
        result = Precision.lessEqual(aFirst, aSecond);
        assertEquals(expResult, result);
    }
}
