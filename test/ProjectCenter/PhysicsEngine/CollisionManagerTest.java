/*
 * Copyright (C) 2016 Malcolm
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ProjectCenter.PhysicsEngine;

import ProjectCenter.CoreEngine.Precision;
import ProjectCenter.CoreEngine.Quaternion;
import ProjectCenter.CoreEngine.Transform;
import ProjectCenter.CoreEngine.Vector3f;
import java.util.AbstractMap;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.BeforeClass;

/**
 *
 * @author Malcolm
 */
public class CollisionManagerTest {
    
    public CollisionManagerTest() {
    }

    @BeforeClass
    public static void setupClass() {
        System.out.println("--- START TEST: CollisionManagerTest ---");
    }

    @AfterClass
    public static void tearDownClass() {
        System.out.println("--- END TEST: CollisionManagerTest ---\n");
    }

    @Test
    public void testGetColliderPair() {
        System.out.println("getColliderPair");
        PhysicsObject aOne = new PhysicsObject(ColliderTypes.ColliderBox, new Vector3f(), new Vector3f(), 0.0f, new Vector3f(), new Vector3f());;
        Transform lTransform = new Transform();
        lTransform.setPosition(new Vector3f(1, 2, 3));
        aOne.getState().set(lTransform);
        
        PhysicsObject aTwo = new PhysicsObject(ColliderTypes.ColliderSphere, new Vector3f(), new Vector3f(), 0.0f, new Vector3f(), new Vector3f());;
        
        CollisionManager instance = new CollisionManager();
        
        AbstractMap.SimpleEntry<Collider, Collider> result = instance.getColliderPair(aOne, aTwo);
        assertEquals(ColliderTypes.ColliderBox, result.getKey().getType());
        assertEquals(ColliderTypes.ColliderSphere, result.getValue().getType());
        assertEquals(true, (new Vector3f(1, 2, 3)).equals(result.getKey().getPosition()));
        assertEquals(true, (new Vector3f(0, 0, 0)).equals(result.getValue().getPosition()));
    }

    @Test
    public void testDetectCollistionsBoxBox() {
        System.out.println("detectCollistions - Box-Box");
        
        PhysicsObject aPhysics_1 = new PhysicsObject(ColliderTypes.ColliderBox, new Vector3f(), new Vector3f(), 0.0f, new Vector3f(), new Vector3f());
        Transform lTransform = new Transform();
        lTransform.setScale(new Vector3f(1, 2, 3));
        aPhysics_1.getState().set(lTransform);
        
        PhysicsObject aPhysics_2 = new PhysicsObject(ColliderTypes.ColliderBox, new Vector3f(), new Vector3f(), 0.0f, new Vector3f(), new Vector3f());
        Transform lOtherTransform = new Transform();
        lOtherTransform.setPosition(new Vector3f(4, 0, 0));
        lOtherTransform.setScale(new Vector3f(3, 2, 1));
        aPhysics_2.getState().set(lOtherTransform);
        
        CollisionManager instance = new CollisionManager();
        
        IntersectData result = instance.detectCollistions(aPhysics_1, aPhysics_2);
        IntersectData expResult = new IntersectData(false, new Vector3f(1, 0, 0));
        assertEquals(expResult.isIntersecting(), result.isIntersecting());
        assertEquals(true, result.getDirection().equals(expResult.getDirection()));
        assertEquals(true, Precision.equals(expResult.getDistance(), result.getDistance()));
        
        
        lTransform = new Transform();
        lTransform.setScale(new Vector3f(1, 1, 1));
        aPhysics_1.getState().set(lTransform);
        lOtherTransform = new Transform();
        lOtherTransform.setPosition(new Vector3f(0, 2.5f, 0));
        lOtherTransform.setScale(new Vector3f(2, 2, 2));
        aPhysics_2.getState().set(lOtherTransform);
        
        result = instance.detectCollistions(aPhysics_1, aPhysics_2);
        expResult = new IntersectData(true, new Vector3f(0, 0.75f, 0));
        assertEquals(expResult.isIntersecting(), result.isIntersecting());
        assertEquals(true, result.getDirection().equals(expResult.getDirection()));
        assertEquals(true, Precision.equals(expResult.getDistance(), result.getDistance()));
        
        lTransform = new Transform();
        lTransform.setScale(new Vector3f(1, 2, 3));
        aPhysics_1.getState().set(lTransform);
        
        lOtherTransform = new Transform();
        lOtherTransform.setPosition(new Vector3f(-1, 1, 0));
        lOtherTransform.setScale(new Vector3f(1, 2, 3));
        aPhysics_2.getState().set(lOtherTransform);
        
        result = instance.detectCollistions(aPhysics_1, aPhysics_2);
        expResult = new IntersectData(true, new Vector3f(-0.5f, 0.5f, 0));
        assertEquals(expResult.isIntersecting(), result.isIntersecting());
        assertEquals(true, result.getDirection().equals(expResult.getDirection()));
        assertEquals(true, Precision.equals(expResult.getDistance(), result.getDistance()));
                
        lTransform = new Transform();
        lTransform.setScale(new Vector3f(1, 1, 1));
        aPhysics_1.getState().set(lTransform);
        
        lOtherTransform = new Transform();
        lOtherTransform.setPosition(new Vector3f(1.5f, 1.5f, 0));
        lOtherTransform.setScale(new Vector3f(1, 1, 1));
        aPhysics_2.getState().set(lOtherTransform);
        
        result = instance.detectCollistions(aPhysics_1, aPhysics_2);
        expResult = new IntersectData(true, new Vector3f(0.75f, 0.75f, 0));
        assertEquals(expResult.isIntersecting(), result.isIntersecting());
        assertEquals(true, result.getDirection().equals(expResult.getDirection()));
        assertEquals(true, Precision.equals(expResult.getDistance(), result.getDistance()));
        
        lOtherTransform = new Transform();
        lOtherTransform.setRotation(new Quaternion(new Vector3f(0, 0, 1), (float) Math.PI/2));
        lOtherTransform.setPosition(new Vector3f(0, 1f, 0));
        lOtherTransform.setScale(new Vector3f(1, 1, 1));
        aPhysics_2.getState().set(lOtherTransform);
        
        result = instance.detectCollistions(aPhysics_1, aPhysics_2);
        expResult = new IntersectData(true, new Vector3f(0, 0.5f, 0));
        assertEquals(expResult.isIntersecting(), result.isIntersecting());
        assertEquals(true, result.getDirection().equals(expResult.getDirection()));
        assertEquals(true, Precision.equals(expResult.getDistance(), result.getDistance()));
        
        lOtherTransform = new Transform();
        lOtherTransform.setRotation(new Quaternion(new Vector3f(1, 1, 0).normalized(), (float) Math.PI/2));
        lOtherTransform.setPosition(new Vector3f(1.5f, 1.5f, 0));
        lOtherTransform.setScale(new Vector3f(1, 1, 1));
        aPhysics_2.getState().set(lOtherTransform);
        
        result = instance.detectCollistions(aPhysics_1, aPhysics_2);
        expResult = new IntersectData(true, new Vector3f(0.75f, 0.75f, 0));
        assertEquals(expResult.isIntersecting(), result.isIntersecting());
        assertEquals(true, result.getDirection().equals(expResult.getDirection()));
        assertEquals(true, Precision.equals(expResult.getDistance(), result.getDistance()));
        
        lTransform = new Transform();
        lTransform.setPosition(new Vector3f(3f, 3f, 0));
        lTransform.setScale(new Vector3f(1, 1, 1));
        aPhysics_1.getState().set(lTransform);        
        lOtherTransform = new Transform();
        lOtherTransform.setPosition(new Vector3f(1.5f, 1.5f, 0));
        lOtherTransform.setScale(new Vector3f(1, 1, 1));
        aPhysics_2.getState().set(lOtherTransform);
        
        result = instance.detectCollistions(aPhysics_1, aPhysics_2);
        expResult = new IntersectData(true, new Vector3f(2.25f, 2.25f, 0));
        assertEquals(expResult.isIntersecting(), result.isIntersecting());
        assertEquals(true, result.getDirection().equals(expResult.getDirection()));
        assertEquals(true, Precision.equals(expResult.getDistance(), result.getDistance()));
        
               
        aPhysics_1 = new PhysicsObject(ColliderTypes.ColliderBox, new Vector3f(), new Vector3f(), 0.0f, new Vector3f(), new Vector3f());
        lTransform = new Transform();
        lTransform.setScale(new Vector3f(1, 2, 3));
        aPhysics_1.getState().set(lTransform);
        
        aPhysics_2 = new PhysicsObject(ColliderTypes.ColliderBox, new Vector3f(), new Vector3f(), 0.0f, new Vector3f(), new Vector3f());
        lOtherTransform = new Transform();
        lOtherTransform.setPosition(new Vector3f(3, 0, 3));
        lOtherTransform.setScale(new Vector3f(3, 2, 1));
        aPhysics_2.getState().set(lOtherTransform);
        
        
        result = instance.detectCollistions(aPhysics_1, aPhysics_2);
        expResult = new IntersectData(true, new Vector3f(0.5f, 0, 2.5f));
        assertEquals(expResult.isIntersecting(), result.isIntersecting());
        assertEquals(true, result.getDirection().equals(expResult.getDirection()));
        assertEquals(true, Precision.equals(expResult.getDistance(), result.getDistance()));
        
        lOtherTransform = new Transform();
        lOtherTransform.setPosition(new Vector3f(3, 0, 0));
        lOtherTransform.setScale(new Vector3f(3, 2, 1));
        aPhysics_2.getState().set(lOtherTransform);        
        
        result = instance.detectCollistions(aPhysics_1, aPhysics_2);
        expResult = new IntersectData(true, new Vector3f(.5f, 0, 0));
        assertEquals(expResult.isIntersecting(), result.isIntersecting());
        assertEquals(true, result.getDirection().equals(expResult.getDirection()));
        assertEquals(true, Precision.equals(expResult.getDistance(), result.getDistance()));
                        
        lOtherTransform = new Transform();
        lOtherTransform.setPosition(new Vector3f(3, 0, 2));
        lOtherTransform.setScale(new Vector3f(3, 2, 1));
        aPhysics_2.getState().set(lOtherTransform);        
        
        result = instance.detectCollistions(aPhysics_1, aPhysics_2);
        expResult = new IntersectData(true, new Vector3f(0.5f, 0, 1.0f));
        assertEquals(expResult.isIntersecting(), result.isIntersecting());
        assertEquals(true, result.getDirection().equals(expResult.getDirection()));
        assertEquals(true, Precision.equals(expResult.getDistance(), result.getDistance()));
                       
        lTransform = new Transform();
        lTransform.setScale(new Vector3f(2, 2, 2));
        aPhysics_1.getState().set(lTransform);
        
        lOtherTransform = new Transform();
        lOtherTransform.setRotation(new Quaternion(new Vector3f(0, 0, 1), (float) Math.PI/4));
        lOtherTransform.setPosition(new Vector3f(3, 0, 0));
        lOtherTransform.setScale(new Vector3f(1, 1, 1));
        aPhysics_2.getState().set(lOtherTransform);       
        
        result = instance.detectCollistions(aPhysics_1, aPhysics_2);
        expResult = new IntersectData(true, new Vector3f( (2+(3-(float)Math.sqrt(2)))/2.0f ,0, 0));
        assertEquals(expResult.isIntersecting(), result.isIntersecting());
        assertEquals(true, result.getDirection().equals(expResult.getDirection()));
        assertEquals(true, Precision.equals(expResult.getDistance(), result.getDistance()));      
    }
    
    @Test
    public void testDetectCollistionsBoxPlane() {
        System.out.println("detectCollistions - Box-Plane");
        
        PhysicsObject aPhysics_1 = new PhysicsObject(ColliderTypes.ColliderBox, new Vector3f(), new Vector3f(), 0.0f, new Vector3f(), new Vector3f());
        Transform lTransform = new Transform();
        lTransform.setScale(new Vector3f(1, 2, 3));
        aPhysics_1.getState().set(lTransform);
        
        PhysicsObject aPhysics_2 = new PhysicsObject(ColliderTypes.ColliderPlane, new Vector3f(-1, 0, 0), new Vector3f(), 0.0f, new Vector3f(), new Vector3f());
        Transform lOtherTransform = new Transform();
        lOtherTransform.setPosition(new Vector3f(5, 0, 0));
        aPhysics_2.getState().set(lOtherTransform);
        
        CollisionManager instance = new CollisionManager();
        
        IntersectData result = instance.detectCollistions(aPhysics_1, aPhysics_2);
        IntersectData expResult = new IntersectData(false, new Vector3f(4, 0, 0));
        assertEquals(expResult.isIntersecting(), result.isIntersecting());
        assertEquals(true, result.getDirection().equals(expResult.getDirection()));
        assertEquals(true, Precision.equals(expResult.getDistance(), result.getDistance()));
        
        aPhysics_2 = new PhysicsObject(ColliderTypes.ColliderPlane, new Vector3f(0, 1, 0), new Vector3f(), 0.0f, new Vector3f(), new Vector3f());
        lOtherTransform = new Transform();
        lOtherTransform.setPosition(new Vector3f(5, 1, 0));
        aPhysics_2.getState().set(lOtherTransform);
        
        result = instance.detectCollistions(aPhysics_1, aPhysics_2);
        expResult = new IntersectData(true, new Vector3f(0, 1, 0));
        assertEquals(expResult.isIntersecting(), result.isIntersecting());
        assertEquals(true, result.getDirection().equals(expResult.getDirection()));
        assertEquals(true, Precision.equals(expResult.getDistance(), result.getDistance()));
                
        
        lTransform.setScale(new Vector3f(1, 1, 1));
        aPhysics_1.getState().set(lTransform);
        aPhysics_2 = new PhysicsObject(ColliderTypes.ColliderPlane, new Vector3f(0, 1, 0), new Vector3f(), 0.0f, new Vector3f(), new Vector3f());
        lOtherTransform = new Transform();
        lOtherTransform.setPosition(new Vector3f(1.5f, 0, 0));
        lOtherTransform.setRotation(new Quaternion(new Vector3f(0, 0, 1), (float) Math.PI/4));
        aPhysics_2.getState().set(lOtherTransform);
        
        result = instance.detectCollistions(aPhysics_1, aPhysics_2);
        expResult = new IntersectData(true, new Vector3f(0.75f, -0.75f, 0));
        assertEquals(expResult.isIntersecting(), result.isIntersecting());
        assertEquals(true, result.getDirection().equals(expResult.getDirection()));
        assertEquals(true, Precision.equals(expResult.getDistance(), result.getDistance()));
              
        
        lTransform.setScale(new Vector3f(2, 2, 2));
        lTransform.setPosition(new Vector3f(5, 0, 0));
        aPhysics_1.getState().set(lTransform);        
        lOtherTransform = new Transform();
        lOtherTransform.setPosition(new Vector3f(5, 1, 0));
        aPhysics_2.getState().set(lOtherTransform);
        
        result = instance.detectCollistions(aPhysics_1, aPhysics_2);
        expResult = new IntersectData(true, new Vector3f(5, 1, 0));
        assertEquals(expResult.isIntersecting(), result.isIntersecting());
        assertEquals(true, result.getDirection().equals(expResult.getDirection()));
        assertEquals(true, Precision.equals(expResult.getDistance(), result.getDistance()));
    }

    @Test
    public void testDetectCollistionsBoxSphere() {
        System.out.println("detectCollistions - Box-Sphere");
        
        PhysicsObject aPhysics_1 = new PhysicsObject(ColliderTypes.ColliderBox, new Vector3f(), new Vector3f(), 0.0f, new Vector3f(), new Vector3f());
        Transform lTransform = new Transform();
        lTransform.setScale(new Vector3f(1, 2, 3));
        aPhysics_1.getState().set(lTransform);
                
        PhysicsObject aPhysics_2 = new PhysicsObject(ColliderTypes.ColliderSphere, new Vector3f(), new Vector3f(), 0.0f, new Vector3f(), new Vector3f());
        Transform lOtherTransform = new Transform();
        lOtherTransform.setPosition(new Vector3f(0, 3, 0));
        lOtherTransform.setScale(new Vector3f(1, 1, 1));
        aPhysics_2.getState().set(lOtherTransform);
        
        CollisionManager instance = new CollisionManager();
        
        IntersectData result = instance.detectCollistions(aPhysics_1, aPhysics_2);
        IntersectData expResult = new IntersectData(false, new Vector3f(0,1,0));
        assertEquals(expResult.isIntersecting(), result.isIntersecting());
        assertEquals(true, result.getDirection().equals(expResult.getDirection()));
        assertEquals(true, Precision.equals(expResult.getDistance(), result.getDistance()));
        
        lOtherTransform = new Transform();
        lOtherTransform.setPosition(new Vector3f(0, 0, 3));
        lOtherTransform.setScale(new Vector3f(1, 1, 1));
        aPhysics_2.getState().set(lOtherTransform);
        result = instance.detectCollistions(aPhysics_1, aPhysics_2);
        expResult = new IntersectData(true, new Vector3f(0, 0, 0));
        assertEquals(expResult.isIntersecting(), result.isIntersecting());
        assertEquals(true, result.getDirection().equals(expResult.getDirection()));
        assertEquals(true, Precision.equals(expResult.getDistance(), result.getDistance()));
        
        lTransform.setScale(new Vector3f(1, 1, 1));
        lTransform.setPosition(new Vector3f(0, 0, 1.5f));
        aPhysics_1.getState().set(lTransform);
        result = instance.detectCollistions(aPhysics_1, aPhysics_2);
        expResult = new IntersectData(true, new Vector3f(0, 0, 2f));
        assertEquals(expResult.isIntersecting(), result.isIntersecting());
        assertEquals(true, result.getDirection().equals(expResult.getDirection()));
        assertEquals(true, Precision.equals(expResult.getDistance(), result.getDistance()));
    }

    @Test
    public void testDetectCollistionsPlanePlane() {
        System.out.println("detectCollistions - Plane-Plane");
        
        PhysicsObject aPhysics_1 = new PhysicsObject(ColliderTypes.ColliderPlane,  new Vector3f(0, 1, 0), new Vector3f(), 0.0f, new Vector3f(), new Vector3f());
        Transform lTransform = new Transform();
        lTransform.setPosition(new Vector3f(0, -1, 0));
        aPhysics_1.getState().set(lTransform);
        
        
        PhysicsObject aPhysics_2 = new PhysicsObject(ColliderTypes.ColliderPlane, new Vector3f(0, 1, 0), new Vector3f(), 0.0f, new Vector3f(), new Vector3f());
        Transform lOtherTransform = new Transform();
        lOtherTransform.setPosition(new Vector3f(0, 5, 0));
        aPhysics_2.getState().set(lOtherTransform);
        
        CollisionManager instance = new CollisionManager();                
        
        IntersectData result = instance.detectCollistions(aPhysics_1, aPhysics_2);        
        IntersectData expResult = new IntersectData(false, new Vector3f(0, 6, 0));
        assertEquals(expResult.isIntersecting(), result.isIntersecting());
        assertEquals(true, result.getDirection().equals(expResult.getDirection()));
        assertEquals(true, Precision.equals(expResult.getDistance(), result.getDistance()));

        aPhysics_2 = new PhysicsObject(ColliderTypes.ColliderPlane, new Vector3f(0, 0, 1), new Vector3f(), 0.0f, new Vector3f(), new Vector3f());
        lOtherTransform = new Transform();
        lOtherTransform.setPosition(new Vector3f(0, 5, 0));
        aPhysics_2.getState().set(lOtherTransform);

        result = instance.detectCollistions(aPhysics_1, aPhysics_2);     
        expResult = new IntersectData(true, new Vector3f(0, -1, 0));
        assertEquals(expResult.isIntersecting(), result.isIntersecting());
        assertEquals(true, result.getDirection().equals(expResult.getDirection()));
        assertEquals(true, Precision.equals(expResult.getDistance(), result.getDistance()));
        
        result = instance.detectCollistions(aPhysics_2, aPhysics_1);     
        assertEquals(expResult.isIntersecting(), result.isIntersecting());
        assertEquals(true, result.getDirection().equals(expResult.getDirection()));
        assertEquals(true, Precision.equals(expResult.getDistance(), result.getDistance()));
    }

    @Test
    public void testDetectCollistionsPlaneSphere() {
        System.out.println("detectCollistions - Plane-Sphere");
        
        PhysicsObject aPhysics_1 = new PhysicsObject(ColliderTypes.ColliderPlane,  new Vector3f(0, 1, 0), new Vector3f(), 0.0f, new Vector3f(), new Vector3f());
        Transform lTransform = new Transform();
        lTransform.setPosition(new Vector3f(0, -1, 0));
        aPhysics_1.getState().set(lTransform);
        
        PhysicsObject aPhysics_2 = new PhysicsObject(ColliderTypes.ColliderSphere, new Vector3f(), new Vector3f(), 0.0f, new Vector3f(), new Vector3f());
        Transform lOtherTransform = new Transform();
        lOtherTransform.setPosition(new Vector3f(0, 0, 0));
        lOtherTransform.setScale(new Vector3f(1, 1, 1));
        aPhysics_2.getState().set(lOtherTransform);
        
        CollisionManager instance = new CollisionManager();
                
        IntersectData result = instance.detectCollistions(aPhysics_1, aPhysics_2);
        IntersectData expResult = new IntersectData(false, new Vector3f(0, -1f, 0));
        assertEquals(expResult.isIntersecting(), result.isIntersecting());
        assertEquals(true, result.getDirection().equals(expResult.getDirection()));
        assertEquals(true, Precision.equals(expResult.getDistance(), result.getDistance()));
        
        lOtherTransform = new Transform();
        lOtherTransform.setPosition(new Vector3f(0, 3, 0));
        lOtherTransform.setScale(new Vector3f(1, 1, 1));
        aPhysics_2.getState().set(lOtherTransform);
        result = instance.detectCollistions(aPhysics_1, aPhysics_2);
        expResult = new IntersectData(false, new Vector3f(0, +2f, 0));
        assertEquals(expResult.isIntersecting(), result.isIntersecting());
        assertEquals(true, result.getDirection().equals(expResult.getDirection()));
        assertEquals(true, Precision.equals(expResult.getDistance(), result.getDistance()));

        lOtherTransform = new Transform();
        lOtherTransform.setPosition(new Vector3f(0, -3, 0));
        lOtherTransform.setScale(new Vector3f(1, 1, 1));
        aPhysics_2.getState().set(lOtherTransform);
        result = instance.detectCollistions(aPhysics_1, aPhysics_2);
        expResult = new IntersectData(false, new Vector3f(0, -2f, 0));
        assertEquals(expResult.isIntersecting(), result.isIntersecting());
        assertEquals(true, result.getDirection().equals(expResult.getDirection()));
        assertEquals(true, Precision.equals(expResult.getDistance(), result.getDistance()));

        lOtherTransform = new Transform();
        lOtherTransform.setPosition(new Vector3f(0, -1, 2));
        lOtherTransform.setScale(new Vector3f(1, 1, 1));
        aPhysics_2.getState().set(lOtherTransform);
        result = instance.detectCollistions(aPhysics_1, aPhysics_2);
        expResult = new IntersectData(true, new Vector3f(0, -2f, 2));
        assertEquals(expResult.isIntersecting(), result.isIntersecting());
        assertEquals(true, result.getDirection().equals(expResult.getDirection()));
        assertEquals(true, Precision.equals(expResult.getDistance(), result.getDistance()));

        lOtherTransform = new Transform();
        lOtherTransform.setPosition(new Vector3f(1, -1, 0));
        lOtherTransform.setScale(new Vector3f(1, 1, 1));
        aPhysics_2.getState().set(lOtherTransform);
        result = instance.detectCollistions(aPhysics_1, aPhysics_2);
        expResult = new IntersectData(true, new Vector3f(1, -2f, 0));
        assertEquals(expResult.isIntersecting(), result.isIntersecting());
        assertEquals(true, result.getDirection().equals(expResult.getDirection()));
        assertEquals(true, Precision.equals(expResult.getDistance(), result.getDistance()));
    }

    @Test
    public void testDetectCollistionsSphereSphere() {
        System.out.println("detectCollistions - Sphere-Sphere");
        
        PhysicsObject aPhysics_1 = new PhysicsObject(ColliderTypes.ColliderSphere, new Vector3f(), new Vector3f(), 0.0f, new Vector3f(), new Vector3f());
        Transform lTransform = new Transform();
        lTransform.setScale(new Vector3f(1, 1, 1));
        aPhysics_1.getState().set(lTransform);
        
        PhysicsObject aPhysics_2 = new PhysicsObject(ColliderTypes.ColliderSphere, new Vector3f(), new Vector3f(), 0.0f, new Vector3f(), new Vector3f());
        Transform lOtherTransform = new Transform();
        lOtherTransform.setPosition(new Vector3f(0, 3, 0));
        lOtherTransform.setScale(new Vector3f(1, 1, 1));
        aPhysics_2.getState().set(lOtherTransform);
        
        CollisionManager instance = new CollisionManager();
        
        IntersectData result = instance.detectCollistions(aPhysics_1, aPhysics_2);        
        IntersectData expResult = new IntersectData(false, new Vector3f(0, 2, 0));
        assertEquals(expResult.isIntersecting(), result.isIntersecting());
        assertEquals(true, result.getDirection().equals(expResult.getDirection()));
        assertEquals(true, Precision.equals(expResult.getDistance(), result.getDistance()));
        
        lOtherTransform.setPosition(new Vector3f(0, 0, 2));
        lOtherTransform.setScale(new Vector3f(1, 1, 1));
        aPhysics_2.getState().set(lOtherTransform);
        result = instance.detectCollistions(aPhysics_1, aPhysics_2);
        expResult = new IntersectData(false, new Vector3f(0, 0, 1));
        assertEquals(expResult.isIntersecting(), result.isIntersecting());
        assertEquals(true, result.getDirection().equals(expResult.getDirection()));
        assertEquals(true, Precision.equals(expResult.getDistance(), result.getDistance()));
        

        lOtherTransform.setPosition(new Vector3f(1, 0, 0));
        lOtherTransform.setScale(new Vector3f(1, 1, 1));
        aPhysics_2.getState().set(lOtherTransform);
        result = instance.detectCollistions(aPhysics_1, aPhysics_2);
        expResult = new IntersectData(true, new Vector3f(0f, 0, 0));
        assertEquals(expResult.isIntersecting(), result.isIntersecting());
        assertEquals(true, result.getDirection().equals(expResult.getDirection()));
        assertEquals(true, Precision.equals(expResult.getDistance(), result.getDistance()));
        
        lTransform.setPosition(new Vector3f(2.5f, 0, 0));
        lTransform.setScale(new Vector3f(1, 1, 1));
        aPhysics_1.getState().set(lTransform);
        result = instance.detectCollistions(aPhysics_1, aPhysics_2);
        expResult = new IntersectData(true, new Vector3f(2f, 0, 0));
        assertEquals(expResult.isIntersecting(), result.isIntersecting());
        assertEquals(true, result.getDirection().equals(expResult.getDirection()));
        assertEquals(true, Precision.equals(expResult.getDistance(), result.getDistance()));
    }

}
