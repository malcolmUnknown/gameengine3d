/* GameEngine 3D - A programm to design games with 3D content 
 * Copyright (C) 2016-2056 Frank Küwen
 *  
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package ProjectCenter.PhysicsEngine;

import ProjectCenter.CoreEngine.Matrix4f;
import ProjectCenter.CoreEngine.Precision;
import ProjectCenter.CoreEngine.Quaternion;
import ProjectCenter.CoreEngine.Transform;
import ProjectCenter.CoreEngine.Vector3f;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;
import java.util.*;
import static org.junit.Assert.*;
import org.junit.BeforeClass;

public class PhysicsObjectTest {

    public PhysicsObjectTest() {
    }

    @BeforeClass
    public static void setupClass() {
        System.out.println("--- START TEST: PhysicsObjectTest ---");
    }

    @AfterClass
    public static void tearDownClass() {
        System.out.println("--- END TEST: PhysicsObjectTest ---\n");
    }

    @Test
    public void testGetMass() {
        System.out.println("getMass");
        PhysicsObject instance = new PhysicsObject(
                ColliderTypes.ColliderSphere,
                new Vector3f(1, 0, 0),
                new Vector3f(),
                3.24f,
                new Vector3f(0, 0, 0),
                new Vector3f(0, 1, 0).mul(0 * (float) (Math.PI / (2 * 5.f))));
        float expResult = 3.24F;
        float result = instance.getMass();
        assertEquals(true, Precision.equals(result, expResult));
    }

    @Test
    public void testGetDimensions() {
        System.out.println("getDimensions");
        PhysicsObject instance = new PhysicsObject(
                ColliderTypes.ColliderBox,
                new Vector3f(1, 0, 0),
                new Vector3f(),
                3.24f,
                new Vector3f(0, 0, 0),
                new Vector3f(0, 1, 0).mul(0 * (float) (Math.PI / (2 * 5.f))));
        instance.updateScale(new Vector3f(1, 8, 2));
        Vector3f expResult = new Vector3f(1, 8, 2);
        Vector3f result = instance.getDimensions();
        assertEquals(true, result.equals(expResult));
    }

    @Test
    public void testGetPosition() {
        System.out.println("getPosition");
        PhysicsObject instance = new PhysicsObject(
                ColliderTypes.ColliderBox,
                new Vector3f(1, 0, 0),
                new Vector3f(1, 0, 0),
                3.24f,
                new Vector3f(5, 5, 5),
                new Vector3f(0, 1, 0).mul(0 * (float) (Math.PI / (2 * 5.f))));
        Vector3f expResult = new Vector3f();
        Vector3f result = instance.getPosition();
        assertEquals(true, result.equals(expResult));
    }

    @Test
    public void testUpdatePosition() {
        System.out.println("updatePosition");
        Vector3f aNewPosition = new Vector3f(3, 4, 5);
        PhysicsObject instance = new PhysicsObject(
                ColliderTypes.ColliderBox,
                new Vector3f(1, 0, 0),
                new Vector3f(1, 0, 0),
                3.24f,
                new Vector3f(5, 5, 5),
                new Vector3f(0, 1, 0).mul(0 * (float) (Math.PI / (2 * 5.f))));
        Vector3f expResult = new Vector3f();
        Vector3f result = instance.getPosition();
        assertEquals(true, result.equals(expResult));
        instance.updatePosition(aNewPosition);
        expResult = new Vector3f(3, 4, 5);
        result = instance.getPosition();
        assertEquals(true, result.equals(expResult));
    }

    @Test
    public void testGetScale() {
        System.out.println("getScale");
        PhysicsObject instance = new PhysicsObject(
                ColliderTypes.ColliderBox,
                new Vector3f(1, 0, 0),
                new Vector3f(1, 0, 0),
                3.24f,
                new Vector3f(5, 5, 5),
                new Vector3f(0, 1, 0).mul(0 * (float) (Math.PI / (2 * 5.f))));
        Vector3f expResult = new Vector3f(1, 1, 1);
        Vector3f result = instance.getScale();
        assertEquals(true, result.equals(expResult));
    }

    @Test
    public void testUpdateScale() {
        System.out.println("updateScale");
        Vector3f aNewScale = new Vector3f(2, 5, 6);
        PhysicsObject instance = new PhysicsObject(
                ColliderTypes.ColliderBox,
                new Vector3f(1, 0, 0),
                new Vector3f(1, 0, 0),
                3.24f,
                new Vector3f(5, 5, 5),
                new Vector3f(0, 1, 0).mul(0 * (float) (Math.PI / (2 * 5.f))));
        Vector3f expResult = new Vector3f(1, 1, 1);
        Vector3f result = instance.getScale();
        assertEquals(true, result.equals(expResult));
        instance.updateScale(aNewScale);
        expResult = new Vector3f(2, 5, 6);
        result = instance.getScale();
        assertEquals(true, result.equals(expResult));
    }

    @Test
    public void testGetRotation() {
        System.out.println("getRotation");
        PhysicsObject instance = new PhysicsObject(
                ColliderTypes.ColliderBox,
                new Vector3f(1, 0, 0),
                new Vector3f(1, 0, 0),
                3.24f,
                new Vector3f(5, 5, 5),
                new Vector3f(0, 1, 0).mul(0 * (float) (Math.PI / (2 * 5.f))));
        Quaternion expResult = new Quaternion(0, 0, 0, 1);
        Quaternion result = instance.getRotation();
        assertEquals(true, result.equals(expResult));
    }

    @Test
    public void testUpdateOrientation() {
        System.out.println("updateOrientation");
        Quaternion aNewOrientation = new Quaternion(new Vector3f(0, 0, 1), (float) (Math.PI / 2));
        PhysicsObject instance = new PhysicsObject(
                ColliderTypes.ColliderBox,
                new Vector3f(1, 0, 0),
                new Vector3f(1, 0, 0),
                3.24f,
                new Vector3f(5, 5, 5),
                new Vector3f(0, 1, 0).mul(0 * (float) (Math.PI / (2 * 5.f))));
        Quaternion expResult = new Quaternion(0, 0, 0, 1);
        Quaternion result = instance.getRotation();
        assertEquals(true, result.equals(expResult));
        instance.updateOrientation(aNewOrientation);
        expResult = new Quaternion(new Vector3f(0, 0, 1), (float) (Math.PI / 2));
        result = instance.getRotation();
        assertEquals(true, result.equals(expResult));
    }

    @Test
    public void testGetVelocity() {
        System.out.println("getVelocity");
        PhysicsObject instance = new PhysicsObject(
                ColliderTypes.ColliderSphere,
                new Vector3f(1, 0, 0),
                new Vector3f(),
                1,
                new Vector3f(0, 5, 2),
                new Vector3f(0, 1, 0).mul(0 * (float) (Math.PI / (2 * 5.f))));
        Vector3f expResult = new Vector3f(0, 5, 2);
        Vector3f result = instance.getVelocity();
        assertEquals(true, result.equals(expResult));
    }

    @Test
    public void testSetVelocity() {
        System.out.println("setVelocity");
        PhysicsObject instance = new PhysicsObject(
                ColliderTypes.ColliderSphere,
                new Vector3f(1, 0, 0),
                new Vector3f(),
                1,
                new Vector3f(0, 5, 2),
                new Vector3f(0, 1, 0).mul(0 * (float) (Math.PI / (2 * 5.f))));
        Vector3f expResult = new Vector3f(0, 5, 2);
        Vector3f result = instance.getVelocity();
        assertEquals(true, result.equals(expResult));
        Vector3f aVelocity = new Vector3f(0, 0, 0);
        instance.setVelocity(aVelocity);
        result = instance.getVelocity();
        expResult = Vector3f.ZEROVECTOR;
        assertEquals(true, result.equals(expResult));
    }

    @Test
    public void testUpdateVelocity() {
        System.out.println("updateVelocity & setUpdatedValues");
        PhysicsObject instance = new PhysicsObject(
                ColliderTypes.ColliderSphere,
                new Vector3f(1, 0, 0),
                new Vector3f(),
                1,
                new Vector3f(0, 5, 2),
                new Vector3f(0, 1, 0).mul(0 * (float) (Math.PI / (2 * 5.f))));
        assertEquals(true, instance.getVelocity().equals(new Vector3f(0, 5, 2)));
        instance.updateVelocity(new Vector3f(1, 0, 0));
        instance.updateVelocity(new Vector3f(0, 1, 0));
        instance.updateVelocity(new Vector3f(0, 0, 1));
        assertEquals(true, instance.getVelocity().equals(new Vector3f(0, 5, 2)));
        instance.setUpdatedValues();
        assertEquals(true, instance.getVelocity().equals(new Vector3f(1/3.f,1/3.f,1/3.f)));
    }

    @Test
    public void testGetAngluarAxis() {
        System.out.println("getAngluarAxis");
        PhysicsObject instance = new PhysicsObject(
                ColliderTypes.ColliderSphere,
                new Vector3f(1, 0, 0),
                new Vector3f(),
                1,
                new Vector3f(0, 5, 2),
                new Vector3f(0, 1, 0).mul((float) (Math.PI / (2 * 5.f))));
        Vector3f expResult = new Vector3f(0, 1, 0);
        Vector3f result = instance.getAngluarAxis();
        assertEquals(true, result.equals(expResult));

        instance = new PhysicsObject(
                ColliderTypes.ColliderSphere,
                new Vector3f(1, 0, 0),
                new Vector3f(),
                1,
                new Vector3f(0, 5, 2),
                new Vector3f(0, 1, 0).mul(0 * (float) (Math.PI / (2 * 5.f))));
        expResult = Vector3f.ZEROVECTOR;
        result = instance.getAngluarAxis();
        assertEquals(true, result.equals(expResult));
    }

    @Test
    public void testGetAngluarSpeed() {
        System.out.println("getAngluarSpeed");
        PhysicsObject instance = new PhysicsObject(
                ColliderTypes.ColliderSphere,
                new Vector3f(1, 0, 0),
                new Vector3f(),
                1,
                new Vector3f(0, 5, 2),
                new Vector3f(0, 1, 0).mul((float) (Math.PI / (2 * 5.f))));
        float expResult = (float) (Math.PI / (2 * 5.f));
        float result = instance.getAngluarSpeed();
        assertEquals(true, Precision.equals(result, expResult));

        instance = new PhysicsObject(
                ColliderTypes.ColliderSphere,
                new Vector3f(1, 0, 0),
                new Vector3f(),
                1,
                new Vector3f(0, 5, 2),
                new Vector3f(0, 1, 0).mul(0 * (float) (Math.PI / (2 * 5.f))));
        expResult = 0.0F;
        result = instance.getAngluarSpeed();
        assertEquals(true, Precision.equals(result, expResult));
    }

    @Test
    public void testSetAngluarVelocity() {
        System.out.println("setAngluarVelocity");
        Vector3f aAngularVelocity = new Vector3f();
        PhysicsObject instance = new PhysicsObject(
                ColliderTypes.ColliderSphere,
                new Vector3f(1, 0, 0),
                new Vector3f(),
                1,
                new Vector3f(0, 5, 2),
                new Vector3f(0, 1, 0).mul((float) (Math.PI / (2 * 5.f))));
        Vector3f expResult = new Vector3f(0, 1, 0);
        Vector3f result = instance.getAngluarAxis();
        assertEquals(true, result.equals(expResult));

        instance.setAngluarVelocity(aAngularVelocity);
        result = instance.getAngluarAxis();
        expResult = new Vector3f();
        assertEquals(true, result.equals(expResult));
        assertEquals(true, Precision.equals(instance.getAngluarSpeed(), 0.0f));
    }

    @Test
    public void testAddGetDeleteCharge() {
        System.out.println("addCharge & getCharge & getChargeCount & deleteCharge");
        ChargeObject aCharge1 = new ChargeObject(ForceTypes.Graviational, 4.f, new Vector3f(), false);
        ChargeObject aCharge2 = new ChargeObject(ForceTypes.Graviational, 2.f, new Vector3f(), false);
        PhysicsObject instance = new PhysicsObject(
                ColliderTypes.ColliderSphere,
                new Vector3f(1, 0, 0),
                new Vector3f(),
                1,
                new Vector3f(0, 5, 2),
                new Vector3f(0, 1, 0).mul((float) (Math.PI / (2 * 5.f))));
        instance.addCharge(aCharge1);
        assertEquals(instance.getChargeCount(), 2);
        instance.addCharge(aCharge2);
        assertEquals(instance.getChargeCount(), 3);

        Iterator<ChargeObject> I = instance.getCharges();
        boolean foundCharge = false;
        while(I.hasNext())
            if(aCharge1 == I.next())
                foundCharge = true;                
        assertEquals(foundCharge, true);
        
        instance.deleteCharge(aCharge1);
        assertEquals(instance.getChargeCount(), 2);
        instance.deleteCharge(aCharge2);
        assertEquals(instance.getChargeCount(), 1);

    }

    @Test
    public void testGetCollider() {
        System.out.println("getCollider");
        PhysicsObject instance = new PhysicsObject(
                ColliderTypes.ColliderSphere,
                new Vector3f(1, 0, 0),
                new Vector3f(0, 2, 0),
                7,
                new Vector3f(0, 5, 2),
                new Vector3f(0, 1, 0).mul((float) (Math.PI / (2 * 5.f))));

        ColliderTypes expResult = ColliderTypes.ColliderSphere;
        ColliderTypes result = instance.getColliderType();
        assertEquals(expResult, result);
    }
//
//    @Test
//    public void testGetDistance() {
//        System.out.println("getDistance");
//        PhysicsObject instance = new PhysicsObject(
//                ColliderTypes.ColliderBox, 
//                new Vector3f(1, 0, 0), 
//                new Vector3f(1, 0, 0), 
//                3.24f, 
//                new Vector3f(5, 5, 5), 
//                new Vector3f(0, 1, 0).mul(0 * (float) (Math.PI / (2 * 5.f))));
//        instance.updateScale(new Vector3f(1, 8, 2));
//        Vector3f expResult = new Vector3f(1, 0, 0);
//        Vector3f result = instance.getDistance(new Vector3f(1, 0, 0));
//        assertEquals(true, result.equals(expResult)); 
//        
//        expResult = new Vector3f(0, 8, 0);
//        result = instance.getDistance(new Vector3f(0, 1,  0));
//        assertEquals(true, result.equals(expResult)); 
//        
//        expResult = new Vector3f(0, 2, 2);
//        result = instance.getDistance(new Vector3f(0, 1,  1).normalized());
//        assertEquals(true, result.equals(expResult)); 
//    }

    @Test
    public void testIntegrate() {
        System.out.println("integrate");
        float aTimeDelta = 5.0F;
        PhysicsObject instance = new PhysicsObject(
                ColliderTypes.ColliderBox,
                new Vector3f(1, 0, 0),
                new Vector3f(1, 0, 0),
                3.24f,
                new Vector3f(5, 1, 3),
                new Vector3f(0, 1, 0).mul((float) (Math.PI / (2 * 5.f))));
        assertEquals(true, instance.getPosition().equals(new Vector3f(0, 0, 0)));
        assertEquals(true, instance.getRotation().equals(new Quaternion(0, 0, 0, 1)));
        instance.integrate(aTimeDelta);
        // Excenter has to be acknowledged
        assertEquals(true, instance.getPosition().equals(new Vector3f(24, 5, 16)));
//                add(new Vector3f(1, 0, 0)).
//                sub(new Quaternion(new Vector3f(0, 1, 0), (float) (Math.PI / 2) * 5).
//                        toRotationMatrix().mul(new Vector3f(1, 0, 0)))));
        assertEquals(true, instance.getRotation().equals(new Quaternion(new Vector3f(0, 1, 0), (float) (Math.PI / 2))));
    }

    @Test
    public void testApplyForce() {
        System.out.println("applyForce");
        ForceObject aGForce = new ForceObject(ForceTypes.Graviational, 2, new Vector3f(2, 0, 0));
        ForceObject aEForce = new ForceObject(ForceTypes.Electrical, 4, new Vector3f(0, 0, -4));
        float aTimeDelta = 2.0F;
        ChargeObject aMass = new ChargeObject(ForceTypes.Graviational, 5.f, new Vector3f(), false);
        ChargeObject aCharge = new ChargeObject(ForceTypes.Electrical, 2.f, new Vector3f(), false);
        PhysicsObject instance = new PhysicsObject(
                ColliderTypes.ColliderBox,
                new Vector3f(1, 0, 0),
                new Vector3f(1, 0, 0),
                3.24f,
                new Vector3f(5, 1, 3),
                new Vector3f(0, 1, 0).mul((float) (Math.PI / (2 * 5.f))));
        assertEquals(true, instance.getVelocity().equals(new Vector3f(5, 1, 3)));
        instance.applyForce(aEForce, aTimeDelta);
        instance.setUpdatedValues();
        Vector3f NewVelocity = new Vector3f(5, 1, 3);
        assertEquals(true, instance.getVelocity().equals(NewVelocity));
        
        instance.addCharge(aCharge);
        instance.applyForce(aGForce, aTimeDelta);
        instance.setUpdatedValues();
        NewVelocity = NewVelocity.add(new Vector3f(1,0,0).mul(-2*3.24f).mul(2/3.24f));
        assertEquals(true, instance.getVelocity().equals(NewVelocity));
        
        instance.addCharge(aMass);
        instance.applyForce(aGForce, aTimeDelta);
        instance.setUpdatedValues();
        NewVelocity = NewVelocity.add(new Vector3f(1,0,0).mul(-2*3.24f).mul(2/8.24f)).add(
                NewVelocity.add(new Vector3f(-2.5f * 2/8.24f, 0, 0)));
        NewVelocity = NewVelocity.div(2);
        assertEquals(true, instance.getVelocity().equals(NewVelocity));
        
        instance.applyForce(aEForce, aTimeDelta);
        instance.setUpdatedValues();
        NewVelocity = NewVelocity.add(new Vector3f(0, 0, 0.5f * 2/8.24f));
        assertEquals(true, instance.getVelocity().equals(NewVelocity));
    }
//
//    @Test
//    public void testUpdateState() {
//        System.out.println("updateState");
//        Transform aNewState = null;
//        PhysicsObject instance = null;
//        instance.updateState(aNewState);
//        fail("The test case is a prototype.");
//    }
//
//    @Test
//    public void testGetMomentumOfInteria() {
//        System.out.println("getMomentumOfInteria");
//        PhysicsObject instance = null;
//        Matrix4f expResult = null;
//        Matrix4f result = instance.getMomentumOfInteria();
//        assertEquals(expResult, result);
//        fail("The test case is a prototype.");
//    }
//

}
