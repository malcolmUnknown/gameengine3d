/* GameEngine 3D - A programm to design games with 3D content 
 * Copyright (C) 2016-2056 Frank Küwen
 *  
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package ProjectCenter.PhysicsEngine;

import ProjectCenter.CoreEngine.Precision;
import ProjectCenter.CoreEngine.Quaternion;
import ProjectCenter.CoreEngine.Vector3f;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

public class ChargeObjectTest {

    public ChargeObjectTest() {
    }

    @BeforeClass
    public static void setUpClass() {
        System.out.println("--- START TEST: ChargeObjectTest ---");
    }

    @AfterClass
    public static void tearDownClass() {
        System.out.println("--- END TEST: ChargeObjectTest ---\n");
    }

    /**
     * Test of getCharge method, of class ChargeObject.
     */
    @Test
    public void testGetCharge() {
        System.out.println("getCharge");
        ChargeObject instance = new ChargeObject(ForceTypes.Graviational, 4, new Vector3f(1, 1, 1), false);
        float expResult = 4.0F;
        float result = instance.getCharge();
        assertEquals(true, Precision.equals(result, expResult));
    }

    /**
     * Test of getType method, of class ChargeObject.
     */
    @Test
    public void testGetType() {
        System.out.println("getType");
        ChargeObject instance = new ChargeObject(ForceTypes.Graviational, 4, new Vector3f(1, 1, 1), false);
        ForceTypes expResult = ForceTypes.Graviational;
        ForceTypes result = instance.getType();
        assertEquals(expResult, result);

        expResult = ForceTypes.Electrical;
        assertNotSame(expResult, result);
    }

    /**
     * Test of generatesForce method, of class ChargeObject.
     */
    @Test
    public void testGeneratesForce() {
        System.out.println("generatesForce");
        ChargeObject instance = new ChargeObject(ForceTypes.Graviational, 4, new Vector3f(1, 1, 1), false);
        boolean expResult = false;
        boolean result = instance.generatesForce();
        assertEquals(expResult, result);

        instance = new ChargeObject(ForceTypes.Graviational, 4, new Vector3f(1, 1, 1), true);
        expResult = true;
        result = instance.generatesForce();
        assertEquals(expResult, result);
    }

    /**
     * Test of getExcenter method, of class ChargeObject.
     */
    @Test
    public void testGetExcenter() {
        System.out.println("getExcenter");
        ChargeObject instance = new ChargeObject(ForceTypes.Graviational, 4, new Vector3f(1, 1, 1), true);
        Vector3f expResult = new Vector3f(1, 1, 1);
        Vector3f result = instance.getExcenter();
        assertEquals(true, result.equals(expResult));
    }

    /**
     * Test of getPosition method, of class ChargeObject.
     */
    @Test
    public void testGetPosition() {
        System.out.println("getPosition");
        ChargeObject instance = new ChargeObject(ForceTypes.Graviational, 4, new Vector3f(1, 1, 1), true);
        Vector3f expResult = new Vector3f(1, 1, 1);
        Vector3f result = instance.getPosition();
        assertEquals(true, result.equals(expResult));
    }

    /**
     * Test of updatePosition method, of class ChargeObject.
     */
    @Test
    public void testUpdatePosition() {
        System.out.println("updatePosition");
        Vector3f aNewCenterOfMass = new Vector3f(2,0,0);
        ChargeObject instance = new ChargeObject(ForceTypes.Graviational, 4, new Vector3f(1, 1, 1), true);
        Vector3f expResult = new Vector3f(1, 1, 1);
        Vector3f result = instance.getPosition();        
        assertEquals(true, result.equals(expResult));
        
        instance.updatePosition(aNewCenterOfMass);
        expResult = new Vector3f(3, 1, 1);
        result = instance.getPosition();     
        assertEquals(true, result.equals(expResult));
    }

    /**
     * Test of updateOrientation method, of class ChargeObject.
     */
    @Test
    public void testUpdateOrientation() {
        System.out.println("updateOrientation");
        Quaternion aNewOrientation = new Quaternion(new Vector3f(0, 0, 1), (float) Math.PI/2);
        ChargeObject instance =new ChargeObject(ForceTypes.Graviational, 4, new Vector3f(1, 0, 1), true);
        Vector3f expResult = new Vector3f(1, 0, 1);
        Vector3f result = instance.getPosition();     
        assertEquals(true, result.equals(expResult)); 
        
        instance.updateOrientation(aNewOrientation);        
        expResult = new Vector3f(0, -1, 1);
        result = instance.getPosition();     
        assertEquals(true, result.equals(expResult));
    }

    /**
     * Test of updateScale method, of class ChargeObject.
     */
    @Test
    public void testUpdateScale() {
        System.out.println("updateScale");
        Vector3f aNewScale = new Vector3f(1,2,3);
        ChargeObject instance = new ChargeObject(ForceTypes.Graviational, 4, new Vector3f(1, 0, 1), true);
        Vector3f expResult = new Vector3f(1, 0, 1);
        Vector3f result = instance.getPosition();      
        assertEquals(true, result.equals(expResult));
        
        instance.updateScale(aNewScale);        
        expResult = new Vector3f(1, 0, 3);
        result = instance.getPosition();     
        assertEquals(true, result.equals(expResult));
    }

}
