/* GameEngine 3D - A programm to design games with 3D content 
 * Copyright (C) 2016-2056 Frank Küwen
 *  
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package ProjectCenter.PhysicsEngine;

import ProjectCenter.CoreEngine.Quaternion;
import ProjectCenter.CoreEngine.Transform;
import ProjectCenter.CoreEngine.Vector3f;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

public class ForceObjectTest {

    public ForceObjectTest() {
    }

    @BeforeClass
    public static void setUpClass() {
        System.out.println("--- START TEST: ForceObjectTest ---");
    }

    @AfterClass
    public static void tearDownClass() {
        System.out.println("--- END TEST: ForceObjectTest ---\n");
    }

    /**
     * Test of getType method, of class ForceObject.
     */
    @Test
    public void testGetType() {
        System.out.println("getType");
        ForceObject instance = new ForceObject(ForceTypes.Electrical, 1, new Vector3f());
        ForceTypes expResult = ForceTypes.Electrical;
        ForceTypes result = instance.getType();
        assertEquals(expResult, result);

        expResult = ForceTypes.Graviational;
        result = instance.getType();
        assertNotSame(expResult, result);
    }

    /**
     * Test of getForce method, of class ForceObject.
     */
    @Test
    public void testGetForce() {
        System.out.println("getForce");
        ChargeObject aCharge = new ChargeObject(ForceTypes.Graviational, 1, new Vector3f(1, 1, 1), false);
        ForceObject instance = new ForceObject(ForceTypes.Electrical, 1, new Vector3f());
        Vector3f expResult = null;
        Vector3f result = instance.getForce(aCharge);
        assertEquals(expResult, result);

        aCharge = new ChargeObject(ForceTypes.Electrical, 1, new Vector3f(1, 1, 1), false);
        result = instance.getForce(aCharge);
        expResult = new Vector3f(1, 1, 1).mul((float) (1.f / Math.pow(Math.sqrt(3),3)));
        assertEquals(true, result.equals(expResult));
    }

    /**
     * Test of updateState method, of class ForceObject.
     */
    @Test
    public void testUpdateState() {
        System.out.println("updateState");
        Transform aNewState = new Transform();
        ForceObject instance = new ForceObject(ForceTypes.Electrical, 1, new Vector3f());
        ChargeObject aCharge = new ChargeObject(ForceTypes.Electrical, 1, new Vector3f(1, 0, 0), false);
        Vector3f result = instance.getForce(aCharge);
        Vector3f expPosition = new Vector3f(1,0,0);
        assertEquals(true, result.equals(expPosition));

        aNewState.setPosition(new Vector3f(-1, 0, 0));
        instance.updateState(aNewState);
        result = instance.getForce(aCharge);
        expPosition = new Vector3f(2,0,0).mul((float) (1.f / (2 *2 * 2)));
        assertEquals(true, result.equals(expPosition));

        aNewState.setPosition(new Vector3f(0, 0, 0));
        aNewState.setRotation(new Quaternion(new Vector3f(0, 0, 1), (float) Math.PI/2));
        instance.updateState(aNewState);
        result = instance.getForce(aCharge);
        expPosition = new Vector3f(1,0,0);
        assertEquals(true, result.equals(expPosition));
    }

}
