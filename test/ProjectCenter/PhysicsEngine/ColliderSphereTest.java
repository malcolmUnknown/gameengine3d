/* GameEngine 3D - A programm to design games with 3D content 
 * Copyright (C) 2016-2056 Frank Küwen
 *  
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package ProjectCenter.PhysicsEngine;

import ProjectCenter.CoreEngine.Precision;
import ProjectCenter.CoreEngine.Quaternion;
import ProjectCenter.CoreEngine.Transform;
import ProjectCenter.CoreEngine.Vector3f;
import org.junit.AfterClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.BeforeClass;

public class ColliderSphereTest {

    public ColliderSphereTest() {
    }

    @BeforeClass
    public static void setupClass() {
        System.out.println("--- START TEST: ColliderSphereTest ---");
    }

    @AfterClass
    public static void tearDownClass() {
        System.out.println("--- END TEST: ColliderSphereTest ---\n");
    }

    @Test
    public void testGetRadius() {
        System.out.println("getRadius");

        Transform lTransform = new Transform();
        lTransform.setPosition(new Vector3f(1, 0, 0));
        lTransform.setScale(new Vector3f(2, 2, 2));
        ColliderSphere instance = new ColliderSphere();
        instance.update(lTransform, Vector3f.ZEROVECTOR);
        float expResult = 2.0F;
        float result = instance.getRadius();
        assertEquals(true, Precision.equals(expResult, result));
    }

    @Test
    public void testUpdate() {
        System.out.println("update");

        Transform lTransform = new Transform();
        lTransform.setPosition(new Vector3f(1, 0, 0));
        lTransform.setScale(new Vector3f(2, 2, 2));
        ColliderSphere instance = new ColliderSphere();
        instance.update(lTransform, Vector3f.ZEROVECTOR);

        Vector3f aNormedDirection = new Vector3f(0, 1, 0);
        Transform lUpdate = new Transform();
        lUpdate.setPosition(new Vector3f(0, -4, 0));
        lUpdate.setScale(new Vector3f(1, 3, 5));
        Vector3f expResult = new Vector3f(0, -4, 0);
        instance.update(lUpdate, aNormedDirection);
        assertEquals(true, instance.getPosition().equals(expResult));
        assertEquals(true, Precision.equals(instance.getRadius(), (new Vector3f(1, 3, 5)).max()));
    }

    @Test
    public void testGetPosition() {
        System.out.println("getPosition");

        Transform lTransform = new Transform();
        lTransform.setPosition(new Vector3f(1, 0, 0));
        lTransform.setScale(new Vector3f(2, 2, 2));
        ColliderSphere instance = new ColliderSphere();
        instance.update(lTransform, Vector3f.ZEROVECTOR);
        Vector3f expResult = Vector3f.UNIT_X;
        Vector3f result = instance.getPosition();
        assertEquals(true, result.equals(expResult));
    }

    @Test
    public void testGetDistanceInDirection() {
        System.out.println("getDistanceInDirection");

        Transform lTransform = new Transform();
        lTransform.setPosition(new Vector3f(1, 0, 0));
        lTransform.setScale(new Vector3f(2, 2, 2));
        ColliderSphere instance = new ColliderSphere();
        instance.update(lTransform, Vector3f.ZEROVECTOR);

        Vector3f aNormedDirection = new Vector3f(0, 1, 0);
        Vector3f expResult = new Vector3f(0, 2, 0);
        Vector3f result = instance.getDistanceInDirection(aNormedDirection);
        assertEquals(true, result.equals(expResult));
    }

    @Test
    public void testGetNormalInDirection() {
        System.out.println("getNormalInDirection");

        Transform lTransform = new Transform();
        lTransform.setPosition(new Vector3f(1, 0, 0));
        lTransform.setScale(new Vector3f(2, 2, 2));
        ColliderSphere instance = new ColliderSphere();
        instance.update(lTransform, Vector3f.ZEROVECTOR);

        Vector3f aNormedDirection = new Vector3f(0, 1, 0);
        Vector3f expResult = new Vector3f(0, 1, 0);
        Vector3f result = instance.getNormalInDirection(aNormedDirection);
        assertEquals(true, result.equals(expResult.normalized()));
    }
}
