/* GameEngine 3D - A programm to design games with 3D content 
 * Copyright (C) 2016-2056 Frank Küwen
 *  
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package MVC_Interface;

import java.util.Observer;
import java.util.UUID;

public interface IControllerGameEngine extends IControllerMain, IControllerMotion, IControllerRotation, IControllerScale{
    
    public void setView(IView aView);
    public IControllerDebug getDebugControll();
    public void registerMouseObserver(Observer aMouseObserver);
    public void deregisterMouseObserver(Observer aMouseObserver);
    public void registerKeyboardObserver(Observer aKeyboardObserver);
    public void deregisterKeyboardObserver(Observer aKeyboardObserver);
    public void registerStructureObserver(Observer aStructureObserver);
    public void deregisterStructureObserver(Observer aStructureObserver);    
    public void deregisterStructureObserver(UUID aGameObjectID, Observer aStructureObserver);
    public void registerStructureObserver(UUID aGameObjectID, Observer aStructureObserver);
}
