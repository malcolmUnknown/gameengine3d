/* GameEngine 3D - A programm to design games with 3D content 
 * Copyright (C) 2016-2056 Frank Küwen
 *  
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package MVC_Interface;

import ProjectCenter.CoreEngine.Game;
import java.awt.Canvas;
import java.util.UUID;

public interface IControllerMain {
    
    public void startEngine();
    public void stopEngine(); 
    
    public void initializeWindow(int aWidth, int aHeight, String aTitle);
    public void initializePanel(Canvas aParent, String aTitle);
    public void setViewOrthographic(boolean aOrthoView);
    public boolean isViewOrthographic();
    public void setFieldOfView(float aFieldOfView);
    public float getFieldOfView();
    public void setResolution(int aWidth, int aHeigth);
    public void activateKeyboard(boolean aActive); 
    public void activateMouse(boolean aActive); 
    public boolean isMouseActive();  
    public boolean isKeyboardActive(); 
    public boolean isEngineRunning(); 
    public void setGame(Game aGame);
    
    
    public void setElementState(UUID aId, boolean aIsActive);
}
