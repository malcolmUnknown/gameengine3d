/* GameEngine 3D - A programm to design games with 3D content 
 * Copyright (C) 2016-2056 Frank Küwen
 *  
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package Controller;

import MVC_Interface.IControllerMain;
import MVC_Interface.IModel;
import ProjectCenter.CoreEngine.Game;
import java.awt.Canvas;
import java.util.Dictionary;
import java.util.UUID;

public class ControllerMain implements IControllerMain {

    private final IModel model;

    public ControllerMain(IModel aModel) {
        model = aModel;
    }

    @Override
    public void startEngine() {
        if (!isEngineRunning()) {
            model.getMainControl().startEngine();
        }
    }

    @Override
    public void stopEngine() {
        if (isEngineRunning()) {
            model.getMainControl().stopEngine();
        }
    }

    @Override
    public boolean isMouseActive() {
        return model.getMainControl().isMouseActive();
    }

    @Override
    public boolean isEngineRunning() {
        return model.getMainControl().isEngineRunning();
    }

    @Override
    public void activateKeyboard(boolean aActive) {
        model.getMainControl().activateKeyboard(aActive);
    }

    @Override
    public void activateMouse(boolean aActive) {
        if (isMouseActive() ^ aActive) {
            model.getMainControl().activateMouse(aActive);
        }
    }

    @Override
    public boolean isKeyboardActive() {
        return model.getMainControl().isKeyboardActive();
    }
        
    @Override
    public void setGame(Game aGame)
    {
        model.getMainControl().setGame(aGame);
    }

    @Override
    public void initializeWindow(int aWidth, int aHeight, String aTitle) {
        model.getMainControl().initializeWindow(aWidth, aHeight, aTitle);
    }

    @Override
    public void initializePanel(Canvas aParent, String aTitle) {
        model.getMainControl().initializePanel(aParent, aTitle);
    }
    @Override
    public void setViewOrthographic(boolean aOrthoView) {
        model.getMainControl().setViewOrthographic(aOrthoView);
    }
    @Override
    public boolean isViewOrthographic( ) {
       return model.getMainControl().isViewOrthographic();
    }
    @Override
    public void setFieldOfView( float aFieldOfView) {
        model.getMainControl().setFieldOfView(aFieldOfView);
    }
    @Override
    public float getFieldOfView() {
       return model.getMainControl().getFieldOfView();
    }
    @Override
    public void setResolution(int aWidth, int aHeigth){
       model.getMainControl().setResolution(aWidth, aHeigth);
    }
    @Override
    public void setElementState(UUID aId, boolean aIsActive){
        model.getMainControl().setElementState(aId,aIsActive);        
    }
}
