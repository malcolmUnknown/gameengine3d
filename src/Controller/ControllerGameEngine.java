/* GameEngine 3D - A programm to design games with 3D content 
 * Copyright (C) 2016-2056 Frank Küwen
 *  
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package Controller;

import MVC_Interface.Axis;
import MVC_Interface.IControllerDebug;
import MVC_Interface.IControllerGameEngine;
import MVC_Interface.IModel;
import MVC_Interface.IView;
import ProjectCenter.CoreEngine.Game;
import ProjectCenter.CoreEngine.Vector2f;
import ProjectCenter.CoreEngine.Vector3f;
import java.awt.Canvas;
import java.util.Observer;
import java.util.UUID;

public class ControllerGameEngine implements IControllerGameEngine {

    private final IModel model;
    private IView view;

    public ControllerGameEngine(IModel aModel) {
        model = aModel;
    }

// Region: IControllerMain
    @Override
    public void setView(IView aView) {
        view = aView;
    }

    @Override
    public IControllerDebug getDebugControll(){
        return model.getDebugControll();
    }
    @Override
    public void registerMouseObserver(Observer aMouseObserver) {
        model.registerMouseObserver(aMouseObserver);
    }

    @Override
    public void deregisterMouseObserver(Observer aMouseObserver) {
        model.deregisterMouseObserver(aMouseObserver);
    }

    @Override
    public void registerKeyboardObserver(Observer aKeyboardObserver) {
        model.registerKeyboardObserver(aKeyboardObserver);
    }

    @Override
    public void deregisterKeyboardObserver(Observer aKeyboardObserver) {
        model.deregisterKeyboardObserver(aKeyboardObserver);
    }

    @Override
    public void registerStructureObserver(Observer aStructureObserver) {
        model.registerStructureObserver(aStructureObserver);
    }

    @Override
    public void registerStructureObserver(UUID aGameObjectID, Observer aStructureObserver) {
        model.registerStructureObserver(aGameObjectID, aStructureObserver);
    }

    @Override
    public void deregisterStructureObserver(Observer aStructureObserver) {
        model.deregisterStructureObserver(aStructureObserver);
    }

    @Override
    public void deregisterStructureObserver(UUID aGameObjectID, Observer aStructureObserver) {
        model.registerStructureObserver(aGameObjectID, aStructureObserver);
    }
// Region end

// Region: IControllerMain
    @Override
    public void startEngine() {
        if ((view != null) && !isEngineRunning()) {
            model.getMainControl().initializePanel(view.getDispalyCanvas(), "ENGINE");
            model.getMainControl().startEngine();
        }
    }

    @Override
    public void stopEngine() {
        if (isEngineRunning() && (view != null)) {
            model.getMainControl().stopEngine();
        }
    }

    @Override
    public void initializeWindow(int aWidth, int aHeight, String aTitle) {
       if(!model.getMainControl().isEngineRunning())
        model.getMainControl().initializeWindow(aWidth, aHeight, aTitle);
    }

    @Override
    public void initializePanel(Canvas aParent, String aTitle) {
       if(!model.getMainControl().isEngineRunning())
        model.getMainControl().initializePanel(aParent, aTitle);
    }
    @Override
    public void setViewOrthographic(boolean aOrthoView) {
        model.getMainControl().setViewOrthographic(aOrthoView);
    }
    @Override
    public boolean isViewOrthographic( ) {
       return model.getMainControl().isViewOrthographic();
    }
    @Override
    public void setFieldOfView( float aFieldOfView) {
        model.getMainControl().setFieldOfView(aFieldOfView);
    }
    @Override
    public float getFieldOfView() {
       return model.getMainControl().getFieldOfView();
    }

    @Override
    public void setResolution(int aWidth, int aHeigth){
       model.getMainControl().setResolution(aWidth, aHeigth);
    }
    
    @Override
    public void activateKeyboard(boolean aActive) {
        if (model.getMainControl().isKeyboardActive() ^ aActive) {
        model.getMainControl().activateKeyboard(aActive);}
    }

    @Override
    public void activateMouse(boolean aActive) {
        if (model.getMainControl().isMouseActive() ^ aActive) {
            model.getMainControl().activateMouse(aActive);
        }
    }

    @Override
    public boolean isKeyboardActive() {
        return model.getMainControl().isKeyboardActive();
    }

    @Override
    public boolean isMouseActive() {
        return model.getMainControl().isMouseActive();
    }

    @Override
    public boolean isEngineRunning() {
        return model.getMainControl().isEngineRunning();
    }

    @Override
    public void setGame(Game aGame) {
       if(!model.getMainControl().isEngineRunning())
        model.getMainControl().setGame(aGame);
    }

    @Override
    public void setElementState(UUID aId, boolean aIsActive) {
       if(model.getMainControl().isEngineRunning())
        model.getMainControl().setElementState(aId,aIsActive);
    }
// Region end

// Region: IControllerMotion
    @Override
    public void moveVector(Vector3f aVector) {
        model.getMotionControll().moveVector(aVector);
    }
    @Override
    public void move(Axis aDirection, boolean aInPositive, boolean aStartSignal) {
        model.getMotionControll().move(aDirection, aInPositive, aStartSignal);;
    }
    @Override
    public void move(Axis aDirection, boolean aInPositive, float aDistance) {
        model.getMotionControll().move(aDirection, aInPositive, aDistance);;
    }
// Region end

// Region: IControllerRotation
    @Override
    public void rotateVector(Vector3f aVector) {
        model.getRotationControll().rotateVector(aVector);
    }
    @Override
    public void rotate(Axis aDirection, boolean aInPositive, boolean aStartSignal) {
        model.getRotationControll().rotate(aDirection, aInPositive, aStartSignal);
    }
    @Override
    public void rotate(Axis aDirection, boolean aInPositive, float aAngle) {
        model.getRotationControll().rotate(aDirection, aInPositive, aAngle);
    }
// Region end

// Region: IControllerScale
    @Override
    public void scaleVector(Vector3f aVector) {
        model.getScalingControll().scaleVector(aVector);
    }
    @Override
    public void scale(Axis aDirection, boolean aInPositive, boolean aStartSignal) {
        model.getScalingControll().scale(aDirection, aInPositive, aStartSignal);
    }
    
    @Override
    public void scale(Axis aDirection, boolean aInPositive, float aGrowthFactor) {
        model.getScalingControll().scale(aDirection, aInPositive, aGrowthFactor);
    }
// Region end
}