/*
 * Copyright (C) 2016 Malcolm
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package Controller;

import Components.BaseLight;
import Components.Camera;
import Components.ForceObjectComponent;
import Components.FreeMoveController;
import Components.FreeRotateController;
import Components.FreeScaleController;
import Components.GameComponent;
import Components.MeshRenderer;
import Components.PhysicsObjectComponent;
import ProjectCenter.CoreEngine.GameObject;
import java.util.ArrayList;
import java.util.UUID;

/**
 *
 * @author Malcolm
 */
public class StructureNote {

    private UUID identity;
    private ArrayList<StructureNote> childIDs;
    private int motionControllerCount;
    private int rotationControllerCount;
    private int scaleControllerCount;
    private int lightsCount;
    private int renderCount;
    private int physicsCount;
    private int forceCount;
    private int cameraCount;
    private boolean isActiveElement;

    public StructureNote(GameObject aGameObject) {
        identity = aGameObject.getIdentity();
        childIDs = new ArrayList<>();
        physicsCount = 0;
        forceCount = 0;
        rotationControllerCount = 0;
        motionControllerCount = 0;
        renderCount = 0;
        lightsCount = 0;
        cameraCount=0;
        scaleControllerCount =0;
        isActiveElement = aGameObject.getActivationState();

        for (GameComponent gc : aGameObject.getComponents()) {
            if (gc instanceof PhysicsObjectComponent) {
                physicsCount++;
            }
            if (gc instanceof ForceObjectComponent) {
                forceCount++;
            }
            if (gc instanceof FreeRotateController) {
                rotationControllerCount++;
            }
            if (gc instanceof FreeMoveController) {
                motionControllerCount++;
            }
            if (gc instanceof BaseLight) {
                lightsCount++;
            }
            if (gc instanceof MeshRenderer) {
                renderCount++;
            }
            if (gc instanceof Camera) {
                cameraCount++;
            }
            if (gc instanceof  FreeScaleController) {
                scaleControllerCount++;
            }
        }

        for (GameObject go : aGameObject.getChildren()) {
            childIDs.add(new StructureNote(go));
        }
    }

    public StructureNote(StructureNote aNote) {
        identity = aNote.getIdentity();
        physicsCount = aNote.getPhysicsCount();
        forceCount = aNote.getForceCount();
        rotationControllerCount = aNote.getRotationControllerCount();
        motionControllerCount = aNote.getMotionControllerCount();
        renderCount = aNote.getRenderCount();
        lightsCount = aNote.getLightsCount();
        cameraCount = aNote.getCameraCount();
        scaleControllerCount = aNote.getScaleControllerCount();
        isActiveElement = aNote.getIsActiveElement();
        childIDs = new ArrayList<>();
        childIDs.addAll(aNote.childIDs);
    }

    public UUID getIdentity() {
        return identity;
    }

    public int getScaleControllerCount() {
        return scaleControllerCount;
    }

    public int getCameraCount() {
        return cameraCount;
    }

    public ArrayList<StructureNote> getChildIDs() {
        return childIDs;
    }

    public int getMotionControllerCount() {
        return motionControllerCount;
    }

    public int getRotationControllerCount() {
        return rotationControllerCount;
    }

    public int getLightsCount() {
        return lightsCount;
    }

    public int getRenderCount() {
        return renderCount;
    }

    public int getPhysicsCount() {
        return physicsCount;
    }

    public int getForceCount() {
        return forceCount;
    }

    public boolean getIsActiveElement() {
        return isActiveElement;
    }

    public void setIsActiveElement(boolean aIsActiveElement) {
        isActiveElement = aIsActiveElement;
    }
    
    

}
