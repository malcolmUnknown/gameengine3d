/*
 * Copyright (C) 2016 Malcolm
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package Controller;

import MVC_Interface.Axis;
import MVC_Interface.IControllerScale;
import MVC_Interface.IModel;
import ProjectCenter.CoreEngine.Vector2f;
import ProjectCenter.CoreEngine.Vector3f;

/**
 *
 * @author Malcolm
 */
public class ControllerScaling implements IControllerScale {

    private final IModel model;

    public ControllerScaling(IModel aModel) {
        model = aModel;
    }

    @Override
    public void scaleVector(Vector3f aVector) {
        model.getScalingControll().scaleVector(aVector);
    }

    @Override
    public void scale(Axis aDirection, boolean aInPositive, boolean aStartSignal) {
        model.getScalingControll().scale(aDirection, aInPositive, aStartSignal);
    }

    @Override
    public void scale(Axis aDirection, boolean aInPositive, float aAngle) {
        model.getScalingControll().scale(aDirection, aInPositive, aAngle);
    }

}
