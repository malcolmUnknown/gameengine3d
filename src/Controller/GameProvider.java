/*
 * Copyright (C) 2016 Malcolm
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package Controller;

import ProjectCenter.CoreEngine.Game;
import ProjectCenter.Game.PhysicsGame;
import ProjectCenter.Game.TestGame;
import java.util.Dictionary;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.UUID;

/**
 *
 * @author Malcolm
 */
public class GameProvider {

    private final Hashtable<UUID, String> gamesLibrary;
    private final Hashtable<UUID, Game> games;
    private Game game;

    public GameProvider() {
        gamesLibrary = new Hashtable<>();
        games = new Hashtable<>();
        fillDictionary();
    }

    private void fillDictionary() {
        game = new TestGame();
        games.put(game.getUUID(), game);
        gamesLibrary.put(game.getUUID(), game.getName());

        game = new PhysicsGame();
        games.put(game.getUUID(), game);
        gamesLibrary.put(game.getUUID(), game.getName());
    }

    public void addToProvider(Game aGame) {
        games.put(aGame.getUUID(), aGame);
        gamesLibrary.put(aGame.getUUID(), aGame.getName());
    }

    public void removeFromProvider(Game aGame) {
        if (gamesLibrary.containsKey(aGame.getUUID()) && games.containsKey(aGame.getUUID())) {
            gamesLibrary.remove(aGame.getUUID());
            games.remove(aGame.getUUID());
        }
    }

    public Dictionary getGames() {
        return gamesLibrary;

    }

    public Game getGame(UUID aID) {
        if (games.containsKey(aID)) {
            return games.get(aID);
        } else {
            return null;
        }
    }

    public Game getGame(String aGameName) {
        Game foundGame;
        if (gamesLibrary.containsValue(aGameName)) {
            Enumeration gameEnum = games.keys();
            while (gameEnum.hasMoreElements()) {
                foundGame = games.get(UUID.fromString(gameEnum.nextElement().toString()));
                if ((foundGame != null) && foundGame.getName().equalsIgnoreCase(aGameName)) {
                    return foundGame;
                }
            }
            return null;
        } else {
            return null;
        }
    }
}
