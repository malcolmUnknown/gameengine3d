/* GameEngine 3D - A programm to design games with 3D content 
 * Copyright (C) 2016-2056 Frank Küwen
 *  
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package Depricated;

import ProjectCenter.CoreEngine.Precision;
import ProjectCenter.CoreEngine.Vector3f;
import ProjectCenter.PhysicsEngine.ColliderTypes;
import ProjectCenter.PhysicsEngine.IntersectData;

public class SectionPlane extends Collider {

    private Vector3f normal;
    private float distance;
    private Vector3f position;

    public SectionPlane(Vector3f aNormal, Vector3f aPosition) {
        super(ColliderTypes.ColliderPlane.ordinal());
        normal = aNormal;
        position = aPosition;
        distance = aNormal.dot(aPosition);
    }
    
    public SectionPlane Normalized()
    {
        float lMagnitude = normal.length();
        return new SectionPlane(normal.div(lMagnitude),position);// distance/lMagnitude
    }
    
    public IntersectData IntersectSphere( BoundingSphere aSphere)
    {
        float lDistanceFromCenter = Math.abs(aSphere.getPosition().dot(normal)-distance);
        float lDistanceFromSphere = lDistanceFromCenter -aSphere.getRadius();
        
        return new IntersectData(Precision.less(lDistanceFromSphere,0), normal.mul(lDistanceFromSphere));
    }

    public Vector3f getNormal() {
        return normal;
    }

    public float getDistance() {
        return distance;
    }
    
    @Override
    public void transform(Vector3f aTranslation)
    {
        position = position.add(aTranslation);
        distance = normal.dot(position);
    }
    @Override
    public Vector3f getPosition() {
        return position;
    }
}
