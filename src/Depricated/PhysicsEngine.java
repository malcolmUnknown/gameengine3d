/* GameEngine 3D - A programm to design games with 3D content 
 * Copyright (C) 2016-2056 Frank Küwen
 *  
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package Depricated;

import ProjectCenter.CoreEngine.Precision;
import ProjectCenter.CoreEngine.Vector3f;
import ProjectCenter.PhysicsEngine.IntersectData;
import java.util.ArrayList;

public class PhysicsEngine {

    private ArrayList<PhysicsObject> objects;

    //TEMP DEBUG-VALUEs
    public static boolean debugBreak = false;

    public PhysicsEngine() {
        objects = new ArrayList<PhysicsObject>();
    }

    public void addObject(PhysicsObject aObject) {
        objects.add(aObject);
    }

    public void simulate(float aTimeDelta) {

        //TEMP DEBUG-VALUEs
        if (!debugBreak) {
            for (int i = 0; i < objects.size(); i++) {
                objects.get(i).integrate(aTimeDelta);
            }
        }
    }

    public PhysicsObject getObjects(int i) {
        return objects.get(i);
    }

    public int getCount() {
        return objects.size();
    }

    public void handleCollision() {
        for (int i = 0; i < objects.size(); i++) {
            for (int j = i + 1; j < objects.size(); j++) {
                IntersectData lIntersect = objects.get(i).getCollider().intersect(objects.get(j).getCollider());
                if (lIntersect.isIntersecting()) {
                    //Tutorial
//                    Vector3f lDirection = lIntersect.getDirection().normalized();
//                    Vector3f lDirection2 = reflect(lDirection, objects.get(i).getVelocity().normalized());

//                    //Correction of Contact Point
//                    Vector3f lRetrace = (objects.get(j).getVelocity().sub(objects.get(i).getVelocity())).div(2);
//                    Vector3f lDisplace = lIntersect.getDirection();
//                    float lSolve = lRetrace.dot(lDisplace) / (2 * lRetrace.dot(lRetrace));
//                    lSolve = -lSolve + (float) Math.sqrt(lSolve * lSolve - (lDisplace.dot(lDisplace) - 1) / (lRetrace.dot(lRetrace)));
//                    Vector3f P1 = objects.get(i).getPosition().add(objects.get(i).getVelocity().mul(-lSolve));
//                    Vector3f P2 = objects.get(j).getPosition().add(objects.get(j).getVelocity().mul(-lSolve));
//                    lDisplace = (P2.sub(P1)).normalized();
                    //Tutorial
//                    objects.get(i).setVelocity(reflect(objects.get(i).getVelocity(), (lDisplace)));
//                    objects.get(j).setVelocity(reflect(objects.get(j).getVelocity(), (lDisplace)));
                    float m1 = 1;
                    float m2 = 1;
                    Vector3f lV1 = objects.get(i).getVelocity();
                    Vector3f lV2 = objects.get(j).getVelocity();

                    Vector3f lCenterV = (lV1.mul(m1).add(lV2.mul(m2))).div(m1 + m2);
                    Vector3f lNormal = lIntersect.getDirection();//.normalized();
                    float lU1n = lNormal.getParameter(lCenterV.mul(2).sub(lV1));
                    float lU2n = lNormal.getParameter(lCenterV.mul(2).sub(lV2));
                    float lV1n = lNormal.getParameter(lV1);
                    float lV2n = lNormal.getParameter(lV2);
                    objects.get(i).setVelocity(lNormal.mul(lU1n).add(lV1.sub(lNormal.mul(lV1n))));
                    objects.get(j).setVelocity(lNormal.mul(lU2n).add(lV2.sub(lNormal.mul(lV2n))));
                }
            }
        }
    }

//Tutorial
    private Vector3f reflect(Vector3f aVector, Vector3f aNormal) {
        return aVector.sub(aNormal.mul(2 * aVector.dot(aNormal)));
    }

    private Vector3f getContactPoint(Vector3f aP1, Vector3f aVelo1, Vector3f aP2, Vector3f aVelo2, Vector3f aIntersectingDirection) {
        Vector3f lRetrace = (aVelo2.sub(aVelo1)).div(2);
        float lSolve = lRetrace.dot(aIntersectingDirection) / (2 * lRetrace.dot(lRetrace));
        lSolve = -lSolve + (float) Math.sqrt(lSolve * lSolve - (aIntersectingDirection.dot(aIntersectingDirection) - 1) / (lRetrace.dot(lRetrace)));
        Vector3f P1 = aP1.add(aVelo1.mul(-lSolve));
        Vector3f P2 = aP2.add(aVelo2.mul(-lSolve));
        return (P2.sub(P1)).normalized();
    }

}
