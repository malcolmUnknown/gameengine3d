/* GameEngine 3D - A programm to design games with 3D content 
 * Copyright (C) 2016-2056 Frank Küwen
 *  
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package Depricated;

import Components.GameComponent;
import ProjectCenter.CoreEngine.CoreEngine;
import ProjectCenter.CoreEngine.Precision;
import ProjectCenter.CoreEngine.Vector2f;
import ProjectCenter.CoreEngine.Vector3f;

public class FreeLook extends GameComponent {

    public static final Vector3f yAxis = new Vector3f(0, 1, 0);
    private boolean mouseLocked = false;
    private Vector2f centerPosition = new Vector2f(CoreEngine.getWidth() / 2, CoreEngine.getHeight() / 2.f);
    private float sensitivity = 0.5f;
    private int unlockMouseKey ;
   
    public FreeLook(float aSensitivity) {
        this(aSensitivity, Input.KEY_ESCAPE);
    }
    public FreeLook(float aSensitivity, int aUnlockMouseKey) {
        sensitivity = aSensitivity;
        unlockMouseKey = aUnlockMouseKey;
    }
    @Override
    public void input(float aDelta) {

        
        if (Input.getMouseDown(0)) {
            Input.setMousePosition(centerPosition);
            Input.setCursor(false);
            mouseLocked = true;
        }
        if (mouseLocked && (Input.getKey(unlockMouseKey) || Input.getMouseUp(1))) {
            Input.setCursor(true);
            mouseLocked = false;
        }
        if (mouseLocked) {
            Vector2f Differential = Input.getMousePosition().sub(centerPosition);

            boolean rotateY = !Precision.equals(Differential.getX(), 0);
            boolean rotateX = !Precision.equals(Differential.getY(), 0);

            if (rotateY)
                getTransform().rotate(getTransform().getRotation().getUp(), (float) Math.toRadians(Differential.getX() * sensitivity));
            if (rotateX)
                getTransform().rotate(getTransform().getRotation().getRight(), (float) Math.toRadians(Differential.getY() * sensitivity));

            if (rotateX || rotateY)
                Input.setMousePosition(new Vector2f(CoreEngine.getWidth() / 2, CoreEngine.getHeight() / 2));
        }
    }
}
