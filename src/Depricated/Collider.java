/* GameEngine 3D - A programm to design games with 3D content 
 * Copyright (C) 2016-2056 Frank Küwen
 *  
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package Depricated;

import ProjectCenter.CoreEngine.ReferenceCounter;
import ProjectCenter.CoreEngine.Vector3f;
import ProjectCenter.PhysicsEngine.ColliderTypes;
import ProjectCenter.PhysicsEngine.IntersectData;

public class Collider extends ReferenceCounter{

    private int colliderType;

    public Collider(int aType) {
        super();
        colliderType = aType;
        
    }

    public int getType() {
        return colliderType;
    }

    public IntersectData intersect(Collider aOther) {
        if ((colliderType == aOther.colliderType) && (colliderType == ColliderTypes.ColliderSphere.ordinal())) {
            BoundingSphere self = (BoundingSphere) this;
            return self.IntersectBoundingSphere((BoundingSphere) aOther);
        } 
        if ((aOther.colliderType == ColliderTypes.ColliderPlane.ordinal()) && (colliderType == ColliderTypes.ColliderSphere.ordinal())) {
            SectionPlane lPlane = (SectionPlane) aOther;
            return lPlane.IntersectSphere((BoundingSphere) this) ;
        } 
        if ((colliderType == ColliderTypes.ColliderPlane.ordinal()) && (aOther.colliderType == ColliderTypes.ColliderSphere.ordinal())) {
            SectionPlane lPlane = (SectionPlane) this;
            return lPlane.IntersectSphere((BoundingSphere) aOther) ;
        }else {
            System.out.println("Error Collisions not implemented between the specified colliders.");
            new Exception().printStackTrace();
            System.exit(1);
            return new IntersectData(false, new Vector3f());
        }
    }

    public void transform(Vector3f aTranslation) {
    }
    
    public Vector3f getPosition() {
        return new Vector3f();
    }
}
