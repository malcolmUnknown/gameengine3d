/* GameEngine 3D - A programm to design games with 3D content 
 * Copyright (C) 2016-2056 Frank Küwen
 *  
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package Depricated;

import Components.GameComponent;
import ProjectCenter.CoreEngine.Vector3f;
import ProjectCenter.PhysicsEngine.PhysicsObject;

public class FreeMove extends GameComponent {

    private int forwardKey;
    private int backwardKey;
    private int leftKey;
    private int rightKey;
    private int upKey = Input.KEY_Y;
    private int downKey = Input.KEY_C;
    private float speed;

    public FreeMove(float aSpeed) {
        this(aSpeed, Input.KEY_W, Input.KEY_S, Input.KEY_A, Input.KEY_D);
    }

    public FreeMove(float aSpeed, int aForwardKey, int aBackwardKey, int aLeftKey, int aRightKey) {

        forwardKey = aForwardKey;
        backwardKey = aBackwardKey;
        leftKey = aLeftKey;
        rightKey = aRightKey;
        speed = aSpeed;
    }

    @Override
    public void input(float aDelta) {
        float moveAmplitude = (speed * aDelta);

        if (Input.getKey(forwardKey)) {
            movePosition(getTransform().getRotation().getForward().mul(moveAmplitude));
        }
        if (Input.getKey(backwardKey)) {
            movePosition(getTransform().getRotation().getForward().mul(moveAmplitude * -1));
        }
        if (Input.getKey(leftKey)) {
            movePosition(getTransform().getRotation().getRight().mul(moveAmplitude * -1));
        }
        if (Input.getKey(rightKey)) {
            movePosition(getTransform().getRotation().getRight().mul(moveAmplitude));
        }
        if (Input.getKey(upKey)) {
            movePosition(getTransform().getRotation().getUp().mul(moveAmplitude));
        }
        if (Input.getKey(downKey)) {
            movePosition(getTransform().getRotation().getUp().mul(moveAmplitude*-1));
        }
    }

    public void movePosition(Vector3f aDirection) {
         getTransform().setPosition(getTransform().getPosition().add(aDirection));
    }
}
