/* GameEngine 3D - A programm to design games with 3D content 
 * Copyright (C) 2016-2056 Frank Küwen
 *  
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package Depricated;

import Depricated.Collider;
import ProjectCenter.CoreEngine.Transform;
import ProjectCenter.CoreEngine.Vector3f;

public class PhysicsObject {

    private Vector3f position;
    private Vector3f velocity;
    private Vector3f oldPosition;
    private Collider collider;

    public PhysicsObject(Collider aCollider, Vector3f aVelocity) {
        position = aCollider.getPosition();
        velocity = aVelocity;
        oldPosition = position;
        collider = aCollider;
    }

    public PhysicsObject(PhysicsObject aOther) {
        position = aOther.getPosition();
        velocity = aOther.getVelocity();
        oldPosition = position;
        collider = aOther.getCollider();
        collider.addReference();
    }

    protected void finalize() {
        collider.removeReference();
    }

    public void integrate(float aTimeDelta) {
        position = position.add(velocity.mul(aTimeDelta));
    }

    public Vector3f getPosition() {
        return position;
    }

    public Vector3f getVelocity() {
        return velocity;
    }

    public void setVelocity(Vector3f aVelocity) {
        velocity = aVelocity;
    }

    public Collider getCollider() {
        Vector3f lTransform = position.sub(oldPosition);
        oldPosition = position;
        collider.transform(lTransform);
        return collider;
    }

}
