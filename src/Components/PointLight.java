/* GameEngine 3D - A programm to design games with 3D content 
 * Copyright (C) 2016-2056 Frank Küwen
 *  
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package Components;

import ProjectCenter.CoreEngine.Vector3f;
import ProjectCenter.RenderEngine.Attenuation;
import ProjectCenter.RenderEngine.Shader;

public class PointLight extends BaseLight {

    private static final int COLOR_DEPTH = 256;
    private final Attenuation attenuation;
    private float range;

    public PointLight(Vector3f aColor, float aIntensity, Attenuation aAttenuation) {
        super(aColor, aIntensity);

        this.attenuation = aAttenuation;
        float a = aAttenuation.getExponent();
        float b = aAttenuation.getLinear();
        float c = aAttenuation.getConstant();
        c -= getColor().max() * COLOR_DEPTH * getIntensity();
        //I = a * x^2 + b * x + c
        this.range = b / (2 * a) * (float) (-1 + Math.sqrt(1 - 4 * a * c / (b * b)));

        setShader(new Shader("forward-point"));
    }

    public float getRange() {
        return this.range;
    }

    public void setRange(float aRange) {
        this.range = aRange;
    }

    public Attenuation getAttenuation() {
        return attenuation;
    }

}
