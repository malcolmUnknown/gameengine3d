/* GameEngine 3D - A programm to design games with 3D content 
 * Copyright (C) 2016-2056 Frank Küwen
 *  
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package Components;

import Controller.CommandMouse;
import Controller.CommandNote;
import Controller.CommandSource;
import ProjectCenter.CoreEngine.CoreEngine;
import ProjectCenter.CoreEngine.Matrix4f;
import ProjectCenter.CoreEngine.Precision;
import ProjectCenter.CoreEngine.Vector3f;
import java.util.Observable;
import java.util.Observer;

public class Camera extends GameComponent implements Observer {

    private final Matrix4f projection;
    private float aspectRatio;
    private final float nearPlane;
    private final float farPlane;
    private float fieldOfView;
    private boolean viewModeOrtho;

    public Camera(boolean aIsOrthoView, float aFieldOfView, float aAspectRatio, float aNearZ, float aFarZ) {
        aspectRatio = aAspectRatio;
        nearPlane = aNearZ;
        farPlane = aFarZ;
        fieldOfView = aFieldOfView;
        viewModeOrtho = aIsOrthoView;
        projection = new Matrix4f();
        setProjectionMatrix();
    }

    private void setProjectionMatrix() {

        if (viewModeOrtho) {
            float lWidth = (float) Math.abs(Math.tan(fieldOfView) * (nearPlane + farPlane) / 2.f) / 2.f;
            projection.setM(new Matrix4f().initOrthographic(-lWidth, lWidth, -lWidth / aspectRatio, lWidth / aspectRatio, nearPlane, farPlane).getM());
        } else {
            projection.setM(new Matrix4f().initPerspective(fieldOfView, aspectRatio, nearPlane, farPlane).getM());
        }
    }

    public Matrix4f getViewProjection() {
        Matrix4f cameraRotation = getTransform().getTransformedRotation().toRotationMatrix();
        Vector3f cameraPosition = getTransform().getTransformedPosition().mul(-1);

        Matrix4f cameraTranslation
                = new Matrix4f().initTranslation(cameraPosition);
        return projection.mul(cameraRotation.mul(cameraTranslation));
    }

    @Override
    public void addToEngine(CoreEngine aEngine) {
        aEngine.getRenderingEngine().addCamera(this);
    }

    @Override
    public void update(Observable o, Object o1) {
        if (o1.getClass() == CommandNote.class) {
            CommandNote lNotice = (CommandNote) o1;
            if ((lNotice.getType() == CommandSource.Mouse)) {
                if (lNotice.getCommand() == CommandMouse.MouseWheelTurn.ordinal()) {
                    setViewMode(lNotice.getCommandState());
                    setFieldOfView(lNotice.getAmount());
                }
            }
        }
    }

    public void setViewMode(boolean aIsOrthographic) {
        if (viewModeOrtho != aIsOrthographic) {
            viewModeOrtho = aIsOrthographic;
            setProjectionMatrix();
        }
    }

    public void setFieldOfView(float aFieldOfView) {
        if (!Precision.equals(fieldOfView, aFieldOfView)) {
            fieldOfView = aFieldOfView;
            setProjectionMatrix();
        }
    }
    public float getFieldOfView() {
        return fieldOfView;
    }
    public boolean getViewMode() {
        return viewModeOrtho;
    }

    public void setAspectRatio(int aWidth, int aHeight) {
        float lAspect = (float) aWidth / (float) aHeight;
        if (!Precision.equals(aspectRatio, lAspect)) {
            aspectRatio = lAspect;
            setProjectionMatrix();
        }
    }

}
