/* GameEngine 3D - A programm to design games with 3D content 
 * Copyright (C) 2016-2056 Frank Küwen
 *  
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package Components;

import ProjectCenter.CoreEngine.Vector3f;
import ProjectCenter.RenderEngine.Shader;

public class DirectionalLight extends BaseLight{


    public DirectionalLight(Vector3f aColor, float aIntensity) {
        super(aColor, aIntensity);
        setShader(new Shader("forward-directional"));
    }

    public Vector3f getDirection() {
        return getTransform().getTransformedRotation().getForward();
    }

}
