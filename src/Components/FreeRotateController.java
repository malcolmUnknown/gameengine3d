/* GameEngine 3D - A programm to design games with 3D content 
 * Copyright (C) 2016-2056 Frank Küwen
 *  
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package Components;

import MVC_Interface.Axis;
import ProjectCenter.CoreEngine.CoreEngine;
import ProjectCenter.CoreEngine.Precision;
import ProjectCenter.CoreEngine.Vector2f;
import ProjectCenter.CoreEngine.Vector3f;

public class FreeRotateController extends GameComponent {

    private float sensitivity = 10;

    private final boolean[] currentSide = new boolean[2];
    private final boolean[] currentTop = new boolean[2];
    private final boolean[] currentFront = new boolean[2];
    private final Vector3f rotationAngle = new Vector3f();
    private Vector3f currentRotate = new Vector3f();

    public FreeRotateController(float aSensitivity) {
        sensitivity = aSensitivity;
        for (int i = 0; i < 2; i++) {
            currentSide[i] = false;
            currentTop[i] = false;
            currentFront[i] = false;
        }
        currentRotate = new Vector3f();
    }

    @Override
    public void input(float aDelta) {
        float lDeltaAngle = (aDelta * sensitivity );
        float lAngle;

        if (currentFront[0] ^ currentFront[1]) {
            lAngle = !Precision.equals(rotationAngle.getZ(), 0) ? rotationAngle.getZ() : lDeltaAngle;
            if (currentFront[1]) {
                lAngle = lAngle * (-1);
            }
            getTransform().rotate(getTransform().getRotation().getForward(), (float) Math.toRadians(lAngle));
            if (!Precision.equals(rotationAngle.getZ(), 0)) {
                rotationAngle.setZ(0);
                currentFront[0] = false;
                currentFront[1] = false;
            }
        }

        if (currentTop[0] ^ currentTop[1]) {
            lAngle = !Precision.equals(rotationAngle.getY(), 0) ? rotationAngle.getY() : lDeltaAngle;
            if (currentTop[1]) {
                lAngle = lAngle * (-1);
            }
            getTransform().rotate(getTransform().getRotation().getUp(), (float) Math.toRadians(lAngle));

            if (!Precision.equals(rotationAngle.getY(), 0)) {
                rotationAngle.setY(0);
                currentTop[0] = false;
                currentTop[1] = false;
            }
        }

        if (currentSide[0] ^ currentSide[1]) {
            lAngle = !Precision.equals(rotationAngle.getX(), 0) ? rotationAngle.getX() : lDeltaAngle;
            if (currentSide[1]) {
                lAngle = lAngle * (-1);
            }
            getTransform().rotate(getTransform().getRotation().getRight(), (float) Math.toRadians(lAngle));

            if (!Precision.equals(rotationAngle.getX(), 0)) {
                rotationAngle.setX(0);
                currentSide[0] = false;
                currentSide[1] = false;
            }
        }
        
        if (!currentRotate.equals(Vector3f.ZEROVECTOR)) {
            Vector3f lDifferential = currentRotate.mul((float) Math.PI / 2 * sensitivity);

            boolean rotateUpside = !Precision.equals(lDifferential.getX(), 0);
            boolean rotateSideways = !Precision.equals(lDifferential.getY(), 0);

            if (rotateUpside) {
                getTransform().rotate(getTransform().getRotation().getUp(), (float) Math.toRadians(lDifferential.getX() * sensitivity));
            }
            if (rotateSideways) {
                getTransform().rotate(getTransform().getRotation().getRight(), (float) Math.toRadians(lDifferential.getY() * sensitivity));
            }
            currentRotate = new Vector3f();
        }

    }

    public void setRotationVector(Vector3f aVector) {
        currentRotate = currentRotate.clone(aVector);
    }

    public void setAxisRotation(Axis aDirection, boolean aInPositive, boolean aStartSignal) {
        int i = aInPositive ? 0 : 1;
        switch (aDirection) {
            case Front: {
                currentFront[i] = aStartSignal;
                break;
            }
            case Top: {
                currentTop[i] = aStartSignal;
                break;
            }
            case Side: {
                currentSide[i] = aStartSignal;
                break;
            }
        }
    }

    public void setAxisRotation(Axis aDirection, boolean aInPositive, float aAngle) {
        int i = aInPositive ? 0 : 1;
        switch (aDirection) {
            case Front: {
                currentFront[i] = true;
                rotationAngle.setZ(aAngle);
                break;
            }
            case Top: {
                currentTop[i] = true;
                rotationAngle.setY(aAngle);
                break;
            }
            case Side: {
                currentSide[i] = true;
                rotationAngle.setX(aAngle);
                break;
            }
        }
    }

    @Override
    public void addToEngine(CoreEngine aEngine) {
        aEngine.addRotationControll(getIdentity(), this);
    }

}
