/* GameEngine 3D - A programm to design games with 3D content 
 * Copyright (C) 2016-2056 Frank Küwen
 *  
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package Components;

import MVC_Interface.Axis;
import ProjectCenter.CoreEngine.CoreEngine;
import ProjectCenter.CoreEngine.Precision;
import ProjectCenter.CoreEngine.Vector2f;
import ProjectCenter.CoreEngine.Vector3f;

public class FreeMoveController extends GameComponent {

    private final float speed;
    private final boolean[] currentSide = new boolean[2];
    private final boolean[] currentTop = new boolean[2];
    private final boolean[] currentFront = new boolean[2];
    private final Vector3f motionDistance = new Vector3f();
    private Vector3f currentMove = new Vector3f();

    public FreeMoveController(float aSpeed) {
        speed = aSpeed;
        for (int i = 0; i < 2; i++) {
            currentSide[i] = false;
            currentTop[i] = false;
            currentFront[i] = false;
        }
        currentMove = new Vector3f();
    }

    @Override
    public void input(float aDelta) {
        float lDeltaDistance = (speed * aDelta);
        float lDistance;

        if (currentFront[0] ^ currentFront[1]) {
            lDistance = !Precision.equals(motionDistance.getZ(), 0) ? motionDistance.getZ() : lDeltaDistance;
            if (currentFront[1]) {
                lDistance = lDistance * (-1);
            }
            movePosition(getTransform().getRotation().getForward().mul(lDistance));
            if (!Precision.equals(motionDistance.getZ(), 0)) {
                motionDistance.setZ(0);
                currentFront[0] = false;
                currentFront[1] = false;
            }
        }
        if (currentTop[0] ^ currentTop[1]) {
            lDistance = !Precision.equals(motionDistance.getY(), 0) ? motionDistance.getY() : lDeltaDistance;
            if (currentTop[1]) {
                lDistance = lDistance * (-1);
            }
            movePosition(getTransform().getRotation().getUp().mul(lDistance));
            if (!Precision.equals(motionDistance.getY(), 0)) {
                motionDistance.setY(0);
                currentTop[0] = false;
                currentTop[1] = false;
            }
        }
        if (currentSide[0] ^ currentSide[1]) {
            lDistance = !Precision.equals(motionDistance.getX(), 0) ? motionDistance.getX() : lDeltaDistance;
            if (currentSide[1]) {
                lDistance = lDistance * (-1);
            }
            movePosition(getTransform().getRotation().getRight().mul(lDistance));
            if (!Precision.equals(motionDistance.getX(), 0)) {
                motionDistance.setX(0);
                currentSide[0] = false;
                currentSide[1] = false;
            }
        }
        
        if (!currentMove.equals(Vector3f.ZEROVECTOR)) {
            movePosition(getTransform().getRotation().getUp().mul(currentMove.getY() * CoreEngine.getHeight()));
            movePosition(getTransform().getRotation().getRight().mul(currentMove.getX() * CoreEngine.getWidth()));
            currentMove = new Vector3f();
        }

    }
    
    public void setMotionVector(Vector3f aVector) {
        currentMove = currentMove.clone(aVector);
    }

    public void setMotion(Axis aDirection, boolean aInPositive, boolean aStartSignal) {
        int i = aInPositive ? 0 : 1;
        switch (aDirection) {
            case Front: {
                currentFront[i] = aStartSignal;
                break;
            }
            case Top: {
                currentTop[i] = aStartSignal;
                break;
            }
            case Side: {
                currentSide[i] = aStartSignal;
                break;
            }
        }
    }

    public void setMotion(Axis aDirection, boolean aInPositive, float aMotionDistance) {
        int i = aInPositive ? 0 : 1;
        switch (aDirection) {
            case Front: {
                currentFront[i] = true;
                motionDistance.setZ(aMotionDistance);
                break;
            }
            case Top: {
                currentTop[i] = true;
                motionDistance.setY(aMotionDistance);
                break;
            }
            case Side: {
                currentSide[i] = true;
                motionDistance.setX(aMotionDistance);
                break;
            }
        }
    }

    public void movePosition(Vector3f aDirection) {
        getTransform().setPosition(getTransform().getPosition().add(aDirection));
    }

    public void addToEngine(CoreEngine aEngine) {
        aEngine.addMotionControll(getIdentity(), this);
    }
}
