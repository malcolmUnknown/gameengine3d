/*
 * Copyright (C) 2016 Malcolm
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package Components;

import MVC_Interface.Axis;
import ProjectCenter.CoreEngine.CoreEngine;
import ProjectCenter.CoreEngine.Precision;
import ProjectCenter.CoreEngine.Vector2f;
import ProjectCenter.CoreEngine.Vector3f;

/**
 *
 * @author Malcolm
 */
public class FreeScaleController extends GameComponent {

    private final boolean[] currentSide = new boolean[2];
    private final boolean[] currentTop = new boolean[2];
    private final boolean[] currentFront = new boolean[2];
    private final Vector3f scaleFactor = new Vector3f();

    private Vector3f currentScale = new Vector3f();
    private float sensitivity = 0.5f;

    public FreeScaleController(float aSensitivity) {
        sensitivity = aSensitivity;
        for (int i = 0; i < 2; i++) {
            currentSide[i] = false;
            currentTop[i] = false;
            currentFront[i] = false;
        }
        currentScale = new Vector3f();
    }

    @Override
    public void input(float aDelta) {
        float lDeltaAmplitude = (aDelta * sensitivity);
        float lAmplitude;
        Vector3f lScaling = new Vector3f();
        boolean lDoScaling = false;

        if (currentFront[0] ^ currentFront[1]) {
            lAmplitude = !Precision.equals(scaleFactor.getZ(), 0) ? (1 + scaleFactor.getZ()) : (1 + lDeltaAmplitude);
            lDoScaling = true;
            if (currentFront[1]) {
                lAmplitude = lAmplitude * (-1);
            }
            if (!Precision.equals(scaleFactor.getZ(), 0)) {
                scaleFactor.setZ(0);
                currentFront[0] = false;
                currentFront[1] = false;
            }
        } else {
            lAmplitude = 1;
        }
        lScaling = lScaling.add(getTransform().getRotation().getForward().mul(lAmplitude));

        if (currentTop[0] ^ currentTop[1]) {
            lAmplitude = !Precision.equals(scaleFactor.getY(), 0) ? (1 + scaleFactor.getY()) : (1 + lDeltaAmplitude);
            lDoScaling = true;
            if (currentTop[1]) {
                lAmplitude = lAmplitude * (-1);
            }
            if (!Precision.equals(scaleFactor.getY(), 0)) {
                scaleFactor.setY(0);
                currentTop[0] = false;
                currentTop[1] = false;
            }
        } else {
            lAmplitude = 1;
        }
        lScaling = lScaling.add(getTransform().getRotation().getUp().mul(lAmplitude));

        if (currentSide[0] ^ currentSide[1]) {
            lAmplitude = !Precision.equals(scaleFactor.getX(), 0) ? (1 + scaleFactor.getX()) : (1 + lDeltaAmplitude);
            lDoScaling = true;
            if (currentSide[1]) {
                lAmplitude = lAmplitude * (-1);
            }
            if (!Precision.equals(scaleFactor.getX(), 0)) {
                scaleFactor.setX(0);
                currentSide[0] = false;
                currentSide[1] = false;
            }
        } else {
            lAmplitude = 1;
        }
        lScaling = lScaling.add(getTransform().getRotation().getRight().mul(lAmplitude));

        if (!lDoScaling) {
            getTransform().setScale(getTransform().getScale().mul(lScaling));
        }
        
        if (!currentScale.equals(Vector3f.ZEROVECTOR)) {

            getTransform().setScale(getTransform().getScale().mul(currentScale.add(new Vector3f(1, 1, 1))));
            currentScale = new Vector3f();
        }

    }

    public void setAxisScaling(Axis aDirection, boolean aInPositive, boolean aStartSignal) {
        int i = aInPositive ? 0 : 1;
        switch (aDirection) {
            case Front: {
                currentFront[i] = aStartSignal;
                break;
            }
            case Top: {
                currentTop[i] = aStartSignal;
                break;
            }
            case Side: {
                currentSide[i] = aStartSignal;
                break;
            }
        }
    }

    public void setAxisScaling(Axis aDirection, boolean aInPositive, float aGrowthFactor) {
        int i = aInPositive ? 0 : 1;
        switch (aDirection) {
            case Front: {
                currentFront[i] = true;
                scaleFactor.setZ(aGrowthFactor);
                break;
            }
            case Top: {
                currentTop[i] = true;
                scaleFactor.setY(aGrowthFactor);
                break;
            }
            case Side: {
                currentSide[i] = true;
                scaleFactor.setX(aGrowthFactor);
                break;
            }
        }
    }

    public void setScalingVector(Vector3f aVector) {
        currentScale = currentScale.clone(aVector);
    }

    @Override
    public void addToEngine(CoreEngine aEngine) {
        aEngine.addScaleControll(getIdentity(), this);
    }
}
