/* GameEngine 3D - A programm to design games with 3D content 
 * Copyright (C) 2016-2056 Frank Küwen
 *  
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package View;

import Controller.CommandMouse;
import Controller.CommandNote;
import Controller.CommandSource;
import Controller.GameProvider;
import Controller.StructureNote;
import MVC_Interface.Axis;
import MVC_Interface.IControllerDebug;
import MVC_Interface.IControllerGameEngine;
import MVC_Interface.IView;
import ProjectCenter.CoreEngine.Game;
import ProjectCenter.CoreEngine.Vector3f;
import java.awt.Canvas;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLayeredPane;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.*;
import java.awt.*;
import java.awt.event.HierarchyBoundsListener;
import java.awt.event.HierarchyEvent;
import java.awt.event.KeyEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.Observable;
import java.util.Observer;
import java.util.UUID;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreeNode;
import javax.swing.tree.TreePath;
import javax.swing.tree.TreeSelectionModel;

public class ViewGameEngine implements ActionListener, Observer, IView, TreeSelectionListener {

    private JFrame jGUI;
    private Canvas canvasDisplay;
    private JButton jButton1;
    private JButton jButton2;
    private JButton jButtonSelect;
    private JLayeredPane jLayeredPane_3D;
    private JPanel jPanelInfo;
    private JMenuBar jMenuBar_Main;
    private JMenu jMenuFile;
    private JMenu jMenuEdit;
    private JMenuItem jMenuItemStart;
    private JMenuItem jMenuItemStop;
    private JRadioButton jRadioGame;
    private JComboBox jComboBox;
    private JTree jTreeStructure;
    private JScrollPane jTreeViewPane;
    private JScrollPane jInfoViewPane;
    private JSplitPane jSplitInfoPane;
    private JSplitPane jSplitContentPane;
    private JTextArea jInfoText;
    private DefaultMutableTreeNode jRootNode;
    private boolean keyAlternate;

    private final IControllerGameEngine controller;
    private final IControllerDebug debugger;
    private final ArrayList<StructureNote> identityList;
    private final ArrayList<UUID> activeElements;

    private String selectedGame = "";
    private final GameProvider gameProvider;

    public ViewGameEngine(IControllerGameEngine aController) {
        keyAlternate = false;
        activeElements = new ArrayList<>();
        controller = aController;
        debugger = controller.getDebugControll();

        gameProvider = new GameProvider();
        identityList = new ArrayList<>();
        setView();
        registerObserver();
    }

    private void setView() {
        controller.setView(this);
    }

    private void registerObserver() {
        controller.registerMouseObserver(this);
        controller.registerKeyboardObserver(this);
        controller.registerStructureObserver(this);
    }

    public void initialize() {
        createComponents();
        try {
            canvasDisplay = new Canvas() {
                @Override
                public final void addNotify() {
                    super.addNotify();
                }

                @Override
                public final void removeNotify() {
                    controller.stopEngine();
                    super.removeNotify();
                }
            };

            jLayeredPane_3D.add(canvasDisplay, java.awt.BorderLayout.CENTER);
            canvasDisplay.setPreferredSize(new Dimension(jLayeredPane_3D.getWidth(), jLayeredPane_3D.getHeight()));
            canvasDisplay.addHierarchyBoundsListener(
                    new HierarchyBoundsListener() {
                        @Override
                        public void ancestorMoved(HierarchyEvent he) {

                        }

                        @Override
                        public void ancestorResized(HierarchyEvent he) {
                            if (controller.isEngineRunning()) {
                                controller.setResolution(canvasDisplay.getWidth(), canvasDisplay.getHeight());
                                jGUI.validate();
                            }
                        }
                    });

            jGUI.setVisible(true);
        } catch (Exception e) {
            System.err.println(e);
            throw new RuntimeException("Unable to create display");
        }
    }

    public void createComponents() {
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        jLayeredPane_3D = new javax.swing.JLayeredPane();
        jMenuBar_Main = new javax.swing.JMenuBar();
        jMenuFile = new javax.swing.JMenu();
        jMenuEdit = new javax.swing.JMenu();
        jGUI = new JFrame("Java Game Engine 3D");
        jMenuItemStart = new JMenuItem();
        jMenuItemStop = new JMenuItem();
        jRadioGame = new JRadioButton();
        jPanelInfo = new JPanel();
        jButtonSelect = new javax.swing.JButton();
        jComboBox = new JComboBox();
        jInfoText = new JTextArea(1, 1);

        jGUI.setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        jGUI.setResizable(true);
        jGUI.setLayout(new java.awt.BorderLayout());

        jLayeredPane_3D.setLayout(new BorderLayout());
        jLayeredPane_3D.setBackground(Color.yellow);
        jLayeredPane_3D.setOpaque(true);
        jLayeredPane_3D.setPreferredSize(new Dimension(1000, 600));
        jLayeredPane_3D.setMinimumSize(new Dimension(600, 480));

        jComboBox.setPreferredSize(new Dimension(200, 30));
        jComboBox.addActionListener(this);

        jRootNode = new DefaultMutableTreeNode(new FormatedId(UUID.randomUUID()));
        jTreeStructure = new JTree(jRootNode);
        jTreeStructure.getSelectionModel().setSelectionMode(TreeSelectionModel.SINGLE_TREE_SELECTION);
        jTreeStructure.addTreeSelectionListener(this);

        jTreeViewPane = new JScrollPane(jTreeStructure);
        jTreeViewPane.setPreferredSize(new Dimension(200, 100));
        jTreeViewPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);

        jButtonSelect.setText("Select Element");
        jButtonSelect.addActionListener(this);
        jPanelInfo.setBackground(Color.DARK_GRAY);
        jPanelInfo.setPreferredSize(new Dimension(200, 200));
        jPanelInfo.setLayout(new BorderLayout());
        jPanelInfo.add(jComboBox, java.awt.BorderLayout.NORTH);
        jPanelInfo.add(jTreeViewPane, java.awt.BorderLayout.CENTER);
        jPanelInfo.add(jButtonSelect, java.awt.BorderLayout.SOUTH);

        //jInfoText.setFont(jInfoText.getFont().deriveFont(Font.BOLD,jInfoText.getFont().getSize()));
        jInfoText.setText("HELLO");
        jInfoText.setBackground(Color.PINK);
        jInfoViewPane = new JScrollPane(jInfoText);
        jInfoViewPane.setPreferredSize(new Dimension(200, 400));
        jInfoViewPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);

        jSplitInfoPane = new JSplitPane(JSplitPane.VERTICAL_SPLIT, jPanelInfo, jInfoViewPane);
        jSplitInfoPane.setMinimumSize(new Dimension(200, 480));

        jMenuFile.setText("File");
        jMenuFile.setMnemonic(KeyEvent.VK_F);
        jMenuBar_Main.add(jMenuFile);

        jMenuItemStart.setText("Run Engine");
        jMenuItemStart.setMnemonic(KeyEvent.VK_R);
        jMenuItemStart.addActionListener(this);
        jMenuFile.add(jMenuItemStart);

        jMenuItemStop.setText("Stop Engine");
        jMenuItemStop.setMnemonic(KeyEvent.VK_S);
        jMenuItemStop.addActionListener(this);
        jMenuFile.add(jMenuItemStop);

        jMenuEdit.setText("Games");
        jMenuEdit.setMnemonic(KeyEvent.VK_E);
        jMenuBar_Main.add(jMenuEdit);
        for (Enumeration e = gameProvider.getGames().elements(); e.hasMoreElements();) {
            jRadioGame = new JRadioButton();
            jRadioGame.setText(e.nextElement().toString());
            jRadioGame.addActionListener(this);
            jMenuEdit.add(jRadioGame);
        }

        jSplitContentPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, jSplitInfoPane, jLayeredPane_3D);
        jSplitContentPane.setOneTouchExpandable(true);
        jGUI.getContentPane().add(jSplitContentPane);
        jButton1.setText("MoveVector -z (Button1)");
        jButton1.addActionListener(this);
        jGUI.getContentPane().add(jButton1, java.awt.BorderLayout.NORTH);

        jButton2.setText("MoveVector +z (Button2)");
        jButton2.addActionListener(this);
        jGUI.getContentPane().add(jButton2, java.awt.BorderLayout.SOUTH);
        jGUI.setJMenuBar(jMenuBar_Main);

        jGUI.pack();
    }

    @Override
    public Canvas getDispalyCanvas() {
        return canvasDisplay;
    }

    private void newCan() {
        canvasDisplay = new Canvas() {
            @Override
            public final void addNotify() {
                super.addNotify();
            }

            @Override
            public final void removeNotify() {
                controller.stopEngine();
                super.removeNotify();
            }
        };

        jLayeredPane_3D.add(canvasDisplay, java.awt.BorderLayout.CENTER);
        canvasDisplay.setPreferredSize(new Dimension(jLayeredPane_3D.getWidth(), jLayeredPane_3D.getHeight()));
        canvasDisplay.addHierarchyBoundsListener(
                new HierarchyBoundsListener() {
                    @Override
                    public void ancestorMoved(HierarchyEvent he) {

                    }

                    @Override
                    public void ancestorResized(HierarchyEvent he) {
                        if (controller.isEngineRunning()) {
                            controller.setResolution(canvasDisplay.getWidth(), canvasDisplay.getHeight());
                            jGUI.validate();
                        }
                    }
                });
    }

    @Override
    public void actionPerformed(ActionEvent ae) {
        if (ae.getSource() == jMenuItemStart) {
            if (!selectedGame.isEmpty()) {
                Game choosen = gameProvider.getGame(selectedGame);
                if (choosen != null) {
                    controller.setGame(choosen);
                    controller.registerStructureObserver(this);
                }
            }
            controller.startEngine();
        } else if (ae.getSource() == jMenuItemStop) {
            controller.stopEngine();
            //newCan();
            canvasDisplay.revalidate();
            //canvasDisplay.revalidate();
        } else if (ae.getSource() == jButton1) {
            controller.moveVector(new Vector3f(0, 0, 0.05f));
        } else if (ae.getSource() == jButton2) {
            controller.moveVector(new Vector3f(0, 0, -0.05f));
        } else if (ae.getSource() == jComboBox) {
            FormatedId selectedId = (FormatedId) jComboBox.getSelectedItem();
            DefaultMutableTreeNode node = (DefaultMutableTreeNode) jTreeStructure.getLastSelectedPathComponent();
            if ((node != null) && (selectedId == (FormatedId) node.getUserObject())) {
                return;
            }
            Enumeration e = jRootNode.breadthFirstEnumeration();
            while (e.hasMoreElements()) {
                node = (DefaultMutableTreeNode) e.nextElement();
                if (selectedId.equals((FormatedId) node.getUserObject())) {
                    jTreeStructure.setSelectionPath(new TreePath(node.getPath()));
                }
            }
        } else if (ae.getSource() == jButtonSelect) {
            DefaultMutableTreeNode node = (DefaultMutableTreeNode) jTreeStructure.getLastSelectedPathComponent();
            if (node != null) {
                boolean isActive = activeElements.contains(((FormatedId) node.getUserObject()).getId());
                if (node != jRootNode && isActive && (node.getParent() instanceof DefaultMutableTreeNode)) {
                    if (activeElements.contains(((FormatedId) ((DefaultMutableTreeNode) node.getParent()).getUserObject()).getId())) {
                        return;
                    }
                }
                Enumeration e = node.breadthFirstEnumeration();
                DefaultMutableTreeNode child;
                while (e.hasMoreElements()) {
                    child = (DefaultMutableTreeNode) e.nextElement();

                    FormatedId elementInfo = (FormatedId) child.getUserObject();
                    controller.setElementState(elementInfo.getId(), !isActive);

                }
                jTreeViewPane.repaint();// More Generic
            }
        } else if (ae.getSource().getClass() == JRadioButton.class) {
            selectedGame = ae.getActionCommand();
        }
    }

    @Override
    public void update(Observable o, Object o1) {
        if (o1 instanceof CommandNote) {
            CommandNote lNote = (CommandNote) o1;
            if ((lNote.getType() == CommandSource.Mouse)) {
                mouseCommands(lNote);
            }
            if ((lNote.getType() == CommandSource.Keyboard)) {
                keyCommands(lNote);
            }
        }
        if (o1 instanceof StructureNote) {
            StructureNote lNote = (StructureNote) o1;
            structureInfo(null, lNote, true);
            jTreeViewPane.repaint(); // More Generic
        }
    }

    private void structureInfo(StructureNote aParentNote, StructureNote aNote, boolean aMakeRegistry) {

        boolean lFound = false;
        boolean activityChanged = false;
        StructureNote lStructNote;
        Iterator<StructureNote> lIterator = identityList.listIterator();
        while (lIterator.hasNext()) {
            lStructNote = lIterator.next();
            if (lStructNote.getIdentity() == aNote.getIdentity()) {
                lFound = true;
                identityList.remove(lStructNote);
                activityChanged = activeElements.contains(aNote.getIdentity()) != aNote.getIsActiveElement();
                if (activityChanged) {
                    if (aNote.getIsActiveElement()) {
                        activeElements.add(aNote.getIdentity());
                    } else {
                        activeElements.remove(aNote.getIdentity());
                    }
                }
                identityList.add(aNote);
                break;
            }
        }

        if (!lFound) {
            identityList.add(aNote);
            if (aMakeRegistry) {
                controller.registerStructureObserver(aNote.getIdentity(), this);
            }
        }
        if (!lFound) {
            DefaultMutableTreeNode jParentNode = searchNode(aParentNote);
            if (jParentNode != null) {
                FormatedId userObject = new FormatedId(aNote);
                jParentNode.add(new DefaultMutableTreeNode(userObject));
                jComboBox.addItem(userObject);
            }
        } else if (activityChanged) {

            FormatedId fId = new FormatedId(aNote);
            for (int i = jComboBox.getItemCount() - 1; i >= 0; i--) {
                if (((FormatedId) jComboBox.getItemAt(i)).getId() == aNote.getIdentity()) {
                    jComboBox.removeItemAt(i);
                    jComboBox.insertItemAt(fId, i);
                }
            }
            Enumeration e = jRootNode.breadthFirstEnumeration();
            DefaultMutableTreeNode node;
            while (e.hasMoreElements()) {
                node = (DefaultMutableTreeNode) e.nextElement();
                if (((FormatedId) node.getUserObject()).getId() == aNote.getIdentity()) {
                    node.setUserObject(fId);
                }
            }
        }

        for (StructureNote lChild : aNote.getChildIDs()) {
            structureInfo(aNote, lChild, aMakeRegistry);
        }
    }

    private void createTreeNode(StructureNote aParentId, StructureNote aId) {
        DefaultMutableTreeNode jParentNode = searchNode(aParentId);
        if (jParentNode != null) {
            FormatedId userObject = new FormatedId(aId);
            jParentNode.add(new DefaultMutableTreeNode(userObject));
            jComboBox.addItem(userObject);
        }
    }

    private DefaultMutableTreeNode searchNode(StructureNote aId) {
        if (aId == null) {
            return jRootNode;
        }
        DefaultMutableTreeNode node;
        Enumeration e = jRootNode.breadthFirstEnumeration();
        while (e.hasMoreElements()) {
            node = (DefaultMutableTreeNode) e.nextElement();
            if (aId.getIdentity().equals(((FormatedId) node.getUserObject()).getId())) {
                return node;
            }
        }
        return null;
    }

    @Override
    public void valueChanged(TreeSelectionEvent e) {
        DefaultMutableTreeNode node = (DefaultMutableTreeNode) jTreeStructure.getLastSelectedPathComponent();

        if (node == null) {
            return;
        }

        FormatedId nodeInfo = (FormatedId) node.getUserObject();
        if (!nodeInfo.equals((FormatedId) jComboBox.getSelectedItem())) {
            jComboBox.setSelectedItem(nodeInfo);
        }
        jTreeViewPane.setPreferredSize(jTreeStructure.getPreferredScrollableViewportSize());
        jTreeViewPane.getViewport().revalidate();
        StructureNote lSelectedNote = null;
        boolean lFound = false;
        Iterator<StructureNote> lIterator = identityList.listIterator();
        while (lIterator.hasNext()) {
            lSelectedNote = lIterator.next();
            if (lSelectedNote.getIdentity() == nodeInfo.getId()) {
                lFound = true;
                break;
            }
        }
        if (lFound && (lSelectedNote != null)) {
            String lInfo = "";
            lInfo += "ID: " + lSelectedNote.getIdentity().toString() + "\n";
            lInfo += "---------------------------------------\n";
            jInfoText.setBackground(Color.WHITE);
            Vector3f lColorVector = new Vector3f(1, 1, 1);
            if (lSelectedNote.getRenderCount() > 0) {
                lColorVector = lColorVector.mul(new Vector3f(0.95f, 0.95f, 0.95f));
                lInfo += "Render-Elements: " + lSelectedNote.getRenderCount() + "\n";
            }
            if (lSelectedNote.getForceCount() > 0) {
                lColorVector = lColorVector.mul(new Vector3f(1, 1, 0.8f));
                lInfo += "Force-Elements: " + lSelectedNote.getForceCount() + "\n";
            }
            if (lSelectedNote.getPhysicsCount() > 0) {
                lColorVector = lColorVector.mul(new Vector3f(1f, 0.8f, 1f));
                lInfo += "Physics-Elements: " + lSelectedNote.getPhysicsCount() + "\n";
            }
            if (lSelectedNote.getLightsCount() > 0) {
                lColorVector = lColorVector.mul(new Vector3f(0.7f, 0.7f, 0.7f));
                lInfo += "Lights-Elements: " + lSelectedNote.getLightsCount() + "\n";
            }
            if (lSelectedNote.getMotionControllerCount() > 0) {
                lColorVector = lColorVector.mul(new Vector3f(1f, 1f, 0.5f));
                lInfo += "MotionController-Elements: " + lSelectedNote.getMotionControllerCount() + "\n";
            }
            if (lSelectedNote.getRotationControllerCount() > 0) {
                lColorVector = lColorVector.mul(new Vector3f(1f, 0.5f, 1f));
                lInfo += "RotationController-Elements: " + lSelectedNote.getRotationControllerCount() + "\n";
            }
            if (lSelectedNote.getScaleControllerCount() > 0) {
                lColorVector = lColorVector.mul(new Vector3f(0.5f, 1f, 1f));
                lInfo += "ScaleController-Elements: " + lSelectedNote.getScaleControllerCount() + "\n";
            }
            if (lSelectedNote.getCameraCount() > 0) {
                lColorVector = lColorVector.mul(new Vector3f(0.9f, 0.9f, 0.9f));
                lInfo += "Camera-Elements: " + lSelectedNote.getCameraCount() + "\n";
            }
            jInfoText.setBackground(new Color(lColorVector.getX(), lColorVector.getY(), lColorVector.getZ()));

            jInfoText.setText(lInfo);
        }
    }

    private void mouseCommands(CommandNote aCommand) {
        if (aCommand.getCommand() == CommandMouse.MouseButton1.ordinal()) {
            controller.activateMouse(true);
        } else if (aCommand.getCommand() == CommandMouse.MouseButton2.ordinal()) {
            controller.activateMouse(false);
        } else if (controller.isMouseActive()) {
            if (aCommand.getCommand() == CommandMouse.MouseMotion.ordinal()) {
                if (!keyAlternate) {
                    controller.rotateVector(aCommand.getPosition());
                } else {
                    controller.scaleVector(new Vector3f(0.01f, 0.01f, 0.01f).mul(aCommand.getPosition().getX() > 0 ? 1 : -1));
                }
            } else if (aCommand.getCommand() == CommandMouse.MouseButton3.ordinal()) {
                controller.rotate(Axis.Front, true, aCommand.getCommandState());
            } else if (aCommand.getCommand() == CommandMouse.MouseWheelTurn.ordinal()) {

                float lChangeScale = aCommand.getAmount() > 0 ? 1.1f : 0.9f;
                if (controller.isViewOrthographic()) {
                    lChangeScale *= controller.getFieldOfView();
                } else {
                    lChangeScale *= (float) Math.toDegrees(controller.getFieldOfView());
                    // f(x): (-inf; inf)->[5; 175] 
                    // mit f(90) = 5+170/2=90 & [90-160/2;90+160/2] in (0.1;0.9)*160
                    lChangeScale = (float) Math.toRadians(5 + 170
                            * 1 / (1 + Math.exp(-(lChangeScale - 90) * 2 * Math.log(9) / 160)));
                }
                controller.setFieldOfView(lChangeScale);
            }
        }
    }

    private void keyCommands(CommandNote aCommand) {
        if ((aCommand.getCommand() == KeyEvent.VK_F2) || (aCommand.getCommand() == KeyEvent.VK_F3)) {
            if (!aCommand.getCommandState()) {
                switch (aCommand.getCommand()) {
                    case KeyEvent.VK_F2:
                        controller.activateKeyboard(!controller.isKeyboardActive());
                    case KeyEvent.VK_F3:
                        controller.activateMouse(!controller.isMouseActive());
                }
            }
        } else if (controller.isKeyboardActive()) {
            switch (aCommand.getCommand()) {
                case KeyEvent.VK_E: {
                    controller.move(Axis.Front, true, aCommand.getCommandState());
                    break;
                }
                case KeyEvent.VK_Q: {
                    controller.move(Axis.Front, false, aCommand.getCommandState());
                    break;
                }
                case KeyEvent.VK_D: {
                    controller.move(Axis.Side, true, aCommand.getCommandState());
                    break;
                }
                case KeyEvent.VK_A: {
                    controller.move(Axis.Side, false, aCommand.getCommandState());
                    break;
                }
                case KeyEvent.VK_Y: {
                    controller.move(Axis.Top, true, aCommand.getCommandState());
                    break;
                }
                case KeyEvent.VK_C: {
                    controller.move(Axis.Top, false, aCommand.getCommandState());
                    break;
                }
                case KeyEvent.VK_ALT: {
                    keyAlternate = aCommand.getCommandState();
                    break;
                }
                case KeyEvent.VK_H: {
                    if (!aCommand.getCommandState()) {
                        debugger.setStatePhysicsEngine(!debugger.getStatePhysicsEngine());
                    }
                    break;
                }
                case KeyEvent.VK_W: {
                    controller.setViewOrthographic(aCommand.getCommandState());
                    if (aCommand.getCommandState()) {
                        controller.setFieldOfView(100);
                    } else {
                        controller.setFieldOfView((float) Math.toRadians(45));
                    }
                    break;
                }
            }
        }
    }
}
