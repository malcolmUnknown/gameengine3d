/*
 * Copyright (C) 2017 Malcolm
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package View;

import Controller.StructureNote;
import java.util.UUID;

/**
 *
 * @author Malcolm
 */
public class FormatedId {

    private UUID id;
    private boolean isActive;

    public FormatedId(UUID aId) {
        id = aId;
        isActive = false;
    }

    public FormatedId(StructureNote aNote) {
        id = aNote.getIdentity();
        isActive = aNote.getIsActiveElement();
    }

    public String toString() {
        if (isActive) {
            return "<html><span style=\"color:blue; \"><u>" + id.toString() + "</u><br />";
        } else {
            return "<html><span style=\"color:green; \">" + id.toString() + "<br />";
        }
    }

    public UUID getId() {
        return id;
    }

    public boolean getIsActive() {
        return isActive;
    }

    public void setIsActive(boolean aIsActive) {
        isActive = aIsActive;
    }

}
