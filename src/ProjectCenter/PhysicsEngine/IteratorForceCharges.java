/*
 * Copyright (C) 2016 Malcolm
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ProjectCenter.PhysicsEngine;

import java.util.ArrayList;
import java.util.Iterator;

/**
 *
 * @author Malcolm
 */
public class IteratorForceCharges implements Iterator<ChargeObject> {

    int position = 0;
    int nextPosition = -1;
    ArrayList<ChargeObject> list;

    public IteratorForceCharges(ArrayList<ChargeObject> aChargeList) {
        list = aChargeList;
    }

    @Override
    public boolean hasNext() {
        boolean lHasNext = false;
        for (int i = position; i < list.size(); i++) {
            if (list.get(i).generatesForce()) {
                lHasNext = true;
                nextPosition = i;
                break;
            }
        }
        return lHasNext;
    }

    @Override
    public ChargeObject next() {
        ChargeObject lReturn = null;
        if (nextPosition <= position) {
            for (int i = position; i < list.size(); i++) {
                if (list.get(i).generatesForce()) {
                    lReturn = list.get(i);
                    position = i;
                    break;
                }
            }
        } else {
            lReturn = list.get(nextPosition);
            position = nextPosition;
        }
        return lReturn;
    }

    @Override
    public void remove() {
        if ((0 < position) && (position < list.size())) {
            list.remove(position);
            nextPosition = -1;
        }
    }

}
