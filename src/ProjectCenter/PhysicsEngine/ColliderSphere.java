/* GameEngine 3D - A programm to design games with 3D content 
 * Copyright (C) 2016-2056 Frank Küwen
 *  
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package ProjectCenter.PhysicsEngine;

import ProjectCenter.CoreEngine.Precision;
import ProjectCenter.CoreEngine.Transform;
import ProjectCenter.CoreEngine.Vector3f;

public class ColliderSphere extends Collider {

    private float radius;

    public ColliderSphere() {
        super(ColliderTypes.ColliderSphere);
        radius = 1;
    }

    public float getRadius() {
        return radius;
    }

//    @Override
//    public void updateScale(Vector3f aNewScale) {
//        radius = aNewScale.max();
//    }
    @Override
    public Vector3f getDistanceInDirection(Vector3f aNormedDirection) {
        return aNormedDirection.mul(radius);
    }

    @Override
    public Vector3f getNormalInDirection(Vector3f aNormedDirection) {
        return aNormedDirection;
    }

    @Override
    public void update(Transform aState, Vector3f aFavoredDir) {
        super.update(aState, aFavoredDir);
        radius = aState.getScale().max();
    }
    /*
     @Override
     public Matrix4f getMomentumOfInteria(float aMass, Vector3f aDimensions) {
     return  new Matrix4f().initMapping(
     new Vector3f(2.f/5*aMass*radius, 0,0), 
     new Vector3f(0,2.f/5*aMass*radius, 0), 
     new Vector3f(0,0,2.f/5*aMass*radius), 
     new Vector3f(0, 0,0));
     }*/

}
