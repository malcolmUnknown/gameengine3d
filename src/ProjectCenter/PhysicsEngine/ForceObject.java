/* GameEngine 3D - A programm to design games with 3D content 
 * Copyright (C) 2016-2056 Frank Küwen
 *  
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package ProjectCenter.PhysicsEngine;

import ProjectCenter.CoreEngine.Transform;
import ProjectCenter.CoreEngine.Vector3f;

public class ForceObject {

    private ForceTypes type;
    private float amplitude;
    private Transform state;

    public ForceObject(ForceTypes aType, float aAmplitude, Vector3f aOrigin) {
        type = aType;
        amplitude = aAmplitude;
        state = new Transform();
        state.setPosition(aOrigin);
    }

    public ForceObject(ChargeObject aCharge) {
        type = aCharge.getType();
        amplitude = getChargeAmplitude(type, aCharge.getCharge());
        state = new Transform();
        state.setPosition(aCharge.getPosition());
    }

    public ForceTypes getType() {
        return type;
    }

    public Vector3f getForce(ChargeObject aCharge) {
        if (aCharge.getType().equals(type)) {
            switch (type) {
                // case Magnetic is defined by velocity & position (circular field)
                case Electrical:
                case Graviational: {
                    Vector3f lR = (state.getPosition().sub(aCharge.getPosition()));
                    float lDistance = lR.length();
                    return lR.mul(-1*amplitude * aCharge.getCharge() / (lDistance * lDistance * lDistance));
                }
                default:
                    return null;
            }
        } else {
            return null;
        }
    }

    private float getChargeAmplitude(ForceTypes aType, float aCharge) {
        float lAmplitude = 0;
        switch (aType) {
            case Electrical: {
                lAmplitude = aCharge / (float) (4 * Math.PI * PhysicalConstant.EPSILON_ZERO);
                break;
            }
            case Graviational: {
                lAmplitude = aCharge * PhysicalConstant.G_ZERO;
                break;
            }
        }
        return lAmplitude;
    }

    public void updateState(Transform aNewState) {
        if (!state.getScale().equals(aNewState.getScale())) {
            state.setScale(aNewState.getScale());
        }
        if (!state.getRotation().equals(aNewState.getRotation())) {
            state.setRotation(aNewState.getRotation());
        }
        if (!state.getPosition().equals(aNewState.getPosition())) {
            state.setPosition(aNewState.getPosition());
        }

    }
}
