/*
 * Copyright (C) 2016 Malcolm
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ProjectCenter.PhysicsEngine;

import ProjectCenter.CoreEngine.Vector3f;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.*;

/**
 *
 * @author Malcolm
 */
public class ChargeManager {

    private final HashMap<ForceTypes, ArrayList<ChargeObject>> charges;

    public ChargeManager() {
        charges = new HashMap<>();
    }

    public void add(ChargeObject aCharge) {
        if (!charges.containsKey(aCharge.getType())) {
            charges.put(aCharge.getType(), new ArrayList<ChargeObject>());
        }
        charges.get(aCharge.getType()).add(aCharge);

    }

    public void delete(ChargeObject aCharge) {
        if (charges.containsKey(aCharge.getType())) {
            if (charges.get(aCharge.getType()).contains(aCharge)) {
                charges.get(aCharge.getType()).remove(aCharge);
            }
        }
    }

    public Iterator<ChargeObject> getCharges(ForceTypes aType) {
        return charges.get(aType).iterator();
    }

    public IteratorForceCharges getChargeForces(ForceTypes aType) {

        return new IteratorForceCharges(charges.get(aType));
    }

    public Iterators<ChargeObject> getCharges() {
        ArrayList<Iterator<ChargeObject>> fullList = new ArrayList<>();
        for (ForceTypes type : charges.keySet()) {
            fullList.add(charges.get(type).iterator());
        }
        return new Iterators<>(fullList);
    }

    public Iterators<ChargeObject> getForceCharges() {        
        ArrayList<Iterator<ChargeObject>> fullList = new ArrayList<>();
        for (ForceTypes type : charges.keySet()) {
            fullList.add(new IteratorForceCharges(charges.get(type)));
        }
        return new Iterators<>(fullList);
    }
    
    public int getChargeCount(ForceTypes aType)
    {
        return charges.get(aType).size();
    }
    public int getChargeCount()
    {   
        int count = 0;
        for (ForceTypes type : charges.keySet()) {
            count += charges.get(type).size();
        }
        return count;
    }
    
    public Vector3f getChargeCenter(ForceTypes aType)
    { 
        Vector3f lPosition = new Vector3f();
        float lSum = 0;
        for (ChargeObject charge : charges.get(aType)) {
            lPosition = lPosition.add(charge.getExcenter().mul(charge.getCharge()));
            lSum += charge.getCharge();
        }
        return lPosition.div(lSum); 
    }
    
    public float getChargeSum(ForceTypes aType)
    { 
        float lSum = 0;
        for (ChargeObject charge : charges.get(aType)) {
            lSum += charge.getCharge();
        }
        return lSum; 
    }

}
