/* GameEngine 3D - A programm to design games with 3D content 
 * Copyright (C) 2016-2056 Frank Küwen
 *  
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package ProjectCenter.PhysicsEngine;

import ProjectCenter.CoreEngine.Quaternion;
import ProjectCenter.CoreEngine.ReferenceCounter;
import ProjectCenter.CoreEngine.Transform;
import ProjectCenter.CoreEngine.Vector3f;

public abstract class Collider {

    private final ColliderTypes colliderType;
    protected Vector3f position;

    public Collider(ColliderTypes aType) {
        colliderType = aType;
        position = new Vector3f();
    }

    public ColliderTypes getType() {
        return colliderType;
    }

//    public IntersectData intersect(Collider aOther) {
//        if ((colliderType == ColliderTypes.ColliderSphere) && (aOther.colliderType == ColliderTypes.ColliderSphere)) {
//            ColliderSphere self = (ColliderSphere) this;
//            return self.IntersectBoundingSphere((ColliderSphere) aOther);
//        } else if ((colliderType == ColliderTypes.ColliderSphere) && (aOther.colliderType == ColliderTypes.ColliderPlane)) {
//            ColliderPlane lPlane = (ColliderPlane) aOther;
//            return lPlane.IntersectSphere((ColliderSphere) this);
//        } else if ((colliderType == ColliderTypes.ColliderPlane) && (aOther.colliderType == ColliderTypes.ColliderSphere)) {
//            ColliderPlane lPlane = (ColliderPlane) this;
//            return lPlane.IntersectSphere((ColliderSphere) aOther);
//        } else if ((colliderType == ColliderTypes.ColliderPlane) && (aOther.colliderType == ColliderTypes.ColliderPlane)) {
//            ColliderPlane lPlane = (ColliderPlane) this;
//            return lPlane.IntersectPlane((ColliderPlane) aOther);
//        } else if ((colliderType == ColliderTypes.ColliderSphere) && (aOther.colliderType == ColliderTypes.ColliderBox)) {
//            ColliderBox lBox = (ColliderBox) aOther;
//            return lBox.IntersectSphere((ColliderSphere) this);
//        } else if ((colliderType == ColliderTypes.ColliderBox) && (aOther.colliderType == ColliderTypes.ColliderSphere)) {
//            ColliderBox lBox = (ColliderBox) this;
//            return lBox.IntersectSphere((ColliderSphere) aOther);
//        } else if ((colliderType == ColliderTypes.ColliderPlane) && (aOther.colliderType == ColliderTypes.ColliderBox)) {
//            ColliderBox lBox = (ColliderBox) aOther;
//            return lBox.IntersectPlane((ColliderPlane) this);
//        } else if ((colliderType == ColliderTypes.ColliderBox) && (aOther.colliderType == ColliderTypes.ColliderPlane)) {
//            ColliderBox lBox = (ColliderBox) this;
//            return lBox.IntersectPlane((ColliderPlane) aOther);
//        } else if ((colliderType == ColliderTypes.ColliderBox) && (aOther.colliderType == ColliderTypes.ColliderBox)) {
//            ColliderBox lBox = (ColliderBox) this;
//            return lBox.IntersectBox((ColliderBox) aOther);
//        } else {
//            System.out.println("Error Collisions not implemented between the specified colliders.");
//            new Exception().printStackTrace();
//            System.exit(1);
//            return new IntersectData(false, new Vector3f());
//        }
//    }    
    public void update(Transform aState, Vector3f aFavoredDir) {
        position = aState.getPosition();
    }

    public Vector3f getPosition() {
        return position;
    }

    public Vector3f getNormalInDirection(Vector3f aNormedDirection) {
        return new Vector3f();
    }

    public Vector3f getDistanceInDirection(Vector3f aNormedDirection) {
        return new Vector3f();
    }
}
