/*
 * Copyright (C) 2016 Malcolm
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ProjectCenter.PhysicsEngine;

import java.util.ArrayList;
import java.util.Iterator;

/**
 *
 * @author Malcolm
 */
public class Iterators<T> implements Iterator<T> {

    ArrayList<Iterator<T>> list;
    int position = 0;
    boolean listSwitched = false;

    public Iterators(ArrayList<Iterator<T>> aWalkerList) {
        list = aWalkerList;
    }

    @Override
    public boolean hasNext() {
        if ((position < list.size()) && list.get(position).hasNext()) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public T next() {
        T lReturn = list.get(position).next();
        if (!list.get(position).hasNext()) {
            position += 1;
            listSwitched = true;
        } else {
            listSwitched = false;
        }
        return lReturn;
    }

    @Override
    public void remove() {
        if (listSwitched) {
            list.get(position - 1).remove();
        } else {
            list.get(position).remove();
        }
    }
}
