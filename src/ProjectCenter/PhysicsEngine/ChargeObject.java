/* GameEngine 3D - A programm to design games with 3D content 
 * Copyright (C) 2016-2056 Frank Küwen
 *  
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package ProjectCenter.PhysicsEngine;

import ProjectCenter.CoreEngine.Quaternion;
import ProjectCenter.CoreEngine.Vector3f;

public class ChargeObject implements IPhysicsTransformable {
        
    private final ForceTypes type;
    private final float charge;
    private Vector3f excenter;
    private final boolean forceSource;
    private Quaternion rotationState;
    private Vector3f scaleState;
    private Vector3f positionState;
    
    public ChargeObject(ForceTypes aType, float aCharge, Vector3f aExcenterPosition, boolean aForceSource)
    {
        type = aType;
        charge = aCharge;
        excenter = aExcenterPosition;
        forceSource = aForceSource;
        scaleState = new Vector3f(1,1,1);
        rotationState = new Quaternion(0, 0, 0, 1);
        positionState = new Vector3f();
    }
    
    public float getCharge()
    {
        return charge;        
    }
    
    public ForceTypes getType()
    {
        return type;
    }
    
    public boolean generatesForce()
    {
        return forceSource;
    }
    public Vector3f getExcenter()
    { // Rotation invers to owner rotation!! 
        return excenter.mul(scaleState).rotate(rotationState.conjugate());
    }
    
    public Vector3f getPosition()
    {
        return positionState.add(getExcenter());
    }
    
    @Override
    public void updatePosition(Vector3f aNewCenter) {
        positionState = aNewCenter;
    }
    @Override
    public void updateOrientation(Quaternion aNewOrientation) {
        rotationState = aNewOrientation;
    }
    @Override
    public void updateScale(Vector3f aNewScale) {
        scaleState = aNewScale;
    }
}
