/* GameEngine 3D - A programm to design games with 3D content 
 * Copyright (C) 2016-2056 Frank Küwen
 *  
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package ProjectCenter.PhysicsEngine;

import ProjectCenter.CoreEngine.Matrix4f;
import ProjectCenter.CoreEngine.Precision;
import ProjectCenter.CoreEngine.Quaternion;
import ProjectCenter.CoreEngine.Transform;
import ProjectCenter.CoreEngine.Vector3f;
import java.util.ArrayList;
import java.util.Iterator;

public class PhysicsObject {

    private Vector3f velocity;
    private Vector3f velocityUpdate;
    private int velocityUpdateCount;

    private Vector3f angularAxis;
    private float angularSpeed;

    private Transform currentState;
    private Vector3f specialDirection;

    private ColliderTypes colliderType;
    private ChargeManager chargeManagment;

    public PhysicsObject(ColliderTypes aColliderType, Vector3f aFavoredDirection, Vector3f aMassDisplacement,
            float aMass, Vector3f aVelocity, Vector3f aAngularVelocity) {
        currentState = new Transform();

        velocity = aVelocity;
        velocityUpdate = new Vector3f();
        velocityUpdateCount = 0;

        angularSpeed = aAngularVelocity.length();
        if (Precision.lessEqual(angularSpeed, 0)) {
            angularAxis = new Vector3f();
            angularSpeed = 0;
        } else {
            angularAxis = aAngularVelocity.div(angularSpeed);
        }

        specialDirection = aFavoredDirection;

        chargeManagment = new ChargeManager();
        chargeManagment.add(new ChargeObject(ForceTypes.Graviational, aMass, aMassDisplacement, false));
        colliderType = aColliderType;
    }

    public PhysicsObject(PhysicsObject aOther) {
        currentState = aOther.currentState;
        velocity = aOther.getVelocity();
        angularAxis = aOther.getAngluarAxis();
        velocity = aOther.getVelocity();
        angularSpeed = getAngluarSpeed();
        colliderType = aOther.getColliderType();
        specialDirection = aOther.getFavoredDirection();
        Iterator<ChargeObject> ChargeWalker = aOther.getCharges();
        while (ChargeWalker.hasNext()) {
            chargeManagment.add(ChargeWalker.next());
        }
    }

    protected void updatePosition(Vector3f aNewPosition) {
        currentState.setPosition(aNewPosition);
        Iterator<ChargeObject> ChargeWalker = chargeManagment.getCharges();
        while (ChargeWalker.hasNext()) {
            ChargeWalker.next().updatePosition(aNewPosition);
        }
    }

    protected void updateOrientation(Quaternion aNewOrientation) {
        currentState.setRotation(aNewOrientation);
        Iterator<ChargeObject> ChargeWalker = chargeManagment.getCharges();
        while (ChargeWalker.hasNext()) {
            ChargeWalker.next().updateOrientation(currentState.getRotation());
        }
    }

    protected void updateScale(Vector3f aNewScale) {
        currentState.setScale(aNewScale);
        Iterator<ChargeObject> ChargeWalker = chargeManagment.getCharges();
        while (ChargeWalker.hasNext()) {
            ChargeWalker.next().updateScale(currentState.getScale());
        }
    }
    // Use Mass-Center here & use Excenter for charges
    public void applyForce(ForceObject aForce, float aTimeDelta) {
        float lInertMass = chargeManagment.getChargeSum(ForceTypes.Graviational);
        Iterator<ChargeObject> ChargeWalker = chargeManagment.getCharges();
        while (ChargeWalker.hasNext()) {
            Vector3f lForce = aForce.getForce(ChargeWalker.next());
            if (lForce != null) {
                updateVelocity(velocity.add(lForce.mul(aTimeDelta / lInertMass)));
            }
        }
    }

    // Use Mass-Center here & use Excenter for charges
    void integrate(float aTimeDelta) {
        if (!Precision.equals(angularSpeed, 0)) {
            Quaternion lRotate = new Quaternion(angularAxis, angularSpeed * aTimeDelta);
            Vector3f massCenter = chargeManagment.getChargeCenter(ForceTypes.Graviational);
            updateOrientation((lRotate.mul(currentState.getRotation())).normalized()); 
            Vector3f rotMassCenter = chargeManagment.getChargeCenter(ForceTypes.Graviational);//massCenter.rotate(lRotate.conjugate());
            if (!massCenter.equals(Vector3f.ZEROVECTOR)) {
                updatePosition(currentState.getPosition().add(rotMassCenter.sub(massCenter)));
            }
        }
        if (!velocity.equals(Vector3f.ZEROVECTOR)) {
            updatePosition(currentState.getPosition().add(velocity.mul(aTimeDelta)));
        }
    }

    public void updateState(Transform aNewState) {
        if (!currentState.getScale().equals(aNewState.getScale())) {
            updateScale(aNewState.getScale());
        }
        if (!currentState.getRotation().equals(aNewState.getRotation())) {
            updateOrientation(aNewState.getRotation());
        }
        if (!currentState.getPosition().equals(aNewState.getPosition())) {
            updatePosition(aNewState.getPosition());
        }

    }

    public Vector3f getScale() {
        return currentState.getScale();
    }

    public Quaternion getRotation() {
        return currentState.getRotation();
    }

    public Vector3f getPosition() {
        return currentState.getPosition();
    }

    public Transform getState() {
        return currentState;
    }

    public float getMass() {
        return chargeManagment.getChargeSum(ForceTypes.Graviational);
    }

    public Vector3f getDimensions() {
        return currentState.getScale();
    }

    public Matrix4f getMomentumOfInteria() {
        return new Matrix4f();
    }

    public Vector3f getVelocity() {
        return velocity;
    }

    public Vector3f getAngluarAxis() {
        return angularAxis;
    }

    public float getAngluarSpeed() {
        return angularSpeed;
    }

    public void addCharge(ChargeObject aCharge) {
        chargeManagment.add(aCharge);
    }

    public void deleteCharge(ChargeObject aCharge) {
        chargeManagment.delete(aCharge);
    }

    public int getChargeCount() {
        return chargeManagment.getChargeCount();
    }

    public Iterator<ChargeObject> getCharges() {
        return chargeManagment.getCharges();
    }

    public Iterator<ChargeObject> getForceCharges() {
        return chargeManagment.getForceCharges();
    }

//---- Deprecated -> UpdateValues ----//
    public void setVelocity(Vector3f aVelocity) {
        velocity = aVelocity;
    }

    public void setAngluarVelocity(Vector3f aAngularVelocity) {
        angularSpeed = aAngularVelocity.length();
        if (Precision.lessEqual(angularSpeed, 0)) {
            angularAxis = new Vector3f();
            angularSpeed = 0;
        } else {
            angularAxis = aAngularVelocity.div(angularSpeed);
        }
    }
//----//

//    public Collider getCollider() {
//        return collider;
//    }
//
//    public Vector3f getDistance(Vector3f aNormedDirection) {
//        return collider.getDistanceForDirection(aNormedDirection);
//    }
//
//    public Vector3f getNormal(Vector3f aDirection) {
//        return collider.getNormalForPoint(aDirection);
//    }
    public ColliderTypes getColliderType() {
        return colliderType;
    }

    public void updateVelocity(Vector3f aVelocity) {
        velocityUpdate = velocityUpdate.add(aVelocity);
        velocityUpdateCount++;
    }

    public void setUpdatedValues() {
        if (velocityUpdateCount > 0) {
            velocity = velocityUpdate.div(velocityUpdateCount);
            velocityUpdate = new Vector3f();
            velocityUpdateCount = 0;
        }
    }

    public Vector3f getFavoredDirection() {
        return specialDirection;
    }
//    public Transform getState() {
//        return currentState;
//    }
}
