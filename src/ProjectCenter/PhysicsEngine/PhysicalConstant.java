/* GameEngine 3D - A programm to design games with 3D content 
 * Copyright (C) 2016-2056 Frank Küwen
 *  
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package ProjectCenter.PhysicsEngine;

public class PhysicalConstant {
    public static float EPSILON_ZERO = (float) (8.845*Math.pow(10, -12));
    public static float SPEED_OF_LIGHT = (float) (3*Math.pow(10, 8));
    public static float MU_ZERO = (float) (4*Math.PI*Math.pow(10, -7));
    public static float G_ZERO = (float) (6.674*Math.pow(10, -11));
    
}
