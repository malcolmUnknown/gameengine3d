/* GameEngine 3D - A programm to design games with 3D content 
 * Copyright (C) 2016-2056 Frank Küwen
 *  
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package ProjectCenter.PhysicsEngine;

import ProjectCenter.CoreEngine.Precision;
import ProjectCenter.CoreEngine.Transform;
import ProjectCenter.CoreEngine.Vector3f;

public class ColliderBox extends Collider {

    private Vector3f normalWidth;
    private Vector3f normalTop;
    private Vector3f normalFront;
    private Vector3f dimensions;

    public ColliderBox() {
        super(ColliderTypes.ColliderBox);
        position = new Vector3f();
        dimensions = new Vector3f(1, 1, 1);
        normalWidth = new Vector3f(1, 0, 0);
        normalTop = new Vector3f(0, 1, 0);
        normalFront = new Vector3f(0, 0, 1);
    }

    public float getDistanceWidth() {
        return dimensions.getX();
    }

    public float getDistanceTop() {
        return dimensions.getY();
    }

    public float getDistanceFront() {
        return dimensions.getZ();
    }

    public Vector3f getDirectionFront() {
        return normalFront;
    }

    public Vector3f getDirectionTop() {
        return normalTop;
    }

    public Vector3f getDirectionWidth() {
        return normalWidth;
    }

    @Override
    public Vector3f getDistanceInDirection(Vector3f aNormedDirection) {

        float lDominantLength = getDistanceTop();
        float lDominantPart = Math.abs(aNormedDirection.dot(normalTop));
        float lComparePart = Math.abs(aNormedDirection.dot(normalWidth));
        if (Precision.lessEqual(lDominantPart / lDominantLength, lComparePart / getDistanceWidth())) {
            //if (Precision.equals(lDominantPart, 0) || Precision.less(getDistanceWidth(), lDominantLength))
            {
                lDominantLength = getDistanceWidth();
                lDominantPart = lComparePart;
            }
        }
        lComparePart = Math.abs(aNormedDirection.dot(normalFront)); //  
        if (Precision.lessEqual(lDominantPart / lDominantLength, lComparePart / getDistanceFront())) {
            //if (Precision.equals(lDominantPart, 0) || Precision.less(getDistanceFront(), lDominantLength))
            {
                lDominantLength = getDistanceFront();
                lDominantPart = lComparePart;
            }
        }
        if (Precision.equals(lDominantPart, 0)) {
            return new Vector3f();
        } else {
            return aNormedDirection.mul(lDominantLength / lDominantPart);
        }
    }

    @Override
    public Vector3f getNormalInDirection(Vector3f aNormedDirection) {
        Vector3f lResult = new Vector3f();
        Vector3f lPercentage = new Vector3f(
                Math.abs(aNormedDirection.dot(normalWidth)) / getDistanceWidth(),
                Math.abs(aNormedDirection.dot(normalTop)) / getDistanceTop(),
                Math.abs(aNormedDirection.dot(normalFront)) / getDistanceFront());
        float lMax = lPercentage.max();

        if (Precision.equals(lMax, lPercentage.getX())) {
            if (Precision.greaterEqual(aNormedDirection.dot(normalWidth), 0)) {
                lResult = lResult.add(normalWidth);
            } else {
                lResult = lResult.sub(normalWidth);
            }
        }
        if (Precision.equals(lMax, lPercentage.getY())) {
            if (Precision.greaterEqual(aNormedDirection.dot(normalTop), 0)) {
                lResult = lResult.add(normalTop);
            } else {
                lResult = lResult.sub(normalTop);
            }
        }
        if (Precision.equals(lMax, lPercentage.getZ())) {
            if (Precision.greaterEqual(aNormedDirection.dot(normalFront), 0)) {
                lResult = lResult.add(normalFront);
            } else {
                lResult = lResult.sub(normalFront);
            }
        }
//        Vector3f lDistance = new Vector3f(
//                Math.abs(Math.abs(aNormedDirection.dot(normalWidth)) - getDistanceWidth()),
//                Math.abs(Math.abs(aNormedDirection.dot(normalTop)) - getDistanceTop()),
//                Math.abs(Math.abs(aNormedDirection.dot(normalFront)) - getDistanceFront()));
//        float lMin = lDistance.min();
//        if (Precision.equals(lMin, lDistance.getX())) {
//            if (Precision.equals(aNormedDirection.dot(normalWidth), 0)) {
//                lResult = lResult.add(normalWidth);
//            } else {
//                lResult = lResult.sub(normalWidth);
//            }
//        }
//        if (Precision.equals(lMin, lDistance.getY())) {
//            if (Precision.equals(aNormedDirection.dot(normalTop), 0)) {
//                lResult = lResult.add(normalTop);
//            } else {
//                lResult = lResult.sub(normalTop);
//            }
//        }
//        if (Precision.equals(lMin, lDistance.getZ())) {
//            if (Precision.equals(aNormedDirection.dot(normalFront), 0)) {
//                lResult = lResult.add(normalFront);
//            } else {
//                lResult = lResult.sub(normalFront);
//            }
//        }
        return lResult.normalized();
    }

//    @Override
//    public void updateScale(Vector3f aNewScale) {
//        dimensions = aNewScale;
//    }
//
//    @Override
//    public void updateOrientation(Quaternion aNewOrientation) {
//        normalWidth = aNewOrientation.toRotationMatrix().mul(normalWidth).normalized();
//        normalTop = aNewOrientation.toRotationMatrix().mul(normalTop).normalized();
//        normalFront = aNewOrientation.toRotationMatrix().mul(normalFront).normalized();
//    }
    @Override
    public void update(Transform aState, Vector3f aFavoredDir) {
        super.update(aState, aFavoredDir);
        normalWidth = (new Vector3f(1, 0, 0).rotate(aState.getRotation())).normalized();
        normalTop = (new Vector3f(0, 1, 0).rotate(aState.getRotation())).normalized();
        normalFront = (new Vector3f(0, 0, 1).rotate(aState.getRotation())).normalized();
        //normalWidth = aState.getRotation().toRotationMatrix().mul(new Vector3f(1, 0, 0)).normalized();
        //normalTop = aState.getRotation().toRotationMatrix().mul(new Vector3f(0, 1, 0)).normalized();
        //normalFront = aState.getRotation().toRotationMatrix().mul(new Vector3f(0, 0, 1)).normalized();
        dimensions = aState.getScale();
    }

}
