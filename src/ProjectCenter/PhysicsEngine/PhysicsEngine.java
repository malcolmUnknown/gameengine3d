/* GameEngine 3D - A programm to design games with 3D content 
 * Copyright (C) 2016-2056 Frank Küwen
 *  
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package ProjectCenter.PhysicsEngine;

import ProjectCenter.CoreEngine.GameObject;
import ProjectCenter.CoreEngine.Precision;
import ProjectCenter.CoreEngine.Vector3f;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.Iterator;

public class PhysicsEngine {

    private boolean engineActive;
    private final ArrayList<PhysicsObject> objects;
    private final HashMap<Integer, IntersectData> collisionList;
    private final ArrayList<ForceObject> forces;
    private final EnumSet forcible;
    private final CollisionManager collisionDetector;

    public PhysicsEngine() {
        engineActive = true;
        objects = new ArrayList<>();
        collisionList = new HashMap<>();
        forces = new ArrayList<>();
        forcible = EnumSet.of(ForceTypes.Electrical, ForceTypes.Magnetical, ForceTypes.Graviational);
        collisionDetector = new CollisionManager();
    }

    public void setState(boolean aIsActive) {
        engineActive = aIsActive;
    }

    public boolean getState() {
        return engineActive;
    }

    public void addObject(PhysicsObject aObject) {
        objects.add(aObject);
    }

    public void addForces(ForceObject aForce) {

        if (forcible.contains(aForce.getType())) {
            forces.add(aForce);
        }
    }

    public void simulate(float aTimeDelta) {

        if (!engineActive) {
            return;
        }
        for (ForceObject force : forces) {
            for (PhysicsObject physics : objects) {
                physics.applyForce(force, aTimeDelta);
            }
        }
        for (int i = 0; i < objects.size(); i++) {
            for (int j = 0; j < objects.size(); j++) {
                if (i == j) {
                    continue;
                }
                Iterator<ChargeObject> ChargeWalker = objects.get(j).getForceCharges();
                while (ChargeWalker.hasNext()) {
                    objects.get(i).applyForce(new ForceObject(ChargeWalker.next()), aTimeDelta);
                }
            }
        }

        for (PhysicsObject physics : objects) {
            physics.setUpdatedValues();
            physics.integrate(aTimeDelta);
        }
    }

    public PhysicsObject getObjects(int i) {
        return objects.get(i);
    }

    public int getCount() {
        return objects.size();
    }

    public void update(GameObject aObject, float aTimeDelta) {
        aObject.refreshStateAll();
        // forces & motion
        simulate(aTimeDelta);
        // motion collision (inner forces & motion)
        detectCollision();
        handleCollision();
        aObject.updateAll(aTimeDelta);
    }

    public void detectCollision() {
        collisionList.clear();
        for (int i = 0; i < objects.size(); i++) {
            for (int j = i + 1; j < objects.size(); j++) {
//                IntersectData lIntersect = objects.get(i).getCollider().intersect(objects.get(j).getCollider());
//                if (lIntersect.isIntersecting()) {
//                    collisionList.put(i * objects.size() + j, lIntersect);
//                }
                IntersectData lIntersect = collisionDetector.detectCollistions(objects.get(i), objects.get(j));
                if (lIntersect.isIntersecting()) {
                    collisionList.put(i * objects.size() + j, lIntersect);
                }
            }
        }

    }

    public void handleCollision() {
        Iterator<Integer> mapIterator = collisionList.keySet().iterator();
        while (mapIterator.hasNext()) {
            int k = mapIterator.next();
            int i = k / objects.size();
            int j = k - i * objects.size();
            IntersectData lIntersect = collisionList.get(k);

            PhysicsObject lPhysics1 = objects.get(i);
            PhysicsObject lPhysics2 = objects.get(j);
            AbstractMap.SimpleEntry<Collider, Collider> lGeoPair
                    = collisionDetector.getColliderPair(lPhysics1, lPhysics2);
            // IntersectData could compute the data drawn from collider in 'GetContactPoint' !!!
            Vector3f lContact = getContactPoint(lPhysics1, lGeoPair.getKey(), lPhysics2, lGeoPair.getValue());// lIntersect.getDirection();// objects.get(i).getPosition().sub(objects.get(j).getPosition());//
            
            float m1 = lPhysics1.getMass();
            float m2 = lPhysics2.getMass();
            Vector3f lV1 = lPhysics1.getVelocity();
            Vector3f lV2 = lPhysics2.getVelocity();

            Vector3f lCenterV = (lV1.mul(m1).add(lV2.mul(m2))).div(m1 + m2);
            Vector3f lNormalOfFirst = lGeoPair.getKey().getNormalInDirection(lContact.normalized().mul(-1));//.sub(objects.get(i).getDistance(lContactDirection)).mul(-1)
            Vector3f lNormalOfSecond = lGeoPair.getValue().getNormalInDirection(lContact.normalized());//.sub(objects.get(j).getDistance(lContactDirection))
            float lU1n = lNormalOfSecond.getParameter(lCenterV.mul(2).sub(lV1));
            float lU2n = lNormalOfFirst.getParameter(lCenterV.mul(2).sub(lV2));
            float lV1n = lNormalOfSecond.getParameter(lV1);
            float lV2n = lNormalOfFirst.getParameter(lV2);
            Vector3f lU1out = lNormalOfSecond.mul(lU1n).add(lV1.sub(lNormalOfSecond.mul(lV1n)));
            Vector3f lU2out = lNormalOfFirst.mul(lU2n).add(lV2.sub(lNormalOfFirst.mul(lV2n)));
            lPhysics1.updateVelocity(lU1out);
            lPhysics2.updateVelocity(lU2out);
        }
        for (PhysicsObject object : objects) {
            object.setUpdatedValues();
        }
    }

    private Vector3f getContactPoint(PhysicsObject aFirst, Collider aForFirst, PhysicsObject aSecond, Collider aForSecond) {
        // P1(t) = P1 + V*t -> |d1+d2|^2 = |dP(t)|^2 = |dP + dV*t|^2
        Vector3f lDeltaP = aFirst.getPosition().sub(aSecond.getPosition());
        Vector3f lDeltaV = aFirst.getVelocity().sub(aSecond.getVelocity());
        float lSolve = lDeltaV.dot(lDeltaP) / (lDeltaV.dot(lDeltaV));
        Vector3f lDistance = lDeltaP.normalized();

        lDistance = aForFirst.getDistanceInDirection(lDistance).add(aForSecond.getDistanceInDirection(lDistance));
        float lRoot = lSolve * lSolve - (lDeltaP.dot(lDeltaP) - lDistance.dot(lDistance)) / (lDeltaV.dot(lDeltaV));
        float dt;
        if (Precision.less(lRoot, 0)) {
            return lDeltaP;
        } else {
            dt = -lSolve - (float) Math.sqrt(lRoot);  // p-q-Equation
            Vector3f lFirstNewPos = aFirst.getVelocity().mul(dt);
            lFirstNewPos = aFirst.getPosition().add(lFirstNewPos);
            Vector3f lSecondNewPos = aSecond.getVelocity().mul(dt);
            lSecondNewPos = aSecond.getPosition().add(lSecondNewPos);
            return lFirstNewPos.sub(lSecondNewPos);
        }

    }

    private Vector3f getContactPoint(Vector3f aP1, Vector3f aVelo1, Vector3f aP2, Vector3f aVelo2, Vector3f aDemandDistance) {
        Vector3f lDeltaP = aP1.sub(aP2);
        Vector3f lDeltaV = aVelo1.sub(aVelo2);
        float lSolve = lDeltaV.dot(lDeltaP) / (2 * lDeltaV.dot(lDeltaV));
        float lRoot = lSolve * lSolve - (lDeltaP.dot(lDeltaP) - aDemandDistance.dot(aDemandDistance)) / (lDeltaV.dot(lDeltaV));
        if (Precision.less(lRoot, 0)) {
            lSolve = -lSolve;
        } else {
            lSolve = -lSolve + (float) Math.sqrt(lRoot);  // p-q-Equation
        }
        return (aP2.add(aVelo2.mul(-lSolve)).sub(aP1.add(aVelo1.mul(-lSolve))));
    }

}
