/*
 * Copyright (C) 2016 Malcolm
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ProjectCenter.PhysicsEngine;

import ProjectCenter.CoreEngine.Matrix4f;
import ProjectCenter.CoreEngine.Precision;
import ProjectCenter.CoreEngine.Transform;
import ProjectCenter.CoreEngine.Vector3f;
import java.util.HashMap;
import java.util.*;
import org.lwjgl.util.mapped.MappedObject;

/**
 *
 * @author Malcolm
 */
public class CollisionManager {

    private final HashMap<ColliderTypes, Collider> colliderList_1;
    private final HashMap<ColliderTypes, Collider> colliderList_2;

    public CollisionManager() {
        colliderList_1 = new HashMap<>();
        colliderList_2 = new HashMap<>();
    }

    private Collider getOrCreateColliderOne(ColliderTypes type, Vector3f special, Transform transform) {
        if (!colliderList_1.containsKey(type)) {
            colliderList_1.put(type, ColliderFactory.getInstance().createColliderByType(type));
        }

        Collider lCollider = colliderList_1.get(type);
        lCollider.update(transform, special);
        return lCollider;
    }

    private Collider getOrCreateColliderTwo(ColliderTypes type, Vector3f special, Transform transform) {
        if (!colliderList_2.containsKey(type)) {
            colliderList_2.put(type, ColliderFactory.getInstance().createColliderByType(type));
        }

        Collider lCollider = colliderList_2.get(type);
        lCollider.update(transform, special);
        return lCollider;
    }

    public AbstractMap.SimpleEntry<Collider, Collider> getColliderPair(PhysicsObject aOne, PhysicsObject aTwo) {
        return new AbstractMap.SimpleEntry<>(
                getOrCreateColliderOne(aOne.getColliderType(), aOne.getFavoredDirection(), aOne.getState()),
                getOrCreateColliderTwo(aTwo.getColliderType(), aTwo.getFavoredDirection(), aTwo.getState()));
    }

    public IntersectData detectCollistions(PhysicsObject aPhysics_1, PhysicsObject aPhysics_2) {
        AbstractMap.SimpleEntry<Collider, Collider> Colliding = getColliderPair(aPhysics_1, aPhysics_2);
        return intersect(Colliding.getKey(), Colliding.getValue());
    }

    private IntersectData IntersectBoxBox(ColliderBox aBox1, ColliderBox aBox2) {
        //For intersection test find a separating axis [see pdfs]
        //Use each axis of the box to project the other box onto to find separating axis (3+3 equations)
        //Do this also with axis which are the cross product of one's box axis with the other's axis (edge-edge-intersection) (3x3 equations)
        Vector3f Differential = aBox2.getPosition().sub(aBox1.getPosition());

        int AxisNo = 15;
        ArrayList<Vector3f> lPossibleAxis = new ArrayList<Vector3f>();
        Vector3f[] lBox1Corners = new Vector3f[AxisNo];
        Vector3f[] lBox2Corners = new Vector3f[AxisNo];
        float[] lDistanceProjection = new float[AxisNo];

        for (int i = 0; i < AxisNo; i++) {
            lPossibleAxis.add(GetPossibleAxis(i, aBox1, aBox2));
            lBox1Corners[i] = ClosestCornerToDirection(aBox1, lPossibleAxis.get(i));
            lBox2Corners[i] = ClosestCornerToDirection(aBox2, lPossibleAxis.get(i));
            lDistanceProjection[i] = lPossibleAxis.get(i).dot(Differential);
        }
        boolean lIntersect = true;
        float lMaxDistance = -1;
        for (int i = 0; i < AxisNo; i++) {
            if (lPossibleAxis.get(i).equals(Vector3f.ZEROVECTOR)) {
                continue;
            }
            lIntersect = lIntersect && Precision.less(Math.abs(lDistanceProjection[i]), lPossibleAxis.get(i).dot(lBox1Corners[i].add(lBox2Corners[i])));
            lMaxDistance = Math.max(lMaxDistance, Math.abs(lDistanceProjection[i]));
        }
        Vector3f lContact3 = new Vector3f();
        Vector3f lContact4= new Vector3f();
        Vector3f lCorner;
        float factor = 0;
        for (int i = 0; i < AxisNo; i++) {
            if (Precision.equals( Math.abs(lDistanceProjection[i]), lMaxDistance)){
             //scaling of the corner according to the overlap with axis and other boxes corner 
             //([t+a-b]/(2t)) & t>0
             factor = (0.5f+Math.abs((lPossibleAxis.get(i).dot(lBox1Corners[i].sub(lBox2Corners[i])))/(2*lMaxDistance)));
             //Correct orientation of the axis
             factor = factor *Precision.signum(lDistanceProjection[i]);
             lCorner  = lBox1Corners[i].mul(factor);
             lContact3 = lContact3.add( lCorner);
             
             //([t+b-a]/(2t)) & t<0 {other axis direction!} -> -1
             lCorner  = lBox2Corners[i].mul(-factor);
             lContact4 = lContact4.add( lCorner);
            }
        }
        Vector3f lCriticalBox1Corner = aBox1.getDistanceInDirection(lContact3.normalized());
        Vector3f lCriticalBox2Corner = aBox2.getDistanceInDirection(lContact4.normalized());
        lCriticalBox1Corner = aBox1.getPosition().add(lCriticalBox1Corner);
        lCriticalBox2Corner = aBox2.getPosition().add(lCriticalBox2Corner);
        return new IntersectData(lIntersect, lCriticalBox1Corner.add(lCriticalBox2Corner).div(2));
    }

    private IntersectData intersect(Collider One, Collider Two) {
        IntersectData lResult = null;
        switch (One.getType()) {
            case ColliderSphere: {
                switch (Two.getType()) {
                    case ColliderSphere: {
                        lResult = IntersectSphereSphere((ColliderSphere) One, (ColliderSphere) Two);
                        break;
                    }
                    case ColliderPlane: {
                        lResult = IntersectPlaneSphere((ColliderPlane) Two, (ColliderSphere) One);
                        break;
                    }
                    case ColliderBox: {
                        lResult = IntersectBoxSphere((ColliderBox) Two, (ColliderSphere) One);
                        break;
                    }
                }
                break;
            }
            case ColliderPlane: {
                switch (Two.getType()) {
                    case ColliderSphere: {
                        lResult = IntersectPlaneSphere((ColliderPlane) One, (ColliderSphere) Two);
                        break;
                    }
                    case ColliderPlane: {
                        lResult = IntersectPlanePlane((ColliderPlane) One, (ColliderPlane) Two);
                        break;
                    }
                    case ColliderBox: {
                        lResult = IntersectBoxPlane((ColliderBox) Two, (ColliderPlane) One);
                        break;
                    }
                }
                break;
            }
            case ColliderBox: {
                switch (Two.getType()) {
                    case ColliderSphere: {
                        lResult = IntersectBoxSphere((ColliderBox) One, (ColliderSphere) Two);
                        break;
                    }
                    case ColliderPlane: {
                        lResult = IntersectBoxPlane((ColliderBox) One, (ColliderPlane) Two);
                        break;
                    }
                    case ColliderBox: {
                        lResult = IntersectBoxBox((ColliderBox) One, (ColliderBox) Two);
                        break;
                    }
                }
                break;
            }
        }
        if (lResult == null) {
            System.out.println("Error Collisions not implemented between the specified colliders.");
            System.exit(1);
        }
        return lResult;
    }

    private Vector3f GetPossibleAxis(int aNo, ColliderBox aBox1, ColliderBox aBox2) {
        Vector3f lResult = new Vector3f();
        int lType = aNo % 3;
        int lSegment = aNo / 3;
        switch (lSegment) {
            case 0: {
                switch (lType) {
                    case 0: {
                        lResult = aBox1.getDirectionWidth();
                        break;
                    }
                    case 1: {
                        lResult = aBox1.getDirectionTop();
                        break;
                    }
                    case 2: {
                        lResult = aBox1.getDirectionFront();
                        break;
                    }
                }
                break;
            }
            case 1: {
                switch (lType) {
                    case 0: {
                        lResult = aBox2.getDirectionWidth();
                        break;
                    }
                    case 1: {
                        lResult = aBox2.getDirectionTop();
                        break;
                    }
                    case 2: {
                        lResult = aBox2.getDirectionFront();
                        break;
                    }
                }
                break;
            }
            case 2: {
                switch (lType) {
                    case 0: {
                        lResult = aBox1.getDirectionWidth().cross(aBox2.getDirectionWidth());
                        lResult = lResult.normalized();
                        break;
                    }
                    case 1: {
                        lResult = aBox1.getDirectionWidth().cross(aBox2.getDirectionTop());
                        lResult = lResult.normalized();
                        break;
                    }
                    case 2: {
                        lResult = aBox1.getDirectionWidth().cross(aBox2.getDirectionFront());
                        lResult = lResult.normalized();
                        break;
                    }
                }
                break;
            }
            case 3: {
                switch (lType) {
                    case 0: {
                        lResult = aBox1.getDirectionTop().cross(aBox2.getDirectionWidth());
                        lResult = lResult.normalized();
                        break;
                    }
                    case 1: {
                        lResult = aBox1.getDirectionTop().cross(aBox2.getDirectionTop());
                        lResult = lResult.normalized();
                        break;
                    }
                    case 2: {
                        lResult = aBox1.getDirectionTop().cross(aBox2.getDirectionFront());
                        lResult = lResult.normalized();
                        break;
                    }
                }
                break;
            }
            case 4: {
                switch (lType) {
                    case 0: {
                        lResult = aBox1.getDirectionFront().cross(aBox2.getDirectionWidth());
                        lResult = lResult.normalized();
                        break;
                    }
                    case 1: {
                        lResult = aBox1.getDirectionFront().cross(aBox2.getDirectionTop());
                        lResult = lResult.normalized();
                        break;
                    }
                    case 2: {
                        lResult = aBox1.getDirectionFront().cross(aBox2.getDirectionFront());
                        lResult = lResult.normalized();
                        break;
                    }
                }
                break;
            }
        }
        return lResult;
    }

    private Vector3f ClosestCornerToDirection(ColliderBox aBox, Vector3f aNormedAxis) {
        //Box can only penetrate plane by a corner
        float lWidth = aNormedAxis.dot(aBox.getDirectionWidth());
        float lTop = aNormedAxis.dot(aBox.getDirectionTop());
        float lFront = aNormedAxis.dot(aBox.getDirectionFront());
        //Find the corner/point which has the largest projection on the axis
        lWidth = Precision.signum(lWidth) * aBox.getDistanceWidth();
        lTop = Precision.signum(lTop) * aBox.getDistanceTop();
        lFront = Precision.signum(lFront) * aBox.getDistanceFront();

        Vector3f lCorner = new Vector3f();
        lCorner = lCorner.add(aBox.getDirectionWidth().mul(lWidth));
        lCorner = lCorner.add(aBox.getDirectionTop().mul(lTop));
        lCorner = lCorner.add(aBox.getDirectionFront().mul(lFront));

        return lCorner;
    }

    private IntersectData IntersectBoxSphere(ColliderBox aBox, ColliderSphere aSphere) {
        //Project distance vector on each axis plane of the box.
        //The distance of the center point has to be reduced by the distance of each plane from box center
        Vector3f Differential = aSphere.getPosition().sub(aBox.getPosition());
        float lDistanceWidth = Math.abs(Differential.dot(aBox.getDirectionWidth())) - aBox.getDistanceWidth();
        float lDistanceTop = Math.abs(Differential.dot(aBox.getDirectionTop())) - aBox.getDistanceTop();
        float lDistanceFront = Math.abs(Differential.dot(aBox.getDirectionFront())) - aBox.getDistanceFront();

        Vector3f lDistance = new Vector3f();
        //Distances less than zero have no impact since the center is on this axis already between the boundary planes
        lDistance = lDistance.add(aBox.getDirectionWidth().mul(Precision.greater(lDistanceWidth, 0) ? lDistanceWidth : 0));
        lDistance = lDistance.add(aBox.getDirectionTop().mul(Precision.greater(lDistanceTop, 0) ? lDistanceTop : 0));
        lDistance = lDistance.add(aBox.getDirectionFront().mul(Precision.greater(lDistanceFront, 0) ? lDistanceFront : 0));

        return new IntersectData(Precision.less(lDistance.length(), aSphere.getRadius()), aBox.getPosition().add(lDistance));
    }

    private IntersectData IntersectBoxPlane(ColliderBox aBox, ColliderPlane aPlane) {
        //This can also be seen as a separation of axis like Box-Box-case with limitation to plane axis!
        Vector3f Differential = aPlane.getPosition().sub(aBox.getPosition());
        
        //Get box center projected on the plane
        float lCenterDistance = (aPlane.getNormal().dot(Differential));
        boolean lInverseNormal = Precision.less(lCenterDistance, 0);
        lCenterDistance = Math.abs(lCenterDistance);
        //Box can only penetrate plane by a corner
        Vector3f lCorner = ClosestCornerToDirection(aBox, aPlane.getNormal().mul(lInverseNormal? -1:1));

        //Project this corner on the plane normal like the center distance vector
        float lCornerDistanceInNormal = (lCorner.dot(aPlane.getNormal().mul(lInverseNormal? -1:1)));
        lInverseNormal = lInverseNormal ^ Precision.less(lCornerDistanceInNormal, 0);
        lCornerDistanceInNormal = Math.abs(lCornerDistanceInNormal);
        
        boolean lIntersects = Precision.less(lCenterDistance, lCornerDistanceInNormal);

        return new IntersectData(lIntersects, aBox.getPosition().add(lCorner.normalized().mul( (lCenterDistance)).add(!lIntersects?lCorner.mul(-1):new Vector3f())));
    }

//    private IntersectData IntersectBoxBox(ColliderBox aBox1, ColliderBox aBox2) {
//        //For intersection test find a separating axis [see pdfs]
//        //Use each axis of the box to project the other box onto to find separating axis (3+3 equations)
//        //Do this also with axis which are the cross product of one's box axis with the other's axis (edge-edge-intersection) (3x3 equations)
//        Vector3f Differential = aBox2.getPosition().sub(aBox1.getPosition());
//
//        int AxisNo = 15;
//        ArrayList<Vector3f> lPossibleAxis = new ArrayList<Vector3f>();
//        float[] lBox1Projection = new float[AxisNo];
//        float[] lDistanceProjection = new float[AxisNo];
//
//        for (int i = 0; i < AxisNo; i++) {
//            lPossibleAxis.add(GetPossibleAxis(i, aBox1, aBox2));
//            lBox1Projection[i] = lPossibleAxis.get(i).dot(ClosestCornerToDirection(aBox1, lPossibleAxis.get(i)));
//            lBox1Projection[i] += lPossibleAxis.get(i).dot(ClosestCornerToDirection(aBox2, lPossibleAxis.get(i)));
//            lDistanceProjection[i] = lPossibleAxis.get(i).dot(Differential);
//        }
//        boolean lIntersect = true;
//        float lLargestDistance = -1;
//        float lMaxDistance = -1;
//        for (int i = 0; i < AxisNo; i++) {
//            if (lPossibleAxis.get(i).equals(Vector3f.ZEROVECTOR)) {
//                continue;
//            }
//            lIntersect = lIntersect && Precision.less(Math.abs(lDistanceProjection[i]), lBox1Projection[i]);
//            lMaxDistance = Math.max(lMaxDistance, Math.abs(lDistanceProjection[i]));
//            lLargestDistance = Math.max(lLargestDistance,
//                    Precision.less(Math.abs(lDistanceProjection[i]), lBox1Projection[i])
//                            ? Math.abs(lDistanceProjection[i]) : lLargestDistance);
//        }
//        Vector3f lContact3 = (aBox1.getDistanceInDirection(Differential).sub(aBox2.getDistanceInDirection(Differential)).add(Differential)).div(2);
//        lContact3 = aBox1.getPosition().add( lContact3);
//        return new IntersectData(lIntersect, lContact3);
//    }

    private IntersectData IntersectPlaneSphere(ColliderPlane aPlane, ColliderSphere aSphere) {

        float lDistanceFromCenter = aPlane.getNormal().dot(aSphere.getPosition().sub(aPlane.getPosition()));
        boolean lIsPositive = Precision.less(lDistanceFromCenter, 0);
        lDistanceFromCenter = Math.abs(lDistanceFromCenter);
        float lDistanceFromSphere = lDistanceFromCenter - aSphere.getRadius();

        return new IntersectData(Precision.less(lDistanceFromSphere, 0), aSphere.getPosition().add(aPlane.getNormal().mul(-(lIsPositive ? -1 : 1) * aSphere.getRadius())));
    }

    private IntersectData IntersectPlanePlane(ColliderPlane aPlane1, ColliderPlane aPlane2) {
        // This is not really good, but infinite planes are no good at all
        Vector3f Differential = aPlane2.getPosition().sub(aPlane1.getPosition());

        float lDistanceFromCenter = Math.abs(Differential.dot(aPlane1.getNormal()));
        Vector3f IntersectionLine = aPlane1.getNormal().cross(aPlane2.getNormal());
        Vector3f lDistance = aPlane1.getNormal().mul(lDistanceFromCenter);
        if (!IntersectionLine.equals(Vector3f.ZEROVECTOR)) {
            //Determine the starting point of the intersection line
            Vector3f lLineVector = IntersectionLine.cross(aPlane1.getNormal());
            float lLineParameter = Differential.dot(aPlane2.getNormal()) / (lLineVector.dot(aPlane2.getNormal()));
            lDistance = aPlane1.getPosition().add(lLineVector.mul(lLineParameter));
        }

        return new IntersectData(!IntersectionLine.equals(Vector3f.ZEROVECTOR), lDistance);
    }

    private IntersectData IntersectSphereSphere(ColliderSphere aSphere1, ColliderSphere aSphere2) {
        float lRadiusDistance = aSphere1.getRadius() + aSphere2.getRadius();
        Vector3f lDirection = aSphere2.getPosition().sub(aSphere1.getPosition());
        float lCenterDistance = (lDirection).length();
        lDirection = lDirection.div(lCenterDistance);

        return new IntersectData(Precision.less(lCenterDistance, lRadiusDistance), aSphere1.getPosition().add(lDirection.mul(lCenterDistance - aSphere2.getRadius()))); 
    }

}
