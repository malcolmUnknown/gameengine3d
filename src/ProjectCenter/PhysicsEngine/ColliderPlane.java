/* GameEngine 3D - A programm to design games with 3D content 
 * Copyright (C) 2016-2056 Frank Küwen
 *  
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package ProjectCenter.PhysicsEngine;

import ProjectCenter.CoreEngine.Precision;
import ProjectCenter.CoreEngine.Transform;
import ProjectCenter.CoreEngine.Vector3f;

public class ColliderPlane extends Collider {

    private Vector3f normal;

    public ColliderPlane() {
        super(ColliderTypes.ColliderPlane);
        normal = new Vector3f(0,0,1);
    }

    public ColliderPlane Normalized() {
        normal = normal.normalized();
        return this;
    }

    public Vector3f getNormal() {
        return normal;
    }

    public float getDistance() {
        return (position).dot(normal);
    }

//    @Override
//    public void updateOrientation(Quaternion aNewOrientation) {
//        normal = aNewOrientation.toRotationMatrix().mul(normal).normalized();
//    }
    @Override
    public Vector3f getDistanceInDirection(Vector3f aNormedDirection) {
        return aNormedDirection.mul(aNormedDirection.dot(normal) * getDistance());
    }

    @Override
    public void update(Transform aState, Vector3f aFavoredDir) {
        super.update(aState, aFavoredDir);
        normal = (aFavoredDir.rotate(aState.getRotation())).normalized();//aState.getRotation().toRotationMatrix().mul(aFavoredDir)
    }

    @Override
    public Vector3f getNormalInDirection(Vector3f aNormedDirection) {
        return normal.mul(aNormedDirection.dot(normal)).normalized();
    }
}
