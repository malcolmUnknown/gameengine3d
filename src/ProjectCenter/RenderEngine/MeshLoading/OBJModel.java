/* GameEngine 3D - A programm to design games with 3D content 
 * Copyright (C) 2016-2056 Frank Küwen
 *  
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package ProjectCenter.RenderEngine.MeshLoading;

import ProjectCenter.CoreEngine.Util;
import ProjectCenter.CoreEngine.Vector2f;
import ProjectCenter.CoreEngine.Vector3f;
import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.HashMap;

public class OBJModel {

    private ArrayList<Vector3f> positions;
    private ArrayList<Vector2f> textureCoordinates;
    private ArrayList<Vector3f> normals;
    private ArrayList<OBJIndex> indices;
    private boolean hasTextureCoordinates;
    private boolean hasNormals;

    public OBJModel(String aFileName) {
        positions = new ArrayList<Vector3f>();
        normals = new ArrayList<Vector3f>();
        textureCoordinates = new ArrayList<Vector2f>();
        indices = new ArrayList<OBJIndex>();
        hasNormals = false;
        hasTextureCoordinates = false;

        BufferedReader meshReader = null;

        try {
            meshReader = new BufferedReader(new FileReader(aFileName));

            String line;
            String[] tokens;
            while ((line = meshReader.readLine()) != null) {
                tokens = line.split(" ");
                tokens = Util.removeEmptyStrings(tokens);

                if (tokens.length == 0 || tokens[0].equals("#")) {
                } else if (tokens[0].equals("v")) {
                    positions.add(
                            new Vector3f(
                                    Float.valueOf(tokens[1]),
                                    Float.valueOf(tokens[2]),
                                    Float.valueOf(tokens[3])
                            ));
                } else if (tokens[0].equals("vt")) {
                    textureCoordinates.add(
                            new Vector2f(
                                    Float.valueOf(tokens[1]),
                                    Float.valueOf(tokens[2])
                            ));
                } else if (tokens[0].equals("vn")) {
                    normals.add(
                            new Vector3f(
                                    Float.valueOf(tokens[1]),
                                    Float.valueOf(tokens[2]),
                                    Float.valueOf(tokens[3])
                            ));
                } else if (tokens[0].equals("f")) //Triangluate convex vertices
                {
                    for (int i = 0; i < tokens.length - 3; i++) {
                        indices.add(parseOBJIndex(tokens[1]));
                        indices.add(parseOBJIndex(tokens[2 + i]));
                        indices.add(parseOBJIndex(tokens[3 + i]));
                    }
                }
            }
            meshReader.close();

        } catch (Exception e) {
            e.printStackTrace();
            System.exit(1);
        }

    }

    public IndexedModel toIndexedModel() {
        IndexedModel lResult = new IndexedModel();
        IndexedModel lNormalModel = new IndexedModel();
        HashMap<OBJIndex, Integer> lResultIndexMap = new HashMap<OBJIndex, Integer>();
        HashMap<Integer, Integer> lNormalIndexMap = new HashMap<Integer, Integer>();
        HashMap<Integer, Integer> lIndexMap = new HashMap<Integer, Integer>();

        for (int i = 0; i < indices.size(); i++) {
            OBJIndex lCurrent = indices.get(i);
            Vector3f lCurrentPosition = positions.get(lCurrent.vertexIndex);
            Vector2f lCurrentTextureCoordinate;
            Vector3f lCurrentNormal;

            if (hasTextureCoordinates) {
                lCurrentTextureCoordinate = textureCoordinates.get(
                        lCurrent.textureCoordinateIndex);
            } else {
                lCurrentTextureCoordinate = new Vector2f(0, 0);
            }

            if (hasNormals) {
                lCurrentNormal = normals.get(lCurrent.normalIndex);
            } else {
                lCurrentNormal = new Vector3f(0, 0, 0);
            }

            Integer lModelVertexIndex = lResultIndexMap.get(lCurrent);

            if (lModelVertexIndex == null) {
                lModelVertexIndex = lResult.getPosition().size();
                lResultIndexMap.put(lCurrent, lModelVertexIndex);

                lResult.getPosition().add(lCurrentPosition);
                lResult.getTextureCoordinate().add(lCurrentTextureCoordinate);
                if (hasNormals) {
                    lResult.getNormal().add(lCurrentNormal);
                }
            }
            Integer lNormalModelIndex = lNormalIndexMap.get(lCurrent.vertexIndex);
            if (lNormalModelIndex == null) {
                lNormalModelIndex = lNormalModel.getPosition().size();
                lNormalIndexMap.put(lCurrent.vertexIndex, lNormalModelIndex);

                lNormalModel.getPosition().add(lCurrentPosition);
                lNormalModel.getTextureCoordinate().add(lCurrentTextureCoordinate);
                lNormalModel.getNormal().add(lCurrentNormal);
            }
            lResult.getIndices().add(lModelVertexIndex);
            lNormalModel.getIndices().add(lNormalModelIndex);
            lIndexMap.put(lModelVertexIndex, lNormalModelIndex);
        }
        if (!hasNormals) {
            lNormalModel.calculateNormals();
            for (int i = 0; i < lResult.getPosition().size(); i++) {
                lResult.getNormal().add(lNormalModel.getNormal().get(lIndexMap.get(i)));
            }

        }
        return lResult;
    }

    private OBJIndex parseOBJIndex(String aToken) {
        String[] lValues = aToken.split("/");
        OBJIndex lResult = new OBJIndex();
        lResult.vertexIndex = Integer.parseInt(lValues[0]) - 1;

        if (lValues.length > 1) {
            hasTextureCoordinates = true;
            lResult.textureCoordinateIndex = Integer.parseInt(lValues[1]) - 1;
            if (lValues.length > 2) {
                hasNormals = true;
                lResult.normalIndex = Integer.parseInt(lValues[2]) - 1;
            }
        }
        return lResult;
    }
}
