/* GameEngine 3D - A programm to design games with 3D content 
 * Copyright (C) 2016-2056 Frank Küwen
 *  
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package ProjectCenter.RenderEngine.MeshLoading;

public class OBJIndex {

    public int vertexIndex;
    public int textureCoordinateIndex;
    public int normalIndex;

    @Override
    public boolean equals(Object aObject) {
        if (aObject == OBJIndex.class) {
            OBJIndex lIndex = (OBJIndex) aObject;
            return vertexIndex == lIndex.vertexIndex
                    && textureCoordinateIndex == lIndex.textureCoordinateIndex
                    && normalIndex == lIndex.normalIndex;
        } else {
            return false;
        }
    }

    @Override
    public int hashCode() {
        final int BASE = 17;
        final int MULTIPLIER = 31;
        int lResult = BASE;
        lResult = MULTIPLIER * lResult + vertexIndex;
        lResult = MULTIPLIER * lResult + textureCoordinateIndex;
        lResult = MULTIPLIER * lResult + normalIndex;
        return lResult;

    }
}
