/* GameEngine 3D - A programm to design games with 3D content 
 * Copyright (C) 2016-2056 Frank Küwen
 *  
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package ProjectCenter.RenderEngine.MeshLoading;

import ProjectCenter.CoreEngine.Vector2f;
import ProjectCenter.CoreEngine.Vector3f;
import java.util.ArrayList;

public class IndexedModel {
    
    private ArrayList<Vector3f> positions;
    private ArrayList<Vector2f> textureCoordinates;
    private ArrayList<Vector3f> normals;
    private ArrayList<Integer> indices;
    
    public IndexedModel()
    {
        positions = new ArrayList<Vector3f>();
        normals = new ArrayList<Vector3f>();
        textureCoordinates = new ArrayList<Vector2f>();
        indices = new ArrayList<Integer>();        
    }

    public ArrayList<Vector3f> getPosition() {
        return positions;
    }

    public ArrayList<Vector2f> getTextureCoordinate() {
        return textureCoordinates;
    }

    public ArrayList<Vector3f> getNormal() {
        return normals;
    }

    public ArrayList<Integer> getIndices() {
        return indices;
    }

    public void calculateNormals() {
        for (int i = 0; i < indices.size(); i += 3) {
            int i0 = indices.get(i);
            int i1 = indices.get(i + 1);
            int i2 = indices.get(i + 2);

            Vector3f v1
                    = positions.get(i1).sub(positions.get(i0));
            Vector3f v2
                    = positions.get(i2).sub(positions.get(i0));

            Vector3f normal = v1.cross(v2).normalized();

            normals.get(i0).set(normals.get(i0).add(normal));
            normals.get(i1).set(normals.get(i1).add(normal));
            normals.get(i2).set(normals.get(i2).add(normal));
        }

        for (int i = 0; i < normals.size(); i++) {
            normals.get(i).set(normals.get(i).normalized());
        }
    }
    
}
