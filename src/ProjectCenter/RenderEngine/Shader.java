/* GameEngine 3D - A programm to design games with 3D content 
 * Copyright (C) 2016-2056 Frank Küwen
 *  
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package ProjectCenter.RenderEngine;

import Components.BaseLight;
import Components.DirectionalLight;
import Components.PointLight;
import Components.SpotLight;
import ProjectCenter.CoreEngine.Matrix4f;
import ProjectCenter.CoreEngine.Transform;
import ProjectCenter.CoreEngine.Util;
import ProjectCenter.CoreEngine.Vector3f;
import ProjectCenter.RenderEngine.ResourceManagment.ShaderResource;
import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.HashMap;
import static org.lwjgl.opengl.GL20.GL_COMPILE_STATUS;
import static org.lwjgl.opengl.GL20.GL_FRAGMENT_SHADER;
import static org.lwjgl.opengl.GL20.GL_LINK_STATUS;
import static org.lwjgl.opengl.GL20.GL_VALIDATE_STATUS;
import static org.lwjgl.opengl.GL20.GL_VERTEX_SHADER;
import static org.lwjgl.opengl.GL20.glAttachShader;
import static org.lwjgl.opengl.GL20.glBindAttribLocation;
import static org.lwjgl.opengl.GL20.glCompileShader;
import static org.lwjgl.opengl.GL20.glCreateShader;
import static org.lwjgl.opengl.GL20.glGetProgrami;
import static org.lwjgl.opengl.GL20.glGetShaderInfoLog;
import static org.lwjgl.opengl.GL20.glGetShaderi;
import static org.lwjgl.opengl.GL20.glGetUniformLocation;
import static org.lwjgl.opengl.GL20.glLinkProgram;
import static org.lwjgl.opengl.GL20.glShaderSource;
import static org.lwjgl.opengl.GL20.glUniform1f;
import static org.lwjgl.opengl.GL20.glUniform1i;
import static org.lwjgl.opengl.GL20.glUniform3f;
import static org.lwjgl.opengl.GL20.glUniformMatrix4;
import static org.lwjgl.opengl.GL20.glUseProgram;
import static org.lwjgl.opengl.GL20.glValidateProgram;
import static org.lwjgl.opengl.GL32.GL_GEOMETRY_SHADER;

public class Shader {

    private static HashMap<String, ShaderResource> loadedShaders = new HashMap<String, ShaderResource>();
    private final ShaderResource resource;
    private final String fileName;

    public Shader(String aFileName) {

        fileName = aFileName;
        ShaderResource oldResource = loadedShaders.get(aFileName);
        if (oldResource != null) {
            resource = oldResource;
            resource.addReference();
        } else {
            resource = new ShaderResource();

            String vertexShaderText = loadShader(aFileName + ".vs");
            String fragmentShaderText = loadShader(aFileName + ".fs");

            addVertexShader(vertexShaderText);
            addFragmentShader(fragmentShaderText);

            addAllAttribures(vertexShaderText);

            compileShader();

            addAllUnifroms(vertexShaderText);
            addAllUnifroms(fragmentShaderText);
            loadedShaders.put(aFileName, resource);
        }
    }

    public void bind() {
        glUseProgram(resource.getProgramm());
    }

    public void updateUniforms(Transform aTransform, Material aMaterial, RenderingEngine aRenderingEngine) {

        Matrix4f lModelMatrix = aTransform.getTransformation();
        Matrix4f lProjectedMatrix = aRenderingEngine.getCamera().getViewProjection().mul(lModelMatrix);

        for (int i = 0; i < resource.getUniformNames().size(); i++) {
            String lUniformName = resource.getUniformNames().get(i);
            String lUniformType = resource.getUniformTypes().get(i);

            if (lUniformType.equals("sampler2D")) {
                int lSamplerSlot = aRenderingEngine.getSamplerSlot(lUniformName);
                aMaterial.getTexture(lUniformName).bind(lSamplerSlot);
                setUniformI(lUniformName, lSamplerSlot);
            } else if (lUniformName.startsWith("T_"))
                if (lUniformName.equals("T_aModelViewPerspectiveTransformation"))
                    setUniform(lUniformName, lProjectedMatrix);
                else if (lUniformName.equals("T_aModelTransformation"))
                    setUniform(lUniformName, lModelMatrix);
                else
                    throw new IllegalArgumentException(lUniformName
                            + "is not a supported type of Transform");
            else if (lUniformName.startsWith("R_")) {
                String lUnprefixedUniformName = lUniformName.substring(2);
                switch (lUniformType) {
                    case "vec3":
                        setUniform(lUniformName, aRenderingEngine.getVector(lUnprefixedUniformName));
                        break;
                    case "float":
                        setUniformF(lUniformName, aRenderingEngine.getFloat(lUnprefixedUniformName));
                        break;
                    case "DirectionalLight":
                        setUniformDirectionalLight(lUniformName, (DirectionalLight) aRenderingEngine.getActiveLight());
                        break;
                    case "PointLight":
                        setUniformPointLight(lUniformName, (PointLight) aRenderingEngine.getActiveLight());
                        break;
                    case "SpotLight":
                        setUniformSpotLight(lUniformName, (SpotLight) aRenderingEngine.getActiveLight());
                        break;
                    default:
                        aRenderingEngine.updateUniformStruct(aTransform, aMaterial, this, lUniformName, lUniformType);
                        break;
                }

            } else if (lUniformName.startsWith("C_"))
                if (lUniformName.equals("C_aCameraPosition"))
                    setUniform(lUniformName, aRenderingEngine.getCamera().getTransform().getTransformedPosition());
                else
                    throw new IllegalArgumentException(lUniformName
                            + "is not a supported type of Camera");
            else if (lUniformType.equals("vec3"))
                setUniform(lUniformName, aMaterial.getVector(lUniformName));
            else if (lUniformType.equals("float"))
                setUniformF(lUniformName, aMaterial.getFloat(lUniformName));
            else
                throw new IllegalArgumentException(lUniformName
                        + " is not a supported type of Material");

        }
    }

    private void addAllAttribures(String aShaderText) {
        final String ATTRIBUTE_KEYWORD = "attribute";
        // string-Position in *.fs/vs before the keyword
        int lAttributeStartLocation = aShaderText.indexOf(ATTRIBUTE_KEYWORD);
        int lAttributeNumber = 0;
        while (lAttributeStartLocation != -1) {
            if (!(lAttributeStartLocation != 0
                    && (Character.isWhitespace(aShaderText.charAt(lAttributeStartLocation - 1))
                    || aShaderText.charAt(lAttributeStartLocation - 1) == ';')
                    && Character.isWhitespace(aShaderText.charAt(
                                    lAttributeStartLocation + ATTRIBUTE_KEYWORD.length()))))
                continue;
            int lLineBegin = lAttributeStartLocation + ATTRIBUTE_KEYWORD.length() + 1;
            int lLineEnd = aShaderText.indexOf(";", lLineBegin);

            String lAttributeLine = aShaderText.substring(lLineBegin, lLineEnd).trim();
            String lAttributeName = lAttributeLine.substring(lAttributeLine.indexOf(' ') + 1).trim();

            setAttributeLocation(lAttributeName, lAttributeNumber);
            lAttributeNumber++;

            lAttributeStartLocation = aShaderText.indexOf(
                    ATTRIBUTE_KEYWORD, lAttributeStartLocation + ATTRIBUTE_KEYWORD.length());
        }
    }

    private class GLSLStruct {

        public String name;
        public String type;
    }

    private HashMap<String, ArrayList<GLSLStruct>> findeUnifromStructs(String aShaderText) {

        HashMap<String, ArrayList<GLSLStruct>> lResult = new HashMap<String, ArrayList<GLSLStruct>>();
        // string-Position in *.fs/vs before the keyword
        final String STRUCT_KEYWORD = "struct";
        int lStructStartLocation = aShaderText.indexOf(STRUCT_KEYWORD);
        while (lStructStartLocation != -1) {
            if (!(lStructStartLocation != 0
                    && (Character.isWhitespace(aShaderText.charAt(lStructStartLocation - 1))
                    || aShaderText.charAt(lStructStartLocation - 1) == ';')
                    && Character.isWhitespace(aShaderText.charAt(
                                    lStructStartLocation + STRUCT_KEYWORD.length()))))
                continue;
            int lNameBegin = lStructStartLocation + STRUCT_KEYWORD.length() + 1;
            int lBraceBegin = aShaderText.indexOf("{", lNameBegin);
            int lBraceEnd = aShaderText.indexOf("}", lBraceBegin);

            String lStructName = aShaderText.substring(lNameBegin, lBraceBegin).trim();
            ArrayList<GLSLStruct> glslStructs = new ArrayList<GLSLStruct>();

            int lComponentSemicolonPosition = aShaderText.indexOf(";", lBraceBegin);
            //Semicolon for substring that is trim() 
            while (lComponentSemicolonPosition != -1 && lComponentSemicolonPosition < lBraceEnd) {
                int lComponetnNameEnd = lComponentSemicolonPosition + 1;
                while (Character.isWhitespace(aShaderText.charAt(lComponetnNameEnd - 1)) || aShaderText.charAt(lComponetnNameEnd - 1) == ';') {
                    lComponetnNameEnd--;
                }

                int lComponentNameStart = lComponentSemicolonPosition;
                while (!Character.isWhitespace(aShaderText.charAt(lComponentNameStart - 1))) {
                    lComponentNameStart--;
                }

                int lComponentTypeEnd = lComponentNameStart;
                while (Character.isWhitespace(aShaderText.charAt(lComponentTypeEnd - 1))) {
                    lComponentTypeEnd--;
                }
                int lComponentTypeStart = lComponentTypeEnd;
                while (!Character.isWhitespace(aShaderText.charAt(lComponentTypeStart - 1))) {
                    lComponentTypeStart--;
                }
                String lComponentName = aShaderText.substring(lComponentNameStart, lComponetnNameEnd);
                String lComponentTpye = aShaderText.substring(lComponentTypeStart, lComponentTypeEnd);

                GLSLStruct glslStruct = new GLSLStruct();
                glslStruct.name = lComponentName;
                glslStruct.type = lComponentTpye;
                glslStructs.add(glslStruct);

                lComponentSemicolonPosition = aShaderText.indexOf(";", lComponentSemicolonPosition + 1);
            }
            lResult.put(lStructName, glslStructs);

            lStructStartLocation = aShaderText.indexOf(
                    STRUCT_KEYWORD, lStructStartLocation + STRUCT_KEYWORD.length());
        }
        return lResult;
    }

    private void addAllUnifroms(String aShaderText) {
        HashMap<String, ArrayList<GLSLStruct>> lStructs = findeUnifromStructs(aShaderText);

        final String UNIFORM_KEYWORD = "uniform";
        // string-Position in *.fs/vs before the keyword
        int lUniformStartLocation = aShaderText.indexOf(UNIFORM_KEYWORD);
        while (lUniformStartLocation != -1) {
            if (!(lUniformStartLocation != 0
                    && (Character.isWhitespace(aShaderText.charAt(lUniformStartLocation - 1))
                    || aShaderText.charAt(lUniformStartLocation - 1) == ';')
                    && Character.isWhitespace(aShaderText.charAt(
                                    lUniformStartLocation + UNIFORM_KEYWORD.length()))))
                continue;
            int lLineBegin = lUniformStartLocation + UNIFORM_KEYWORD.length() + 1;
            int lLineEnd = aShaderText.indexOf(";", lLineBegin);

            String lUniformLine = aShaderText.substring(lLineBegin, lLineEnd).trim();
            int lWhiteSpacePosition = lUniformLine.indexOf(' ');
            String lUniformName = lUniformLine.substring(lWhiteSpacePosition + 1, lUniformLine.length()).trim();
            String lUniformType = lUniformLine.substring(0, lWhiteSpacePosition).trim();

            resource.getUniformNames().add(lUniformName);
            resource.getUniformTypes().add(lUniformType);// Use GLSLstruct!
            addUniform(lUniformName, lUniformType, lStructs);
            lUniformStartLocation = aShaderText.indexOf(
                    UNIFORM_KEYWORD, lUniformStartLocation + UNIFORM_KEYWORD.length());
        }
    }

    private void addUniform(String aUniformName, String aUniformType, HashMap<String, ArrayList<GLSLStruct>> aStructs) {
        boolean lAddThis = true;

        ArrayList<GLSLStruct> lStructComponents = aStructs.get(aUniformType);

        if (lStructComponents != null) {
            lAddThis = false;
            for (GLSLStruct struct : lStructComponents) {
                addUniform(aUniformName + "." + struct.name, struct.type, aStructs);
            }
        }

        if (!lAddThis)
            return;

        int uniformLocation = glGetUniformLocation(resource.getProgramm(), aUniformName);

        if (uniformLocation == -1) {
            System.err.println("Error: Could not find uniform: " + aUniformName);
            new Exception().printStackTrace();
            System.exit(1);
        }

        resource.getUniforms().put(aUniformName, uniformLocation);
    }

    private void addVertexShader(String aText) {
        addProgram(aText, GL_VERTEX_SHADER);
    }

    private void addGeometryShader(String aText) {
        addProgram(aText, GL_GEOMETRY_SHADER);
    }

    private void addFragmentShader(String aText) {
        addProgram(aText, GL_FRAGMENT_SHADER);
    }

    private void setAttributeLocation(String aAttributeName, int aLocation) {

        glBindAttribLocation(resource.getProgramm(), aLocation, aAttributeName);
    }

    private void compileShader() {
        glLinkProgram(resource.getProgramm());// glGetProgram replaced by glGetProgramI
        if (glGetProgrami(resource.getProgramm(), GL_LINK_STATUS) == 0) {
            System.err.println(glGetShaderInfoLog(resource.getProgramm(), 1024));
            System.exit(1);
        }
        glValidateProgram(resource.getProgramm());// glGetProgram replaced by glGetProgramI
        if (glGetProgrami(resource.getProgramm(), GL_VALIDATE_STATUS) == 0) {
            System.err.println(glGetShaderInfoLog(resource.getProgramm(), 1024));
            System.exit(1);
        }

    }

    private void addProgram(String aText, int aType) {
        int shader = glCreateShader(aType);

        if (shader == 0) {
            System.err.println("Shader creation failed: Could not find valid memory location when adding shader.");
            System.exit(1);
        }

        glShaderSource(shader, aText);
        glCompileShader(shader);

        // glGetShader replaced by glGetShaderI
        if (glGetShaderi(shader, GL_COMPILE_STATUS) == 0) {
            System.err.println(glGetShaderInfoLog(shader, 1024));
            System.exit(1);
        }

        glAttachShader(resource.getProgramm(), shader);
    }

    public void setUniformI(String aUniformName, int aValue) {
        glUniform1i(resource.getUniforms().get(aUniformName), aValue);
    }

    public void setUniformF(String aUniformName, float aValue) {
        glUniform1f(resource.getUniforms().get(aUniformName), aValue);
    }

    public void setUniform(String aUniformName, Vector3f aVector) {
        glUniform3f(resource.getUniforms().get(aUniformName), aVector.getX(), aVector.getY(), aVector.getZ());
    }

    public void setUniform(String aUniformName, Matrix4f aMatrix) { 
        // true = row-major order matrix | false = colum-major order matrix
        glUniformMatrix4(resource.getUniforms().get(aUniformName), true, Util.createFlippedBuffer(aMatrix));

    }

    public void setUniformBaseLight(String aUniformName, BaseLight aBaseLight) {
        setUniform(aUniformName + ".color", aBaseLight.getColor());
        setUniformF(aUniformName + ".intensity", aBaseLight.getIntensity());
    }

    public void setUniformDirectionalLight(String aUniformName, DirectionalLight aDirectionalLight) {
        setUniformBaseLight(aUniformName + ".base", aDirectionalLight);
        setUniform(aUniformName + ".direction", aDirectionalLight.getDirection());
    }

    public void setUniformPointLight(String aUniformName, PointLight aPointLight) {
        setUniformBaseLight(aUniformName + ".base", aPointLight);
        setUniformF(aUniformName + ".attenuation.constant", aPointLight.getAttenuation().getConstant());
        setUniformF(aUniformName + ".attenuation.linear", aPointLight.getAttenuation().getLinear());
        setUniformF(aUniformName + ".attenuation.exponent", aPointLight.getAttenuation().getExponent());
        setUniform(aUniformName + ".position", aPointLight.getTransform().getTransformedPosition());
        setUniformF(aUniformName + ".range", aPointLight.getRange());
    }

    public void setUniformSpotLight(String aUniformName, SpotLight aSpotLight) {

        setUniformPointLight(aUniformName + ".pointLight", aSpotLight);
        setUniform(aUniformName + ".direction", aSpotLight.getDirection());
        setUniformF(aUniformName + ".cutoff", aSpotLight.getCutoff());
    }

    private static String loadShader(String aFileName) {
        StringBuilder shaderSource = new StringBuilder();
        BufferedReader shaderReader = null;
        final String INCLUDE_DIRECTIVE = "#include";

        try {
            shaderReader = new BufferedReader(new FileReader(".\\res\\shader\\" + aFileName));

            String line;
            while ((line = shaderReader.readLine()) != null) {
                if (line.startsWith(INCLUDE_DIRECTIVE))  //#include "file.h"  <= Attention ' "' before and '"' after file are not allowed to be changed!!!
                    shaderSource.append(
                            loadShader(
                                    line.substring(INCLUDE_DIRECTIVE.length() + 2, line.length() - 1)));
                else
                    shaderSource.append(line).append("\n");
            }
            shaderReader.close();
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(1);
        }

        return shaderSource.toString();
    }
}
