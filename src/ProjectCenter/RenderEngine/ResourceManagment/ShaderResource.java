/* GameEngine 3D - A programm to design games with 3D content 
 * Copyright (C) 2016-2056 Frank Küwen
 *  
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package ProjectCenter.RenderEngine.ResourceManagment;

import java.util.ArrayList;
import java.util.HashMap;
import static org.lwjgl.opengl.GL20.glCreateProgram;

public class ShaderResource {
    
    private final int program;
    private HashMap<String, Integer> uniforms;
    private ArrayList<String> uniformNames;
    private ArrayList<String> uniformTypes;
    private int refCount;

    public ShaderResource() {
        program = glCreateProgram();
        refCount = 1;
        
        if (program == 0) {
            System.err.println("Shader creation failed: Could not find valid memory location in constructor.");
            System.exit(1);
        }
        uniforms = new HashMap<String, Integer>();
        uniformNames = new ArrayList<String>();
        uniformTypes = new ArrayList<String>();
    }

    public void addReference() {
        refCount++;
    }

    public boolean removeReference() {
        refCount--;
        return refCount == 0;
    }

    public int getProgramm() {
        return program;
    }

    public ArrayList<String> getUniformNames() {
        return uniformNames;
    }

    public ArrayList<String> getUniformTypes() {
        return uniformTypes;
    }

    public HashMap<String, Integer> getUniforms() {
        return uniforms;
    }
    
    
}
