/* GameEngine 3D - A programm to design games with 3D content 
 * Copyright (C) 2016-2056 Frank Küwen
 *  
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package ProjectCenter.RenderEngine.ResourceManagment;

import ProjectCenter.CoreEngine.Vector3f;
import java.util.HashMap;

public abstract class MappedValues {
    private final HashMap<String, Vector3f> vector3fHashMap;
    private final HashMap<String, Float> floatHashMap;
    
    public MappedValues()
    {        
        vector3fHashMap = new HashMap<String, Vector3f>();
        floatHashMap = new HashMap<String, Float>();
    }
    
    
    public final void addVector(String aName, Vector3f aVector) {
        vector3fHashMap.put(aName, aVector);
    }

    public Vector3f getVector(String aName) {
        Vector3f lResult = vector3fHashMap.get(aName);
        if (lResult != null)
            return vector3fHashMap.get(aName);
        else
            return new Vector3f();
    }

    public void addFloat(String aName, Float aFloat) {
        floatHashMap.put(aName, aFloat);
    }

    public float getFloat(String aName) {
        Float lResult = floatHashMap.get(aName);
        if (lResult != null)
            return floatHashMap.get(aName);
        else
            return 0;
    }
    
}
