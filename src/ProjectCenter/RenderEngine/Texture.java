/* GameEngine 3D - A programm to design games with 3D content 
 * Copyright (C) 2016-2056 Frank Küwen
 *  
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package ProjectCenter.RenderEngine;

import ProjectCenter.CoreEngine.Util;
import ProjectCenter.RenderEngine.ResourceManagment.TextureResource;
import java.awt.image.BufferedImage;
import java.io.File;
import java.nio.ByteBuffer;
import java.util.HashMap;
import javax.imageio.ImageIO;
import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL13.*;

public class Texture {

    private static HashMap<String, TextureResource> loadedTextures = new HashMap<String, TextureResource>();
    private final TextureResource resource;
    private final String fileName;

    public Texture(String aFileName) {

        fileName = aFileName;
        TextureResource oldResource = loadedTextures.get(aFileName);
        if (oldResource != null) {
            resource = oldResource;
            resource.addReference();
        } else {
            resource = loadTexture(aFileName);
            loadedTextures.put(aFileName, resource);
        }
    }

    protected void finialize() {
        if (resource.removeReference() && (!fileName.isEmpty()))
            loadedTextures.remove(fileName);
    }

    public void bind() {
        bind(0);
    }

    public void bind(int aSlot) {
        assert (0 <= aSlot && aSlot <= 31);
        glActiveTexture(GL_TEXTURE0 + aSlot);
        glBindTexture(GL_TEXTURE_2D, resource.getId());
    }

    public int getID() {
        return resource.getId();
    }

    private static TextureResource loadTexture(String aFileName) {

        String[] splitArray = aFileName.split("\\.");
        String extention = splitArray[splitArray.length - 1];
        try {

            BufferedImage lImage
                    = ImageIO.read(new File(".\\res\\textures\\" + aFileName));
            int[] lPixels = lImage.getRGB(
                    0, 0,
                    lImage.getWidth(), lImage.getHeight(),
                    null,
                    0, lImage.getWidth());
            ByteBuffer lBuffer = Util.createByteBuffer(
                    lImage.getHeight() * lImage.getWidth() * 4);// 4 = ARGB
            boolean lHasAlpha = lImage.getColorModel().hasAlpha();
            for (int y = 0; y < lImage.getHeight(); y++) {
                for (int x = 0; x < lImage.getWidth(); x++) {
                    int lCurrentPixel = lPixels[y * lImage.getWidth() + x];
                    lBuffer.put((byte) ((lCurrentPixel >> 16) & 0xFF));  //R
                    lBuffer.put((byte) ((lCurrentPixel >> 8) & 0xFF));   //G
                    lBuffer.put((byte) ((lCurrentPixel) & 0xFF));        //B
                    if (lHasAlpha)
                        lBuffer.put((byte) ((lCurrentPixel >> 24) & 0xFF));        //B
                    else
                        lBuffer.put((byte) (0xFF));

                }
            }
            lBuffer.flip();
            TextureResource lResource = new TextureResource();
            glBindTexture(GL_TEXTURE_2D, lResource.getId());

            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

            glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
            glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

            glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, lImage.getWidth(), lImage.getHeight(), 0, GL_RGBA, GL_UNSIGNED_BYTE, lBuffer);

            return lResource;
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(1);
        }
        return null;
    }
}
