/* GameEngine 3D - A programm to design games with 3D content 
 * Copyright (C) 2016-2056 Frank Küwen
 *  
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package ProjectCenter.RenderEngine;

import ProjectCenter.CoreEngine.Vector2f;
import java.awt.Canvas;
import org.lwjgl.LWJGLException;
import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.DisplayMode;

public class Window {
    
    private Canvas parentCanvas;
    private String title;

    public void createWin(int aWidth, int aHeight) {
        Display.setTitle(title);
        try {
            Display.setDisplayMode(new DisplayMode(aWidth, aHeight));
            Display.create();
            Keyboard.create();
            Mouse.create();
        } catch (LWJGLException e) {
            e.printStackTrace();
        }
    }
    public void createToPanel()
    {
       Display.setTitle(title);
        try {
            Display.setDisplayMode(new DisplayMode(parentCanvas.getWidth(), parentCanvas.getHeight()));
            Display.setParent(parentCanvas);
            Display.setResizable(true);
            Display.create();
            Keyboard.create();
            Mouse.create();
        } catch (LWJGLException e) {
            e.printStackTrace();
        } 
    }

    public void render() {
        Display.update();
    }

    public void dispose() {
        Display.destroy();
        Keyboard.destroy();
        Mouse.destroy();
    }

    public static boolean isClosedRequested() {
        return Display.isCloseRequested();
    }

    public int getWidth() {
        return Display.getDisplayMode().getWidth();
    }

    public int getHeight() {
        return Display.getDisplayMode().getHeight();
    }

    public String getTitle() {
        return Display.getTitle();
    }

    public void setTitle(String aTitle) {
        title = aTitle;
    }
    
    public Vector2f getCenter()
    {
        return new Vector2f(getWidth()/2.0f, getHeight()/2.0f);
    }
    
    public void setPanel(Canvas aParent)
    {
        parentCanvas = aParent;
    }
    
    public boolean isPanelAssigned()
    {
        return parentCanvas != null;
    }
}
