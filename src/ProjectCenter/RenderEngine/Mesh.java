/* GameEngine 3D - A programm to design games with 3D content 
 * Copyright (C) 2016-2056 Frank Küwen
 *  
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package ProjectCenter.RenderEngine;

import ProjectCenter.CoreEngine.Util;
import ProjectCenter.CoreEngine.Vector2f;
import ProjectCenter.CoreEngine.Vector3f;
import ProjectCenter.RenderEngine.MeshLoading.IndexedModel;
import ProjectCenter.RenderEngine.MeshLoading.OBJModel;
import ProjectCenter.RenderEngine.ResourceManagment.MeshResource;
import java.util.ArrayList;
import java.util.HashMap;
import static org.lwjgl.opengl.GL11.GL_FLOAT;
import static org.lwjgl.opengl.GL11.GL_TRIANGLES;
import static org.lwjgl.opengl.GL11.GL_UNSIGNED_INT;
import static org.lwjgl.opengl.GL11.glDrawElements;
import static org.lwjgl.opengl.GL15.GL_ARRAY_BUFFER;
import static org.lwjgl.opengl.GL15.GL_ELEMENT_ARRAY_BUFFER;
import static org.lwjgl.opengl.GL15.GL_STATIC_DRAW;
import static org.lwjgl.opengl.GL15.glBindBuffer;
import static org.lwjgl.opengl.GL15.glBufferData;
import static org.lwjgl.opengl.GL20.glDisableVertexAttribArray;
import static org.lwjgl.opengl.GL20.glEnableVertexAttribArray;
import static org.lwjgl.opengl.GL20.glVertexAttribPointer;

public class Mesh {

    public static final int FLOAT_BYTE = Float.SIZE / 8;
    private static HashMap<String, MeshResource> loadedModels = new HashMap<String, MeshResource>();
    private MeshResource resource;
    private String fileName;

    public Mesh(String aFileName) {
        fileName = aFileName;
        MeshResource oldResource = loadedModels.get(aFileName);
        if (oldResource != null) {
            resource = oldResource;
            resource.addReference();
        } else {
            loadMesh(aFileName);
            loadedModels.put(aFileName, resource);
        }
    }

    public Mesh(Vertex[] aVertexArray, int[] aIndexList) {
        this(aVertexArray, aIndexList, false);
    }

    public Mesh(Vertex[] aVertexArray, int[] aIndexList, boolean aCalclulateNormal) {
        fileName = "";
        addVertices(aVertexArray, aIndexList, aCalclulateNormal);
    }

    protected void finialize() {
        if (resource.removeReference() && (!fileName.isEmpty()))
            loadedModels.remove(fileName);
    }

    private void addVertices(Vertex[] aVertexArray, int[] aIndexList, boolean aCalclulateNormal) {

        if (aCalclulateNormal)
            calculateNormals(aVertexArray, aIndexList);

        resource = new MeshResource(aIndexList.length);

        glBindBuffer(GL_ARRAY_BUFFER, resource.getVertexaBufferObject());
        //GL_STATIC_DRAW -> low rate of changing the date!
        glBufferData(GL_ARRAY_BUFFER, Util.createFlippedBuffer(aVertexArray), GL_STATIC_DRAW);

        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, resource.getIndexBufferObject());
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, Util.createFlippedBuffer(aIndexList), GL_STATIC_DRAW);

    }

    public void draw() {
        glEnableVertexAttribArray(0);//Reference to vertices
        glEnableVertexAttribArray(1);//Reference to textures 
        glEnableVertexAttribArray(2);//Reference to normal 

        glBindBuffer(GL_ARRAY_BUFFER, resource.getVertexaBufferObject());

        glVertexAttribPointer(0, 3, GL_FLOAT, false,
                Vertex.SIZE * FLOAT_BYTE, 0); // Float.Size = # of bits!
        glVertexAttribPointer(1, 2, GL_FLOAT, false,
                Vertex.SIZE * FLOAT_BYTE, Vector3f.SIZE * FLOAT_BYTE);
        glVertexAttribPointer(2, 3, GL_FLOAT, false,
                Vertex.SIZE * FLOAT_BYTE, (Vector3f.SIZE + Vector2f.SIZE) * FLOAT_BYTE);

        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, resource.getIndexBufferObject());
        glDrawElements(GL_TRIANGLES, resource.getSize(), GL_UNSIGNED_INT, 0);

        glDisableVertexAttribArray(0);
        glDisableVertexAttribArray(1);
    }

    private void calculateNormals(Vertex[] aVertexArray, int[] aIndexList) {
        for (int i = 0; i < aIndexList.length; i += 3) {
            int i0 = aIndexList[i];
            int i1 = aIndexList[i + 1];
            int i2 = aIndexList[i + 2];

            Vector3f v1
                    = aVertexArray[i1].getPosition().sub(aVertexArray[i0].getPosition());
            Vector3f v2
                    = aVertexArray[i2].getPosition().sub(aVertexArray[i0].getPosition());

            Vector3f normal = v1.cross(v2).normalized();

            aVertexArray[i0].setNormal(aVertexArray[i0].getNormal().add(normal));
            aVertexArray[i1].setNormal(aVertexArray[i1].getNormal().add(normal));
            aVertexArray[i2].setNormal(aVertexArray[i2].getNormal().add(normal));
        }

        for (Vertex aVertex : aVertexArray) {
            aVertex.setNormal(aVertex.getNormal().normalized());
        }
    }

    private Mesh loadMesh(String aFileName) {

        String[] splitArray = aFileName.split("\\.");
        String extention = splitArray[splitArray.length - 1];
        if (!extention.equals("obj")) {
            System.err.println("Error: FileFormat not supported for mesh data: " + extention);
            new Exception().printStackTrace();
            System.exit(1);
        }
        OBJModel lTest = new OBJModel(".\\res\\models\\" + aFileName);
        IndexedModel lModel = lTest.toIndexedModel();
        lModel.calculateNormals();

        ArrayList<Vertex> vertices = new ArrayList<Vertex>();
        for (int i = 0; i < lModel.getPosition().size(); i++) {
            vertices.add(
                    new Vertex(
                            lModel.getPosition().get(i),
                            lModel.getTextureCoordinate().get(i),
                            lModel.getNormal().get(i)));
        }
        Vertex[] vertexData = new Vertex[vertices.size()];
        vertices.toArray(vertexData);

        Integer[] indexData = new Integer[lModel.getIndices().size()];
        lModel.getIndices().toArray(indexData);

        addVertices(vertexData, Util.toIntArray(indexData), false);
        return null;
    }
}
