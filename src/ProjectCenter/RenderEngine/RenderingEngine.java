/* GameEngine 3D - A programm to design games with 3D content 
 * Copyright (C) 2016-2056 Frank Küwen
 *  
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package ProjectCenter.RenderEngine;

import Components.Camera;
import Components.BaseLight;
import ProjectCenter.CoreEngine.CoreEngine;
import ProjectCenter.CoreEngine.GameObject;
import ProjectCenter.CoreEngine.Transform;
import ProjectCenter.CoreEngine.Vector3f;
import ProjectCenter.RenderEngine.ResourceManagment.MappedValues;
import java.util.ArrayList;
import java.util.HashMap;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL11.GL_VERSION;
import static org.lwjgl.opengl.GL32.GL_DEPTH_CLAMP;

public class RenderingEngine extends MappedValues{

    private final HashMap<String, Integer> samplerMap;
    private final ArrayList<BaseLight> lights;
    private BaseLight activeLight;

    private final Shader forwardAmbient;
    private Camera mainCamera;
    

    public RenderingEngine() {
        super();
        lights = new ArrayList<BaseLight>();
        samplerMap = new HashMap<String, Integer>();     
        
        samplerMap.put("aDiffuseSampler",0);
        addVector("aAmbientIntensity", new Vector3f(0.1f,0.1f,0.1f));

        forwardAmbient = new Shader("forward-ambient");//ForwardAmbient.getInstance();
        glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

        glFrontFace(GL_CW);
        glCullFace(GL_BACK);
        glEnable(GL_CULL_FACE);
        glEnable(GL_DEPTH_TEST);
        glEnable(GL_DEPTH_CLAMP);

        glEnable(GL_TEXTURE_2D);
    }

//    public Vector3f getAmbientLight() {
//        return ambientLight;
//    }
    public void updateUniformStruct(Transform aTransform, Material aMaterial, Shader aShader, String aUniformName, String aUniformType)
    {

                    throw new IllegalArgumentException(aUniformName
                            + " of type " +aUniformType + " is not a supported type of RenderingEngine");        
    }
    
    public void render(GameObject aObject) {
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        aObject.renderAll(forwardAmbient, this);

        glEnable(GL_BLEND);
        glBlendFunc(GL_ONE, GL_ONE);
        glDepthMask(false); // no writing on a list which tells which pixel is infront of which 
        glDepthFunc(GL_EQUAL);

        for (BaseLight light : lights) {
            activeLight = light;
            aObject.renderAll(light.getShader(), this);
        }

        glDepthFunc(GL_LESS);
        glDepthMask(true);
        glDisable(GL_BLEND);
    }
    public String getOpenGlVersion() {
        return "Using OpenGL-Version: " + glGetString(GL_VERSION);
    }
    public int getSamplerSlot(String aSamplerName)
    {
        return samplerMap.get(aSamplerName);
    }
    public void addCamera(Camera aCamera)
    {
        mainCamera = aCamera;
    }

    public void addLight(BaseLight aLight) {
        lights.add(aLight);
    }

    public BaseLight getActiveLight() {
        return activeLight;
    }

    public Camera getCamera() {
        return mainCamera;
    }

    public void setCamera(Camera aCamera) {
        mainCamera = aCamera;
    }
    
    public void setResolution(){
        glViewport(0, 0, CoreEngine.getWidth(), CoreEngine.getHeight());
        
    }

}
