/* GameEngine 3D - A programm to design games with 3D content 
 * Copyright (C) 2016-2056 Frank Küwen
 *  
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package ProjectCenter.RenderEngine;

import ProjectCenter.CoreEngine.Vector3f;

public class Attenuation extends Vector3f{
    
    public Attenuation(float aConstant, float aLinear, float aExponent)
    {
        super(aConstant, aLinear, aExponent);
    }
    
    public float getConstant()
    {
        return getX();
    }
    
    public float getLinear()
    {
        return getY();
    }
    
    public float getExponent()
    {
        return getZ();
    }
    @Override
    public Attenuation mul(float ascalar)
    {
        return new Attenuation(ascalar*this.getConstant(), ascalar*this.getLinear(), ascalar*this.getExponent());
    }
}
