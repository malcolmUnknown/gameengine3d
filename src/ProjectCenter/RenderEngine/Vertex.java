/* GameEngine 3D - A programm to design games with 3D content 
 * Copyright (C) 2016-2056 Frank Küwen
 *  
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package ProjectCenter.RenderEngine;

import ProjectCenter.CoreEngine.Vector2f;
import ProjectCenter.CoreEngine.Vector3f;

public class Vertex {

    public static final int SIZE = 8;

    private Vector3f position;
    private Vector2f textureCoordinate;
    private Vector3f normal;

    public Vertex(Vector3f aPosition) {

        this(aPosition, new Vector2f(0.0f, 0.0f));
    }

    public Vertex(Vector3f aPosition, Vector2f aTextureCooriante) {

        this(aPosition, aTextureCooriante, new Vector3f(0.0f, 0.0f, 0.0f));
    }

    public Vertex(Vector3f aPosition, Vector2f aTextureCooriante, Vector3f aNormal) {
        position = aPosition;
        textureCoordinate = aTextureCooriante;
        normal = aNormal;
    }

    public Vector3f getPosition() {
        return position;
    }

    public void setPosition(Vector3f aPosition) {
        this.position = aPosition;
    }

    public Vector2f getTextureCoordinate() {
        return textureCoordinate;
    }

    public void setTextureCoordinate(Vector2f aTextureCoordinate) {
        this.textureCoordinate = aTextureCoordinate;
    }

    public Vector3f getNormal() {
        return normal;
    }

    public void setNormal(Vector3f aNormal) {
        this.normal = aNormal;
    }
}
