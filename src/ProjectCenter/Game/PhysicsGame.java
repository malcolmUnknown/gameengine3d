/* GameEngine 3D - A programm to design games with 3D content 
 * Copyright (C) 2016-2056 Frank Küwen
 *  
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package ProjectCenter.Game;

import Components.Camera;
import Components.ForceObjectComponent;
import Components.FreeMoveController;
import Components.FreeRotateController;
import Components.MeshRenderer;
import Components.PhysicsObjectComponent;
import Components.PointLight;
import Components.SpotLight;
import ProjectCenter.CoreEngine.CoreEngine;
import ProjectCenter.CoreEngine.Game;
import ProjectCenter.CoreEngine.GameObject;
import ProjectCenter.CoreEngine.Quaternion;
import ProjectCenter.CoreEngine.Vector3f;
import ProjectCenter.PhysicsEngine.ChargeObject;
import ProjectCenter.PhysicsEngine.ColliderTypes;
import ProjectCenter.PhysicsEngine.ForceObject;
import ProjectCenter.PhysicsEngine.ForceTypes;
import ProjectCenter.PhysicsEngine.PhysicsObject;
import ProjectCenter.RenderEngine.Attenuation;
import ProjectCenter.RenderEngine.Material;
import ProjectCenter.RenderEngine.Mesh;
import ProjectCenter.RenderEngine.Texture;
import java.util.Iterator;

public class PhysicsGame extends Game {

    @Override
    public void init() {
        super.init();
        Material lMaterialBrick = new Material();
        lMaterialBrick.addTexture("aDiffuseSampler", new Texture("bricks_disp.png"));
        lMaterialBrick.addFloat("aSpecularIntensity", 1f);
        lMaterialBrick.addFloat("aSpecularPower", 1.8f);
        Mesh mesh = new Mesh("Cube3D.obj");

        float lBarrier = 0.5f;
        float lDimension = 10;
        int lSwitchX;
        int lSwitchZ;
        Vector3f lPosition;
        Vector3f lScaleVector;
        Quaternion lRotation;

        PhysicsObject lPO;
        GameObject lWall;
            lPO = new PhysicsObject(
                    ColliderTypes.ColliderPlane,
                    new Vector3f(0,1,0),
                    new Vector3f(),
                    1000000,
                    new Vector3f(0, 0, 0),
                    new Vector3f(1, 0, 0).mul(0));
            lWall = new GameObject();
            lWall.addComponent(new MeshRenderer(new Mesh("plane3d.obj"), lMaterialBrick));
            lWall.getTransform().setPosition(new Vector3f(0,-2,0));
            lWall.getTransform().setScale(new Vector3f(16, 16, 16));
            lWall.addComponent(new PhysicsObjectComponent(lPO));
            addObjectToRoot(lWall);
//        for (int i = 0; i < 1; i++) {
//            switch (i) {
//                default: 
//                case 0:
//                    lPosition = new Vector3f(0, -(lDimension / 2), 0);
//                    lScaleVector = new Vector3f(lDimension, lBarrier, lDimension);
//                    break;
//                case 1:
//                    lPosition = new Vector3f(lDimension + lBarrier, 0, 0);
//                    lScaleVector = new Vector3f(lBarrier, lDimension / 2, lDimension);
//                    break;
//                case 2:
//                    lPosition = new Vector3f(lDimension + lBarrier, 0, 0).mul(-1);
//                    lScaleVector = new Vector3f(lBarrier, lDimension / 2, lDimension);
//                    break;
//                case 3:
//                    lPosition = new Vector3f(0, 0, lDimension + lBarrier);
//                    lScaleVector = new Vector3f(lDimension, lDimension / 2, lBarrier);
//                    break;
//                case 4:
//                    lPosition = new Vector3f(0, 0, lDimension + lBarrier).mul(-1);
//                    lScaleVector = new Vector3f(lDimension, lDimension / 2, lBarrier);
//                    break;
//
//            }
//
//            lPO = new PhysicsObject(
//                    ColliderTypes.ColliderBox,
//                    new Vector3f(),
//                    new Vector3f(),
//                    1000000,
//                    new Vector3f(0, 0, 0),
//                    new Vector3f(1, 0, 0).mul(0));
//            lWall = new GameObject();
//            lWall.addComponent(new MeshRenderer(mesh, lMaterialBrick));
//            lWall.getTransform().setScale(lScaleVector);
//            lWall.getTransform().setPosition(lPosition);
//            {
//                lWall.addComponent(new PhysicsObjectComponent(lPO));
//                addObjectToRoot(lWall);
//            }
//        }

        GameObject lPoint;
        lSwitchX = -2;
        lSwitchZ = -2;
        for (int i = 0; i < 25; i++) {
            lPosition = new Vector3f(lSwitchX * lDimension / 2, -(lDimension / 2 - lBarrier) + .1f, lSwitchZ * lDimension / 2);

            lPoint = new GameObject();
            if ((lSwitchX == 0) && (lSwitchZ == 0)) {
                lScaleVector = new Vector3f(1, 1, 1);
            } else if ((lSwitchX == 0) || (lSwitchZ == 0)) {
                lScaleVector = new Vector3f(1, 1, 0);
            } else {
                lScaleVector = new Vector3f(1, 0, 0);
            }
            lPoint.addComponent(new PointLight(lScaleVector, 0.4f, new Attenuation(0, 0, 2f)));
            lPoint.getTransform().setPosition(lPosition);
            addObjectToRoot(lPoint);
            lSwitchX++;
            if (lSwitchX > 2) {
                lSwitchX = -2;
                lSwitchZ++;
            }
        }
        GameObject lCameraObject = new GameObject();

        lCameraObject.addComponent(
                new Camera(false,
                        (float) Math.toRadians(45.f),
                        (float) CoreEngine.getWidth() / (float) CoreEngine.getWidth(),
                        0.01f, 1000.f));
        lCameraObject.addComponent(
                new FreeRotateController(10));
        lCameraObject.addComponent(
                new FreeMoveController(10.0f));
        lCameraObject.getTransform()
                .setPosition(new Vector3f(0, 15, 0).mul(1));
        lCameraObject.getTransform()
                .setRotation(new Quaternion(new Vector3f(1, 0, 0), (float) Math.PI / 2));
        SpotLight sslight = new SpotLight(
                new Vector3f(1, 1, 1), 0.1f, new Attenuation(0, 0, 0.1f),
                0.17f);

        lCameraObject.addComponent(sslight);

        addObjectToRoot(lCameraObject);
        lCameraObject.setActivation(true);

        Mesh lMeshSphere = new Mesh("sphere.obj");
        Mesh lMeshCube = new Mesh("Cube3d.obj");
        Material lMaterialTest = new Material();

        lMaterialTest.addTexture(
                "aDiffuseSampler", new Texture("test.png"));
        lMaterialTest.addFloat(
                "aSpecularIntensity", 0.5f);
        lMaterialTest.addFloat(
                "aSpecularPower", 8f);
//        GameObject ForceObj;
//        ForceObject lFO = new ForceObject(ForceTypes.Graviational, 50, new Vector3f());
//        ForceObj = new GameObject();
//        ForceObj.getTransform().setPosition(new Vector3f(0, 0, lDimension + lBarrier + 2));
//        ForceObj.addComponent(new ForceObjectComponent(lFO));
//        addObjectToRoot(ForceObj);

        GameObject PhysicObj_01;

        lPO = new PhysicsObject(
                ColliderTypes.ColliderSphere,
                new Vector3f(1, 0, 0),
                new Vector3f(),
                1,
                new Vector3f(0, -1, 0).mul(1),
                new Vector3f(1, 0, 0).mul(0 * (float) (Math.PI / (2 * 5.f))));
        lPO.addCharge(new ChargeObject(ForceTypes.Electrical, -2, new Vector3f(), false));
        lPO.addCharge(new ChargeObject(ForceTypes.Electrical, -0, new Vector3f(), false));
        lPO.addCharge(new ChargeObject(ForceTypes.Electrical, -0, new Vector3f(), false));
        lPO.addCharge(new ChargeObject(ForceTypes.Electrical, -0, new Vector3f(), false));
        lPO.addCharge(new ChargeObject(ForceTypes.Graviational, -0, new Vector3f(), false));
        lPO.addCharge(new ChargeObject(ForceTypes.Graviational, -0, new Vector3f(), false));
        lPO.addCharge(new ChargeObject(ForceTypes.Graviational, 4, new Vector3f(), false));
        Iterator<ChargeObject> SSX = lPO.getCharges();
//        while(SSX.hasNext())
//        {
//            ChargeObject l = SSX.next();
//            if (l.getCharge() > 0)
//                SSX.remove();
//            l.getExcenter().setX(2);
//        }
        PhysicObj_01 = new GameObject();
//        PhysicObj_01.getTransform()
//                .setRotation(new Quaternion(new Vector3f(0, 0, 1), (float) Math.PI
//                                / 2.f));
        PhysicObj_01.getTransform()
                .setPosition(new Vector3f(5, 5, 0));
        PhysicObj_01.addComponent(
                new PhysicsObjectComponent(lPO));
        PhysicObj_01.addComponent(
                new MeshRenderer(lMeshSphere, lMaterialTest));
        addObjectToRoot(PhysicObj_01);
//        
        lPO= new PhysicsObject(
                ColliderTypes.ColliderSphere,
                new Vector3f(1, 0, 0),
                new Vector3f(),
                1,
                new Vector3f(0, -1, 0).mul(1),
                new Vector3f(0, 1, 0).mul(1 * (float) (Math.PI / (2 * 5.f))));
        PhysicObj_01 = new GameObject();
//        PhysicObj_01.getTransform()
//                .setRotation(new Quaternion(new Vector3f(0, 0, 1), (float) Math.PI
//                                / 2.f));
        PhysicObj_01.getTransform()
                .setPosition(new Vector3f(-5, 5, 0));
        PhysicObj_01.addComponent(
                new PhysicsObjectComponent(lPO));
        PhysicObj_01.addComponent(
                new MeshRenderer(lMeshSphere, lMaterialBrick));
        addObjectToRoot(PhysicObj_01);

//        lPO = new PhysicsObject(
//                ColliderTypes.ColliderSphere,
//                new Vector3f(1, 0, 0),
//                new Vector3f(),
//                1,
//                new Vector3f(1, 0, 1).mul(0),
//                new Vector3f(0, 0, 0));
//        PhysicObj_01 = new GameObject();
//        PhysicObj_01.getTransform()
//                .setPosition(new Vector3f(0, 0, 5));
//        PhysicObj_01.addComponent(
//                new PhysicsObjectComponent(lPO));
//        PhysicObj_01.addComponent(
//                new MeshRenderer(lMeshSphere, lMaterialTest));
//        addObjectToRoot(PhysicObj_01);
//
//        lPO = new PhysicsObject(
//                ColliderTypes.ColliderBox,
//                new Vector3f(),
//                new Vector3f(),
//                1,
//                new Vector3f(0, 0, 1).mul(5),
//                new Vector3f(0, 1, 0).mul(0 * (float) (Math.PI) / (2 * 5f)));
//        PhysicObj_01 = new GameObject();
//        PhysicObj_01.getTransform()
//                .setScale(new Vector3f(1, 1, 1));
//        PhysicObj_01.getTransform()
//                .setRotation(new Quaternion(new Vector3f(0, 1f, 0), 0 * (float) (Math.PI
//                                / 4)));
//        PhysicObj_01.getTransform()
//                .setPosition(new Vector3f(0, 0, -8));
//        PhysicObj_01.addComponent(
//                new PhysicsObjectComponent(lPO));
//        PhysicObj_01.addComponent(
//                new MeshRenderer(lMeshCube, lMaterialTest));
//        addObjectToRoot(PhysicObj_01);
    }

    @Override
    public String getName() {
        return "Physics-Test-Game";
    }
}
