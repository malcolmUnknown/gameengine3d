/* GameEngine 3D - A programm to design games with 3D content 
 * Copyright (C) 2016-2056 Frank Küwen
 *  
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package ProjectCenter.Game;

import Components.DirectionalLight;
import Components.MeshRenderer;
import Components.PointLight;
import ProjectCenter.CoreEngine.Game;
import ProjectCenter.CoreEngine.GameObject;
import ProjectCenter.CoreEngine.Vector2f;
import ProjectCenter.CoreEngine.Vector3f;
import Components.Camera;
import Components.FreeMoveController;
import Components.FreeRotateController;
import Components.FreeScaleController;
import ProjectCenter.RenderEngine.Material;
import ProjectCenter.RenderEngine.Mesh;
import Components.SpotLight;
import ProjectCenter.CoreEngine.CoreEngine;
import ProjectCenter.CoreEngine.Quaternion;
import ProjectCenter.RenderEngine.Attenuation;
import ProjectCenter.RenderEngine.Texture;
import ProjectCenter.RenderEngine.Vertex;

public class TestGame extends Game {

    @Override
    public void init() {
        super.init();
        GameObject planeObject = new GameObject();
        float floorW = 8.0f;
        float floorH = 8.0f;

        Material materialbase = new Material();
        materialbase.addTexture("aDiffuseSampler", new Texture("bricks_disp.png"));
        materialbase.addFloat("aSpecularIntensity", 1f);
        materialbase.addFloat("aSpecularPower", 1.8f);
        Mesh mesh = new Mesh("plane3d.obj");//Mesh(vertices, indices, true);
        MeshRenderer meshRenderer = new MeshRenderer(mesh, materialbase);
        planeObject.addComponent(meshRenderer);
        planeObject.getTransform().getPosition().set(0, -1f, 0);
        planeObject.getTransform().setScale(new Vector3f(16, 1, 16));
        addObjectToRoot(planeObject);

        GameObject testObject4 = new GameObject();
        Vertex[] vertices3 = new Vertex[]{
            new Vertex(new Vector3f(- 2, 2, -2), new Vector2f(0.0f, 0.0f)),
            new Vertex(new Vector3f(- 2, 2, 2), new Vector2f(0.0f, 1.0f)),
            new Vertex(new Vector3f(2, 2, -2), new Vector2f(1.0f, 0.0f)),
            new Vertex(new Vector3f(2, 2, 2), new Vector2f(1.0f, 1.0f)),
            new Vertex(new Vector3f(- 2, -2, -2), new Vector2f(0.0f, 0.0f)),
            new Vertex(new Vector3f(- 2, -2, 2), new Vector2f(0.0f, 1.0f)),
            new Vertex(new Vector3f(2, -2, -2), new Vector2f(1.0f, 0.0f)),
            new Vertex(new Vector3f(2, -2, 2), new Vector2f(1.0f, 1.0f))
        };
        int[] indices3 = new int[]{
            0, 1, 2, 2, 1, 3,
            6, 5, 4, 7, 5, 6,
            2, 3, 7, 2, 7, 6,
            2, 6, 4, 2, 4, 0,
            0, 4, 1, 1, 4, 5,
            1, 5, 3, 5, 7, 3
        };
        Material material = new Material();//new Texture("test.png"), new Vector3f(1.0f, 1.0f, 1.0f), 1, 8);
        material.addTexture("aDiffuseSampler", new Texture("bricks_disp.png"));
        material.addFloat("aSpecularIntensity", 0.1f);
        material.addFloat("aSpecularPower", 2f);
        Mesh mesh3 = new Mesh(vertices3, indices3, true);
        testObject4.addComponent(new MeshRenderer(mesh3, material));
        testObject4.getTransform().getPosition().set(0, 4, 8);
        testObject4.getTransform().setRotation(
                new Quaternion(new Vector3f(0, 1, 0), (float) -Math.PI / 2 * 0));
         testObject4.addComponent(new FreeScaleController(0.5f));
        addObjectToRoot(testObject4);

        Mesh tempMesh = new Mesh("sphere.obj");
        Mesh tempMesh2 = new Mesh("Cube3d.obj");
        Material mat2 = new Material();
        mat2.addTexture("aDiffuseSampler", new Texture("test.png"));
        mat2.addFloat("aSpecularIntensity", 0.5f);
        mat2.addFloat("aSpecularPower", 8f);

        Vertex[] vertices2 = new Vertex[]{
            new Vertex(new Vector3f(- 2, 0, -2), new Vector2f(0.0f, 0.0f)),
            new Vertex(new Vector3f(- 2, 0, 2), new Vector2f(0.0f, 1.0f)),
            new Vertex(new Vector3f(2, 0, -2), new Vector2f(1.0f, 0.0f)),
            new Vertex(new Vector3f(2, 0, 2), new Vector2f(1.0f, 1.0f))};
        int[] indices2 = new int[]{
            0, 1, 2,
            2, 1, 3};
        Mesh mesh2 = new Mesh(vertices2, indices2, true);

        GameObject testObject1 = new GameObject();
        testObject1.addComponent(new MeshRenderer(mesh2, material));
        testObject1.getTransform().getPosition().set(0, 2, 0);
        testObject1.getTransform().setRotation(
                new Quaternion(new Vector3f(0, 1, 0), (float) Math.PI / 4));
        addObjectToRoot(testObject1);
        testObject1 = new GameObject();
        testObject1.addComponent(new MeshRenderer(mesh2, material));
        testObject1.getTransform().getPosition().set(1, 1, 2.5f);
        testObject1.getTransform().setRotation(
                new Quaternion(new Vector3f(0, 1, 0), (float) Math.PI / 6));
        addObjectToRoot(testObject1);

        GameObject testObject2 = new GameObject();
        testObject2.addComponent(new MeshRenderer(mesh2, material));
        testObject2.getTransform().getPosition().set(2, 0, 5);

         GameObject t4 = new GameObject();
         t4.addComponent(new MeshRenderer(new Mesh("monkey3.obj"), mat2));        
         t4.addComponent(new LookAtComponent());
         t4.addComponent(new FreeMoveController(15));
         t4.getTransform().setPosition(new Vector3f(0,4,0));
         testObject2.addChild(t4);;
        testObject1.addChild(testObject2);

        GameObject lDLightObject = new GameObject();
        DirectionalLight dlight
                = new DirectionalLight(
                        new Vector3f(0, 0, 1), 0.3f);
        lDLightObject.addComponent(dlight);
        lDLightObject.getTransform().setRotation(
                new Quaternion(new Vector3f(0, 0, 1), (float) Math.PI / 4));
        addObjectToRoot(lDLightObject);

        Attenuation atten = new Attenuation(0, 0, 1f);
        //addObjectToRoot(lPLightObject);
        int lLightFiedWidth = 5;
        int lLightFiedHeight = 5;
        Vector2f lLightStart = new Vector2f(-2 * (floorW - 1), -2 * (floorH - 1));
        Vector2f lLightStep = new Vector2f(floorW - 1, floorH - 1);
        for (int i = 0; i < lLightFiedWidth; i++) {
            for (int j = 0; j < lLightFiedHeight; j++) {
                GameObject lPLightObject = new GameObject();
                lPLightObject.addComponent(new PointLight(new Vector3f(0, 1, 0), 0.4f, atten));
                lPLightObject.getTransform().setPosition(new Vector3f(
                        lLightStart.getX() + lLightStep.getX() * i,
                        -.0f,
                        lLightStart.getY() + lLightStep.getY() * j));
                //addObjectToRoot(lPLightObject);
                lDLightObject.addChild(lPLightObject);
            }
        }

        GameObject lSLightObject = new GameObject();
        lSLightObject.addComponent(
                new SpotLight(
                        new Vector3f(1, 0, 0), 0.4f, new Attenuation(0, 0, 0.1f), 0.8f));
        lSLightObject.getTransform().getPosition().set(-5, 0, -5);
        lSLightObject.getTransform().setRotation(
                new Quaternion(new Vector3f(0, 1, 0), -(float) Math.toRadians(90.0)));
        addObjectToRoot(lSLightObject);

        GameObject lCameraObject = new GameObject();        
        lCameraObject.addComponent(
                new Camera(false, (float) Math.toRadians(70.f), (float) CoreEngine.getWidth() / (float) CoreEngine.getHeight(), 0.01f, 1000.f));
        lCameraObject.addComponent(new FreeRotateController(10));
        lCameraObject.addComponent(new FreeMoveController(10.0f));
        lCameraObject.getTransform().setPosition(new Vector3f(0, 15, 0).mul(1));
        lCameraObject.getTransform().setRotation(new Quaternion(new Vector3f(1, 0, 0), (float) Math.PI / 2));
        SpotLight sslight = new SpotLight(
                new Vector3f(1, 1, 1), 0.1f, new Attenuation(0, 0, 0.1f),
                0.17f);
        lCameraObject.addComponent(sslight);

        //testObject2.addChild(lCameraObject);
        addObjectToRoot(lCameraObject);
        lCameraObject.setActivation(true);
    }
    @Override
    public String getName() {
        return "Test-Game";
    }
}
