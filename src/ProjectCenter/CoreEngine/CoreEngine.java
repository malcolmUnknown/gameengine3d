/* GameEngine 3D - A programm to design games with 3D content 
 * Copyright (C) 2016-2056 Frank Küwen
 *  
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package ProjectCenter.CoreEngine;

import Components.FreeMoveController;
import Components.FreeRotateController;
import Components.FreeScaleController;
import Components.GameComponent;
import MVC_Interface.IControllerDebug;
import MVC_Interface.IControllerRotation;
import MVC_Interface.IControllerMain;
import MVC_Interface.IControllerMotion;
import MVC_Interface.IControllerScale;
import MVC_Interface.IModel;
import ProjectCenter.PhysicsEngine.PhysicsEngine;
import ProjectCenter.RenderEngine.RenderingEngine;
import ProjectCenter.RenderEngine.Window;
import java.awt.Canvas;
import java.util.Iterator;
import java.util.Observer;
import java.util.UUID;

public class CoreEngine implements IModel, IControllerMain, IControllerDebug {

    private boolean isRunning;
    private boolean panelMode;
    private Game game;
    private RenderingEngine renderingEngine;
    private PhysicsEngine physicsEngine;
    private final InputMouse mouseInput;
    private final InputKeyboard keyboardInput;
    private static int width;
    private static int height;
    private final double frameTime;
    private final InputMotion motionInput;
    private final InputRotation rotationInput;
    private final InputScale scaleInput;
    private Thread engineThread;
    private final Window window;
    private boolean changeResolution;
    private int fps;

    public CoreEngine(double aFramerate, Game aGame) {

        isRunning = false;
        panelMode = false;
        changeResolution = false;
        game = aGame;
        frameTime = 1 / aFramerate;
        width = 200;
        height = 300;
        fps = 0;
        game.setEngine(this);
        window = new Window();
        mouseInput = new InputMouse();
        keyboardInput = new InputKeyboard();
        rotationInput = new InputRotation();
        motionInput = new InputMotion();
        scaleInput = new InputScale();
    }

    private void runEngine() {
        isRunning = true;

        fps = 0;
        double frameCounter = 0;

        game.init();
        double lastTime = Time.getTime();
        double unprocessedTime = 0;

        if (!panelMode) {
            registerMouseObserver(renderingEngine.getCamera());
            registerKeyboardObserver(renderingEngine.getCamera());
        }

        while (isRunning) {
            boolean render = false;
            double startTime = Time.getTime();
            double passedTime = startTime - lastTime;
            lastTime = startTime;

            unprocessedTime += passedTime;
            frameCounter += passedTime;

            while (unprocessedTime > frameTime) {
                render = true;
                unprocessedTime -= frameTime;
                if (Window.isClosedRequested()) {
                    stopEngine();
                }
                game.input((float) frameTime);

                game.update((float) frameTime, physicsEngine);
                keyboardInput.update();
                mouseInput.update();
                if (frameCounter >= 1.0) {
                    // Debug mode : 
                    // System.out.println("Acutal frames/s: "+ frames);
                    fps = 0;
                    frameCounter = 0;
                }
            }
            if (render) {
                game.render(renderingEngine);
                window.render();
                fps++;
            } else {
                if (changeResolution) {
                    renderingEngine.setResolution();
                    renderingEngine.getCamera().setAspectRatio(width, height);
                    changeResolution = false;
                }
                try {
                    Thread.sleep(1);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
        cleanUp();
    }

    private void cleanUp() {
        window.dispose();
    }

    public boolean getRunningState() {
        return isRunning == true;
    }

    public static int getWidth() {
        return width;
    }

    public static int getHeight() {
        return height;
    }

    public void addMotionControll(UUID aIdentity, FreeMoveController aMotionControl) {
        motionInput.addMotionController(aIdentity, aMotionControl);
    }

    public void removeMotionControll(UUID aIdentity, FreeMoveController aMotionControl) {
        motionInput.removeMotionController(aIdentity, aMotionControl);
    }

    public void addRotationControll(UUID aIdentity, FreeRotateController aRotationControl) {
        rotationInput.addRotationController(aIdentity, aRotationControl);
    }

    public void removeRotationControll(UUID aIdentity, FreeRotateController aRotationControl) {
        rotationInput.removeRotationController(aIdentity, aRotationControl);
    }

    public void addScaleControll(UUID aIdentity, FreeScaleController aScaleControl) {
        scaleInput.addScaleController(aIdentity, aScaleControl);
    }

    public void removeScaleControll(UUID aIdentity, FreeScaleController aScaleControl) {
        scaleInput.removeScaleControll(aIdentity, aScaleControl);
    }

    public RenderingEngine getRenderingEngine() {

        if (isRunning) {
            return renderingEngine;
        } else {
            return null;
        }
    }

    public PhysicsEngine getPhysicsEngine() {
        return physicsEngine;
    }

// Region: IControllerDebug
    @Override
    public void setStatePhysicsEngine(boolean aActive) {
        if (isRunning) {
            physicsEngine.setState(aActive);
        }
    }

    public boolean getStatePhysicsEngine() {
        if (isRunning) {
            return physicsEngine.getState();
        } else {
            return false;
        }
    }

    @Override
    public int getFramerate() {
        return isRunning ? fps : 0;
    }

    @Override
    public String getOpenGLVersion() {
        if (isRunning) {
            return renderingEngine.getOpenGlVersion();
        } else {
            return "";
        }
    }
// Region end

// Region: IControllerMain
    @Override
    public void startEngine() {
        if (isRunning) {
            return;
        }
        engineThread = new Thread() {

            @Override
            public void run() {
                panelMode = window.isPanelAssigned();
                if (panelMode) {
                    window.createToPanel();
                } else {
                    window.createWin(width, height);
                    registerKeyboardObserver(motionInput);
                    registerMouseObserver(rotationInput);
                }
                mouseInput.init();
                keyboardInput.init();

                renderingEngine = new RenderingEngine();
                System.out.println(renderingEngine.getOpenGlVersion());
                physicsEngine = new PhysicsEngine();
                activateKeyboard(true);
                activateMouse(true);
                runEngine();
            }

            @Override
            public void interrupt() {
                activateMouse(false);
                activateKeyboard(false);
                stopEngine();
            }
        };
        engineThread.start();
    }

    @Override
    public void stopEngine() {
        if (!isRunning) {
            return;
        }
        isRunning = false;
    }

    @Override
    public void initializeWindow(int aWidth, int aHeight, String aTitle) {
        width = aWidth;
        height = aHeight;
        window.setTitle(aTitle);
    }

    @Override
    public void initializePanel(Canvas aParent, String aTitle) {
        width = aParent.getWidth();
        height = aParent.getHeight();
        window.setPanel(aParent);
        window.setTitle(aTitle);
    }

    @Override
    public void setViewOrthographic(boolean aOrthoView) {
        if (isRunning) {
            renderingEngine.getCamera().setViewMode(aOrthoView);
        }
    }

    @Override
    public boolean isViewOrthographic() {
        if (isRunning) {
            return renderingEngine.getCamera().getViewMode();
        }
        return false;
    }

    @Override
    public void setFieldOfView(float aFieldOfView) {
        if (isRunning) {
            renderingEngine.getCamera().setFieldOfView(aFieldOfView);
        }
    }

    @Override
    public void setResolution(int aWidth, int aHeight) {
        width = aWidth;
        height = aHeight;
        if (isRunning) {
            changeResolution = true;
        }
    }

    @Override
    public float getFieldOfView() {
        if (isRunning) {
            return renderingEngine.getCamera().getFieldOfView();
        } else {
            return 0;
        }
    }

    @Override
    public void activateKeyboard(boolean aActive) {
        keyboardInput.setKeyboardLock(aActive);
    }

    @Override
    public void activateMouse(boolean aActive) {
        mouseInput.setMouseLock(aActive);
    }

    @Override
    public boolean isMouseActive() {
        return mouseInput.getMouseState();
    }

    @Override
    public boolean isKeyboardActive() {
        return keyboardInput.getKeyboardState();
    }

    @Override
    public boolean isEngineRunning() {
        return isRunning;
    }

    @Override
    public void setGame(Game aGame) {
        if (!isRunning) {
            for (GameObject go : game.getRootObject().getAllAttached()) {
                go.deleteObservers();
            }
            game = aGame;
            game.setEngine(this);
        }
    }

    @Override
    public void setElementState(UUID aId, boolean aIsActive) {
        if (game != null) {
            boolean foundID;
            boolean isNotified = false;

            Iterator<GameObject> gameObjectWalker = game.getRootObject().getAllAttached().iterator();
            GameObject currentObject;
            while (gameObjectWalker.hasNext()) {
                currentObject = gameObjectWalker.next();
                foundID = currentObject.getIdentity().equals(aId);
                if (foundID) {
                    if (!isNotified) {
                        currentObject.setActivation(aIsActive);
                        isNotified = true;
                    }
                    Iterator<GameComponent> gameComponentWalker = currentObject.getComponents().iterator();
                    GameComponent currentComponent;
                    while (gameComponentWalker.hasNext()) {
                        currentComponent = gameComponentWalker.next();
                        if (currentComponent instanceof FreeMoveController) {
                            motionInput.setElementState(aId, aIsActive);
                        }
                        if (currentComponent instanceof FreeRotateController) {
                            rotationInput.setElementState(aId, aIsActive);
                        }
                        if (currentComponent instanceof FreeScaleController) {
                            scaleInput.setElementState(aId, aIsActive);
                        }
                    }
                }
            }
        }
    }
// Region end

// Region: IModel
    @Override
    public IControllerMain getMainControl() {
        return this;
    }

    @Override
    public IControllerDebug getDebugControll() {
        return this;
    }

    @Override
    public IControllerMotion getMotionControll() {
        return motionInput;
    }

    @Override
    public IControllerRotation getRotationControll() {
        return rotationInput;
    }

    @Override
    public IControllerScale getScalingControll() {
        return scaleInput;
    }

    @Override
    public void registerMouseObserver(Observer aMouseObserver) {
        mouseInput.addObserver(aMouseObserver);
    }

    @Override
    public void deregisterMouseObserver(Observer aMouseObserver) {
        mouseInput.deleteObserver(aMouseObserver);
    }

    @Override
    public void registerKeyboardObserver(Observer aKeyboardObserver) {
        keyboardInput.addObserver(aKeyboardObserver);
    }

    @Override
    public void deregisterKeyboardObserver(Observer aKeyboardObserver) {
        keyboardInput.deleteObserver(aKeyboardObserver);
    }

    @Override
    public void registerStructureObserver(Observer aStructureObserver) {
        for (GameObject go : game.getRootObject().getAllAttached()) {
            go.addObserver(aStructureObserver);
        }
    }

    @Override
    public void deregisterStructureObserver(Observer aStructureObserver) {
        for (GameObject go : game.getRootObject().getAllAttached()) {
            go.deleteObserver(aStructureObserver);
        }
    }

    @Override
    public void registerStructureObserver(UUID aGameObjectID, Observer aStructureObserver) {
        for (GameObject go : game.getRootObject().getAllAttached()) {
            if (go.getIdentity() == aGameObjectID) {
                go.addObserver(aStructureObserver);
            }
        }
    }

    @Override
    public void deregisterStructureObserver(UUID aGameObjectID, Observer aStructureObserver) {
        for (GameObject go : game.getRootObject().getAllAttached()) {
            if (go.getIdentity() == aGameObjectID) {
                go.addObserver(aStructureObserver);
            }
        }
    }
// Region end
}
