/* GameEngine 3D - A programm to design games with 3D content 
 * Copyright (C) 2016-2056 Frank Küwen
 *  
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package ProjectCenter.CoreEngine;

public class Transform {

    private Vector3f position;
    private Quaternion rotation;
    private Vector3f scale;

    private Transform parent;
    private Matrix4f parentMatrix;

    private Vector3f oldPosition;
    private Quaternion oldRotation;
    private Vector3f oldScale;

    public Transform() {
        scale = new Vector3f(1.0f, 1.0f, 1.0f);
        rotation = new Quaternion(0, 0, 0, 1);
        position = new Vector3f(0, 0, 0);
        parentMatrix = new Matrix4f().Identity();
        
        oldPosition = (new Vector3f()).clone(position).add(1.0f);
        oldRotation = (new Quaternion()).clone(rotation).mul(0.5f);
        oldScale = (new Vector3f()).clone(scale).add(1.0f);
}

    public Transform(Transform aOther) {
        scale = aOther.getScale();
        rotation = aOther.getRotation();
        position = aOther.getPosition();
        parentMatrix = aOther.getParentMatrix();
    }

    public void update() {
        if (oldPosition == null || oldRotation == null || oldScale == null) {
            oldPosition = (new Vector3f()).clone(position).add(1.0f);
            oldRotation = (new Quaternion()).clone(rotation).mul(0.5f);
            oldScale = (new Vector3f()).clone(scale).add(1.0f);
        } else {
            if (oldPosition != null) {
                oldPosition.set(position);
            }

            if (oldRotation != null) {
                oldRotation.set(rotation);
            }

            if (oldScale != null) {
                oldScale.set(scale);
            }
        }
    }

    public void rotate(Vector3f aNormedAxis, float aAngle) {
        rotation = (new Quaternion(aNormedAxis, aAngle)).mul(rotation).normalized();
    }

    public void getLookAt(Vector3f aPoint, Vector3f aUpDirection) {
        rotation = getLookAtDirection(aPoint.sub(position).normalized(), aUpDirection);
    }

    public Quaternion getLookAtPoint(Vector3f aPoint, Vector3f aUpDirection) {
        return getLookAtDirection(aPoint.sub(position).normalized(), aUpDirection);
    }

    public Quaternion getLookAtDirection(Vector3f aDirection, Vector3f aUpDirection) {
        return new Quaternion(new Matrix4f().initRotation(aDirection, aUpDirection));
    }

    public boolean hasChanged() {

        if (parent != null && parent.hasChanged()) {
            return true;
        }

        return (!oldScale.equals(scale)) || 
                (!oldRotation.equals(rotation)) || 
                (!oldPosition.equals(position));
    }

    public Matrix4f getTransformation() {
        Matrix4f lTranslate = new Matrix4f().initTranslation(position);

        Matrix4f lRotate = rotation.toRotationMatrix();

        Matrix4f lScale = new Matrix4f().initScaling(scale);

        return (getParentMatrix()).mul(lTranslate.mul(lRotate.mul(lScale)));
    }

    private Matrix4f getParentMatrix() {
        if (parent != null && parent.hasChanged()) {
            parentMatrix = parent.getTransformation();
        }
        return parentMatrix;
    }

    public void setParent(Transform aParent) {
        parent = aParent;
    }

    public Vector3f getTransformedPosition() {
        return getParentMatrix().mul(position);
    }

    public Quaternion getTransformedRotation() {
        Quaternion lParentRotation = new Quaternion(0, 0, 0, 1);

        if (parent != null) {
            lParentRotation = parent.getTransformedRotation();
        }

        return lParentRotation.mul(rotation);
    }
    

    public Vector3f getTransformedScale() {
        Vector3f lParentScale = new Vector3f(1,1, 1);
        Quaternion lParentRotation = new Quaternion(0, 0, 0, 1);

        if (parent != null) {
            lParentRotation = getTransformedRotation();
            lParentScale = parent.getTransformedScale();
        }

        return lParentScale.mul(scale.rotate(lParentRotation));//lParentRotation.toRotationMatrix().mul(scale)
    }

    public Vector3f getPosition() {
        return position;
    }

    public void setPosition(Vector3f position) {
        this.position = position;
    }

    public Quaternion getRotation() {
        return rotation;
    }

    public void setRotation(Quaternion rotation) {
        this.rotation = rotation;
    }

    public Vector3f getScale() {
        return scale;
    }

    public void setScale(Vector3f scale) {
        this.scale = scale;
    }

    public void set(Transform aState) {
        this.scale = aState.getScale();
        this.rotation = aState.getRotation();
        this.position = aState.getPosition();
    }

}
