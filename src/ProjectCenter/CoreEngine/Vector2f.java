/* GameEngine 3D - A programm to design games with 3D content 
 * Copyright (C) 2016-2056 Frank Küwen
 *  
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package ProjectCenter.CoreEngine;

public class Vector2f {

    public static final Vector2f ZEROVECTOR = new Vector2f(0, 0);
    public static final Vector2f UNIT_X = new Vector2f(1, 0);
    public static final Vector2f UNIT_Y = new Vector2f(0, 1);
    public static final int SIZE = 2;

    private float x;
    private float y;

    public Vector2f() {
        set(ZEROVECTOR);
    }
    public Vector2f(float aX, float aY) {
        x = aX;
        y = aY;
    }

    public void set(float aX, float aY) {
        this.x = Precision.round( aX);
        this.y = Precision.round( aY);
    }
    public void set(Vector2f aOriginal) {
        set(
                aOriginal.getX(), 
                aOriginal.getY());
    }

    public Vector2f clone(Vector2f aOriginal) {
        set(aOriginal);
        return this;
    }

    @Override
    public String toString() {
        return "(" + Precision.NUMBERFORMAT.format(x) + 
                "|" + Precision.NUMBERFORMAT.format(y) + ")";
    }

    public float length() {
        return (float) Math.sqrt(x * x + y * y);
    }

    public Vector2f abs() {
        return new Vector2f(Math.abs(x), Math.abs(y));
    }

    public float max() {
        return Math.max(this.x, this.y);
    }

    public float min() {
        return Math.min(this.x, this.y);
    }

    public float dot(Vector2f aVector) {
        return ((aVector.getX() * x) + (aVector.getY() * y));
    }

    public Vector2f add(Vector2f aVector) {
        return new Vector2f(x + aVector.getX(), y + aVector.getY());
    }

    public Vector2f add(float aScalar) {
        return new Vector2f(x + aScalar, y + aScalar);
    }

    public Vector2f sub(Vector2f aVector) {
        return new Vector2f(x - aVector.getX(), y - aVector.getY());
    }

    public Vector2f sub(float aScalar) {
        return new Vector2f(x - aScalar, y - aScalar);
    }

    public Vector2f mul(Vector2f aVector) {
        return new Vector2f(x * aVector.getX(), y * aVector.getY());
    }

    public Vector2f mul(float aScalar) {
        return new Vector2f(x * aScalar, y * aScalar);
    }

    public Vector2f div(Vector2f aVector) {
        if (Precision.greater(aVector.length(), 0))
            return new Vector2f(x / aVector.getX(), y / aVector.getY());
        else
            return new Vector2f(0, 0);
    }

    public Vector2f div(float aScalar) {
        if (Precision.greater(aScalar, 0))
            return new Vector2f(x / aScalar, y / aScalar);
        else
            return new Vector2f(0, 0);
    }

    public Vector2f linearExtrapolation(Vector2f aVector, float aScalar) {
        return ((aVector.sub(this)).mul(aScalar)).add(this);
    }

    public Vector2f rotateWithDegree(float aAngle) {
        double rad = Math.toRadians(aAngle);
        double cos = Math.cos(rad);
        double sin = Math.sin(rad);

        return new Vector2f((float) (x * cos - y * sin), (float) (x * sin + y * cos));
    }

    public Vector2f normalized() {
        float length = length();
        if (Precision.greater(length, 0))
            return new Vector2f(x / length, y / length);
        else
            return new Vector2f(0, 0);
    }

    public float determinate(Vector2f aVector) {
        return x * aVector.getY() - y * aVector.getX();
    }

    public float getX() {
        return x;
    }

    public void setX(float aX) {
        this.x = Precision.round( aX);
    }

    public float getY() {
        return y;
    }

    public void setY(float aY) {
        this.y = Precision.round( aY);
    }

    public boolean equals(Vector2f aVector) {
        return (Precision.equals(x, aVector.getX()) && Precision.equals(y, aVector.getY()));
    }
}
