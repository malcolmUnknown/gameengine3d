/* GameEngine 3D - A programm to design games with 3D content 
 * Copyright (C) 2016-2056 Frank Küwen
 *  
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package ProjectCenter.CoreEngine;

import Controller.CommandNote;
import Controller.CommandSource;
import java.awt.event.KeyEvent;
import java.util.Observable;
import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;

public class InputKeyboard extends Observable {

    public static final int NUM_KEYCODES = 256;
    private final boolean[] lastKeys;
    private boolean keyboardLock;

    public InputKeyboard() {
        super();
        lastKeys = new boolean[NUM_KEYCODES];
        keyboardLock = false;
        for (int i = 0; i < NUM_KEYCODES; i++) {
            lastKeys[i] = false;
        }
    }

    public void init() {
    }

    public void setKeyboardLock(boolean aKeyState) {
        keyboardLock = aKeyState;
    }

    public boolean getKeyboardState() {
        return keyboardLock;
    }

    public void update() {
        if (countObservers() > 0) {
            for (int i = 0; i < NUM_KEYCODES; i++) {
                if (getKeyDown(i)) {
                    setChanged();
                    notifyObservers(new CommandNote(
                            CommandSource.Keyboard,
                            lwjglKeyNoToKeyNo(i),
                            true,
                            0,
                            new Vector3f()));
                }
                if (getKeyUp(i)) {
                    setChanged();
                    notifyObservers(new CommandNote(
                            CommandSource.Keyboard,
                            lwjglKeyNoToKeyNo(i),
                            false,
                            0,
                            new Vector3f()));
                }
                lastKeys[i] = getKey(i);

            }

            if (!Vector2f.ZEROVECTOR.equals(Vector2f.ZEROVECTOR)) {
                setChanged();
                notifyObservers(new CommandNote(
                        CommandSource.Keyboard,
                        KeyEvent.VK_EXCLAMATION_MARK,
                        true,
                        0,
                        new Vector3f()));
            }
        }
    }

    private boolean getKey(int aKeyCode) {
        return Keyboard.isKeyDown(aKeyCode);
    }

    private boolean getKeyDown(int aKeyCode) {
        return getKey(aKeyCode) && !lastKeys[aKeyCode];
    }

    private boolean getKeyUp(int aKeyCode) {
        return !getKey(aKeyCode) && lastKeys[aKeyCode];
    }

    private int lwjglKeyNoToKeyNo(int aLwjglKeyNo) {
        int lResult = -1;
        switch (aLwjglKeyNo) {
            case Keyboard.KEY_NONE: {
                lResult = KeyEvent.VK_UNDEFINED;
                break;
            }
            case Keyboard.KEY_ESCAPE: {
                lResult = KeyEvent.VK_ESCAPE;
                break;
            }
            case Keyboard.KEY_1: {
                lResult = KeyEvent.VK_1;
                break;
            }
            case Keyboard.KEY_2: {
                lResult = KeyEvent.VK_2;
                break;
            }
            case Keyboard.KEY_3: {
                lResult = KeyEvent.VK_3;
                break;
            }
            case Keyboard.KEY_4: {
                lResult = KeyEvent.VK_4;
                break;
            }
            case Keyboard.KEY_5: {
                lResult = KeyEvent.VK_5;
                break;
            }
            case Keyboard.KEY_6: {
                lResult = KeyEvent.VK_6;
                break;
            }
            case Keyboard.KEY_7: {
                lResult = KeyEvent.VK_7;
                break;
            }
            case Keyboard.KEY_8: {
                lResult = KeyEvent.VK_8;
                break;
            }
            case Keyboard.KEY_9: {
                lResult = KeyEvent.VK_9;
                break;
            }
            case Keyboard.KEY_0: {
                lResult = KeyEvent.VK_0;
                break;
            }
            case Keyboard.KEY_MINUS: {
                lResult = KeyEvent.VK_MINUS;
                break;
            }
            case Keyboard.KEY_EQUALS: {
                lResult = KeyEvent.VK_EQUALS;
                break;
            }
            case Keyboard.KEY_BACK: {
                lResult = KeyEvent.VK_BACK_SPACE;
                break;
            }// backspace 
            case Keyboard.KEY_TAB: {
                lResult = KeyEvent.VK_TAB;
                break;
            }
            case Keyboard.KEY_Q: {
                lResult = KeyEvent.VK_Q;
                break;
            }
            case Keyboard.KEY_W: {
                lResult = KeyEvent.VK_W;
                break;
            }
            case Keyboard.KEY_E: {
                lResult = KeyEvent.VK_E;
                break;
            }
            case Keyboard.KEY_R: {
                lResult = KeyEvent.VK_R;
                break;
            }
            case Keyboard.KEY_T: {
                lResult = KeyEvent.VK_T;
                break;
            }
            case Keyboard.KEY_Y: {
                lResult = KeyEvent.VK_Y;
                break;
            }
            case Keyboard.KEY_U: {
                lResult = KeyEvent.VK_U;
                break;
            }
            case Keyboard.KEY_I: {
                lResult = KeyEvent.VK_I;
                break;
            }
            case Keyboard.KEY_O: {
                lResult = KeyEvent.VK_O;
                break;
            }
            case Keyboard.KEY_P: {
                lResult = KeyEvent.VK_P;
                break;
            }
            case Keyboard.KEY_LBRACKET: {
                lResult = KeyEvent.VK_LEFT_PARENTHESIS;
                break;
            }
            case Keyboard.KEY_RBRACKET: {
                lResult = KeyEvent.VK_RIGHT_PARENTHESIS;
                break;
            }
            case Keyboard.KEY_RETURN: {
                lResult = KeyEvent.VK_RIGHT;
                break;
            }
            /* Enter on main keyboard */
            case Keyboard.KEY_LCONTROL: {
                lResult = KeyEvent.VK_CONTROL;
                break;
            }
            case Keyboard.KEY_A: {
                lResult = KeyEvent.VK_A;
                break;
            }
            case Keyboard.KEY_S: {
                lResult = KeyEvent.VK_S;
                break;
            }
            case Keyboard.KEY_D: {
                lResult = KeyEvent.VK_D;
                break;
            }
            case Keyboard.KEY_F: {
                lResult = KeyEvent.VK_F;
                break;
            }
            case Keyboard.KEY_G: {
                lResult = KeyEvent.VK_G;
                break;
            }
            case Keyboard.KEY_H: {
                lResult = KeyEvent.VK_H;
                break;
            }
            case Keyboard.KEY_J: {
                lResult = KeyEvent.VK_J;
                break;
            }
            case Keyboard.KEY_K: {
                lResult = KeyEvent.VK_K;
                break;
            }
            case Keyboard.KEY_L: {
                lResult = KeyEvent.VK_L;
                break;
            }
            case Keyboard.KEY_SEMICOLON: {
                lResult = KeyEvent.VK_SEMICOLON;
                break;
            }
            case Keyboard.KEY_APOSTROPHE: {
                lResult = KeyEvent.VK_ALPHANUMERIC;
                break;
            }//?????
            case Keyboard.KEY_GRAVE: {
                lResult = KeyEvent.VK_SEPARATOR;
                break;
            }//???    /* accent grave */

            case Keyboard.KEY_LSHIFT: {
                lResult = KeyEvent.VK_SHIFT;
                break;
            }
            case Keyboard.KEY_BACKSLASH: {
                lResult = KeyEvent.VK_BACK_SLASH;
                break;
            }
            case Keyboard.KEY_Z: {
                lResult = KeyEvent.VK_Z;
                break;
            }
            case Keyboard.KEY_X: {
                lResult = KeyEvent.VK_X;
                break;
            }
            case Keyboard.KEY_C: {
                lResult = KeyEvent.VK_C;
                break;
            }
            case Keyboard.KEY_V: {
                lResult = KeyEvent.VK_V;
                break;
            }
            case Keyboard.KEY_B: {
                lResult = KeyEvent.VK_B;
                break;
            }
            case Keyboard.KEY_N: {
                lResult = KeyEvent.VK_N;
                break;
            }
            case Keyboard.KEY_M: {
                lResult = KeyEvent.VK_M;
                break;
            }
            case Keyboard.KEY_COMMA: {
                lResult = KeyEvent.VK_COMMA;
                break;
            }
            case Keyboard.KEY_PERIOD: {
                lResult = KeyEvent.VK_PERIOD;
                break;
            }
            /* . on main keyboard */

            case Keyboard.KEY_SLASH: {
                lResult = KeyEvent.VK_SLASH;
                break;
            }
            /* / on main keyboard */

            case Keyboard.KEY_RSHIFT: {
                lResult = KeyEvent.VK_SHIFT;
                break;
            }//???
            case Keyboard.KEY_MULTIPLY: {
                lResult = KeyEvent.VK_MULTIPLY;
                break;
            }
            /* * on numeric keypad */

            case Keyboard.KEY_LMENU: {
                lResult = KeyEvent.VK_ALT;
                break;
            }
            /* left Alt */

            case Keyboard.KEY_SPACE: {
                lResult = KeyEvent.VK_SPACE;
                break;
            }
            case Keyboard.KEY_CAPITAL: {
                lResult = KeyEvent.VK_CAPS_LOCK;
                break;
            }
            case Keyboard.KEY_F1: {
                lResult = KeyEvent.VK_F1;
                break;
            }
            case Keyboard.KEY_F2: {
                lResult = KeyEvent.VK_F2;
                break;
            }
            case Keyboard.KEY_F3: {
                lResult = KeyEvent.VK_F3;
                break;
            }
            case Keyboard.KEY_F4: {
                lResult = KeyEvent.VK_F4;
                break;
            }
            case Keyboard.KEY_F5: {
                lResult = KeyEvent.VK_F5;
                break;
            }
            case Keyboard.KEY_F6: {
                lResult = KeyEvent.VK_F6;
                break;
            }
            case Keyboard.KEY_F7: {
                lResult = KeyEvent.VK_F7;
                break;
            }
            case Keyboard.KEY_F8: {
                lResult = KeyEvent.VK_F8;
                break;
            }
            case Keyboard.KEY_F9: {
                lResult = KeyEvent.VK_F9;
                break;
            }
            case Keyboard.KEY_F10: {
                lResult = KeyEvent.VK_F10;
                break;
            }
            case Keyboard.KEY_NUMLOCK: {
                lResult = KeyEvent.VK_NUM_LOCK;
                break;
            }
            case Keyboard.KEY_SCROLL: {
                lResult = KeyEvent.VK_SCROLL_LOCK;
                break;
            }
            /* Scroll Lock */

            case Keyboard.KEY_NUMPAD7: {
                lResult = KeyEvent.VK_NUMPAD7;
                break;
            }
            case Keyboard.KEY_NUMPAD8: {
                lResult = KeyEvent.VK_NUMPAD8;
                break;
            }
            case Keyboard.KEY_NUMPAD9: {
                lResult = KeyEvent.VK_NUMPAD9;
                break;
            }
            case Keyboard.KEY_SUBTRACT: {
                lResult = KeyEvent.VK_SUBTRACT;
                break;
            }
            /* - on numeric keypad */

            case Keyboard.KEY_NUMPAD4: {
                lResult = KeyEvent.VK_NUMPAD4;
                break;
            }
            case Keyboard.KEY_NUMPAD5: {
                lResult = KeyEvent.VK_NUMPAD5;
                break;
            }
            case Keyboard.KEY_NUMPAD6: {
                lResult = KeyEvent.VK_NUMPAD6;
                break;
            }
            case Keyboard.KEY_ADD: {
                lResult = KeyEvent.VK_ADD;
                break;
            }
            /* + on numeric keypad */

            case Keyboard.KEY_NUMPAD1: {
                lResult = KeyEvent.VK_NUMPAD1;
                break;
            }
            case Keyboard.KEY_NUMPAD2: {
                lResult = KeyEvent.VK_NUMPAD2;
                break;
            }
            case Keyboard.KEY_NUMPAD3: {
                lResult = KeyEvent.VK_NUMPAD3;
                break;
            }
            case Keyboard.KEY_NUMPAD0: {
                lResult = KeyEvent.VK_NUMPAD0;
                break;
            }
            case Keyboard.KEY_DECIMAL: {
                lResult = KeyEvent.VK_DECIMAL;
                break;
            }
            /* . on numeric keypad */

            case Keyboard.KEY_F11: {
                lResult = KeyEvent.VK_F11;
                break;
            }
            case Keyboard.KEY_F12: {
                lResult = KeyEvent.VK_F12;
                break;
            }
            case Keyboard.KEY_F13: {
                lResult = KeyEvent.VK_F13;
                break;
            }
            /*  (NEC PC98) */

            case Keyboard.KEY_F14: {
                lResult = KeyEvent.VK_F14;
                break;
            }
            /*  (NEC PC98) */

            case Keyboard.KEY_F15: {
                lResult = KeyEvent.VK_F15;
                break;
            }
            /*  (NEC PC98) */

            case Keyboard.KEY_KANA: {
                lResult = KeyEvent.VK_KANA;
                break;
            }
            /* (Japanese keyboard) */

            case Keyboard.KEY_CONVERT: {
                lResult = KeyEvent.VK_CONVERT;
                break;
            }
            /* (Japanese keyboard) */

            case Keyboard.KEY_NOCONVERT: {
                lResult = KeyEvent.VK_NONCONVERT;
                break;
            }
            /* (Japanese keyboard) */

            case Keyboard.KEY_YEN: {
                lResult = KeyEvent.VK_ALL_CANDIDATES;
                break;
            }//???
            /* (Japanese keyboard) */

            case Keyboard.KEY_NUMPADEQUALS: {
                lResult = KeyEvent.VK_EQUALS;
                break;
            }//???
            /* : lresult=  on numeric keypad (NEC PC98) */

            case Keyboard.KEY_CIRCUMFLEX: {
                lResult = KeyEvent.VK_CIRCUMFLEX;
                break;
            }
            /* (Japanese keyboard)            */

            case Keyboard.KEY_AT: {
                lResult = KeyEvent.VK_AT;
                break;
            }
            /*  (NEC PC98) */

            case Keyboard.KEY_COLON: {
                lResult = KeyEvent.VK_COLON;
                break;
            }
            /*  (NEC PC98) */

            case Keyboard.KEY_UNDERLINE: {
                lResult = KeyEvent.VK_UNDERSCORE;
                break;
            }
            /*  (NEC PC98) */

            case Keyboard.KEY_KANJI: {
                lResult = KeyEvent.VK_KANJI;
                break;
            }
            /* (Japanese keyboard)            */

            case Keyboard.KEY_STOP: {
                lResult = KeyEvent.VK_STOP;
                break;
            }
            /*  (NEC PC98) */

            case Keyboard.KEY_AX: {
                lResult = KeyEvent.VK_AGAIN;
                break;
            }//????
            /* (Japan AX) */

            case Keyboard.KEY_UNLABELED: {
                lResult = KeyEvent.VK_UNDEFINED;
                break;
            }
            /* (J3100) */

            case Keyboard.KEY_NUMPADENTER: {
                lResult = KeyEvent.VK_ENTER;
                break;
            }
            /* Enter on numeric keypad */

            case Keyboard.KEY_RCONTROL: {
                lResult = KeyEvent.VK_CONTROL;
                break;
            }
            case Keyboard.KEY_NUMPADCOMMA: {
                lResult = KeyEvent.VK_COMMA;
                break;
            }
            /* , on numeric keypad (NEC PC98) */

            case Keyboard.KEY_DIVIDE: {
                lResult = KeyEvent.VK_DIVIDE;
                break;
            }
            /* / on numeric keypad */

            case Keyboard.KEY_SYSRQ: {
                lResult = KeyEvent.VK_WINDOWS;
                break;
            }//??
            case Keyboard.KEY_RMENU: {
                lResult = KeyEvent.VK_MODECHANGE;
                break;
            } //??
            /* right Alt */

            case Keyboard.KEY_PAUSE: {
                lResult = KeyEvent.VK_PAUSE;
                break;
            }
            /* Pause */

            case Keyboard.KEY_HOME: {
                lResult = KeyEvent.VK_HOME;
                break;
            }
            /* Home on arrow keypad */

            case Keyboard.KEY_UP: {
                lResult = KeyEvent.VK_UP;
                break;
            }
            /* UpArrow on arrow keypad */

            case Keyboard.KEY_PRIOR: {
                lResult = KeyEvent.VK_PREVIOUS_CANDIDATE;
                break;
            }
            /* PgUp on arrow keypad */

            case Keyboard.KEY_LEFT: {
                lResult = KeyEvent.VK_LEFT;
                break;
            }
            /* LeftArrow on arrow keypad */

            case Keyboard.KEY_RIGHT: {
                lResult = KeyEvent.VK_RIGHT;
                break;
            }
            /* RightArrow on arrow keypad */

            case Keyboard.KEY_END: {
                lResult = KeyEvent.VK_END;
                break;
            }
            /* End on arrow keypad */

            case Keyboard.KEY_DOWN: {
                lResult = KeyEvent.VK_DOWN;
                break;
            }
            /* DownArrow on arrow keypad */

            case Keyboard.KEY_NEXT: {
                lResult = KeyEvent.VK_PAGE_DOWN;
                break;
            }
            /* PgDn on arrow keypad */

            case Keyboard.KEY_INSERT: {
                lResult = KeyEvent.VK_INSERT;
                break;
            }
            /* Insert on arrow keypad */

            case Keyboard.KEY_DELETE: {
                lResult = KeyEvent.VK_DELETE;
                break;
            }
            /* Delete on arrow keypad */

            case Keyboard.KEY_LMETA: {
                lResult = KeyEvent.VK_WINDOWS;
                break;
            }
            /* Left Windows/Option key */

            case Keyboard.KEY_RMETA: {
                lResult = KeyEvent.VK_META;
                break;
            }
            /* Right Windows/Option key */

            case Keyboard.KEY_APPS: {
                lResult = KeyEvent.VK_ALL_CANDIDATES;
                break;
            }//????
            /* AppMenu key */

            case Keyboard.KEY_POWER: {
                lResult = KeyEvent.VK_FINAL;
                break;
            }//????
            case Keyboard.KEY_SLEEP: {
                lResult = KeyEvent.VK_AMPERSAND;
                break;
            }//????
        }
        return lResult;
    }
}
