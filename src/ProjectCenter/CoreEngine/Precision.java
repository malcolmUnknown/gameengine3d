/* GameEngine 3D - A programm to design games with 3D content 
 * Copyright (C) 2016-2056 Frank Küwen
 *  
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package ProjectCenter.CoreEngine;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.math.BigDecimal;
import java.util.Locale;

public class Precision {

    private static float EPSILON = 0.000001f;
    private static final DecimalFormatSymbols SYMBOLS = new DecimalFormatSymbols(Locale.UK);
    public static DecimalFormat NUMBERFORMAT = new DecimalFormat("+#0.00;-#", SYMBOLS);

    public static float getEpsilon() {
        return EPSILON;
    }

    public static void setEpsilon(float aEpsilon) {
        EPSILON = aEpsilon;
    }

    public static void resetEpsilon() {
        EPSILON = 0.000001f;
    }

    public static boolean equals(float aFirst, float aSecond, float aEpsilon) {
        return Math.abs(aFirst - aSecond) < aEpsilon;
    }

    public static boolean greater(float aFirst, float aSecond, float aEpsilon) {
        return (aFirst - aSecond) > aEpsilon;
    }

    public static boolean less(float aFirst, float aSecond, float aEpsilon) {
        return (aFirst - aSecond) < -aEpsilon;
    }

    public static boolean greaterEqual(float aFirst, float aSecond, float aEpsilon) {
        return (Precision.greater(aFirst, aSecond, aEpsilon) || Precision.equals(aFirst, aSecond, aEpsilon));
    }

    public static boolean lessEqual(float aFirst, float aSecond, float aEpsilon) {
        return (Precision.less(aFirst, aSecond, aEpsilon) || Precision.equals(aFirst, aSecond, aEpsilon));
    }

    public static boolean equals(float aFirst, float aSecond) {
        return Precision.equals(aFirst, aSecond, EPSILON);
    }

    public static boolean less(float aFirst, float aSecond) {
        return Precision.less(aFirst, aSecond, EPSILON);
    }

    public static boolean greater(float aFirst, float aSecond) {
        return Precision.greater(aFirst, aSecond, EPSILON);
    }

    public static boolean greaterEqual(float aFirst, float aSecond) {
        return (greater(aFirst, aSecond) || equals(aFirst, aSecond));
    }

    public static boolean lessEqual(float aFirst, float aSecond) {
        return (less(aFirst, aSecond) || equals(aFirst, aSecond));
    }

    public static float signum(float aFirst, float aEpsilon) {
        return less(aFirst, 0, aEpsilon) ? -1 : (greater(aFirst, 0, aEpsilon) ? 1 : 0);
    }

    public static float signum(float aFirst) {
        return less(aFirst, 0, EPSILON) ? -1 : (greater(aFirst, 0, EPSILON) ? 1 : 0);
    }

    public static float round(double value, float aEpsilon) {
        int numberOfDigitsAfterDecimalPoint = (int) Math.round(Math.log10(1 / aEpsilon)); 
        BigDecimal bigDecimal = new BigDecimal(value);
        bigDecimal = bigDecimal.setScale(numberOfDigitsAfterDecimalPoint,
                BigDecimal.ROUND_HALF_UP);
        return bigDecimal.floatValue();
    }

    public static float round(double value) {
        return round(value, EPSILON);
    }
}
