/* GameEngine 3D - A programm to design games with 3D content 
 * Copyright (C) 2016-2056 Frank Küwen
 *  
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package ProjectCenter.CoreEngine;

import Components.FreeMoveController;
import Components.FreeRotateController;
import Controller.CommandMouse;
import Controller.CommandNote;
import Controller.CommandSource;
import MVC_Interface.Axis;
import MVC_Interface.IControllerRotation;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Observable;
import java.util.Observer;
import java.util.UUID;

public class InputRotation implements IControllerRotation, Observer {

    private final Hashtable<UUID, FreeRotateController> dictionary;
    private final ArrayList<UUID> activeController;

    public InputRotation() {
        dictionary = new Hashtable<UUID, FreeRotateController>();
        activeController = new ArrayList<>();
    }

    @Override
    public void rotateVector(Vector3f aVector) {
        if (!activeController.isEmpty()) {
            Iterator<UUID> lIds = activeController.listIterator();
            while (lIds.hasNext()) {
                FreeRotateController lRotor = dictionary.get(lIds.next());
                lRotor.setRotationVector(aVector);
            }
        } 
    }

    @Override
    public void rotate(Axis aDirection, boolean aInPositive, boolean aStartSignal) {
        if (!activeController.isEmpty()) {
            Iterator<UUID> lIds = activeController.listIterator();
            while (lIds.hasNext()) {
                FreeRotateController lRotor = dictionary.get(lIds.next());
                lRotor.setAxisRotation(aDirection, aInPositive, aStartSignal);
            }
        }
    }

    @Override
    public void rotate(Axis aDirection, boolean aInPositive, float aAngle) {
        if (!activeController.isEmpty()) {
            Iterator<UUID> lIds = activeController.listIterator();
            while (lIds.hasNext()) {
                FreeRotateController lRotor = dictionary.get(lIds.next());
                lRotor.setAxisRotation(aDirection, aInPositive, aAngle);
            }
        } 
    }

    public void addRotationController(UUID aGameObjectIdentity, FreeRotateController aRotationController) {
        dictionary.put(aGameObjectIdentity, aRotationController);
    }

    public void removeRotationController(UUID aGameObjectIdentity, FreeRotateController aRotationController) {
        if (dictionary.get(aGameObjectIdentity) != null) {
            FreeRotateController remove = dictionary.remove(aGameObjectIdentity);
        }
    }
    
    public void setElementState(UUID aId, boolean aIsActive) {
        if (aIsActive) {
            if (!activeController.contains(aId)) {
                activeController.add(aId);
            }
        } else {
            if (activeController.contains(aId)) {
                activeController.remove(aId);
            }
        }
    }

    @Override
    public void update(Observable o, Object o1) {
        if (o1 instanceof CommandNote) {
            CommandNote lNotice = (CommandNote) o1;
            if ((lNotice.getType() == CommandSource.Mouse)) {
                MouseCommands(lNotice);
            }
        }
    }

    private void MouseCommands(CommandNote aCommand) {
        if (aCommand.getCommand() == CommandMouse.MouseMotion.ordinal()) {
            rotateVector(aCommand.getPosition());
        }
        if (aCommand.getCommand() == CommandMouse.MouseButton3.ordinal()) {
            rotate(Axis.Top, true, aCommand.getCommandState());
        }
    }
}
