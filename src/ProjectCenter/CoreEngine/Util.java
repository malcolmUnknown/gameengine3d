/* GameEngine 3D - A programm to design games with 3D content 
 * Copyright (C) 2016-2056 Frank Küwen
 *  
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package ProjectCenter.CoreEngine;

import ProjectCenter.RenderEngine.Vertex;
import java.nio.ByteBuffer;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.util.ArrayList;
import org.lwjgl.BufferUtils;

public class Util {

    public static FloatBuffer createFloatBuffer(int aSize) {
        return BufferUtils.createFloatBuffer(aSize);
    }

    public static IntBuffer createIntBuffer(int aSize) {
        return BufferUtils.createIntBuffer(aSize);
    }
    public static ByteBuffer createByteBuffer(int aSize) {
        return BufferUtils.createByteBuffer(aSize);
    }

    public static IntBuffer createFlippedBuffer(int... values) {
        IntBuffer buffer = createIntBuffer(values.length);
        buffer.put(values);
        buffer.flip();
        return buffer;
    }

    public static FloatBuffer createFlippedBuffer(Vertex[] aVertices) {
        FloatBuffer buffer = createFloatBuffer(aVertices.length * Vertex.SIZE);

        for (Vertex aVertex : aVertices) {
            buffer.put(aVertex.getPosition().getX());
            buffer.put(aVertex.getPosition().getY());
            buffer.put(aVertex.getPosition().getZ());
            buffer.put(aVertex.getTextureCoordinate().getX());
            buffer.put(aVertex.getTextureCoordinate().getY());
            buffer.put(aVertex.getNormal().getX());
            buffer.put(aVertex.getNormal().getY());
            buffer.put(aVertex.getNormal().getZ());
        }
        buffer.flip();
        return buffer;
    }

    public static FloatBuffer createFlippedBuffer(Matrix4f aMatrix) {

        FloatBuffer buffer = createFloatBuffer(Matrix4f.COLUMNS * Matrix4f.ROWS);

        for (int i = 0; i < Matrix4f.ROWS; i++) {
            for (int j = 0; j < Matrix4f.COLUMNS; j++) {
                buffer.put(aMatrix.get(i, j));
            }
        }

        buffer.flip();
        return buffer;
    }

    public static String[] removeEmptyStrings(String[] aStringArray) {
        ArrayList<String> checkedList = new ArrayList<String>();

        for (String aString : aStringArray) {
            if (!aString.equals("")) {
                checkedList.add(aString);
            }
        }
        String[] lResult = new String[checkedList.size()];
        checkedList.toArray(lResult);

        return lResult;
    }

    public static int[] toIntArray(Integer[] aIntegerArray) {
        int[] lResult = new int[aIntegerArray.length];
        for (int i = 0; i < aIntegerArray.length; i++) {
            lResult[i] = aIntegerArray[i].intValue();
        }

        return lResult;
    }
}
