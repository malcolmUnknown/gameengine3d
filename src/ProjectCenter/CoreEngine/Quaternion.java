/* GameEngine 3D - A programm to design games with 3D content 
 * Copyright (C) 2016-2056 Frank Küwen
 *  
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package ProjectCenter.CoreEngine;

public class Quaternion {

    public static final Quaternion ZERO = new Quaternion(0, 0, 0, 0);
    private float x;
    private float y;
    private float z;
    private float w;

    public Quaternion(float aX, float aY, float aZ, float aW) {
        x = aX;
        y = aY;
        z = aZ;
        w = aW;
    }

    public Quaternion() {
        this(0, 0, 0, 0);
    }
    public Quaternion(Quaternion aOther) {
        x = aOther.getX();
        y = aOther.getY();
        z = aOther.getZ();
        w = aOther.getW();
    }

    public Quaternion(Vector3f aNormedAxis, float aAngle) {

        float sinHalfAngle = (float) Math.sin(aAngle / 2.f);
        float cosHalfAngle = (float) Math.cos(aAngle / 2.f);

        this.x = aNormedAxis.getX() * sinHalfAngle;
        this.y = aNormedAxis.getY() * sinHalfAngle;
        this.z = aNormedAxis.getZ() * sinHalfAngle;
        this.w = cosHalfAngle;
    }

    @Override
    public String toString() {
        return "[" + Precision.NUMBERFORMAT.format(x)
                + "|" + Precision.NUMBERFORMAT.format(y)
                + "|" + Precision.NUMBERFORMAT.format(z)
                + "|" + Precision.NUMBERFORMAT.format(w) + "]";
    }
    
    public void set(float aX, float aY, float aZ, float aW) {
        this.x = Precision.round( aX);
        this.y = Precision.round( aY);
        this.z = Precision.round( aZ);
        this.w = Precision.round( aW);
    }

    public void set(Quaternion aOriginal) {
        set(
                aOriginal.getX(),
                aOriginal.getY(),
                aOriginal.getZ(),
                aOriginal.getW());
    }

    public Quaternion clone(Quaternion aOriginal) {
        set(aOriginal);
        return this;
    }

    public float length() {
        return (float) Math.sqrt(x * x + y * y + z * z + w * w);
    }

    public Quaternion normalized() {
        float length = this.length();
        if (Precision.greater(length, 0))
            return new Quaternion(x / length, y / length, z / length, w / length);
        else
            return new Quaternion(0, 0, 0, 0);
    }

    public Quaternion conjugate() {
        return new Quaternion(-x, -y, -z, w);
    }

    public Quaternion mul(Quaternion aQuarterion) {
        float _w = w * aQuarterion.getW() - x * aQuarterion.getX() - y * aQuarterion.getY() - z * aQuarterion.getZ();
        float _x = w * aQuarterion.getX() + x * aQuarterion.getW() + y * aQuarterion.getZ() - z * aQuarterion.getY();
        float _y = w * aQuarterion.getY() - x * aQuarterion.getZ() + y * aQuarterion.getW() + z * aQuarterion.getX();
        float _z = w * aQuarterion.getZ() + x * aQuarterion.getY() - y * aQuarterion.getX() + z * aQuarterion.getW();

        return new Quaternion(_x, _y, _z, _w);
    }

    public Quaternion mul(Vector3f aVector) {
        // return  ( v_x, v_y, v_z, 0)*q
        float _w =-x * aVector.getX() - y * aVector.getY() - z * aVector.getZ();
        float _x = w * aVector.getX() + y * aVector.getZ() - z * aVector.getY();
        float _y = w * aVector.getY() + z * aVector.getX() - x * aVector.getZ();
        float _z = w * aVector.getZ() + x * aVector.getY() - y * aVector.getX();

        return new Quaternion(_x, _y, _z, _w);
    }

    public Quaternion mul(float aScalar) {
        return new Quaternion(x * aScalar, y * aScalar, z * aScalar, w * aScalar);
    }

    public float dot(Quaternion aQuaternion) {
        return (x * aQuaternion.getX() + y * aQuaternion.getY() + z * aQuaternion.getZ() + w * aQuaternion.getW());
    }

    public Quaternion add(Quaternion aQuaternion) {
        return new Quaternion(x + aQuaternion.getX(), y + aQuaternion.getY(), z + aQuaternion.getZ(), w + aQuaternion.getW());
    }

    public Quaternion sub(Quaternion aQuaternion) {
        return new Quaternion(x - aQuaternion.getX(), y - aQuaternion.getY(), z - aQuaternion.getZ(), w - aQuaternion.getW());
    }

    public Quaternion linearExtrapolationNormalized(Quaternion aQuaternion, float aScalar, boolean aShortest) {

        Quaternion lCorrectedDestination = aQuaternion;
        if (aShortest && Precision.less(this.dot(aQuaternion), 0))
            lCorrectedDestination = aQuaternion.mul(-1);
        return lCorrectedDestination.sub(this).mul(aScalar).add(this).normalized();
    }

    public Quaternion linearExtrapolationSpherical(Quaternion aQuaternion, float aScalar, boolean aShortest) {
        float lCos = this.dot(aQuaternion);
        Quaternion lCorrectedDestination = aQuaternion;
        if (aShortest && Precision.less(lCos, 0)) {
            lCos = -lCos;
            lCorrectedDestination = aQuaternion.mul(-1);
        }
        if (Precision.lessEqual(lCos, -1) || Precision.greaterEqual(lCos, 1))
            return linearExtrapolationNormalized(aQuaternion, aScalar, aShortest);
        float lSin = (float) Math.sqrt(1.0f - lCos * lCos);
        float lAngle = (float) Math.atan2(lSin, lCos);
        float lInvSin = 1.0f / lSin;
        float lSrcFactor = (float) Math.sin((1.0f - aScalar) * lAngle) * lInvSin;
        float lDestFactor = (float) Math.sin((aScalar) * lAngle) * lInvSin;
        return this.mul(lSrcFactor).add(lCorrectedDestination.mul(lDestFactor));
    }

    //From Ken Shoemake's "Quaternion Calculus and Fast Animation" article
    public Quaternion(Matrix4f aRotation) {
        float lTrace = aRotation.get(0, 0) + aRotation.get(1, 1) + aRotation.get(2, 2);

        if (lTrace > 0) {
            float s = 0.5f / (float) Math.sqrt(lTrace + 1.0f);
            w = 0.25f / s;
            x = (aRotation.get(1, 2) - aRotation.get(2, 1)) * s;
            y = (aRotation.get(2, 0) - aRotation.get(0, 2)) * s;
            z = (aRotation.get(0, 1) - aRotation.get(1, 0)) * s;
        } else
            if (aRotation.get(0, 0) > aRotation.get(1, 1) && aRotation.get(0, 0) > aRotation.get(2, 2)) {
                float s = 2.0f * (float) Math.sqrt(1.0f + aRotation.get(0, 0) - aRotation.get(1, 1) - aRotation.get(2, 2));
                w = (aRotation.get(1, 2) - aRotation.get(2, 1)) / s;
                x = 0.25f * s;
                y = (aRotation.get(1, 0) + aRotation.get(0, 1)) / s;
                z = (aRotation.get(2, 0) + aRotation.get(0, 2)) / s;
            } else if (aRotation.get(1, 1) > aRotation.get(2, 2)) {
                float s = 2.0f * (float) Math.sqrt(1.0f + aRotation.get(1, 1) - aRotation.get(0, 0) - aRotation.get(2, 2));
                w = (aRotation.get(2, 0) - aRotation.get(0, 2)) / s;
                x = (aRotation.get(1, 0) + aRotation.get(0, 1)) / s;
                y = 0.25f * s;
                z = (aRotation.get(2, 1) + aRotation.get(1, 2)) / s;
            } else {
                float s = 2.0f * (float) Math.sqrt(1.0f + aRotation.get(2, 2) - aRotation.get(0, 0) - aRotation.get(1, 1));
                w = (aRotation.get(0, 1) - aRotation.get(1, 0)) / s;
                x = (aRotation.get(2, 0) + aRotation.get(0, 2)) / s;
                y = (aRotation.get(1, 2) + aRotation.get(2, 1)) / s;
                z = 0.25f * s;
            }

        float length = (float) Math.sqrt(x * x + y * y + z * z + w * w);
        x /= length;
        y /= length;
        z /= length;
        w /= length;
    }

    public Matrix4f toRotationMatrix() {
        // use rotate function of Vector3f class and expand
        // Compare Wikipedia
        Vector3f lRight = new Vector3f(
                1.0f - 2.0f * (y * y + z * z), 
                2.0f * (x * y + w * z), 
                2.0f * (x * z - w * y));
        Vector3f lUp = new Vector3f(
                2.0f * (x * y - w * z), 
                1.0f - 2.0f * (x * x + z * z), 
                2.0f * (y * z + w * x));
        Vector3f lForward = new Vector3f(
                2.0f * (x * z + w * y), 
                2.0f * (y * z - w * x), 
                1.0f - 2.0f * (x * x + y * y));

        return (new Matrix4f().initMapping(
                new Vector3f(lRight.getX(), lUp.getX(), lForward.getX()),
                new Vector3f(lRight.getY(), lUp.getY(), lForward.getY()),
                new Vector3f(lRight.getZ(), lUp.getZ(), lForward.getZ()),
                new Vector3f()));
    }

    public Vector3f getRight() {
        return (new Vector3f(1, 0, 0).rotate(this));
    }

    public Vector3f getUp() {
        return (new Vector3f(0, 1, 0).rotate(this));
    }

    public Vector3f getForward() {
        return (new Vector3f(0, 0, 1).rotate(this));
    }

    public Vector3f getLeft() {
        return getRight().mul(-1.0f);
    }

    public Vector3f getDown() {
        return getUp().mul(-1.0f);
    }

    public Vector3f getBackward() {
        return getForward().mul(-1.f);
    }

    public float getX() {
        return x;
    }

    public void setX(float aX) {
        this.x = Precision.round( aX);
    }

    public float getY() {
        return y;
    }

    public void setY(float aY) {
        this.y = Precision.round( aY);;
    }

    public float getZ() {
        return z;
    }

    public void setZ(float aZ) {
        this.z = Precision.round( aZ);;
    }

    public float getW() {
        return w;
    }

    public void setW(float aW) {
        this.w = Precision.round( aW);;
    }

    public boolean equals(Quaternion aQuaternion) {
        return (Precision.equals(x, aQuaternion.getX()) && Precision.equals(y, aQuaternion.getY())
                && Precision.equals(z, aQuaternion.getZ()) && Precision.equals(w, aQuaternion.getW()));
    }
}
