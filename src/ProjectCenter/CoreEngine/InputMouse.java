/* GameEngine 3D - A programm to design games with 3D content 
 * Copyright (C) 2016-2056 Frank Küwen
 *  
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package ProjectCenter.CoreEngine;

import Controller.CommandMouse;
import Controller.CommandNote;
import Controller.CommandSource;
import java.util.Observable;
import org.lwjgl.input.Mouse;

public class InputMouse extends Observable {

    public static final int NUM_MOUSECODES = 5;
    private boolean[] lastMouse ;
    private final Vector2f centerPosition;
    private boolean mouseLock;
    private float mouseWheel;

    public InputMouse() {
        super();
        lastMouse = new boolean[NUM_MOUSECODES];
        centerPosition = new Vector2f();
        mouseLock = false;
        for (int i = 0; i < NUM_MOUSECODES; i++) {
            lastMouse[i] = false;
        }
    }

    public void init() {
        centerPosition.set( CoreEngine.getWidth() / 2, CoreEngine.getHeight() / 2.f);
    }

    public void setMouseLock(boolean aMouseState) {
        Mouse.setGrabbed(aMouseState);
        mouseLock = aMouseState;
    }

    public boolean getMouseState() {
        return mouseLock;
    }

    public void update() {
        if (countObservers() > 0) {
            Vector2f lMonitorPosition = (getMousePosition().sub(centerPosition)).mul(2).div(centerPosition);
            Vector3f lDisplace = new Vector3f(lMonitorPosition.getX(),lMonitorPosition.getY(), 0);
            for (int i = 0; i < NUM_MOUSECODES; i++) {
                if (getMouseDown(i)) {
                    setChanged();
                    notifyObservers(new CommandNote(CommandSource.Mouse, i, true, mouseWheel, lDisplace));
                }
                if (getMouseUp(i)) {
                    setChanged();
                    notifyObservers(new CommandNote(CommandSource.Mouse, i, false, mouseWheel, lDisplace));
                }
                lastMouse[i] = getMouse(i);

            }
            mouseWheel = getMouseWheel();
            if (mouseWheel !=0)
            {            
                    setChanged();
                    notifyObservers(new CommandNote(CommandSource.Mouse, CommandMouse.MouseWheelTurn.ordinal(), true, mouseWheel, new Vector3f(0,0,0)));
                    mouseWheel= 0;
            }
            if (mouseLock && Mouse.isInsideWindow()) {
                if (!lDisplace.equals(Vector2f.ZEROVECTOR)) {
                    setChanged();
                    notifyObservers(new CommandNote(CommandSource.Mouse, CommandMouse.MouseMotion.ordinal(), true, mouseWheel, lDisplace));
                    setMousePosition(centerPosition);
                }
            }
        }
    }

    private boolean getMouse(int aMouseButton) {
        return Mouse.isButtonDown(aMouseButton);
    }

    private boolean getMouseDown(int aMouseButton) {
        return getMouse(aMouseButton) && !lastMouse[aMouseButton];
    }

    private boolean getMouseUp(int aMouseButton) {
        return !getMouse(aMouseButton) && lastMouse[aMouseButton];
    }

    private int getMouseWheel() {
        return Mouse.getDWheel();
    }

    private Vector2f getMousePosition() {
        return new Vector2f(Mouse.getX(), Mouse.getY());
    }

    private void setMousePosition(Vector2f aPosition) {
        Mouse.setCursorPosition((int) aPosition.getX(), (int) aPosition.getY());
    }

}
