/*
 * Copyright (C) 2016 Malcolm
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ProjectCenter.CoreEngine;

import Components.FreeScaleController;
import Controller.CommandMouse;
import Controller.CommandNote;
import Controller.CommandSource;
import MVC_Interface.Axis;
import MVC_Interface.IControllerScale;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Observable;
import java.util.Observer;
import java.util.UUID;

/**
 *
 * @author Malcolm
 */
public class InputScale implements IControllerScale, Observer {

    private Hashtable<UUID, FreeScaleController> dictionary;
    private final ArrayList<UUID> activeController;

    public InputScale() {
        dictionary = new Hashtable<>();
        activeController = new ArrayList<>();
    }

    @Override
    public void scaleVector(Vector3f aVector) {
        if (!activeController.isEmpty()) {
            Iterator<UUID> lIds = activeController.listIterator();
            while (lIds.hasNext()) {
                FreeScaleController lScaler = dictionary.get(lIds.next());
                lScaler.setScalingVector(aVector);
            }
        } 
    }

    @Override
    public void scale(Axis aDirection, boolean aInPositive, boolean aStartSignal) {
        if (!activeController.isEmpty()) {
            Iterator<UUID> lIds = activeController.listIterator();
            while (lIds.hasNext()) {
                FreeScaleController lScaler = dictionary.get(lIds.next());
                lScaler.setAxisScaling(aDirection, aInPositive, aStartSignal);
            }
        } 
    }

    @Override
    public void scale(Axis aDirection, boolean aInPositive, float aGrowthFactor) {
        if (!activeController.isEmpty()) {
            Iterator<UUID> lIds = activeController.listIterator();
            while (lIds.hasNext()) {
                FreeScaleController lScaler = dictionary.get(lIds.next());
                lScaler.setAxisScaling(aDirection, aInPositive, aGrowthFactor);
            }
        } 
    }
public void setElementState(UUID aId, boolean aIsActive) {
        if (aIsActive) {
            if (!activeController.contains(aId)) {
                activeController.add(aId);
            }
        } else {
            if (activeController.contains(aId)) {
                activeController.remove(aId);
            }
        }
    }

    @Override
    public void update(Observable o, Object o1) {
        if (o1.getClass() == CommandNote.class) {
            CommandNote lNotice = (CommandNote) o1;
            if ((lNotice.getType() == CommandSource.Mouse)) {
                MouseCommands(lNotice);
            }
        }
    }

    public void addScaleController(UUID aGameObjectIdentity, FreeScaleController aScaleControl) {
        dictionary.put(aGameObjectIdentity, aScaleControl);
    }

    public void removeScaleControll(UUID aGameObjectIdentity, FreeScaleController aScaleControl) {
        if (dictionary.get(aGameObjectIdentity) != null) {
            FreeScaleController remove = dictionary.remove(aGameObjectIdentity);
        }
    }

    private void MouseCommands(CommandNote aCommand) {
        if (aCommand.getCommand() == CommandMouse.MouseWheelTurn.ordinal()) {
            if (Precision.greater(aCommand.getAmount(), 0)) {
                scale(Axis.Front, true, true);
            } else if (Precision.less(aCommand.getAmount(), 0)) {
                scale(Axis.Front, false, true);
            } else {
                scale(Axis.Front, false, false);

                scale(Axis.Front, true, false);
            }
        }
    }
}
