/* GameEngine 3D - A programm to design games with 3D content 
 * Copyright (C) 2016-2056 Frank Küwen
 *  
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package ProjectCenter.CoreEngine;

public class Matrix4f {

    public static final int ROWS = 4;
    public static final int COLUMNS = 4;

    private float[][] m;

    public Matrix4f() {
        setM(new float[ROWS][COLUMNS]);
    }

    public Matrix4f clone(Matrix4f aOriginal) {
        setM(aOriginal.getM());
        return this;
    }

    @Override
    public String toString() {
        return "| " + Precision.NUMBERFORMAT.format(m[0][0])
                + " | " + Precision.NUMBERFORMAT.format(m[0][1])
                + " | " + Precision.NUMBERFORMAT.format(m[0][2])
                + " | " + Precision.NUMBERFORMAT.format(m[0][3]) + " |\n"
                + "| " + Precision.NUMBERFORMAT.format(m[1][0])
                + " | " + Precision.NUMBERFORMAT.format(m[1][1])
                + " | " + Precision.NUMBERFORMAT.format(m[1][2])
                + " | " + Precision.NUMBERFORMAT.format(m[1][3]) + " |\n"
                + "| " + Precision.NUMBERFORMAT.format(m[2][0])
                + " | " + Precision.NUMBERFORMAT.format(m[2][1])
                + " | " + Precision.NUMBERFORMAT.format(m[2][2])
                + " | " + Precision.NUMBERFORMAT.format(m[2][3]) + " |\n"
                + "| " + Precision.NUMBERFORMAT.format(m[3][0])
                + " | " + Precision.NUMBERFORMAT.format(m[3][1])
                + " | " + Precision.NUMBERFORMAT.format(m[3][2])
                + " | " + Precision.NUMBERFORMAT.format(m[3][3]) + " |\n";

    }

    public Matrix4f initTranslation(Vector3f aVector) {
        this.setM(Identity().getM());
        m[0][3] = aVector.getX();
        m[1][3] = aVector.getY();
        m[2][3] = aVector.getZ();

        return this;
    }

    public Matrix4f initRotation(Vector3f aNormedAxis, float aAngle) {
        this.setM(Identity().getM());
        Vector3f aMapX = (new Vector3f(1, 0, 0)).rotateWithDegreeAroundAxis(aNormedAxis, aAngle);
        Vector3f aMapY = (new Vector3f(0, 1, 0)).rotateWithDegreeAroundAxis(aNormedAxis, aAngle);
        Vector3f aMapZ = (new Vector3f(0, 0, 1)).rotateWithDegreeAroundAxis(aNormedAxis, aAngle);

        m[0][0] = aMapX.getX();
        m[1][0] = aMapX.getY();
        m[2][0] = aMapX.getZ();

        m[0][1] = aMapY.getX();
        m[1][1] = aMapY.getY();
        m[2][1] = aMapY.getZ();

        m[0][2] = aMapZ.getX();
        m[1][2] = aMapZ.getY();
        m[2][2] = aMapZ.getZ();

        return this;
    }

    public Matrix4f initScaling(Vector3f aVector) {
        this.setM(Identity().getM());
        m[0][0] = aVector.getX();
        m[1][1] = aVector.getY();
        m[2][2] = aVector.getZ();

        return this;
    }

    public Matrix4f initMapping(Vector3f aMapX, Vector3f aMapY, Vector3f aMapZ, Vector3f aLocation) {
        this.setM(Identity().getM());
        m[0][0] = aMapX.getX();
        m[1][0] = aMapX.getY();
        m[2][0] = aMapX.getZ();

        m[0][1] = aMapY.getX();
        m[1][1] = aMapY.getY();
        m[2][1] = aMapY.getZ();

        m[0][2] = aMapZ.getX();
        m[1][2] = aMapZ.getY();
        m[2][2] = aMapZ.getZ();

        m[0][3] = aLocation.getX();
        m[1][3] = aLocation.getY();
        m[2][3] = aLocation.getZ();

        return this;
    }

    public Matrix4f initPerspective(float aFieldOfView,
            float aAspectRatio, float aZNear, float aZFar) {
        this.setM(Identity().getM());

        float tanHalfFieldOfView = (float) Math.tan(aFieldOfView / 2.f);

        /*     Projects into the GraphicCards-World
         [1/(dX/dZ)    0              0       0]   |x|     |x'|    [-1,1]
         [0       (dX/dY)/(dX/dZ)     0       0] * |y| =   |y'| in [-1,1]
         [0            0              A       B]   |z|     |z'|    [-1,1]
         [0            0             -1       0]   |1|     |-z|    [-1,1]        
         A := -(zFar+zNear)/(zNear-zFar)  
         B := 2 zFar zNear / (zNear-zFar)
         */
        float zRange = aZNear - aZFar;
        float zScale = -1 * (aZNear + aZFar) / zRange;
        float zOffset = 2 * aZFar * aZNear / zRange;

        m[0][0] = 1.0f / (tanHalfFieldOfView);
        m[1][1] = aAspectRatio / tanHalfFieldOfView;
        m[2][2] = zScale;
        m[2][3] = zOffset;
        m[3][2] = 1.0f;
        m[3][3] = 0.0f;
        //Projection (openGl divides automatically by z Value)
        return this;
    }

    public Matrix4f initOrthographic(float aLeft, float aRight, float aBottom, float aTop, float aFar, float aNear) {
        this.setM(Identity().getM());

        float lWidth = aRight - aLeft;
        float lHeight = aTop - aBottom;
        float lDepth = aNear - aFar;

        /*     Projects into the GraphicCards-World
         [2/DX         0        0       -xMid/DX]   |x|     |x'|    [-1,1]
         [0           2/DY      0       -yMid/DY] * |y| =   |y'| in [-1,1]
         [0            0      2/DZ      -zMid/DZ]   |z|     |z'|    [-1,1]
         [0            0        0              1]   |1|     |1 |    [-1,1] 
         */
        m[0][0] = 2.0f / (lWidth);
        m[1][1] = 2.0f / (lHeight);
        m[2][2] = 2.0f / (lDepth);
        m[0][3] = -(aRight + aLeft) / (lWidth);
        m[1][3] = -(aTop + aBottom) / (lHeight);
        m[2][3] = -(aNear + aFar) / (lDepth);
//        float zRange = aNear - aFar;
//        float zScale = -1 * (aNear + aFar) / zRange;
//        float zOffset = 2 * aFar * aNear / zRange;
//        this.set(2, 2, zScale);
//        this.set(2, 3, zOffset);
//        this.set(3, 2, 1.0f);
//        this.set(3, 3, 0.0f);
        return this;
    }

//    public Matrix4f initProjectionOrthographic(float aOrthoScale,
//            float aWidth, float aHeight, float aZNear, float aZFar) {
//        this.setM(Identity().getM());
//
//        /*     Projects into the GraphicCards-World
//         [1/(dX/dY)    0           0                0]   |x|     |x'|    [-1,1]
//         [0       (dX/dY)/(dX/dY)  0                0] * |y| =   |y'| in [-1,1]
//         [0            0   1/(dX/dY)*1/(dZ)  -zOffset]   |z|     |z'|    [-1,1]
//         [0            0           0                1]   |1|     |1 |    [-1,1] 
//         */
//        float AspectRatio = aWidth / aHeight;
//        float zRange = aZNear - aZFar;
//        float zOffset = -aZNear;
//        float zScale = 1.0f / (zRange * AspectRatio * aOrthoScale);
//
//        this.set(0, 0, 1.0f / (AspectRatio * aOrthoScale));
//        this.set(1, 1, 1.0f / aOrthoScale);
//        this.set(2, 2, zScale);
//        this.set(2, 3, zOffset);
//
//        return this;
//    }
    public Matrix4f initRotation(Vector3f aForwardView, Vector3f aUpView) {

        Vector3f lForward = aForwardView.normalized();
        Vector3f lUp = aUpView.normalized();
        Vector3f lRight = lUp.cross(lForward);
        lUp = lForward.cross(lRight);

        return this.initMapping(
                new Vector3f(lRight.getX(), lUp.getX(), lForward.getX()),
                new Vector3f(lRight.getY(), lUp.getY(), lForward.getY()),
                new Vector3f(lRight.getZ(), lUp.getZ(), lForward.getZ()),
                new Vector3f());
    }

    public void initZeroMatrix() {
        for (int i = 0; i < ROWS; i++) {
            for (int j = 0; j < COLUMNS; j++) {
                m[i][j] = 0;
            }
        }
    }

    public Matrix4f Identity() {
        this.initZeroMatrix();
        m[0][0] = 1;
        m[1][1] = 1;
        m[2][2] = 1;
        m[3][3] = 1;
        return this;
    }

    public Vector3f mul(Vector3f aVector) {
        Vector3f lResult = new Vector3f();
        lResult.setX(m[0][0] * aVector.getX() + m[0][1] * aVector.getY() + m[0][2] * aVector.getZ() + m[0][3]);
        lResult.setY(m[1][0] * aVector.getX() + m[1][1] * aVector.getY() + m[1][2] * aVector.getZ() + m[1][3]);
        lResult.setZ(m[2][0] * aVector.getX() + m[2][1] * aVector.getY() + m[2][2] * aVector.getZ() + m[2][3]);
        return lResult;
    }

    public Matrix4f mul(Matrix4f aMatrix) {
        Matrix4f lResult = new Matrix4f();
        lResult.initZeroMatrix();

        for (int i = 0; i < ROWS; i++) {
            for (int j = 0; j < COLUMNS; j++) {
                for (int k = 0; k < ROWS; k++) {
                    lResult.set(i, j, lResult.get(i, j) + this.get(i, k) * aMatrix.get(k, j));
                }
            }
        }
        return lResult;
    }

    public Matrix4f transposed() {
        Matrix4f lResult = new Matrix4f().clone(this);
        lResult.transpose();
        return lResult;
    }
    private void transpose()
    {
        float temp;
        for (int i = 0; i < ROWS; i++) {
            for (int j = i + 1; j < COLUMNS; j++) {
                temp = m[i][j];
                m[i][j] = m[j][i];
                m[j][i] = temp;
            }
        }
    }

    public float[][] getM() {
        float[][] lResult = new float[ROWS][COLUMNS];
        for (int i = 0; i < ROWS; i++) {
            System.arraycopy(m[i], 0, lResult[i], 0, COLUMNS);
        }
        return lResult;
    }

    public final void setM(float[][] m) {
        this.m = m;
    }

    public float get(int x, int y) {
        return m[x][y];
    }

    public void set(int x, int y, float aValue) {
        m[x][y] = Precision.round(aValue);
    }

    public boolean equals(Matrix4f aMatrix) {
        boolean lResult = true;
        for (int i = 0; i < ROWS; i++) {
            for (int j = 0; j < COLUMNS; j++) {
                lResult = lResult && Precision.equals(m[i][j], aMatrix.get(i, j));
            }
        }
        return lResult;
    }

}
