/* GameEngine 3D - A programm to design games with 3D content 
 * Copyright (C) 2016-2056 Frank Küwen
 *  
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package ProjectCenter.CoreEngine;

public class Vector3f {

    public static final Vector3f ZEROVECTOR = new Vector3f(0, 0, 0);
    public static final Vector3f UNIT_X = new Vector3f(1, 0, 0);
    public static final Vector3f UNIT_Y = new Vector3f(0, 1, 0);
    public static final Vector3f UNIT_Z = new Vector3f(0, 0, 1);
    public static final int SIZE = 3;

    private float x;
    private float y;
    private float z;
    
    public Vector3f() {
        set(ZEROVECTOR);
    }

    public Vector3f(float aX, float aY, float aZ) {
        x = aX;
        y = aY;
        z = aZ;
    }

    public void set(float aX, float aY, float aZ) {
        this.x = Precision.round( aX);
        this.y = Precision.round( aY);
        this.z = Precision.round( aZ);
    }

    public void set(Vector3f aOriginal) {
        set(
                aOriginal.getX(), 
                aOriginal.getY(), 
                aOriginal.getZ());
    }

    public Vector3f clone(Vector3f aOriginal) {
        x = aOriginal.getX();
        y = aOriginal.getY();
        z = aOriginal.getZ();
        return this;
    }

    @Override
    public String toString() {
        return  "(" + Precision.NUMBERFORMAT.format(x) + 
                "|" + Precision.NUMBERFORMAT.format(y) + 
                "|" + Precision.NUMBERFORMAT.format(z) + ")";
    }

    public float length() {
        return (float) Math.sqrt(x * x + y * y + z * z);
    }

    public Vector3f abs() {
        return new Vector3f(Math.abs(x), Math.abs(y), Math.abs(z));
    }

    public float max() {
        return Math.max(x, Math.max(y, z));
    }

    public float min() {
        return Math.min(x, Math.min(y, z));
    }

    public float dot(Vector3f aVector) {
        return ((aVector.getX() * x) + (aVector.getY() * y) + (aVector.getZ() * z));
    }

    public Vector3f add(Vector3f aVector) {
        return new Vector3f(x + aVector.getX(), y + aVector.getY(), z + aVector.getZ());
    }

    public Vector3f add(float aScalar) {
        return new Vector3f(x + aScalar, y + aScalar, z + aScalar);
    }

    public Vector3f sub(Vector3f aVector) {
        return new Vector3f(x - aVector.getX(), y - aVector.getY(), z - aVector.getZ());
    }

    public Vector3f sub(float aScalar) {
        return new Vector3f(x - aScalar, y - aScalar, z - aScalar);
    }

    public Vector3f mul(Vector3f aVector) {
        return new Vector3f(x * aVector.getX(), y * aVector.getY(), z * aVector.getZ());
    }

    public Vector3f mul(float aScalar) {
        return new Vector3f(x * aScalar, y * aScalar, z * aScalar);
    }

    public Vector3f div(Vector3f aVector) {
        if (Precision.greater(aVector.length(), 0))
            return new Vector3f(x / aVector.getX(), y / aVector.getY(), z / aVector.getZ());
        else
            return new Vector3f(0, 0, 0);
    }

    public Vector3f div(float aScalar) {
        if (Precision.greater(Math.abs(aScalar), 0))
            return new Vector3f(x / aScalar, y / aScalar, z / aScalar);
        else
            return new Vector3f(0, 0, 0);
    }

    public Vector3f linearExtrapolation(Vector3f aVector, float aScalar) {
        return ((aVector.sub(this)).mul(aScalar)).add(this);
    }

    public Vector3f rotateWithDegreeAroundAxis(Vector3f aNormedAxis, float aAngle) {
        // Normed Axis needed
        if (!Precision.equals(aNormedAxis.dot(aNormedAxis), 1)) {
            aNormedAxis = aNormedAxis.normalized();
            if (!Precision.equals(aNormedAxis.dot(aNormedAxis), 1,
                    Precision.getEpsilon() * Precision.getEpsilon()))
                return new Vector3f(0, 0, 0);
        }

        float sinAngle = (float) Math.sin(-aAngle);
        float cosAngle = (float) Math.cos(-aAngle);

        // View Wikipedia (Drehmatrix)
        // part perpendicular to this & Axis
        Vector3f partOne = (this.cross(aNormedAxis)).mul(sinAngle);
        // part along this
        Vector3f partTwo = (aNormedAxis.cross(this)).cross(aNormedAxis).mul(cosAngle);
        // part along axis
        Vector3f partThree = (aNormedAxis.mul(this.dot(aNormedAxis)));

        return partOne.add(partTwo.add(partThree));
    }

    public Vector3f rotate(Vector3f aNormedAxis, float aAngle) {
        //return rotateWithDegreeAroundAxis(aNormedAxis, aAngle) ;
        return rotate(new Quaternion(aNormedAxis, aAngle));
    }
    
    public Vector3f rotate(Quaternion aRotation) {
        
        Quaternion w = aRotation.mul(new Quaternion(x, y, z, 0)).mul(aRotation.conjugate());
        Vector3f lResult =new Vector3f();
        // Eliminate small errors due to complex calculation by rounding in set()
        lResult.set(w.getX(),w.getY(),w.getZ());
        return lResult;
    }

    public Vector3f normalized() {
        float length = length();
        if (Precision.greater(length, 0))
            return new Vector3f(x / length, y / length, z / length);
        else
            return new Vector3f(0, 0, 0);
    }

    public Vector3f cross(Vector3f aVector) {
        return new Vector3f(y * aVector.getZ() - z * aVector.getY(), z * aVector.getX() - x * aVector.getZ(),
                x * aVector.getY() - y * aVector.getX());
    }
    
    public float getParameter(Vector3f aVector)
    { //Parameter = v*u/(u*u) (Coordinate)
      return (aVector).dot(this.div(this.dot(this)));
    }

    public float getX() {
        return x;
    }

    public void setX(float aX) {
        this.x =  Precision.round( aX);
    }

    public float getY() {
        return y;
    }

    public void setY(float aY) {
        this.y = Precision.round( aY);
    }

    public float getZ() {
        return z;
    }

    public void setZ(float aZ) {
        this.z = Precision.round( aZ);
    }

    public Vector2f getYZ() {
        return new Vector2f(y, z);
    }

    public Vector2f getXY() {
        return new Vector2f(x, y);
    }

    public Vector2f getZX() {
        return new Vector2f(z, x);
    }

    public Vector2f getZY() {
        return new Vector2f(z, y);
    }

    public Vector2f getYX() {
        return new Vector2f(y, x);
    }

    public Vector2f getXZ() {
        return new Vector2f(x, z);
    }

    public boolean equals(Vector3f aVector) {
        return (Precision.equals(x, aVector.getX()) && Precision.equals(y, aVector.getY())
                && Precision.equals(z, aVector.getZ()));
    }
}
