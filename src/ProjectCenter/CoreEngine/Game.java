/* GameEngine 3D - A programm to design games with 3D content 
 * Copyright (C) 2016-2056 Frank Küwen
 *  
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package ProjectCenter.CoreEngine;

import ProjectCenter.PhysicsEngine.PhysicsEngine;
import ProjectCenter.RenderEngine.RenderingEngine;
import java.util.Observer;
import java.util.UUID;

public abstract class Game {

    private GameObject root;
    
    public abstract String getName();
    
    public UUID getUUID(){        
        return UUID.nameUUIDFromBytes(this.getClass().toString().getBytes());
    }   

    public void init() {
    }

    public void input(float aDelta) {
        getRootObject().inputAll(aDelta);
    }

    public void update(float aDelta, PhysicsEngine aPhysicsEngine) {
        //getRootObject().updateAll(aDelta, aPhysicsEngine);
        aPhysicsEngine.update(getRootObject(), aDelta);
    }

    public void render(RenderingEngine aRenderingEngine) {
        aRenderingEngine.render(getRootObject());
    }

    public void addObjectToRoot(GameObject aObject) {
        getRootObject().addChild(aObject);
    }

    public GameObject getRootObject() {
        if (root == null) {
            root = new GameObject();
        }
        return root;
    }

    public void setEngine(CoreEngine aEngine) {
        getRootObject().setEngine(aEngine);
    }
}
