/* GameEngine 3D - A programm to design games with 3D content 
 * Copyright (C) 2016-2056 Frank Küwen
 *  
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package ProjectCenter.CoreEngine;

import Components.ForceObjectComponent;
import ProjectCenter.RenderEngine.RenderingEngine;
import Components.GameComponent;
import Components.PhysicsObjectComponent;
import Controller.StructureNote;
import ProjectCenter.PhysicsEngine.PhysicsObject;
import ProjectCenter.RenderEngine.Shader;
import java.util.ArrayList;
import java.util.Observable;
import java.util.UUID;

public class GameObject extends Observable {

    private final ArrayList<GameObject> children;
    private final ArrayList<GameComponent> components;
    private final ArrayList<String> lonerComponents;
    private final Transform transform;
    private final UUID identity;
    private CoreEngine engine;
    private boolean activeCounter;

    public GameObject() {
        children = new ArrayList<>();
        components = new ArrayList<>();
        transform = new Transform();
        engine = null;
        identity = UUID.randomUUID();
        lonerComponents = new ArrayList<>();
        activeCounter = false;
    }

    public void addChild(GameObject aNewChild) {

        if (aNewChild == null) {
            return;
        }

        children.add(aNewChild);
        aNewChild.setEngine(engine);
        aNewChild.getTransform().setParent(transform);

        setChanged();
        notifyObservers(new StructureNote(this));
    }

    public void addComponent(GameComponent aNewComponent) {

        if (aNewComponent == null)
            return;
        
        boolean lContainsLoner = false;
        
        if (aNewComponent.IsLoner())
        {    
            if(lonerComponents.contains(aNewComponent.getClass().toString()))
              lContainsLoner = true;
        }

        if (!lContainsLoner){
            components.add(aNewComponent);
            aNewComponent.setParent(this);
            if (aNewComponent.IsLoner())
                lonerComponents.add(aNewComponent.getClass().toString());

            setChanged();
            notifyObservers(new StructureNote(this));
        }
    }

    public void inputAll(float aDelta) {
        input(aDelta);

        for (GameObject child : children) {
            child.inputAll(aDelta);
        }
    }

    public void updateAll(float aDelta) {
        update(aDelta);
        for (GameObject child : children) {
            child.updateAll(aDelta);
        }
    }

    public void refreshStateAll() {
        refreshState();
        for (GameObject child : children) {
            child.refreshState();
        }
    }

    public void renderAll(Shader aShader, RenderingEngine aRenderingEngine) {
        render(aShader, aRenderingEngine);
        for (GameObject child : children) {
            child.renderAll(aShader, aRenderingEngine);
        }
    }

    public void input(float aDelta) {
        transform.update();

        for (GameComponent component : components) {
            component.input(aDelta);
        }

    }

    public void refreshState() {
        for (GameComponent component : components) {
            component.refreshState();
        }
    }

    public void update(float aDelta) {
        for (GameComponent component : components) {
            component.update(aDelta);
        }
    }

    public void render(Shader aShader, RenderingEngine aRenderingEngine) {
        for (GameComponent component : components) {
            component.render(aShader, aRenderingEngine);
        }
    }

    public ArrayList<GameObject> getChildren() {
        ArrayList<GameObject> lResult = new ArrayList<>();

        for (GameObject lChild : children) {
            lResult.add(lChild);
        }
        return lResult;
    }

    public ArrayList<GameObject> getAllAttached() {
        ArrayList<GameObject> lResult = new ArrayList<>();

        for (GameObject lChild : children) {
            lResult.addAll(lChild.getAllAttached());
        }
        lResult.add(this);
        return lResult;
    }

    public ArrayList<GameComponent> getAllAttachedComponents() {
        ArrayList<GameComponent> lResult = new ArrayList<>();
        for (GameObject lChild : children) {
            lResult.addAll(lChild.getAllAttachedComponents());
        }
        for (GameComponent lComponent : components) {
            lResult.add(lComponent);
        }
        return lResult;
    }

    public ArrayList<GameComponent> getComponents() {
        ArrayList<GameComponent> lResult = new ArrayList<>();

        for (GameComponent lComponent : components) {
            lResult.add(lComponent);
        }
        return lResult;
    }

    public Transform getTransform() {
        return transform;
    }

    public UUID getIdentity() {
        return identity;
    }

    public void setEngine(CoreEngine aEngine) {
        if (engine != aEngine) {
            engine = aEngine;

            for (GameComponent component : components) {
                component.addToEngine(aEngine);
            }
            for (GameObject child : children) {
                child.setEngine(aEngine);
            }
        }
    }
    public boolean getActivationState()
    {
        return activeCounter ;
    }
    public void setActivation(boolean isActive)
    {
        if (activeCounter !=isActive)
        {      
            activeCounter = isActive;
            engine.setElementState(identity, isActive);
            setChanged();
            notifyObservers(new StructureNote(this));
        }
    }
}
