/* GameEngine 3D - A programm to design games with 3D content 
 * Copyright (C) 2016-2056 Frank Küwen
 *  
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package ProjectCenter.CoreEngine;

import Components.FreeMoveController;
import Controller.CommandNote;
import Controller.CommandSource;
import MVC_Interface.Axis;
import MVC_Interface.IControllerMotion;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Observable;
import java.util.Observer;
import java.util.UUID;

public class InputMotion implements IControllerMotion, Observer {

    private Hashtable<UUID, FreeMoveController> dictionary;
    private final ArrayList<UUID> activeController;

    private int forwardKey = KeyEvent.VK_E;
    private int backwardKey = KeyEvent.VK_Q;
    private int leftKey = KeyEvent.VK_A;
    private int rightKey = KeyEvent.VK_D;
    private int upKey = KeyEvent.VK_W;
    private int downKey = KeyEvent.VK_S;

    public InputMotion() {
        dictionary = new Hashtable<>();
        activeController = new ArrayList<>();
    }

    public void setMotionKey(int aForwardKey, int aBackwardKey, int aRightKey,
            int aLeftKey, int aUpwardKey, int aDownwardKey) {
        forwardKey = aForwardKey;
        backwardKey = aBackwardKey;
        rightKey = aRightKey;
        leftKey = aLeftKey;
        upKey = aUpwardKey;
        downKey = aDownwardKey;
    }

    @Override
    public void moveVector(Vector3f aVector) {
            Iterator<UUID> lIds = activeController.listIterator();
            while (lIds.hasNext()) {
                FreeMoveController lMover = dictionary.get(lIds.next());
                lMover.setMotionVector(aVector);
            }
    }

    @Override
    public void move(Axis aDirection, boolean aInPositive, boolean aStartSignal) {
            Iterator<UUID> lIds = activeController.listIterator();
            while (lIds.hasNext()) {
                FreeMoveController lMover = dictionary.get(lIds.next());
                lMover.setMotion(aDirection, aInPositive, aStartSignal);
            }
    }

    @Override
    public void move(Axis aDirection, boolean aInPositive, float aDistance) {
            Iterator<UUID> lIds = activeController.listIterator();
            while (lIds.hasNext()) {
                FreeMoveController lMover = dictionary.get(lIds.next());
                lMover.setMotion(aDirection, aInPositive, aDistance);
            }
    }

    public void addMotionController(UUID aGameObjectIdentity, FreeMoveController aMotionController) {
        dictionary.put(aGameObjectIdentity, aMotionController);
    }

    public void removeMotionController(UUID aGameObjectIdentity, FreeMoveController aMotionController) {
        if (dictionary.get(aGameObjectIdentity) != null) {
            FreeMoveController remove = dictionary.remove(aGameObjectIdentity);
        }
    }

    public void setElementState(UUID aId, boolean aIsActive) {
        if (aIsActive) {
            if (!activeController.contains(aId)) {
                activeController.add(aId);
            }
        } else {
            if (activeController.contains(aId)) {
                activeController.remove(aId);
            }
        }
    }

    @Override
    public void update(Observable o, Object o1) {
        if (o1 instanceof CommandNote) {
            CommandNote lNotice = (CommandNote) o1;
            if ((lNotice.getType() == CommandSource.Keyboard)) {
                KeyCommands(lNotice);
            }
        }
    }

    private void KeyCommands(CommandNote aCommand) {
        if (aCommand.getCommand() == forwardKey) {
            move(Axis.Front, true, aCommand.getCommandState());
        }
        if (aCommand.getCommand() == backwardKey) {
            move(Axis.Front, false, aCommand.getCommandState());
        }
        if (aCommand.getCommand() == rightKey) {
            move(Axis.Side, true, aCommand.getCommandState());
        }
        if (aCommand.getCommand() == leftKey) {
            move(Axis.Side, false, aCommand.getCommandState());
        }
        if (aCommand.getCommand() == upKey) {
            move(Axis.Top, true, aCommand.getCommandState());
        }
        if (aCommand.getCommand() == downKey) {
            move(Axis.Top, false, aCommand.getCommandState());
        }
    }
}
